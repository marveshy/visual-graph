package vg.shared.graph.utils;

import com.google.common.collect.*;
import org.apache.commons.lang.NotImplementedException;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.Validate;
import org.apache.commons.lang.mutable.MutableBoolean;
import vg.interfaces.graph_view_service.data.VGEdgeCell;
import vg.shared.utils.MapUtils;
import vg.impl.main_service.VGMainServiceHelper;
import vg.interfaces.data_base_service.data.graph.*;
import vg.interfaces.data_base_service.data.record.*;
import vg.interfaces.graph_layout_service.GraphLayoutFactory;
import vg.interfaces.graph_view_service.data.VGVertexCell;

import java.util.*;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * @author Timur Zolotuhin (e-mail: tzolotuhin@gmail.com)
 */
public class GraphUtils {
    // Constants
    private static final String GRAPH_PREFIX = "g";
    private static final String VERTEX_PREFIX = "v";
    private static final String EDGE_PREFIX = "e";

    private static final String ATTRIBUTE_NAME_FOR_LINK_TO_ATTACHMENT_FILE = "link_to_src";

    // 0 - start, -1 - end, 0 - is master part of composite edge
    public static final String ATTRIBUTE_NAME_FOR_COMPOSITE_EDGE_ORDER = "comp_edge_order";
    private static final String ATTRIBUTE_NAME_FOR_COMPOSITE_EDGE_MASTER_ID = "comp_edge_master_id";
    private static final String ATTRIBUTE_NAME_FOR_SRC_PORT_ID = "src_port_id";
    private static final String ATTRIBUTE_NAME_FOR_TRG_PORT_ID = "trg_port_id";
    public static final String ATTRIBUTE_NAME_FOR_PORT = "is_fake_port";

    private static final String ATTRIBUTE_NAME_FOR_DIRECTED_EDGE = "directed_edge";

    private static final String ATTRIBUTE_NAME_FOR_INNER_GRAPH_NAME = "inner_graph_name";

    private static List<Vertex> getVerticesByOwnerId(int ownerId, int count) {
        List<VertexRecord> vertexRecords = VGMainServiceHelper.graphDataBaseService.getVertexRecordsByOwnerId(ownerId, count);
        List<Vertex> result = Lists.newArrayList();
        for (VertexRecord vertexRecord : vertexRecords) {
            result.add(VGMainServiceHelper.graphDataBaseService.getVertex(vertexRecord.getId()));
        }
        return result;
    }

    public static void addInnerGraphNameAttribute(int vertexId, String innerGraphName) {
        VGMainServiceHelper.graphDataBaseService.createVertexAttribute(vertexId, ATTRIBUTE_NAME_FOR_INNER_GRAPH_NAME, innerGraphName, AttributeRecord.STRING_ATTRIBUTE_TYPE, false);
    }

    public static String getLinkToAttachmentFileAttribute(Vertex vertex) {
        Attribute attr = vertex.getAttribute(ATTRIBUTE_NAME_FOR_LINK_TO_ATTACHMENT_FILE);
        return attr != null ? attr.getStringValue() : null;
    }

    public static String getInnerGraphNameAttribute(int vertexId) {
        Vertex vertex = VGMainServiceHelper.graphDataBaseService.getVertex(vertexId);
        Attribute attr = vertex.getAttribute(ATTRIBUTE_NAME_FOR_INNER_GRAPH_NAME);
        return attr != null ? attr.getStringValue() : null;
    }

    public static int createPort(int graphId, int vertexId, boolean isFake, int index) {
        int secondVertexId = VGMainServiceHelper.graphDataBaseService.createVertex(graphId, vertexId);
        VGMainServiceHelper.graphDataBaseService.createVertexAttribute(secondVertexId, ATTRIBUTE_NAME_FOR_PORT, Boolean.toString(isFake), AttributeRecord.BOOLEAN_ATTRIBUTE_TYPE, false);
        return secondVertexId;
    }

    public static boolean isPort(Vertex vertex) {
        return vertex != null && vertex.getAttribute(ATTRIBUTE_NAME_FOR_PORT) != null;
    }

    /**
     * Return List[0-master, 1, 2, ..., -1]
     */
    public static List<Integer> createEdge(int graphId, int sourceVertexId, int targetVertexId, int sourcePortId, int targetPortId, boolean directed) {
        Map<Integer, List<Integer>> parentIds = VGMainServiceHelper.graphDataBaseService.findParents(Arrays.asList(sourceVertexId, targetVertexId));

        Validate.isTrue(parentIds.size() == 2);

        List<Integer> sourceParentIds = parentIds.get(sourceVertexId);
        List<Integer> targetParentIds = parentIds.get(targetVertexId);

        if (sourceParentIds.size() == 1 && targetParentIds.size() == 1) {
            int edgeId = VGMainServiceHelper.graphDataBaseService.createEdge(graphId, sourceVertexId, targetVertexId);
            VGMainServiceHelper.graphDataBaseService.createEdgeAttribute(edgeId, ATTRIBUTE_NAME_FOR_DIRECTED_EDGE, Boolean.toString(directed), AttributeRecord.BOOLEAN_ATTRIBUTE_TYPE, false);
            if (sourcePortId > 0) {
                VGMainServiceHelper.graphDataBaseService.createEdgeAttribute(edgeId, ATTRIBUTE_NAME_FOR_SRC_PORT_ID, Integer.toString(sourcePortId), AttributeRecord.INTEGER_ATTRIBUTE_TYPE, false);
            }
            if (targetPortId > 0) {
                VGMainServiceHelper.graphDataBaseService.createEdgeAttribute(edgeId, ATTRIBUTE_NAME_FOR_TRG_PORT_ID, Integer.toString(targetPortId), AttributeRecord.INTEGER_ATTRIBUTE_TYPE, false);
            }

            return Collections.singletonList(edgeId);
        } else {
            if (sourceParentIds.size() >= 2 && targetParentIds.size() >= 2) {
                int masterId = -1;
                int order = 0;
                List<Integer> result = Lists.newArrayList();
                for (int i = 0; i < sourceParentIds.size() - 1; i++) {
                    int sourceParentId = sourceParentIds.get(i);
                    int portId = targetPortId;
                    if (portId < 0 || i != sourceParentIds.size() - 2) {
                        portId = createPort(graphId, sourceParentId, true, -1);
                    }
                    Integer edgeId = createEdge(graphId, sourceVertexId, portId, -1, -1, directed).get(0);
                    if (i == 0) {
                        masterId = edgeId;
                    }
                    VGMainServiceHelper.graphDataBaseService.createEdgeAttribute(edgeId, ATTRIBUTE_NAME_FOR_COMPOSITE_EDGE_MASTER_ID, Integer.toString(masterId), AttributeRecord.INTEGER_ATTRIBUTE_TYPE, false);
                    VGMainServiceHelper.graphDataBaseService.createEdgeAttribute(edgeId, ATTRIBUTE_NAME_FOR_COMPOSITE_EDGE_ORDER, Integer.toString(order), AttributeRecord.INTEGER_ATTRIBUTE_TYPE, false);
                    sourceVertexId = portId;
                    result.add(edgeId);
                    order++;
                }

                {
                    int edgeId = createEdge(graphId, sourceParentIds.get(sourceParentIds.size() - 2), targetParentIds.get(targetParentIds.size() - 2), -1, -1, directed).get(0);
                    VGMainServiceHelper.graphDataBaseService.createEdgeAttribute(edgeId, ATTRIBUTE_NAME_FOR_COMPOSITE_EDGE_MASTER_ID, Integer.toString(masterId), AttributeRecord.INTEGER_ATTRIBUTE_TYPE, false);
                    VGMainServiceHelper.graphDataBaseService.createEdgeAttribute(edgeId, ATTRIBUTE_NAME_FOR_COMPOSITE_EDGE_ORDER, Integer.toString(order), AttributeRecord.INTEGER_ATTRIBUTE_TYPE, false);
                    result.add(edgeId);
                    order++;
                }

                for (int i = 0; i < targetParentIds.size() - 1; i++) {
                    int targetParentId = targetParentIds.get(i);
                    int portId = sourcePortId;
                    if (portId < 0 || i != targetParentIds.size() - 2) {
                        portId = createPort(graphId, targetParentId, true, -1);
                    }
                    if (i == targetParentIds.size() - 2) {
                        order = -1;
                    }
                    Integer edgeId = createEdge(graphId, portId, targetVertexId, -1, -1, directed).get(0);
                    VGMainServiceHelper.graphDataBaseService.createEdgeAttribute(edgeId, ATTRIBUTE_NAME_FOR_COMPOSITE_EDGE_MASTER_ID, Integer.toString(masterId), AttributeRecord.INTEGER_ATTRIBUTE_TYPE, false);
                    VGMainServiceHelper.graphDataBaseService.createEdgeAttribute(edgeId, ATTRIBUTE_NAME_FOR_COMPOSITE_EDGE_ORDER, Integer.toString(order), AttributeRecord.INTEGER_ATTRIBUTE_TYPE, false);
                    targetVertexId = portId;
                    result.add(edgeId);
                    order++;
                }
                return result;
            } else if (sourceParentIds.size() >= 2 && targetParentIds.size() < 2) {
                int masterId = -1;
                int order = 0;
                List<Integer> result = Lists.newArrayList();
                for (int i = 0; i < sourceParentIds.size() - 1; i++) {
                    int sourceParentId = sourceParentIds.get(i);
                    int portId = targetPortId;
                    if (portId < 0 || i != sourceParentIds.size() - 2) {
                        portId = createPort(graphId, sourceParentId, true, -1);
                    }
                    Integer edgeId = createEdge(graphId, sourceVertexId, portId, -1, -1, directed).get(0);
                    if (i == 0) {
                        masterId = edgeId;
                    }
                    if (i == sourceParentIds.size() - 2) {
                        order = -1;
                    }
                    VGMainServiceHelper.graphDataBaseService.createEdgeAttribute(edgeId, ATTRIBUTE_NAME_FOR_COMPOSITE_EDGE_MASTER_ID, Integer.toString(masterId), AttributeRecord.INTEGER_ATTRIBUTE_TYPE, false);
                    VGMainServiceHelper.graphDataBaseService.createEdgeAttribute(edgeId, ATTRIBUTE_NAME_FOR_COMPOSITE_EDGE_ORDER, Integer.toString(order), AttributeRecord.INTEGER_ATTRIBUTE_TYPE, false);
                    result.add(edgeId);
                    sourceVertexId = portId;
                    order++;
                }
                return result;
            } else if (sourceParentIds.size() < 2 && targetParentIds.size() >= 2) {
                int masterId = -1;
                int order = 0;
                List<Integer> result = Lists.newArrayList();
                for (int i = 0; i < targetParentIds.size() - 1; i++) {
                    int targetParentId = targetParentIds.get(i);
                    int portId = sourcePortId;
                    if (portId < 0 || i != targetParentIds.size() - 2) {
                        portId = createPort(graphId, targetParentId, true, -1);
                    }
                    Integer edgeId = createEdge(graphId, portId, targetVertexId, -1, -1, directed).get(0);
                    if (i == 0) {
                        masterId = edgeId;
                    }
                    if (i == targetParentIds.size() - 2) {
                        order = -1;
                    }
                    VGMainServiceHelper.graphDataBaseService.createEdgeAttribute(edgeId, ATTRIBUTE_NAME_FOR_COMPOSITE_EDGE_MASTER_ID, Integer.toString(masterId), AttributeRecord.INTEGER_ATTRIBUTE_TYPE, false);
                    VGMainServiceHelper.graphDataBaseService.createEdgeAttribute(edgeId, ATTRIBUTE_NAME_FOR_COMPOSITE_EDGE_ORDER, Integer.toString(order), AttributeRecord.INTEGER_ATTRIBUTE_TYPE, false);
                    result.add(edgeId);
                    targetVertexId = portId;
                    order++;
                }
                return result;
            } else {
                throw new NotImplementedException();
            }
        }
    }

    public static boolean isDirectedEdge(Edge edge) {
        Attribute attr = edge.getAttribute(ATTRIBUTE_NAME_FOR_DIRECTED_EDGE);
        return attr != null && attr.isBooleanType() && attr.getBooleanValue();
    }

    public static void setEdgeOrientation(Edge edge, boolean directed) {
        edge.addBooleanAttribute(ATTRIBUTE_NAME_FOR_DIRECTED_EDGE, directed, false);
    }

    public static Map.Entry<List<VGVertexCell>, List<VGVertexCell>> findPorts(List<VGVertexCell> vertices, Set<VGEdgeCell> edges) {
        List<VGVertexCell> ports = Lists.newArrayList();
        for (VGVertexCell vertex : vertices) {
            if (GraphUtils.isPort(vertex.getVGObject())) {
                ports.add(vertex);
            }
        }

        List<VGVertexCell> sourcePorts = Lists.newArrayList();
        List<VGVertexCell> targetPorts = Lists.newArrayList();
        for (VGVertexCell port : ports) {
            boolean sourcePort = false;
            boolean targetPort = false;

            for (VGEdgeCell edge : edges) {
                Attribute srcPortId = edge.getVGObject().getAttribute(GraphUtils.ATTRIBUTE_NAME_FOR_SRC_PORT_ID);
                Attribute trgPortId = edge.getVGObject().getAttribute(GraphUtils.ATTRIBUTE_NAME_FOR_TRG_PORT_ID);
                if (srcPortId != null && port.getVGObject().getLinkToVertexRecord().getId() == srcPortId.getIntegerValue()) {
                    sourcePort = true;
                    break;
                }
                if (trgPortId != null && port.getVGObject().getLinkToVertexRecord().getId() == trgPortId.getIntegerValue()) {
                    targetPort = true;
                    break;
                }
            }

            if (!sourcePort && !targetPort) {
                for (VGEdgeCell edge : port.vgGetEdges()) {
                    if (edge.getVGSoure() == port) {
                        if (edge.getParent() == port.getParent())
                            targetPort = true;
                        else
                            sourcePort = true;
                    }
                    if (edge.getVGTarget() == port) {
                        if (edge.getParent() == port.getParent())
                            sourcePort = true;
                        else
                            targetPort = true;
                    }
                }
            }

            if (sourcePort) {
                sourcePorts.add(port);
            } else if (targetPort) {
                targetPorts.add(port);
            } else {
                targetPorts.add(port);
            }
        }

        return new AbstractMap.SimpleImmutableEntry<>(sourcePorts, targetPorts);
    }


    /**
     * Download children for graph if graph components places in database and they have links to database.
     */
    public static void downloadChildren(final Graph graph) {
        Queue<Vertex> vertices = Queues.newArrayDeque(graph.getAllVertices());

        while (!vertices.isEmpty()) {
            Vertex vertex = vertices.poll();

            if (vertex.getLinkToVertexRecord() == null)
                continue;

            // download inner vertices
            List<Vertex> tmpVertices = getVerticesByOwnerId(vertex.getLinkToVertexRecord().getId(), -1);
            for (Vertex tmpVertex : tmpVertices) {
                graph.insertVertex(tmpVertex, vertex);
            }
            vertices.addAll(tmpVertices);
        }

        graph.bfs(new Graph.GraphSearchListener() {
            @Override
            public void onVertex(Vertex vertex, Vertex parent) {
                if (vertex.getLinkToVertexRecord() == null)
                    return;

                List<EdgeRecord> edgeRecords = VGMainServiceHelper.graphDataBaseService.getEdgeRecordsByVertexId(vertex.getLinkToVertexRecord().getId());
                for (EdgeRecord edgeRecord : edgeRecords) {
                    // check that edge or edge elements exists in graph
                    boolean check = false;
                    for (Edge edge : graph.getAllEdges()) {
                        if (edge.getLinkToEdgeRecord().getId() == edgeRecord.getId())
                            check = true;
                    }
                    if (check)
                        continue;

                    // check that src and trg vertices exists in graph
                    Edge edge = VGMainServiceHelper.graphDataBaseService.getEdge(edgeRecord.getId());
                    Vertex srcVertex = null, trgVertex = null;
                    for (Vertex tmpVertex : graph.getAllVertices()) {
                        if (tmpVertex.getLinkToVertexRecord().getId() == edgeRecord.getSourceId())
                            srcVertex = tmpVertex;
                        if (tmpVertex.getLinkToVertexRecord().getId() == edgeRecord.getTargetId())
                            trgVertex = tmpVertex;
                    }
                    if (srcVertex != null && trgVertex != null) {
                        edge.setSource(srcVertex);
                        edge.setTarget(trgVertex);
                        graph.addEdge(edge);
                    }
                }
            }
        });
    }

    public static void analyzeGraphAndLayout(Graph graph, GraphLayoutFactory graphLayoutFactory) {
        final MutableBoolean graphHasPorts = new MutableBoolean();
        final MutableBoolean graphIsHierarchical = new MutableBoolean();
        graph.bfs(new Graph.GraphSearchListener(){
            @Override
            public void onVertex(Vertex vertex, Vertex parent) {
                graphHasPorts.setValue(graphHasPorts.booleanValue() | isPort(vertex));
                graphIsHierarchical.setValue(graphIsHierarchical.booleanValue() | parent != null);
            }
        });

        if (graphIsHierarchical.booleanValue() && !graphLayoutFactory.isHierarchicalSupport())
            throw new IllegalArgumentException("Graph is hierarchical but the layout is not support it");
        if (graphHasPorts.booleanValue() && !graphLayoutFactory.isPortsSupport())
            throw new IllegalArgumentException("Graph has ports but the layout is not support it");
    }

//==============================================================================
//------------------Naming------------------------------------------------------
    public static String generateGraphTitle(Graph graph) {
        String defaultTitle = "unknown";

        if(graph == null)
            return defaultTitle;

        int graphId = -1;

        if (graph.getLinkToVertexRecord() == null) {
            for (Vertex vertex : graph.getAllVertices()) {
                if (vertex.getLinkToVertexRecord() != null) {
                    graphId = vertex.getLinkToVertexRecord().getGraphId();
                    break;
                }
            }
        }

        if (graphId >= 0) {
            return generateHumanGraphId(graphId);
        }

        return defaultTitle;
    }

    public static String generateHumanGraphId(int graphId) {
        return generateHumanId(graphId, GRAPH_PREFIX);
    }

    public static String generateHumanVertexId(int vertexId) {
        return generateHumanId(vertexId, VERTEX_PREFIX);
    }

    public static String generateHumanVertexId(Vertex vertex) {
        return generateHumanVertexId(vertex.getLinkToVertexRecord().getId());
    }

    /**
     * Save input order of vertices.
     */
    public static List<String> generateHumanVertexIds(Collection<Vertex> vertices) {
        List<String> result = Lists.newArrayList();

        for (Vertex vertex : vertices) {
            result.add(generateHumanVertexId(vertex));
        }
        return result;
    }

    public static String generateHumanEdgeId(int edgeId) {
        return generateHumanId(edgeId, EDGE_PREFIX);
    }

    public static String generateHumanId(int someId, String prefix) {
        Validate.isTrue(someId >= 0, "Id must be more zero");
        return prefix + someId;
    }

//==============================================================================
//------------------Algorithms--------------------------------------------------
    public static List<Graph> findAllPaths(Graph graph, Vertex source, Vertex target, AtomicBoolean findProcessInterrupter) {
        List<List<Vertex>> paths = Lists.newArrayList();
        dfs(graph, source, target, paths, findProcessInterrupter);

        List<Graph> result = Lists.newArrayList();
        for (List<Vertex> path : paths) {
            List<Edge> edges = Lists.newArrayList();
            for (int i = 0; i < path.size() - 1; i++) {
                Vertex tmpSource = path.get(i);
                Vertex tmpTarget = path.get(i+1);
                edges.addAll(graph.findEdgesBetweenVertices(tmpSource, tmpTarget));
            }

            result.add(new Graph(path, edges));
        }

        return result;
    }

    public static List<Graph> findAllCycles(Graph graph, Vertex source, AtomicBoolean findProcessInterrupter) {
        List<List<Vertex>> paths = Lists.newArrayList();

        for (Vertex vertex : graph.getTrgNeighborhoods(source)) {
            List<List<Vertex>> tmpPaths = Lists.newArrayList();
            dfs(graph, vertex, source, tmpPaths, findProcessInterrupter);
            paths.addAll(tmpPaths);
        }

        List<Graph> result = Lists.newArrayList();
        for (List<Vertex> path : paths) {
            List<Edge> edges = Lists.newArrayList();
            for (int i = 0; i < path.size() - 1; i++) {
                Vertex tmpSource = path.get(i);
                Vertex tmpTarget = path.get(i+1);
                edges.addAll(graph.findEdgesBetweenVertices(tmpSource, tmpTarget));
            }
            if (path.size() > 1) {
                Vertex tmpSource = path.get(0);
                Vertex tmpTarget = path.get(path.size() - 1);
                edges.addAll(graph.findEdgesBetweenVertices(tmpTarget, tmpSource));
            }

            if (!(path.size() == 2 && edges.size() == 2 && edges.get(0) == edges.get(1))) {
                result.add(new Graph(path, edges));
            }
        }

        return result;
    }

    public static Map<Graph, Float> findCriticalPaths(Graph graph, Vertex source, Vertex target, String attributeName, AtomicBoolean findProcessInterrupter) {
        List<Graph> paths = findAllPaths(graph, source, target, findProcessInterrupter);

        Map<Graph, Float> result = Maps.newLinkedHashMap();
        for (Graph path : paths) {
            if (StringUtils.isEmpty(attributeName)) {
                result.put(path, (float)path.getAllEdges().size());
            } else {
                float count = 0;
                for (Edge edge : path.getAllEdges()) {
                    Attribute attribute = edge.getAttribute(attributeName);
                    if (attribute != null && attribute.castValueToReal() != null) {
                        count += attribute.castValueToReal();
                    }
                }

                result.put(path, count);
            }
        }

        return new MapUtils<Graph, Float>().sortMapByValue(result);
    }

    /**
     * 1 - edge exists between two vertices.
     * 0 - no edge between two vertices.
     */
    public static int[][] doIntAlgorithmFloydUorshell(int[][] adjacencyMatrix) {
        Validate.notNull(adjacencyMatrix);

        int n = adjacencyMatrix.length;
        if (n == 0)
            return adjacencyMatrix;

        Validate.isTrue(adjacencyMatrix[0].length == n);

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                for (int k = 0; k < n; k++) {
                    if (adjacencyMatrix[j][i] != 0 && adjacencyMatrix[i][k] != 0) {
                        if (adjacencyMatrix[j][k] > adjacencyMatrix[j][i] + adjacencyMatrix[i][k] || adjacencyMatrix[j][k] == 0) {
                            adjacencyMatrix[j][k] = adjacencyMatrix[j][i] + adjacencyMatrix[i][k];
                        }
                    }
                }
            }
        }

        return adjacencyMatrix;
    }

    /**
     * Depth first search algorithm for finding all paths from source to target.
     */
    public static void dfs(Graph graph, Vertex source, Vertex target, List<List<Vertex>> paths, AtomicBoolean findProcessInterrupter) {
        Stack<Vertex> stack = new Stack<>();
        stack.add(source);

        Map<Vertex, Integer> path = Maps.newLinkedHashMap();
        while (!stack.isEmpty() && !findProcessInterrupter.get()) {
            //VGMainServiceHelper.logger.printDebug("Stack size: " + stack.size() + ", path count: " + paths.size());
            Vertex curr = stack.pop();

            int count = 0;
            if (curr != target) {
                for (Vertex vertex : graph.getTrgNeighborhoods(curr)) {
                    if (!path.containsKey(vertex)) {
                        stack.add(vertex);
                        count++;
                    }
                }
            }

            path.put(curr, count);
            if (count == 0) {
                if (curr == target) {
                    paths.add(Lists.newArrayList(path.keySet()));
                }

                List<Vertex> reversePath = Lists.reverse(Lists.newArrayList(path.keySet()));
                for (Vertex vertex : reversePath) {
                    if (path.get(vertex) > 1) {
                        path.put(vertex, path.get(vertex) - 1);
                        break;
                    } else {
                        path.remove(vertex);
                    }
                }
            }
        }
    }
}
