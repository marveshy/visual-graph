package vg.shared.file_utils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class FileList {
	/**
	 * Traverse a directory and get all files, and add the file into fileList
	 * 
	 * @param node
	 *            file or directory
	 */
	public static List<String> generateFileList(File node) {
		return generateFileList(node, node.getAbsolutePath(), new ArrayList<String>());
	}
	
	private static List<String> generateFileList(File node, String parent, List<String> fileList) {
		// add file only
		if (node.isFile()) {
			fileList.add(generateZipEntry(node.getAbsoluteFile().toString(), parent));
		}

		if (node.isDirectory()) {
			String[] subNote = node.list();
			for (String filename : subNote) {
				generateFileList(new File(node, filename), parent, fileList);
			}
		}
		
		return fileList;
	}
	
	/**
	 * Format the file path for zip
	 * 
	 * @param file
	 *            file path
	 * @return Formatted file path
	 */
	private static String generateZipEntry(String file, String sourceFolder) {
		return file.substring(sourceFolder.length() + 1, file.length());
	}
}
