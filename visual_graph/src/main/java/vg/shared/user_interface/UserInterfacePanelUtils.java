package vg.shared.user_interface;

import org.apache.commons.lang.StringUtils;
import vg.shared.gui.components.CompoundIcon;
import vg.shared.gui.components.RotatedIcon;
import vg.shared.gui.components.TextIcon;
import vg.impl.main_service.VGMainServiceHelper;
import vg.interfaces.main_service.VGMainGlobals;
import vg.interfaces.user_interface_service.UserInterfaceInstrument;
import vg.interfaces.user_interface_service.UserInterfaceListener;
import vg.interfaces.user_interface_service.UserInterfacePanel;
import vg.interfaces.user_interface_service.UserInterfaceService;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * @author Timur Zolotuhin (e-mail: tzolotuhin@gmail.com)
 */
public class UserInterfacePanelUtils {
    public static void createPanel(final String name, final UserInterfacePanel userInterfacePanel, final int instrumentPanel, final int place) {
        // create navigator button
        final JToggleButton componentButton = new JToggleButton();
        TextIcon ti = new TextIcon(componentButton, name);
        ti.setFont(UserInterfaceService.INSTRUMENT_PANEL_FONT);
        ImageIcon ii = new ImageIcon();
        CompoundIcon ci = new CompoundIcon(ii, ti);
        int angle = 0;
        if (instrumentPanel == UserInterfaceService.WEST_INSTRUMENT_PANEL)
            angle = 270;
        if (instrumentPanel == UserInterfaceService.EAST_INSTRUMENT_PANEL)
            angle = 90;

        RotatedIcon ri = new RotatedIcon(ci, angle);
        componentButton.setIcon(ri);

        final JPanel componentButtonOutView = new JPanel(new GridBagLayout());
        componentButtonOutView.add(componentButton, new GridBagConstraints(0,0, 1,1, 1,1, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0,0,0,0), 0,0));

        VGMainServiceHelper.userInterfaceService.addInstrument(new UserInterfaceInstrument() {
            @Override
            public JPanel getView() {
                return componentButtonOutView;
            }
        }, instrumentPanel);

        VGMainServiceHelper.userInterfaceService.addObserver(new UserInterfaceListener() {
            @Override
            public void onAddPanel(UserInterfacePanel panel, int newPlace) {
                if (panel == userInterfacePanel) {
                    componentButton.setSelected(true);
                }
            }

            @Override
            public void onRemovePanel(UserInterfacePanel panel, int place) {
                if (panel == userInterfacePanel) {
                    componentButton.setSelected(false);
                }
            }
        });

        componentButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (componentButton.isSelected()) {
                    // save to config
                    if (place == UserInterfaceService.SOUTH_INSTRUMENT_PANEL) {
                        VGMainServiceHelper.config.setProperty(VGMainGlobals.UI_SOUTH_PANEL_SELECTED_INSTRUMENT_KEY, name);
                    }

                    // set panel
                    VGMainServiceHelper.userInterfaceService.setPanel(userInterfacePanel, place);
                } else {
                    VGMainServiceHelper.userInterfaceService.setPanel(null, place);
                }
            }
        });

        String initSelectedInstrument = VGMainServiceHelper.config.getStringProperty(VGMainGlobals.UI_SOUTH_PANEL_SELECTED_INSTRUMENT_KEY);
        if (place == UserInterfaceService.SOUTH_INSTRUMENT_PANEL) {
            if (StringUtils.equals(initSelectedInstrument, name))
                VGMainServiceHelper.userInterfaceService.setPanel(userInterfacePanel, place);
        } else {
            VGMainServiceHelper.userInterfaceService.setPanel(userInterfacePanel, place);
        }
    }

    public static void createBorder(JComponent panel) {
        panel.setBorder(BorderFactory.createLineBorder(Color.BLACK, 1));
    }
}
