package vg.shared.user_interface;

import vg.shared.gui.SwingUtils;
import vg.impl.main_service.VGMainServiceHelper;

/**
 * @author Timur Zolotuhin (tzolotuhin@parallels.com)
 */
public class DefaultSwingUtilsCallBack extends SwingUtils.SwingUtilsCallBack {
    @Override
    public void onException(Throwable ex) {
        VGMainServiceHelper.logger.printException(ex);
    }
}
