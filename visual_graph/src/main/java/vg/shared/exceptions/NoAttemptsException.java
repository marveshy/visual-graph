package vg.shared.exceptions;

/**
 * @author Timur Zolotuhin (e-mail: tzolotuhin@gmail.com)
 */
public class NoAttemptsException extends RuntimeException {
    public NoAttemptsException(String message) {
        super(message);
    }

    public NoAttemptsException(String message, Throwable cause) {
        super(message, cause);
    }

    public NoAttemptsException(Throwable cause) {
        super(cause);
    }

    public NoAttemptsException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
