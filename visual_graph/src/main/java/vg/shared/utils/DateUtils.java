package vg.shared.utils;

import java.text.SimpleDateFormat;

/**
 * @author Timur Zolotuhin (e-mail: tzolotuhin@gmail.com)
 */
public class DateUtils {
    // Constants
    private static final long START_APP_TIME = System.currentTimeMillis();

    public static String getStartAppTimeInDefaultFormat() {
        return new SimpleDateFormat("dd.M.yyyy H.mm.ss").format(START_APP_TIME);
    }
}
