package vg.shared.utils;

/**
 * @author Timur Zolotuhin (e-mail: tzolotuhin@gmail.com)
 */
public class IOUtils {
    private static final String NEW_LINE_SEPARATOR = System.getProperty("line.separator");

    public static String getNewLineSeparator() {
        return NEW_LINE_SEPARATOR;
    }
}
