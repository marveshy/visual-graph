package vg.shared.utils;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import javax.swing.filechooser.FileSystemView;
import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;
import java.util.Map;

/**
 * @author Timur Zolotuhin (e-mail: tzolotuhin@gmail.com)
 */
public class FileUtils {
    // Main data
    private static String workingDir = System.getProperty("user.dir");
    private static final String userHomeDir = System.getProperty("user.home");
    private static final String tmpHomeDir = getWorkingDir() + "tmp";

    public static String getWorkingDir() {
        return workingDir + File.separator;
    }

    public static void setWorkingDir(String workingDir) {
        FileUtils.workingDir = workingDir;
    }

    public static String getUserHomeDir() {
        return userHomeDir + File.separator;
    }

    public static String getTmpHomeDir() {
        return tmpHomeDir + File.separator;
    }

    public static void cleanDirectory(String directoryName) throws IOException {
        cleanDirectory(new File(directoryName));
    }

    public static void cleanDirectory(File directory) throws IOException {
        if (directory.isDirectory() && directory.list().length != 0) {
            //list all the directory contents
            String files[] = directory.list();

            for (String temp : files) {
                //construct the file structure
                File fileDelete = new File(directory, temp);

                //recursive delete
                delete(fileDelete);
            }
        }
    }

    public static void moveFilesFromDirectoryToAnotherDirectory(String srcDirName, String trgDirName) throws IOException {
        moveFilesFromDirectoryToAnotherDirectory(new File(srcDirName), new File(trgDirName));
    }

    public static void moveFilesFromDirectoryToAnotherDirectory(File srcDirName, File trgDirName) throws IOException {
        if(srcDirName.isDirectory()) {
            File[] content = srcDirName.listFiles();
            if (content != null) {
                for (File aContent : content) {
                    if (!aContent.renameTo(new File(trgDirName + File.separator + aContent.getName())))
                        throw new IOException("Can't rename the file: " + aContent.getAbsolutePath());
                }
            }
        }
    }

    public static void copyFileToDirectory(File srcFile, File dstDir) throws IOException {
        try (FileInputStream inStream = new FileInputStream(srcFile); FileOutputStream outStream = new FileOutputStream(dstDir + File.separator + srcFile.getName())) {
            byte[] buffer = new byte[1024];
            int length;
            while ((length = inStream.read(buffer)) > 0) {
                outStream.write(buffer, 0, length);
            }
        }
    }

    public static void delete(File file) throws IOException {
        if (file.isDirectory()) {
            //list all the directory contents
            String files[] = file.list();

            for (String temp : files) {
                //construct the file structure
                File fileDelete = new File(file, temp);

                //recursive delete
                delete(fileDelete);
            }

            //check the directory again, if empty then delete it
            if (!file.delete())
                throw new IOException("Can't delete following directory: " + file.getAbsolutePath());
        } else {
            //if file, then delete it
            if (!file.delete())
                throw new IOException("Can't delete following file: " + file.getAbsolutePath());
        }
    }

    public static File createTmpFileWithName(String fileName) {
        String dirName = RandomUtils.generateEnglishRandomWord(16);
        File dirTmpFile = new File(getTmpHomeDir() + dirName);
        if (!dirTmpFile.mkdirs())
            throw new RuntimeException("mkdir return false");
        return new File (dirTmpFile.getAbsolutePath() + File.separator + fileName);
    }

    public static File createTmpDir() {
        String dirName = RandomUtils.generateEnglishRandomWord(16);
        File dirTmpFile = new File(getTmpHomeDir() + dirName);
        if (!dirTmpFile.mkdirs())
            throw new RuntimeException("mkdir return false");
        return dirTmpFile;
    }

    /**
     * Note: first page is 1, but not 0.
     */
    public static Map<Integer, String> readLinesFromFile(File input, List<Integer> lineNumbers) throws IOException {
        Map<Integer, String> result = Maps.newLinkedHashMap();
        try(BufferedReader br = new BufferedReader(new FileReader(input))) {
            int lineNumber = 1;
            for(String line; (line = br.readLine()) != null; ) {
                if (lineNumbers.contains(lineNumber)) {
                    result.put(lineNumber, line);
                }
                lineNumber++;
            }
        }
        return result;
    }

    public static List<File> readFilesOnlyInDir(File folder) {
        if (!folder.exists())
            throw new RuntimeException("exists return false");

        File[] files = folder.listFiles();
        List<File> result = Lists.newArrayList();

        if (files != null)
        for (File fileEntry : files) {
            if (!fileEntry.isDirectory()) {
                result.add(fileEntry);
            }
        }
        return result;
    }

    public static List<File> getUsbDevices() {
        FileSystemView fsv = FileSystemView.getFileSystemView();
        List<File> usbDevices = Lists.newArrayList();
        for (File f : File.listRoots()) {
            if (fsv.isDrive(f) && !fsv.isFloppyDrive(f) && f.canRead()) {
                usbDevices.add(f);
            }
        }

        return usbDevices;
    }

    public static int tryGetFileSize(URL url) throws IOException{
        URLConnection urlConnection = url.openConnection();
        List values = urlConnection.getHeaderFields().get("Content-Length");
        if (values != null && !values.isEmpty()) {
            String sLength = (String) values.get(0);

            if (sLength != null) {
                return Integer.valueOf(sLength);
            }
        }

        return -1;
    }
}
