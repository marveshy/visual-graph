package vg.shared.utils;

/**
 * @author Timur Zolotuhin (tzolotuhin@parallels.com)
 */
public class ArrayUtils {
    public static Float[][] toObject(float[][] array) {
        Float[][] result = new Float[array.length][];

        for (int i = 0; i < array.length; i++) {
            result[i] = org.apache.commons.lang.ArrayUtils.toObject(array[i]);
        }

        return result;
    }
}
