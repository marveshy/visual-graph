package vg.shared.gui;

import vg.impl.main_service.VGMainServiceHelper;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

/**
 * @author Timur Zolotuhin (e-mail: tzolotuhin@gmail.com)
 */
public class StorageFrame {
    protected final JFrame frame;

    protected static final int DEFAULT_WINDOW_POSITION_X = 0, DEFAULT_WINDOW_POSITION_Y = 0;
    protected static final int DEFAULT_WINDOW_WIDTH = 640, DEFAULT_WINDOW_HEIGHT = 480;

    protected final String configPositionX, configPositionY;
    protected final String configWidth, configHeight;

    protected Integer windowWidth, windowHeight;
    protected Integer windowPositionX, windowPositionY;

    public StorageFrame(String configPositionX, String configPositionY, String configWidth, String configHeight) {
        this(configPositionX, configPositionY, configWidth, configHeight, true);
    }

    public StorageFrame(String configPositionX, String configPositionY, String configWidth, String configHeight, boolean visible) {
        this.configPositionX = configPositionX;
        this.configPositionY = configPositionY;
        this.configWidth = configWidth;
        this.configHeight = configHeight;

        frame = new JFrame();

        refreshWindowSettingsFromConfig();
        updateWindowSettings();
        saveWindowSettingsToConfig();

        frame.addComponentListener(new ComponentAdapter() {
            public void componentResized(ComponentEvent e) {
                super.componentResized(e);
                Component c = (Component)e.getSource();
                windowWidth = c.getSize().width;
                windowHeight = c.getSize().height;
                saveWindowSettingsToConfig();
            }
            public void componentMoved(ComponentEvent e) {
                super.componentMoved(e);
                Component c = (Component)e.getSource();
                windowPositionX = c.getLocation().x;
                windowPositionY = c.getLocation().y;
                saveWindowSettingsToConfig();
            }
        });
        frame.setVisible(visible);
    }

    protected void refreshWindowSettingsFromConfig() {
        windowPositionX = VGMainServiceHelper.config.getIntegerProperty(configPositionX, DEFAULT_WINDOW_POSITION_X);
        windowPositionY = VGMainServiceHelper.config.getIntegerProperty(configPositionY, DEFAULT_WINDOW_POSITION_Y);
        windowWidth = VGMainServiceHelper.config.getIntegerProperty(configWidth, DEFAULT_WINDOW_WIDTH);
        windowHeight = VGMainServiceHelper.config.getIntegerProperty(configHeight, DEFAULT_WINDOW_HEIGHT);
    }

    protected void saveWindowSettingsToConfig() {
        VGMainServiceHelper.config.setProperty(configPositionX, windowPositionX.toString());
        VGMainServiceHelper.config.setProperty(configPositionY, windowPositionY.toString());
        VGMainServiceHelper.config.setProperty(configWidth, windowWidth.toString());
        VGMainServiceHelper.config.setProperty(configHeight, windowHeight.toString());
    }

    protected void updateWindowSettings() {
        frame.setSize(windowWidth, windowHeight);
        frame.setLocation(windowPositionX, windowPositionY);
    }
}
