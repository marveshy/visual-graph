package vg.shared.gui;

import javax.swing.*;

/**
 * @author Timur Zolotuhin (tzolotuhin@parallels.com)
 */
public class SwingUtils {
    public static void invokeInEDTIfNeeded(final Runnable runnable, final SwingUtilsCallBack callBack) {
        if (SwingUtilities.isEventDispatchThread()) {
            try {
                runnable.run();
            } catch (Throwable ex) {
                if (callBack != null)
                    callBack.onException(ex);
            }
        } else {
            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    invokeInEDTIfNeeded(runnable, callBack);
                }
            });
        }
    }

    public static void invokeAndWaitInEDTIfNeeded(final Runnable runnable, final SwingUtilsCallBack callBack) {
        if (SwingUtilities.isEventDispatchThread()) {
            try {
                runnable.run();
            } catch (Throwable ex) {
                if (callBack != null)
                    callBack.onException(ex);
            }
        } else {
            try {
                SwingUtilities.invokeAndWait(new Runnable() {
                    @Override
                    public void run() {
                        invokeAndWaitInEDTIfNeeded(runnable, callBack);
                    }
                });
            } catch (Throwable ex) {
                if (callBack != null)
                    callBack.onException(ex);
            }
        }
    }

    public static abstract class SwingUtilsCallBack {
        public void onException(Throwable ex) {}
    }
}
