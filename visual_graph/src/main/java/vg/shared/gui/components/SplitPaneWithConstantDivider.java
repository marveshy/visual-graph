package vg.shared.gui.components;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.HierarchyEvent;
import java.awt.event.HierarchyListener;

/**
 * @author Timur Zolotuhin (tzolotuhin@parallels.com)
 */
public class SplitPaneWithConstantDivider extends JSplitPane {
    private double constantDividerLocation = 0.5;

    public SplitPaneWithConstantDivider() {}

    public SplitPaneWithConstantDivider(int newOrientation) {
        super(newOrientation);
    }

    public SplitPaneWithConstantDivider(int newOrientation, boolean newContinuousLayout) {
        super(newOrientation, newContinuousLayout);
    }

    public SplitPaneWithConstantDivider(int newOrientation, Component newLeftComponent, Component newRightComponent) {
        super(newOrientation, newLeftComponent, newRightComponent);
    }

    public SplitPaneWithConstantDivider(int newOrientation, boolean newContinuousLayout, Component newLeftComponent, Component newRightComponent) {
        super(newOrientation, newContinuousLayout, newLeftComponent, newRightComponent);
    }

    public double getConstantDividerLocation() {
        return constantDividerLocation;
    }

    public void setConstantDividerLocation(double constantDividerLocation) {
        this.constantDividerLocation = constantDividerLocation;
        setDividerLocation(constantDividerLocation);
        SplitPaneWithConstantDivider.setDividerLocation(this, 0.5);
    }

    @Override
    public int getDividerLocation() {
        if (super.getOrientation() == JSplitPane.VERTICAL_SPLIT)
            return (int)(constantDividerLocation * this.getSize().getWidth());
        else
            return (int)(constantDividerLocation * this.getSize().getWidth());
    }

    @Override
    public int getLastDividerLocation() {
        if (super.getOrientation() == JSplitPane.VERTICAL_SPLIT)
            return (int)(constantDividerLocation * this.getSize().getWidth());
        else
            return (int)(constantDividerLocation * this.getSize().getWidth());
    }

//==============================================================================
//------------------STATIC METHODS----------------------------------------------
    public static JSplitPane setDividerLocation(final JSplitPane splitter, final double proportion) {
        if (splitter.isShowing()) {
            if (splitter.getWidth() > 0 && splitter.getHeight() > 0) {
                splitter.setDividerLocation(proportion);
            } else {
                splitter.addComponentListener(new ComponentAdapter() {
                    @Override
                    public void componentResized(ComponentEvent ce) {
                        //splitter.removeComponentListener(this);
                        setDividerLocation(splitter, proportion);
                    }
                });
            }
        } else {
            splitter.addHierarchyListener(new HierarchyListener() {
                @Override
                public void hierarchyChanged(HierarchyEvent e) {
                    if ((e.getChangeFlags() & HierarchyEvent.SHOWING_CHANGED) != 0 && splitter.isShowing()) {
                        splitter.removeHierarchyListener(this);
                        setDividerLocation(splitter, proportion);
                    }
                }
            });
        }
        return splitter;
    }
}
