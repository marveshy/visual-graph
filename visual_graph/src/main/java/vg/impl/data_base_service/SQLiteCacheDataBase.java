package vg.impl.data_base_service;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.lang.NotImplementedException;
import vg.impl.main_service.VGMainServiceHelper;
import vg.interfaces.data_base_service.data.record.*;

import java.util.*;

/**
 * @author Timur Zolotuhin (e-mail: tzolotuhin@gmail.com)
 */
public class SQLiteCacheDataBase {
    // Constants: cache
    private static final int MAX_GRAPH_MODEL_CACHE = 100;
    private static final int MAX_VERTEX_CACHE      = 100000;
    private static final int MAX_EDGE_CACHE        = 100000;
    private static final int MAX_ATTRIBUTE_CACHE   = 500000;
    private static final int MAX_FLUSH = 5000;

    // Constants: no cache
//    private static final int MAX_GRAPH_MODEL_CACHE = 0;
//    private static final int MAX_VERTEX_CACHE      = 0;
//    private static final int MAX_EDGE_CACHE        = 0;
//    private static final int MAX_ATTRIBUTE_CACHE   = 0;
//    private static final int MAX_FLUSH = 0;

    // cache for graph elements
    private Map<Integer, GraphRecord> cacheGraphModels;
    private Map<Integer, VertexRecord> cacheVertices;
    private Map<Integer, EdgeRecord> cacheEdges;
    private Map<Integer, AttributeRecord> cacheVertexAttributes, cacheEdgeAttributes;
    private Map<Integer, List<Integer>> indexVertexOwner2Attributes, indexEdgeOwner2Attributes;
    //private Map<Integer, List<Integer>> indexGraphId2Vertex, indexGraphId2Edge;
    //private Map<Integer, List<Integer>> indexGraphModelId2Graphs;

    // counters
    private int graphModelIdCounter;
    private int vertexIdCounter;
    private int edgeIdCounter;
    private int attributeIdCounter;

    // database data
    private SQLiteDataBase_1_0 dataBase;

    public SQLiteCacheDataBase() {
        dataBase = new SQLiteDataBase_1_0();

        // cache
        cacheGraphModels = Maps.newHashMap();
        cacheVertices = Maps.newHashMap();
        cacheEdges = Maps.newHashMap();
        cacheVertexAttributes = Maps.newHashMap();
        cacheEdgeAttributes = Maps.newHashMap();
        indexVertexOwner2Attributes = Maps.newHashMap();
        indexEdgeOwner2Attributes = Maps.newHashMap();
//        indexGraphId2Vertex = Maps.newHashMap();
//        indexGraphId2Edge = Maps.newHashMap();
//        indexGraphModelId2Graphs = Maps.newHashMap();

        // counters
        graphModelIdCounter = 0;
        vertexIdCounter = 0;
        edgeIdCounter = 0;
        attributeIdCounter = 0;
    }

    public int createGraphModelHeader(String name)  {
        GraphRecord graphRecord = new GraphRecord(graphModelIdCounter++);
        graphRecord.setName(name);
        cacheGraphModels.put(graphRecord.getId(), graphRecord);
        checkAndCommit();

        // indexing
//        indexGraphModelId2Graphs.put(graphHeader.getId(), new ArrayList<Integer>());

        return graphRecord.getId();
    }

    public int createVertexHeader(int graphId, int ownerId) {
        VertexRecord vertexRecord = new VertexRecord(vertexIdCounter++, graphId);
        vertexRecord.setOwnerId(ownerId);
        cacheVertices.put(vertexRecord.getId(), vertexRecord);

        // indexing
        indexVertexOwner2Attributes.put(vertexRecord.getId(), new ArrayList<Integer>());
//        indexGraphId2Vertex.get(graphId).add(vertexHeader.getId());

        checkAndCommit();

        return vertexRecord.getId();
    }

    public int createEdgeHeader(int graphId, int sourceVertexId, int targetVertexId) {
        // check preconditions
        VertexRecord sourceVertexRecord = getVertexRecord(sourceVertexId);
        VertexRecord targetVertexRecord = getVertexRecord(targetVertexId);

        if (sourceVertexRecord.getOwnerId() != targetVertexRecord.getOwnerId())
            throw new IllegalArgumentException(
                    String.format(
                            "Source owner id is '%d', target owner id is '%d'. But they should have same owner id.",
                            sourceVertexRecord.getOwnerId(),
                            targetVertexRecord.getOwnerId()));

        // add new record
        EdgeRecord edgeRecord = new EdgeRecord(edgeIdCounter++, graphId, sourceVertexId, targetVertexId);
        cacheEdges.put(edgeRecord.getId(), edgeRecord);

        // indexing
        indexEdgeOwner2Attributes.put(edgeRecord.getId(), new ArrayList<Integer>());
//        indexGraphId2Edge.get(graphId).add(edgeHeader.getId());

        checkAndCommit();

        return edgeRecord.getId();
    }

    public int createEdgeAttributeRecord(int edgeId, String name, String strValue, int type, boolean visible) {
        // check existing attribute with same name
        for (AttributeRecord attributeRecord : getAttributeHeadersByOwner(edgeId, AttributeRecord.EDGE_OWNER_TYPE)) {
            if (name.hashCode() == attributeRecord.getName().hashCode() && attributeRecord.getName().equals(name)) {
                attributeRecord.setValue(strValue, type);
                attributeRecord.setVisible(visible);
                modifyAttributeHeader(attributeRecord);
                return attributeRecord.getId();
            }
        }

        // add attribute record
        AttributeRecord attributeRecord = new AttributeRecord(attributeIdCounter++, edgeId, AttributeRecord.EDGE_OWNER_TYPE, name, strValue, type, visible);
        cacheEdgeAttributes.put(attributeRecord.getId(), attributeRecord);

        // indexing by owner
        indexEdgeOwner2Attributes.get(attributeRecord.getOwnerId()).add(attributeRecord.getId());

        checkAndCommit();
        return attributeRecord.getId();
    }

    public int createVertexAttributeRecord(int vertexId, String name, String strValue, int type, boolean visible) {
        // check existing attribute with same name
        for (AttributeRecord attributeRecord : getAttributeHeadersByOwner(vertexId, AttributeRecord.VERTEX_OWNER_TYPE)) {
            if (name.hashCode() == attributeRecord.getName().hashCode() && attributeRecord.getName().equals(name)) {
                attributeRecord.setValue(strValue, type);
                attributeRecord.setVisible(visible);
                modifyAttributeHeader(attributeRecord);
                return attributeRecord.getId();
            }
        }

        // add attribute record
        AttributeRecord attributeRecord = new AttributeRecord(attributeIdCounter++, vertexId, AttributeRecord.VERTEX_OWNER_TYPE, name, strValue, type, visible);
        cacheVertexAttributes.put(attributeRecord.getId(), attributeRecord);

        // indexing by owner
        indexVertexOwner2Attributes.get(attributeRecord.getOwnerId()).add(attributeRecord.getId());

        checkAndCommit();
        return attributeRecord.getId();
    }

    public VertexRecord getVertexRecord(int vertexId) {
        VertexRecord vertexRecord = cacheVertices.get(vertexId);
        if(vertexRecord != null) {
            return vertexRecord.clone();
        } else {
            return dataBase.getVertexRecord(vertexId);
        }
    }

    public VertexRecord getVertexHeaderByInnerGraphId(int graphId) {
//        for (VertexHeader vertexHeader : cacheVertices.values()) {
//            if (vertexHeader.getLink2InnerGraph() == graphId)
//                return vertexHeader.clone();
//        }
//        return dataBase.getVertexHeaderByInnerGraphId(graphId);
        throw new NotImplementedException("Please, implement this");
    }

    public List<VertexRecord> getVertexRecordsByOwnerId(int ownerId) {
        List<VertexRecord> result = Lists.newArrayList();

        // TODO: Please optimize it, using index maps
        for (VertexRecord vertexRecord : cacheVertices.values()) {
            if (vertexRecord.getOwnerId() == ownerId)
                result.add(vertexRecord.clone());
        }
        result.addAll(dataBase.getVertexRecordsByOwnerId(ownerId));
        return result;
    }

    public EdgeRecord getEdgeHeader(int edgeId) {
        EdgeRecord eh = cacheEdges.get(edgeId);
        if(eh != null) {
            return eh.clone();
        } else {
            return dataBase.getEdgeHeader(edgeId);
        }
    }

    public List<EdgeRecord> getEdgeRecordsByGraphId(int graphId) {
        List<EdgeRecord> result = Lists.newArrayList();

        // TODO: Please optimize it, using index maps
        for (EdgeRecord edgeRecord : cacheEdges.values()) {
            if (edgeRecord.getGraphId() == graphId)
                result.add(edgeRecord.clone());
        }
        result.addAll(dataBase.getEdgeRecordsByGraphId(graphId));
        return result;
    }

    public List<EdgeRecord> getEdgeRecordsByVertexId(int vertexId) {
        List<EdgeRecord> result = Lists.newArrayList();

        // TODO: Please optimize it, using index maps
        for (EdgeRecord edgeRecord : cacheEdges.values()) {
            if (edgeRecord.getSourceId() == vertexId || edgeRecord.getTargetId() == vertexId)
                result.add(edgeRecord.clone());
        }
        result.addAll(dataBase.getEdgeHeadersByVertexId(vertexId));
        return result;
    }

    public AttributeRecord getAttributeRecord(int attributeId) {
        AttributeRecord attributeRecord = cacheVertexAttributes.get(attributeId);
        if (attributeRecord == null)
            attributeRecord = cacheEdgeAttributes.get(attributeId);

        if(attributeRecord != null) {
            return attributeRecord.clone();
        } else {
            return dataBase.getAttributeRecord(attributeId);
        }
    }

    public List<AttributeRecord> getAttributeRecords(List<Integer> attributeIds) {
        List<AttributeRecord> result = Lists.newArrayList();
        if (attributeIds != null) {
            for (Integer id : attributeIds) {
                result.add(getAttributeRecord(id));
            }
        }
        return result;
    }

    public List<AttributeRecord> getAttributeHeadersByOwner(int ownerId, int ownerType) {
        List<AttributeRecord> result = Lists.newArrayList();

        switch (ownerType) {
            case GraphDataBaseFormat_1_0.ATTRIBUTE_TABLE_VERTEX_OWNER_TYPE:
                result.addAll(getAttributeRecords(indexVertexOwner2Attributes.get(ownerId)));
                break;
            case GraphDataBaseFormat_1_0.ATTRIBUTE_TABLE_EDGE_OWNER_TYPE:
                result.addAll(getAttributeRecords(indexEdgeOwner2Attributes.get(ownerId)));
                break;
        }
        return result;
    }

    public GraphRecord getGraphHeader(int graphId) {
        GraphRecord gh = cacheGraphModels.get(graphId);
        if(gh != null) {
            return gh.clone();
        } else {
            return dataBase.getGraphModelHeader(graphId);
        }
    }

    public List<VertexRecord> getRoots(int graphId) {
        List<VertexRecord> result = Lists.newArrayList();

        // TODO: Please optimize it, using index maps
        for (VertexRecord vertexRecord : cacheVertices.values()) {
            if (vertexRecord.getGraphId() == graphId && vertexRecord.getOwnerId() == VertexRecord.NO_OWNER_ID)
                result.add(vertexRecord.clone());
        }
        result.addAll(dataBase.getRoots(graphId));
        return result;
    }

    public List<GraphRecord> getGraphModelHeaders(int count) {
        List<GraphRecord> result = Lists.newArrayList();

        for (GraphRecord header : cacheGraphModels.values()) {
            result.add(header);
        }

        result.addAll(dataBase.getGraphModelHeaders(count));
        return result;
    }

    public void close() {
        dataBase.close();
    }

    public int getGraphCount(int graphModelId) {
//        return indexGraphModelId2Graphs.get(graphModelId).size();
        return 0;
    }

    public int getVertexCount(int graphModelId) {
        int count = 0;

//        List<Integer> graphIds = indexGraphModelId2Graphs.get(graphModelId);
//        for (Integer graphId : graphIds) {
//            count += indexGraphId2Vertex.get(graphId).size();
//        }

        return count;
    }

    public int getEdgeCount(int graphModelId) {
        int count = 0;

//        List<Integer> graphIds = indexGraphModelId2Graphs.get(graphModelId);
//        for (Integer graphId : graphIds) {
//            count += indexGraphId2Edge.get(graphId).size();
//        }

        return count;
    }

//=============================================================================
//--------------Modifying methods----------------------------------------------
    public void modifyGraphModelHeader(GraphRecord newGraphRecord) {
        GraphRecord old_graph_header = cacheGraphModels.get(newGraphRecord.getId());

        if(old_graph_header != null) {
            cacheGraphModels.put(newGraphRecord.getId(), newGraphRecord);
        } else {
            dataBase.modifyGraphModelHeader(newGraphRecord);
        }
    }

    public void modifyVertexHeader(VertexRecord newVertexRecord) {
        VertexRecord old_vertex_header = cacheVertices.get(newVertexRecord.getId());

        if(old_vertex_header != null) {
            cacheVertices.put(newVertexRecord.getId(), newVertexRecord);
        } else {
            dataBase.modifyVertexHeader(newVertexRecord);
        }
    }

    public void modifyEdgeHeader(EdgeRecord newEdgeRecord) {
        EdgeRecord old_edge_header = cacheEdges.get(newEdgeRecord.getId());

        if(old_edge_header != null) {
            cacheEdges.put(newEdgeRecord.getId(), newEdgeRecord);
        } else {
            dataBase.modifyEdgeHeader(newEdgeRecord);
        }
    }

    public void modifyAttributeHeader(AttributeRecord newAttributeRecord) {
        AttributeRecord oldAttrHeader = null;
        switch (newAttributeRecord.getOwnerType()) {
            case GraphDataBaseFormat_1_0.ATTRIBUTE_TABLE_VERTEX_OWNER_TYPE:
                oldAttrHeader = cacheVertexAttributes.get(newAttributeRecord.getId());
                if (oldAttrHeader != null) {
                    cacheVertexAttributes.put(newAttributeRecord.getId(), newAttributeRecord);
                }
                break;
            case GraphDataBaseFormat_1_0.ATTRIBUTE_TABLE_EDGE_OWNER_TYPE:
                oldAttrHeader = cacheEdgeAttributes.get(newAttributeRecord.getId());
                if (oldAttrHeader != null) {
                    cacheEdgeAttributes.put(newAttributeRecord.getId(), newAttributeRecord);
                }
                break;
        }

        if (oldAttrHeader == null)
            dataBase.modifyAttributeHeader(newAttributeRecord);
    }

    public void flush() {
        //VGMainServiceHelper.logger.printDebug("Flushing...");
        dataBase.addGraphModelHeaders(copyAndRemoveFromMap(cacheGraphModels, false));
        dataBase.addVertexHeaders(copyAndRemoveFromMap(cacheVertices, false));
        dataBase.addEdgeHeaders(copyAndRemoveFromMap(cacheEdges, false));

        dataBase.addAttributeHeaders(copyAndRemoveFromMap(cacheVertexAttributes, false));
        dataBase.addAttributeHeaders(copyAndRemoveFromMap(cacheEdgeAttributes, false));
        //VGMainServiceHelper.logger.printDebug("Finish flushing");
    }

//=============================================================================
//--------------PRIVATE METHODS------------------------------------------------
    private static <T> Collection<T> copyAndRemoveFromMap(Map<Integer, T> map, boolean all) {
        //VGMainServiceHelper.logger.printDebug("Map size before: " + map.size());
        Collection<T> buffAttributeHeaders = Lists.newArrayList();

        if (all) {
            buffAttributeHeaders = map.values();
            map.clear();
        } else {
            if (map.size() >= MAX_FLUSH) {
                Iterator<Map.Entry<Integer, T>> it = map.entrySet().iterator();
                for (int i = 0; i < MAX_FLUSH && it.hasNext(); i++) {
                    Map.Entry<Integer, T> entry = it.next();
                    buffAttributeHeaders.add(entry.getValue());
                    it.remove();
                }
            }
        }
        //VGMainServiceHelper.logger.printDebug("Map size after: " + map.size());
        return buffAttributeHeaders;
    }

    private void checkAndCommit() {
        if(cacheGraphModels.size() > MAX_GRAPH_MODEL_CACHE) {
            VGMainServiceHelper.logger.printDebug("Commit graph models");
            dataBase.addGraphModelHeaders(copyAndRemoveFromMap(cacheGraphModels, true));
            VGMainServiceHelper.logger.printDebug("Finish commit graph models");
        }

        if(cacheVertices.size() > MAX_VERTEX_CACHE) {
            VGMainServiceHelper.logger.printDebug("Commit vertices");
            dataBase.addVertexHeaders(copyAndRemoveFromMap(cacheVertices, true));
            VGMainServiceHelper.logger.printDebug("Finish commit vertices");
        }

        if(cacheEdges.size() > MAX_EDGE_CACHE) {
            VGMainServiceHelper.logger.printDebug("Commit edges");
            dataBase.addEdgeHeaders(copyAndRemoveFromMap(cacheEdges, true));
            VGMainServiceHelper.logger.printDebug("Finish commit edges");
        }

        if(cacheVertexAttributes.size() > MAX_ATTRIBUTE_CACHE) {
            VGMainServiceHelper.logger.printDebug("Commit vertex attributes");
            dataBase.addAttributeHeaders(copyAndRemoveFromMap(cacheVertexAttributes, true));
            VGMainServiceHelper.logger.printDebug("Finish commit vertex attributes");
        }

        if(cacheEdgeAttributes.size() > MAX_ATTRIBUTE_CACHE) {
            VGMainServiceHelper.logger.printDebug("Commit edge attributes");
            dataBase.addAttributeHeaders(copyAndRemoveFromMap(cacheEdgeAttributes, true));
            VGMainServiceHelper.logger.printDebug("Finish commit edge attributes");
        }
    }
}
