package vg.impl.data_base_service;

import vg.interfaces.data_base_service.data.record.AttributeRecord;

public class GraphDataBaseFormat_1_0 {
    // Format version table
    public static final String VERSION_TABLE = "version_table";
    public static final String VERSION_TABLE_ID = "id";
    public static final String VERSION_TABLE_VERSION = "db_format_version";

	// Vertex table
    public static final String VERTEX_TABLE = "vertex";
	public static final String VERTEX_TABLE_ID = "id";
	public static final String VERTEX_TABLE_GRAPH_ID = "graph_id";
	public static final String VERTEX_TABLE_OWNER_ID = "owner_id";
	
	// Edge table
    public static final String EDGE_TABLE = "edge";
	public static final String EDGE_TABLE_ID = "id";
	public static final String EDGE_TABLE_GRAPH_ID = "graph_id";
	public static final String EDGE_TABLE_SRC_VERTEX = "src_id";
	public static final String EDGE_TABLE_TRG_VERTEX = "trg_id";
	
	// Attribute table
    public static final String ATTRIBUTE_TABLE = "attribute";
	public static final String ATTRIBUTE_TABLE_ID = "id";
	public static final String ATTRIBUTE_TABLE_OWNER_ID = "owner_id";
	public static final String ATTRIBUTE_TABLE_OWNER_TYPE = "owner_type";
	public static final String ATTRIBUTE_TABLE_NAME = "name";
	public static final String ATTRIBUTE_TABLE_VALUE = "value";
    public static final String ATTRIBUTE_TABLE_VISIBLE = "visible";
	public static final String ATTRIBUTE_TABLE_VALUE_TYPE = "value_type";

	public static final int ATTRIBUTE_TABLE_VERTEX_OWNER_TYPE = AttributeRecord.VERTEX_OWNER_TYPE;
	public static final int ATTRIBUTE_TABLE_EDGE_OWNER_TYPE = AttributeRecord.EDGE_OWNER_TYPE;

    public static final int ATTRIBUTE_TABLE_STRING_ATTRIBUTE_TYPE = AttributeRecord.STRING_ATTRIBUTE_TYPE;
    public static final int ATTRIBUTE_TABLE_BOOLEAN_ATTRIBUTE_TYPE = AttributeRecord.BOOLEAN_ATTRIBUTE_TYPE;
    public static final int ATTRIBUTE_TABLE_INTEGER_ATTRIBUTE_TYPE = AttributeRecord.INTEGER_ATTRIBUTE_TYPE;
    public static final int ATTRIBUTE_TABLE_DOUBLE_ATTRIBUTE_TYPE = AttributeRecord.DOUBLE_ATTRIBUTE_TYPE;

	// Graph model table
    public static final String GRAPH_TABLE = "graph_model";
	public static final String GRAPH_TABLE_ID = "id";
	public static final String GRAPH_TABLE_NAME = "name";
}
