package vg.impl.data_base_service;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.lang.Validate;
import vg.shared.gui.SwingUtils;
import vg.impl.main_service.VGMainServiceHelper;
import vg.interfaces.data_base_service.GraphDataBaseListener;
import vg.interfaces.data_base_service.IGraphDataBaseService;
import vg.interfaces.data_base_service.data.graph.*;
import vg.interfaces.data_base_service.data.record.*;
import vg.shared.user_interface.DefaultSwingUtilsCallBack;

import java.util.AbstractMap;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * This class realizes model, which uses SQL data base.
 * This model may cache graph elements in operating memory.
 *
 * @author Timur Zolotuhin (e-mail: tzolotuhin@gmail.com)
 */
public class SQLiteCacheGraphDataBaseService implements IGraphDataBaseService, Runnable {
	// Main data
	private SQLiteCacheDataBase sqliteCacheDataBase;
    private List<GraphDataBaseListener> graphDataBaseListeners;
    private boolean close = false;

    // Mutex
    private final Object generalMutex = new Object();

	public SQLiteCacheGraphDataBaseService() {
        sqliteCacheDataBase = new SQLiteCacheDataBase();
        graphDataBaseListeners = Lists.newArrayList();
        Thread t = new Thread(this);
        t.setDaemon(true);
        t.start();
	}

    @Override
    public void run() {
        while (!close) {
            try {
                synchronized (generalMutex) {
                    sqliteCacheDataBase.flush();
                }
                Thread.sleep(100);
            } catch (Throwable ex) {
                VGMainServiceHelper.logger.printDebug(ex.getMessage(), ex);
            }
        }
    }

    @Override
	public int createGraph(String name)  {
        int graphModelId;
        synchronized (generalMutex) {
    		graphModelId = sqliteCacheDataBase.createGraphModelHeader(name);
        }
        doNotifyOnNewGraphModel(graphModelId);
        return graphModelId;
	}
	
	@Override
	public int createVertex(int graphId, int innerGraphId) {
		synchronized (generalMutex) {
            return sqliteCacheDataBase.createVertexHeader(graphId, innerGraphId);
        }
	}
	
	@Override
	public int createEdge(int graphId, int sourceVertexId, int targetVertexId) {
        synchronized (generalMutex) {
            return sqliteCacheDataBase.createEdgeHeader(graphId, sourceVertexId, targetVertexId);
        }
	}
	
	@Override
	public int createEdgeAttribute(int edgeId, String name, String value, int type, boolean visible) {
        synchronized (generalMutex) {
            return sqliteCacheDataBase.createEdgeAttributeRecord(edgeId, name, value, type, visible);
        }
	}
	
	@Override
	public int createVertexAttribute(int vertexId, String name, String value, int type, boolean visible) {
        synchronized (generalMutex) {
            return sqliteCacheDataBase.createVertexAttributeRecord(vertexId, name, value, type, visible);
        }
	}

    @Override
    public void modifyVertexHeader(VertexRecord newVertexRecord) {
        synchronized (generalMutex) {
            sqliteCacheDataBase.modifyVertexHeader(newVertexRecord);
        }
    }

    @Override
    public void modifyEdgeHeader(EdgeRecord newEdgeRecord) {
        synchronized (generalMutex) {
            sqliteCacheDataBase.modifyEdgeHeader(newEdgeRecord);
        }
    }

    @Override
    public List<GraphRecord> getGraphRecords(int count) {
        synchronized (generalMutex) {
            return sqliteCacheDataBase.getGraphModelHeaders(count);
        }
    }

    @Override
    public VertexRecord getVertexRecord(int vertexId) {
        synchronized (generalMutex) {
            return sqliteCacheDataBase.getVertexRecord(vertexId);
        }
    }

    @Override
    public List<VertexRecord> getVertexRecordsByOwnerId(int ownerId, int count) {
        synchronized (generalMutex) {
            return sqliteCacheDataBase.getVertexRecordsByOwnerId(ownerId);
        }
    }

    @Override
    public EdgeRecord getEdgeRecord(int edgeId) {
        synchronized (generalMutex) {
            return sqliteCacheDataBase.getEdgeHeader(edgeId);
        }
    }

    @Override
    public List<EdgeRecord> getEdgeRecordsByVertexId(int vertexId) {
        synchronized (generalMutex) {
            return sqliteCacheDataBase.getEdgeRecordsByVertexId(vertexId);
        }
    }

    @Override
    public List<EdgeRecord> getEdgeRecordsByGraphId(int graphId) {
        synchronized (generalMutex) {
            return sqliteCacheDataBase.getEdgeRecordsByGraphId(graphId);
        }
    }

    @Override
    public List<VertexRecord> getRoots(int graphId) {
        synchronized (generalMutex) {
            return sqliteCacheDataBase.getRoots(graphId);
        }
    }

    @Override
    public GraphRecord getGraphRecord(int graphId) {
        synchronized (generalMutex) {
            return sqliteCacheDataBase.getGraphHeader(graphId);
        }
    }

    @Override
    public AttributeRecord getAttributeHeader(int attributeId) {
        synchronized (generalMutex) {
            return sqliteCacheDataBase.getAttributeRecord(attributeId);
        }
    }

    @Override
    public List<AttributeRecord> getAttributeRecordsByOwner(int ownerId, int ownerType) {
        synchronized (generalMutex) {
            return sqliteCacheDataBase.getAttributeHeadersByOwner(ownerId, ownerType);
        }
    }

    @Override
    public Vertex getVertex(int vertexId) {
        synchronized (generalMutex) {
            VertexRecord vertexRecord = getVertexRecord(vertexId);
            if (vertexRecord == null)
                return null;
            return new Vertex(getAttributesByOwner(vertexRecord.getId(), AttributeRecord.VERTEX_OWNER_TYPE), vertexRecord);
        }
    }

	@Override
	public Edge getEdge(int edgeId) {
        synchronized (generalMutex) {
            EdgeRecord edgeRecord = getEdgeRecord(edgeId);
            if (edgeRecord == null)
                return null;
            return new Edge(getVertex(edgeRecord.getSourceId()), getVertex(edgeRecord.getTargetId()), getAttributesByOwner(edgeRecord.getId(), AttributeRecord.EDGE_OWNER_TYPE), edgeRecord);
        }
	}

    @Override
    public Map.Entry<List<Vertex>, Map.Entry<List<Edge>, List<Edge>>> getSubGraphElements(int vertexOwnerId) {
        synchronized (generalMutex) {
            List<Vertex> vertices = getVertices(vertexOwnerId);
            Map.Entry<List<Edge>, List<Edge>> edges = getEdges(vertices);

            return new AbstractMap.SimpleEntry<>(vertices, edges);
        }
    }

    @Override
    public Attribute getAttribute(int attributeId) {
        synchronized (generalMutex) {
            return convertToAttribute(getAttributeHeader(attributeId));
        }
    }

    @Override
    public void close() {
        synchronized (generalMutex) {
            close = true;
            sqliteCacheDataBase.close();
        }
    }

    @Override
    public void addListener(GraphDataBaseListener graphDataBaseListener) {
        synchronized (generalMutex) {
            graphDataBaseListeners.add(graphDataBaseListener);
        }
    }

    @Override
    public int getGraphCount(int graphModelId) {
        synchronized(generalMutex) {
            return sqliteCacheDataBase.getGraphCount(graphModelId);
        }
    }

    @Override
    public int getVertexCount(int graphModelId) {
        synchronized(generalMutex) {
            return sqliteCacheDataBase.getVertexCount(graphModelId);
        }
    }


    @Override
    public int getEdgeCount(int graphModelId) {
        synchronized(generalMutex) {
            return sqliteCacheDataBase.getEdgeCount(graphModelId);
        }
    }

    @Override
    public Map<Integer, List<Integer>> findParents(List<Integer> vertexIds) {
        Map<Integer, List<Integer>> hierarchies = Maps.newHashMap();
	    for (int vertexId : vertexIds) {
            List<Integer> hierarchy = Lists.newArrayList();
            int tempVertexId = vertexId;
            do {
                VertexRecord vertexRecord = getVertexRecord(tempVertexId);
                tempVertexId = vertexRecord.getOwnerId();
                hierarchy.add(tempVertexId);
            } while (tempVertexId > -1);

            hierarchies.put(vertexId, Lists.reverse(hierarchy));
        }

        List<Integer> parentIds = hierarchies.get(vertexIds.get(0));
        int index;
	    for (index = 0; index < parentIds.size(); index++) {
            int currentParentId = parentIds.get(index);
            boolean check = false;
            for (List<Integer> hierarchy : hierarchies.values()) {
                if (index >= hierarchy.size()) {
                    check = true;
                    break;
                }

                if (hierarchy.get(index) != currentParentId) {
                    check = true;
                    break;
                }
            }

            if (check) {
                break;
            }
        }
        index--;

        Map<Integer, List<Integer>> result = Maps.newHashMap();
        for (int vertexId : vertexIds) {
            List<Integer> hierarchy = hierarchies.get(vertexId);
            List<Integer> newHierarchy = Lists.newArrayList();

            for (int i = index; i < hierarchy.size(); i++) {
                newHierarchy.add(hierarchy.get(i));
            }

            result.put(vertexId, Lists.reverse(newHierarchy));
        }

        return result;
    }

//==============================================================================
//------------------PRIVATE METHODS---------------------------------------------
    private List<GraphDataBaseListener> syncCopyListeners() {
        List<GraphDataBaseListener> copyListeners;

        synchronized (generalMutex) {
            copyListeners = Lists.newArrayList(graphDataBaseListeners);
        }

        return copyListeners;
    }

    private void doNotifyOnNewGraphModel(final int graphModelId) {
        SwingUtils.invokeInEDTIfNeeded(new Runnable() {
            @Override
            public void run() {
                for (GraphDataBaseListener listener : syncCopyListeners()) {
                    listener.onOpenNewGraphModel(graphModelId);
                }
            }
        }, new DefaultSwingUtilsCallBack());
    }

    private List<Vertex> getVertices(int vertexOwnerId) {
        List<Vertex> vertices = Lists.newArrayList();
        List<VertexRecord> vertexRecords = sqliteCacheDataBase.getVertexRecordsByOwnerId(vertexOwnerId);
        for (VertexRecord vertexRecord : vertexRecords) {
            vertices.add(getVertex(vertexRecord.getId()));
        }
        return vertices;
    }

    /**
     * Returns entity of edges: first value is edges between input vertices,
     * second value is edge outsiders.
     */
    private Map.Entry<List<Edge>, List<Edge>> getEdges(List<Vertex> vertices) {
        int graphId = -1;
        if (vertices.iterator().hasNext()) {
            int vertexId = vertices.iterator().next().getLinkToVertexRecord().getId();
            if (vertexId >= 0) {
                graphId = getVertexRecord(vertexId).getGraphId();
            }
        }

        List<Edge> inEdges = Lists.newArrayList();
        List<Edge> outEdges = Lists.newArrayList();

        List<EdgeRecord> edgeRecords = sqliteCacheDataBase.getEdgeRecordsByGraphId(graphId);

        for (EdgeRecord edgeRecord : edgeRecords) {
            Vertex src = null, trg = null;

            for (Vertex vertex : vertices) {
                if (vertex.getLinkToVertexRecord().getId() == edgeRecord.getSourceId())
                    src = vertex;
                if (vertex.getLinkToVertexRecord().getId() == edgeRecord.getTargetId())
                    trg = vertex;
            }

            if (src != null && trg != null) {
                inEdges.add(new Edge(src, trg, getAttributesByOwner(edgeRecord.getId(), AttributeRecord.EDGE_OWNER_TYPE), edgeRecord));
            } else if (src != null || trg != null) {
                outEdges.add(new Edge(src, trg, getAttributesByOwner(edgeRecord.getId(), AttributeRecord.EDGE_OWNER_TYPE), edgeRecord));
            }
        }

        return new AbstractMap.SimpleEntry<>(inEdges, outEdges);
    }

    private List<Attribute> getAttributesByOwner(int ownerId, int ownerType) {
        List<AttributeRecord> attributeRecords = sqliteCacheDataBase.getAttributeHeadersByOwner(ownerId, ownerType);
        return convertToAttributes(attributeRecords);
    }

    private static Attribute convertToAttribute(AttributeRecord attributeRecord) {
        Validate.notNull(attributeRecord);

        return new Attribute(attributeRecord.getName(), attributeRecord.getStringValue(), attributeRecord.getType(), attributeRecord.isVisible());
    }

    private static List<Attribute> convertToAttributes(List<AttributeRecord> attributeRecords) {
        Validate.notNull(attributeRecords);

        List<Attribute> result = Lists.newArrayList();
        for (AttributeRecord header : attributeRecords) {
            result.add(convertToAttribute(header));
        }

        return result;
    }
}
