package vg.impl.data_base_service;

import com.google.common.collect.Lists;
import org.apache.commons.lang.Validate;
import vg.shared.utils.DateUtils;
import vg.shared.utils.FileUtils;
import vg.shared.utils.RandomUtils;
import vg.interfaces.data_base_service.data.record.*;

import java.io.File;
import java.sql.*;
import java.util.Collection;
import java.util.List;

public class SQLiteDataBase_1_0 {
    // Constants: creates
    private static final String CREATE_VERTEX_TABLE = "create table " + GraphDataBaseFormat_1_0.VERTEX_TABLE + " (" +
            GraphDataBaseFormat_1_0.VERTEX_TABLE_ID + " INT PRIMARY KEY, " +
            GraphDataBaseFormat_1_0.VERTEX_TABLE_GRAPH_ID + " INT, " +
            GraphDataBaseFormat_1_0.VERTEX_TABLE_OWNER_ID + " INT);";

    private static final String CREATE_EDGE_TABLE = "create table " + GraphDataBaseFormat_1_0.EDGE_TABLE + " (" +
            GraphDataBaseFormat_1_0.EDGE_TABLE_ID + " INT PRIMARY KEY," +
            GraphDataBaseFormat_1_0.EDGE_TABLE_GRAPH_ID + " INT, " +
            GraphDataBaseFormat_1_0.EDGE_TABLE_SRC_VERTEX + " INT, " +
            GraphDataBaseFormat_1_0.EDGE_TABLE_TRG_VERTEX + " INT);";


    private static final String CREATE_ATTRIBUTE_TABLE = "create table " + GraphDataBaseFormat_1_0.ATTRIBUTE_TABLE + " (" +
            GraphDataBaseFormat_1_0.ATTRIBUTE_TABLE_ID + " INTEGER PRIMARY KEY, " +
            GraphDataBaseFormat_1_0.ATTRIBUTE_TABLE_OWNER_ID + " INT, " +
            GraphDataBaseFormat_1_0.ATTRIBUTE_TABLE_OWNER_TYPE + " INT, " +
            GraphDataBaseFormat_1_0.ATTRIBUTE_TABLE_NAME + " VARCHAR, " +
            GraphDataBaseFormat_1_0.ATTRIBUTE_TABLE_VALUE + " VARCHAR, " +
            GraphDataBaseFormat_1_0.ATTRIBUTE_TABLE_VALUE_TYPE + " INT, " +
            GraphDataBaseFormat_1_0.ATTRIBUTE_TABLE_VISIBLE + " BOOL);";

    private static final String CREATE_GRAPH_TABLE = "create table " + GraphDataBaseFormat_1_0.GRAPH_TABLE + " (" +
            GraphDataBaseFormat_1_0.GRAPH_TABLE_ID + " INT PRIMARY KEY, " +
            GraphDataBaseFormat_1_0.GRAPH_TABLE_NAME + " VARCHAR);";

    private static final String CREATE_VERTEX_INDEX_TABLE = "create index Idx_vertex on " + GraphDataBaseFormat_1_0.VERTEX_TABLE + "(" +
            GraphDataBaseFormat_1_0.VERTEX_TABLE_OWNER_ID + ");";

    private static final String CREATE_EDGE_INDEX_TABLE = "create index Idx_edge on " + GraphDataBaseFormat_1_0.EDGE_TABLE + "(" +
            GraphDataBaseFormat_1_0.EDGE_TABLE_SRC_VERTEX + ", " +
            GraphDataBaseFormat_1_0.EDGE_TABLE_TRG_VERTEX + ");";

    private static final String CREATE_ATTRIBUTE_INDEX_TABLE = "create index Idx_attribute on " + GraphDataBaseFormat_1_0.ATTRIBUTE_TABLE + "(" +
            GraphDataBaseFormat_1_0.ATTRIBUTE_TABLE_OWNER_ID + ", " +
            GraphDataBaseFormat_1_0.ATTRIBUTE_TABLE_OWNER_TYPE + ", " +
            GraphDataBaseFormat_1_0.ATTRIBUTE_TABLE_NAME + ");";

    // Constants: inserts
    private static final String INSERT_VERTEX = "insert into " + GraphDataBaseFormat_1_0.VERTEX_TABLE + " values(%d, %d, %d);";

    private static final String INSERT_EDGE = "insert into " + GraphDataBaseFormat_1_0.EDGE_TABLE + " values(%d, %d, %d, %d);";

    private static final String INSERT_ATTRIBUTE = "insert into " + GraphDataBaseFormat_1_0.ATTRIBUTE_TABLE + " values(%d, %d, %d, '%s','%s', %d, %d);";

    private static final String INSERT_GRAPH_MODEL = "insert into " + GraphDataBaseFormat_1_0.GRAPH_TABLE + " values(%d, '%s');";

    // Constants: selects
    private static final String SELECT_VERTEX_BY_INNER_GRAPH_ID = "SELECT * " +
            "FROM " + GraphDataBaseFormat_1_0.VERTEX_TABLE + " s1 " +
            "WHERE s1." + GraphDataBaseFormat_1_0.VERTEX_TABLE_OWNER_ID + "=%d;";

    private static final String SELECT_VERTEX_BY_OWNER_ID = "SELECT * " +
            "FROM " + GraphDataBaseFormat_1_0.VERTEX_TABLE + " s1 " +
            "WHERE s1." + GraphDataBaseFormat_1_0.VERTEX_TABLE_OWNER_ID + "=%d;";

    private static final String SELECT_EDGE_BY_GRAPH_ID = "SELECT * " +
            "FROM " + GraphDataBaseFormat_1_0.EDGE_TABLE + " s1 " +
            "WHERE s1." + GraphDataBaseFormat_1_0.EDGE_TABLE_GRAPH_ID + "=%d;";

    private static final String SELECT_EDGE_BY_VERTEX_ID = "SELECT * " +
            "FROM " + GraphDataBaseFormat_1_0.EDGE_TABLE + " s1 " +
            "WHERE s1." + GraphDataBaseFormat_1_0.EDGE_TABLE_SRC_VERTEX + "=%d or s1." + GraphDataBaseFormat_1_0.EDGE_TABLE_TRG_VERTEX + "=%d;";

    private static final String SELECT_ROOTS_BY_GRAPH_ID = "SELECT * " +
            "FROM " + GraphDataBaseFormat_1_0.VERTEX_TABLE + " s1 " +
            "WHERE s1." + GraphDataBaseFormat_1_0.VERTEX_TABLE_GRAPH_ID + "=%d and " + GraphDataBaseFormat_1_0.VERTEX_TABLE_OWNER_ID + "=" + VertexRecord.NO_OWNER_ID + ";";

    private static final String SELECT_GRAPH_MODEL_BY_ID = "SELECT * " +
            "FROM " + GraphDataBaseFormat_1_0.GRAPH_TABLE+" s1 " +
            "WHERE s1." + GraphDataBaseFormat_1_0.GRAPH_TABLE_ID + "=%d;";

    private static final String SELECT_GRAPH_MODELS = " SELECT * " +
            " FROM " + GraphDataBaseFormat_1_0.GRAPH_TABLE + " s1;";

    private static final String SELECT_VERTEX_BY_ID = "SELECT * " +
            "FROM " + GraphDataBaseFormat_1_0.VERTEX_TABLE + " s1 " +
            "WHERE s1." + GraphDataBaseFormat_1_0.VERTEX_TABLE_ID + "=%d;";

    private static final String SELECT_EDGE_BY_ID = "SELECT * " +
            "FROM " + GraphDataBaseFormat_1_0.EDGE_TABLE + " s1 " +
            "WHERE s1." + GraphDataBaseFormat_1_0.EDGE_TABLE_ID + "=%d;";

    private static final String SELECT_ATTRIBUTE_BY_ID = " SELECT * " +
            " FROM " + GraphDataBaseFormat_1_0.ATTRIBUTE_TABLE + " s1 " +
            " WHERE s1." + GraphDataBaseFormat_1_0.ATTRIBUTE_TABLE_ID + "=%d;";

    private static final String SELECT_ATTRIBUTES_BY_OWNER = " SELECT * " +
            " FROM " + GraphDataBaseFormat_1_0.ATTRIBUTE_TABLE + " s1 " +
            " WHERE s1." + GraphDataBaseFormat_1_0.ATTRIBUTE_TABLE_OWNER_ID + "=%d and s1." + GraphDataBaseFormat_1_0.ATTRIBUTE_TABLE_OWNER_TYPE + "=%d;";

    // Constants: updates
    private static final String UPDATE_GRAPH_HEADER_BY_ID = "UPDATE " + GraphDataBaseFormat_1_0.GRAPH_TABLE +
            " SET " + GraphDataBaseFormat_1_0.GRAPH_TABLE_NAME + "='%s' " +
            " WHERE " + GraphDataBaseFormat_1_0.GRAPH_TABLE_ID + "=%d;";

    private static final String UPDATE_VERTEX_HEADER_BY_ID = "UPDATE " + GraphDataBaseFormat_1_0.VERTEX_TABLE +
						 " SET " + GraphDataBaseFormat_1_0.VERTEX_TABLE_GRAPH_ID + "=%d, " +
								   GraphDataBaseFormat_1_0.VERTEX_TABLE_OWNER_ID + "=%d "+
						 " WHERE " + GraphDataBaseFormat_1_0.VERTEX_TABLE_ID + "=%d;";

    private static final String UPDATE_EDGE_HEADER_BY_ID = "UPDATE " + GraphDataBaseFormat_1_0.EDGE_TABLE +
            " SET " + GraphDataBaseFormat_1_0.EDGE_TABLE_SRC_VERTEX + "=%d, " +
            GraphDataBaseFormat_1_0.EDGE_TABLE_TRG_VERTEX + "=%d "+
            " WHERE " + GraphDataBaseFormat_1_0.EDGE_TABLE_ID + "=%d;";

    private static final String UPDATE_ATTRIBUTE_HEADER_BY_ID = "UPDATE " + GraphDataBaseFormat_1_0.ATTRIBUTE_TABLE +
            " SET " + GraphDataBaseFormat_1_0.ATTRIBUTE_TABLE_VALUE + "='%s'," +
                      GraphDataBaseFormat_1_0.ATTRIBUTE_TABLE_VALUE_TYPE + "=%d " +
            " WHERE " + GraphDataBaseFormat_1_0.ATTRIBUTE_TABLE_ID + "=%d;";


    // Main data
    private Connection connection = null;

	public SQLiteDataBase_1_0() {
        try {
            // load the sqlite-JDBC driver using the current class loader
            Class.forName("org.sqlite.JDBC");

            String dataBaseDirectoryName = FileUtils.getWorkingDir() + "data" + File.separator + "db" + File.separator;
            File dataBaseDirectory = new File(dataBaseDirectoryName);
            if (!dataBaseDirectory.exists() && !dataBaseDirectory.mkdirs()) {
                throw new RuntimeException("Couldn't create directory for database");
            }

            // create connection
            connection = DriverManager.getConnection("jdbc:sqlite:" + dataBaseDirectoryName + "database_" + DateUtils.getStartAppTimeInDefaultFormat() + "_" + RandomUtils.generateRandomUUID() + ".db");
            connection.setAutoCommit(false);

            // create tables
            Statement statement = connection.createStatement();
            statement.setQueryTimeout(30);

            statement.execute(CREATE_VERTEX_TABLE);
            statement.execute(CREATE_EDGE_TABLE);
            statement.execute(CREATE_ATTRIBUTE_TABLE);
            statement.execute(CREATE_GRAPH_TABLE);
            statement.execute(CREATE_VERTEX_INDEX_TABLE);
            statement.execute(CREATE_EDGE_INDEX_TABLE);
            statement.execute(CREATE_ATTRIBUTE_INDEX_TABLE);
            connection.commit();
        } catch (ClassNotFoundException | SQLException ex) {
            throw new RuntimeException(ex);
        }
	}
	
	public void close() {
		try {
            connection.close();
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }
	}
	
	public void addGraphModelHeaders(Collection<GraphRecord> headers) {
        Validate.notNull(headers);

        try (Statement statement = connection.createStatement()){
            for (GraphRecord graphRecord : headers) {
                statement.execute(String.format(INSERT_GRAPH_MODEL, graphRecord.getId(), graphRecord.getName()));
            }
            connection.commit();
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }
	}
	
	public void addVertexHeaders(Collection<VertexRecord> headers) {
        Validate.notNull(headers);

        try (Statement statement = connection.createStatement()){
            for (VertexRecord vertexRecord : headers) {
                statement.execute(String.format(INSERT_VERTEX, vertexRecord.getId(), vertexRecord.getGraphId(), vertexRecord.getOwnerId()));
            }
            connection.commit();
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }
	}
	
	public void addEdgeHeaders(Collection<EdgeRecord> headers) {
        Validate.notNull(headers);

        try (Statement statement = connection.createStatement()){
            for (EdgeRecord edgeRecord : headers) {
                statement.execute(String.format(INSERT_EDGE, edgeRecord.getId(), edgeRecord.getGraphId(), edgeRecord.getSourceId(), edgeRecord.getTargetId()));
            }
            connection.commit();
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }
	}
	
	public void addAttributeHeaders(Collection<AttributeRecord> headers) {
        Validate.notNull(headers);

        try (Statement statement = connection.createStatement()){
            for (AttributeRecord attributeRecord : headers) {
                statement.execute(String.format(INSERT_ATTRIBUTE,
                        attributeRecord.getId(),
                        attributeRecord.getOwnerId(),
                        attributeRecord.getOwnerType(),
                        attributeRecord.getName(),
                        attributeRecord.getStringValue(),
                        attributeRecord.isVisible() ? 1 : 0,
                        attributeRecord.getType()));
            }
            connection.commit();
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }
	}

//==============================================================================
//------------------MODIFYING METHODS-------------------------------------------
	public void modifyGraphModelHeader(GraphRecord newGraphRecord) {
		if (newGraphRecord == null) return;

        try (Statement statement = connection.createStatement()) {
            statement.executeUpdate(String.format(UPDATE_GRAPH_HEADER_BY_ID, newGraphRecord.getName(), newGraphRecord.getId()));
            connection.commit();
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }
	}
	
	public void modifyVertexHeader(VertexRecord newVertexRecord) {
        if (newVertexRecord == null) return;

        try (Statement statement = connection.createStatement()) {
            statement.executeUpdate(String.format(UPDATE_VERTEX_HEADER_BY_ID,
                    newVertexRecord.getGraphId(),
                    newVertexRecord.getOwnerId(),
                    newVertexRecord.getId()));
            connection.commit();
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }
    }

    public void modifyEdgeHeader(EdgeRecord newEdgeRecord) {
        if (newEdgeRecord == null) return;

        try (Statement statement = connection.createStatement()) {
            statement.executeUpdate(String.format(UPDATE_EDGE_HEADER_BY_ID,
                    newEdgeRecord.getSourceId(),
                    newEdgeRecord.getTargetId(),
                    newEdgeRecord.getId()));
            connection.commit();
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }
    }
	
	public void modifyAttributeHeader(AttributeRecord newAttributeRecord) {
        if (newAttributeRecord == null) return;

        try (Statement statement = connection.createStatement()) {
            statement.executeUpdate(String.format(UPDATE_ATTRIBUTE_HEADER_BY_ID,
                    newAttributeRecord.getStringValue(),
                    newAttributeRecord.getType(),
                    newAttributeRecord.getId()));
            connection.commit();
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }
	}
	
//==============================================================================
//------------------GET DATA----------------------------------------------------
    public List<GraphRecord> getGraphModelHeaders(int count) {
        List<GraphRecord> result = Lists.newArrayList();

        try (Statement statement = connection.createStatement()){
            ResultSet resultSet = statement.executeQuery(String.format(SELECT_GRAPH_MODELS));
            while(resultSet.next()) {
                result.add(new GraphRecord(resultSet.getInt(GraphDataBaseFormat_1_0.GRAPH_TABLE_ID), resultSet.getString(GraphDataBaseFormat_1_0.GRAPH_TABLE_NAME)));
            }
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }

        return result;
    }


    public GraphRecord getGraphModelHeader(int graphModelId) {
        try (Statement statement = connection.createStatement()){
            ResultSet resultSet = statement.executeQuery(String.format(SELECT_GRAPH_MODEL_BY_ID, graphModelId));
            if (resultSet.next()) {
                return new GraphRecord(resultSet.getInt(GraphDataBaseFormat_1_0.GRAPH_TABLE_ID), resultSet.getString(GraphDataBaseFormat_1_0.GRAPH_TABLE_NAME));
            }
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }

        return null;
	}
	
	public VertexRecord getVertexRecord(int vertexId) {
        try (Statement statement = connection.createStatement()){
            ResultSet resultSet = statement.executeQuery(String.format(SELECT_VERTEX_BY_ID, vertexId));
            if (resultSet.next()) {
                return new VertexRecord(resultSet.getInt(GraphDataBaseFormat_1_0.VERTEX_TABLE_ID),
                        resultSet.getInt(GraphDataBaseFormat_1_0.VERTEX_TABLE_GRAPH_ID),
                        resultSet.getInt(GraphDataBaseFormat_1_0.VERTEX_TABLE_OWNER_ID));
            }
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }

        return null;
	}

    public VertexRecord getVertexHeaderByInnerGraphId(int graphId) {
        try (Statement statement = connection.createStatement()){
            ResultSet resultSet = statement.executeQuery(String.format(SELECT_VERTEX_BY_INNER_GRAPH_ID, graphId));
            if (resultSet.next()) {
                return new VertexRecord(resultSet.getInt(GraphDataBaseFormat_1_0.VERTEX_TABLE_ID),
                        resultSet.getInt(GraphDataBaseFormat_1_0.VERTEX_TABLE_GRAPH_ID),
                        resultSet.getInt(GraphDataBaseFormat_1_0.VERTEX_TABLE_OWNER_ID));
            }
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }

        return null;
    }

    public List<VertexRecord> getVertexRecordsByOwnerId(int ownerId) {
        List<VertexRecord> result = Lists.newArrayList();
        try (Statement statement = connection.createStatement()){
            ResultSet resultSet = statement.executeQuery(String.format(SELECT_VERTEX_BY_OWNER_ID, ownerId));
            while (resultSet.next()) {
                result.add(new VertexRecord(resultSet.getInt(GraphDataBaseFormat_1_0.VERTEX_TABLE_ID),
                        resultSet.getInt(GraphDataBaseFormat_1_0.VERTEX_TABLE_GRAPH_ID),
                        resultSet.getInt(GraphDataBaseFormat_1_0.VERTEX_TABLE_OWNER_ID)));
            }
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }

        return result;
    }

    public List<EdgeRecord> getEdgeRecordsByGraphId(int graphId) {
        List<EdgeRecord> result = Lists.newArrayList();
        try (Statement statement = connection.createStatement()){
            ResultSet resultSet = statement.executeQuery(String.format(SELECT_EDGE_BY_GRAPH_ID, graphId));
            while (resultSet.next()) {
                result.add(new EdgeRecord(resultSet.getInt(GraphDataBaseFormat_1_0.EDGE_TABLE_ID),
                        resultSet.getInt(GraphDataBaseFormat_1_0.EDGE_TABLE_GRAPH_ID),
                        resultSet.getInt(GraphDataBaseFormat_1_0.EDGE_TABLE_SRC_VERTEX),
                        resultSet.getInt(GraphDataBaseFormat_1_0.EDGE_TABLE_TRG_VERTEX)));
            }
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }

        return result;
    }

    public List<EdgeRecord> getEdgeHeadersByVertexId(int vertexId) {
        List<EdgeRecord> result = Lists.newArrayList();
        try (Statement statement = connection.createStatement()){
            ResultSet resultSet = statement.executeQuery(String.format(SELECT_EDGE_BY_VERTEX_ID, vertexId, vertexId));
            while (resultSet.next()) {
                result.add(new EdgeRecord(resultSet.getInt(GraphDataBaseFormat_1_0.EDGE_TABLE_ID),
                        resultSet.getInt(GraphDataBaseFormat_1_0.EDGE_TABLE_GRAPH_ID),
                        resultSet.getInt(GraphDataBaseFormat_1_0.EDGE_TABLE_SRC_VERTEX),
                        resultSet.getInt(GraphDataBaseFormat_1_0.EDGE_TABLE_TRG_VERTEX)));
            }
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }

        return result;
    }

    public List<VertexRecord> getRoots(int graphId) {
        List<VertexRecord> result = Lists.newArrayList();
        try (Statement statement = connection.createStatement()){
            ResultSet resultSet = statement.executeQuery(String.format(SELECT_ROOTS_BY_GRAPH_ID, graphId));
            while (resultSet.next()) {
                result.add(new VertexRecord(resultSet.getInt(GraphDataBaseFormat_1_0.VERTEX_TABLE_ID),
                        resultSet.getInt(GraphDataBaseFormat_1_0.GRAPH_TABLE_ID),
                        resultSet.getInt(GraphDataBaseFormat_1_0.VERTEX_TABLE_OWNER_ID)));
            }
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }

        return result;
    }
	
	public EdgeRecord getEdgeHeader(int edgeId) {
        try (Statement statement = connection.createStatement()){
            ResultSet resultSet = statement.executeQuery(String.format(SELECT_EDGE_BY_ID, edgeId));
            if (resultSet.next()) {
                return new EdgeRecord(resultSet.getInt(GraphDataBaseFormat_1_0.EDGE_TABLE_ID),
                        resultSet.getInt(GraphDataBaseFormat_1_0.EDGE_TABLE_GRAPH_ID),
                        resultSet.getInt(GraphDataBaseFormat_1_0.EDGE_TABLE_SRC_VERTEX),
                        resultSet.getInt(GraphDataBaseFormat_1_0.EDGE_TABLE_TRG_VERTEX));
            }
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }

        return null;
	}
	
	public AttributeRecord getAttributeRecord(int attributeId) {
        try (Statement statement = connection.createStatement()){
            ResultSet resultSet = statement.executeQuery(String.format(SELECT_ATTRIBUTE_BY_ID, attributeId));
            if (resultSet.next()) {
                return new AttributeRecord(resultSet.getInt(GraphDataBaseFormat_1_0.ATTRIBUTE_TABLE_ID),
                        resultSet.getInt(GraphDataBaseFormat_1_0.ATTRIBUTE_TABLE_OWNER_ID),
                        resultSet.getInt(GraphDataBaseFormat_1_0.ATTRIBUTE_TABLE_OWNER_TYPE),
                        resultSet.getString(GraphDataBaseFormat_1_0.ATTRIBUTE_TABLE_NAME),
                        resultSet.getString(GraphDataBaseFormat_1_0.ATTRIBUTE_TABLE_VALUE),
                        resultSet.getInt(GraphDataBaseFormat_1_0.ATTRIBUTE_TABLE_VALUE_TYPE),
                        resultSet.getBoolean(GraphDataBaseFormat_1_0.ATTRIBUTE_TABLE_VISIBLE));
            }
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }

        return null;
	}

    public List<AttributeRecord> getAttributeRecordsByOwner(int ownerId, int ownerType) {
        List<AttributeRecord> result = Lists.newArrayList();

        try (Statement statement = connection.createStatement()){
            ResultSet resultSet = statement.executeQuery(String.format(SELECT_ATTRIBUTES_BY_OWNER, ownerId, ownerType));
            while (resultSet.next()) {
                result.add(new AttributeRecord(resultSet.getInt(GraphDataBaseFormat_1_0.ATTRIBUTE_TABLE_ID),
                        resultSet.getInt(GraphDataBaseFormat_1_0.ATTRIBUTE_TABLE_OWNER_ID),
                        resultSet.getInt(GraphDataBaseFormat_1_0.ATTRIBUTE_TABLE_OWNER_TYPE),
                        resultSet.getString(GraphDataBaseFormat_1_0.ATTRIBUTE_TABLE_NAME),
                        resultSet.getString(GraphDataBaseFormat_1_0.ATTRIBUTE_TABLE_VALUE),
                        resultSet.getInt(GraphDataBaseFormat_1_0.ATTRIBUTE_TABLE_VALUE_TYPE),
                        resultSet.getBoolean(GraphDataBaseFormat_1_0.ATTRIBUTE_TABLE_VISIBLE)));
            }
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }

        return result;
    }
}
