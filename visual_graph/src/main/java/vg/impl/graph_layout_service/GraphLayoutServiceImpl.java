package vg.impl.graph_layout_service;

import com.google.common.collect.Lists;
import org.apache.commons.lang.Validate;
import vg.interfaces.graph_layout_service.GraphLayoutFactory;
import vg.interfaces.graph_layout_service.GraphLayoutService;

import java.util.List;

public class GraphLayoutServiceImpl implements GraphLayoutService {
    // Main data
	private List<GraphLayoutFactory> layoutFactories = Lists.newArrayList();
	
	// Mutex
	private final Object generalMutex = new Object();

    @Override
    public void registerGraphLayoutFactory(GraphLayoutFactory graphLayoutFactory) {
        Validate.notNull(graphLayoutFactory);

        synchronized (generalMutex) {
            layoutFactories.add(graphLayoutFactory);
        }
    }

    @Override
    public List<GraphLayoutFactory> getInstalledLayoutFactories() {
        synchronized (generalMutex) {
            return Lists.newArrayList(layoutFactories);
        }
    }

    @Override
    public GraphLayoutFactory getLayoutFactoryByName(String layoutName) {
        synchronized (generalMutex) {
            for (GraphLayoutFactory graphLayoutFactory : layoutFactories) {
                if (graphLayoutFactory.getLayoutName().equals(layoutName))
                    return graphLayoutFactory;
            }
        }
        return null;
    }
}
