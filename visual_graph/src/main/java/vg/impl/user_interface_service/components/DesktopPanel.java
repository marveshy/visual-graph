package vg.impl.user_interface_service.components;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import org.apache.commons.lang.Validate;
import vg.impl.main_service.VGMainServiceHelper;
import vg.impl.user_interface_service.UserInterfaceServiceImplCallback;
import vg.interfaces.user_interface_service.UserInterfaceService;
import vg.interfaces.user_interface_service.UserInterfaceTab;
import vg.shared.user_interface.UserInterfacePanelUtils;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.*;
import java.util.AbstractMap;
import java.util.Map;

/**
 * @author Timur Zolotuhin (tzolotuhin@parallels.com)
 */
public class DesktopPanel {
    // Main components
    private final JPanel outView, innerView;
    private JTabbedPane tabs;

    // Main data
    private UserInterfaceServiceImplCallback userInterfaceServiceImplCallback;
    private BiMap<UserInterfaceTab, JComponent> userInterfaceTabs = HashBiMap.create();

    // Mutex
    private final Object generalMutex = new Object();

    public DesktopPanel(UserInterfaceServiceImplCallback callback) {
        this.userInterfaceServiceImplCallback = callback;

        outView = new JPanel(new GridLayout(1, 1));
        innerView = new JPanel(new GridBagLayout());
        outView.add(innerView);

        tabs = new JTabbedPane(JTabbedPane.TOP, JTabbedPane.SCROLL_TAB_LAYOUT);

        // add listeners
        tabs.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                synchronized (generalMutex) {
                    doChangeTab();
                }
            }
        });

        // ctrl-tab and ctrl-shift-tab
        KeyboardFocusManager.getCurrentKeyboardFocusManager().addKeyEventDispatcher(new KeyEventDispatcher() {
            public boolean dispatchKeyEvent(KeyEvent e) {
                if (e.getID() == KeyEvent.KEY_PRESSED &&  e.getKeyCode() == KeyEvent.VK_TAB && e.isControlDown()) {
                    synchronized (generalMutex) {
                        int tabCount = tabs.getTabCount();
                        if (tabCount > 0) {
                            int sel = tabs.getSelectedIndex();
                            if (e.isShiftDown()) {
                                sel = sel - 1;
                                if (sel < 0)
                                    sel = tabCount - 1;
                            } else {
                                sel = sel + 1;
                                if (sel >= tabCount)
                                    sel = 0;
                            }
                            tabs.setSelectedIndex(sel);
                        }
                    }
                    return true;
                }
                return false;
            }
        });

        rebuildView();
    }

    public JComponent getView() {
        return outView;
    }

    public void addTab(final UserInterfaceTab userInterfaceTab, final UserInterfaceService.FinishActionCallBack callBack) {
        new SwingWorker<Map.Entry<String, JComponent>, Void>() {
            @Override
            protected Map.Entry<String, JComponent> doInBackground() throws Exception {
                VGMainServiceHelper.logger.printDebug("Preparing to add new tab");
                try {
                    String title = userInterfaceTab.getTabTitle();
                    JComponent view = userInterfaceTab.getView();
                    VGMainServiceHelper.logger.printDebug("Finish preparing to add new tab");
                    return new AbstractMap.SimpleEntry<>(title, view);
                } catch (Throwable ex) {
                    VGMainServiceHelper.logger.printException(ex);
                    throw ex;
                }
            }

            @Override
            protected void done() {
                try {
                    VGMainServiceHelper.logger.printDebug("Add new tab");
                    String title = get().getKey();
                    JComponent view = get().getValue();
                    synchronized (generalMutex) {
                        doAddTab(userInterfaceTab, title, view, -1);
                        rebuildView();
                    }
                    VGMainServiceHelper.logger.printDebug("Finish adding new tab");
                    if (callBack != null) {
                        callBack.onFinishAction();
                    }
                } catch (Throwable ex) {
                    VGMainServiceHelper.logger.printException(ex);
                }
            }
        }.execute();
    }

    public void replaceTab(final UserInterfaceTab oldTab, final UserInterfaceTab newTab, final UserInterfaceService.FinishActionCallBack callBack) {
        new SwingWorker<Map.Entry<String, JComponent>, Void>() {
            @Override
            protected Map.Entry<String, JComponent> doInBackground() throws Exception {
                try {
                    String title = newTab.getTabTitle();
                    JComponent view = newTab.getView();
                    return new AbstractMap.SimpleEntry<>(title, view);
                } catch (Throwable ex) {
                    VGMainServiceHelper.logger.printException(ex);
                    throw ex;
                }
            }

            @Override
            protected void done() {
                try {
                    String title = get().getKey();
                    JComponent view = get().getValue();
                    synchronized (generalMutex) {
                        int oldIndex = -1;
                        for (int index = 0; index < tabs.getTabCount(); index++) {
                            if (tabs.getTabComponentAt(index) == oldTab.getView()) {
                                oldIndex = index;
                            }
                        }

                        doRemoveTab(oldTab);
                        doAddTab(newTab, title, view, oldIndex);
                        rebuildView();
                    }
                    if (callBack != null) {
                        callBack.onFinishAction();
                    }
                } catch (Throwable ex) {
                    VGMainServiceHelper.logger.printException(ex);
                }
            }
        }.execute();
    }

    public void selectTab(UserInterfaceTab userInterfaceTab) {
        synchronized (generalMutex) {
            JComponent tab = userInterfaceTabs.get(userInterfaceTab);
            if (tab != null) {
                tabs.setSelectedComponent(tab);
                rebuildView();
            }
        }
    }

    public void removeTab(UserInterfaceTab userInterfaceTab) {
        synchronized (generalMutex) {
            doRemoveTab(userInterfaceTab);
            rebuildView();
        }
    }

    public void refresh() {
        synchronized (generalMutex) {
            SwingUtilities.updateComponentTreeUI(tabs);
            SwingUtilities.updateComponentTreeUI(innerView);
            SwingUtilities.updateComponentTreeUI(outView);
        }
    }

//==============================================================================
//---------------PRIVATE METHODS------------------------------------------------
    private void rebuildView() {
        //clear panel
        innerView.removeAll();

        innerView.add(tabs, new GridBagConstraints(0,0, 1,1, 1,1, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0,0,0,0), 0,0));
        UserInterfacePanelUtils.createBorder(innerView);

        // update ui
        innerView.updateUI();
    }

    private void doAddTab(final UserInterfaceTab userInterfaceTab, String title, JComponent view, int index) {
        Validate.notNull(userInterfaceTab);

        userInterfaceTabs.put(userInterfaceTab, view);
        tabs.addTab(title, view);
        if (index < 0)
            index = tabs.getTabCount() - 1;

        // add close button for new tab
        tabs.setTabComponentAt(index, new SimpleTabWithCloseButton(title, new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                removeTab(userInterfaceTab);
            }
        }));
        tabs.setSelectedIndex(index);
        userInterfaceServiceImplCallback.notifyOnOpenTab(userInterfaceTab);
    }

    private void doRemoveTab(UserInterfaceTab userInterfaceTab) {
        tabs.remove(userInterfaceTabs.get(userInterfaceTab));
        userInterfaceTabs.remove(userInterfaceTab);
        userInterfaceServiceImplCallback.notifyOnCloseTab(userInterfaceTab);
	}

    public void doChangeTab() {
        int index = tabs.getSelectedIndex();
        if(index >= 0 && index < tabs.getTabCount()) {
            JComponent view = (JComponent)tabs.getComponentAt(index);
            UserInterfaceTab userInterfaceTab = userInterfaceTabs.inverse().get(view);
            userInterfaceServiceImplCallback.notifyOnChangeDesktopTab(userInterfaceTab);
        } else {
            //if all tabs close
            userInterfaceServiceImplCallback.notifyOnChangeDesktopTab(null);
        }
    }

//==============================================================================
//------------------PRIVATE CLASSES---------------------------------------------
    private static class SimpleTabWithCloseButton extends JPanel {
        // Icons
        private static final ImageIcon closeActiveImageIcon;
        private static final ImageIcon closeImageIcon;

        static {
            closeActiveImageIcon = new ImageIcon("./data/resources/textures/closeActive.png");
            closeImageIcon = new ImageIcon("./data/resources/textures/close.png");
        }

        public SimpleTabWithCloseButton(String title, final ActionListener closeAction) {
            super(new GridBagLayout());

            setOpaque(false);

            final JLabel closeButton = new JLabel(closeImageIcon);
            closeButton.setPreferredSize(new Dimension(20, 20));
            JLabel textLabel = new JLabel(title);
            textLabel.setPreferredSize(new Dimension(title.length() * 5 + 25, 20));
            textLabel.setHorizontalAlignment(JLabel.CENTER);

            // pack ui
            add(textLabel, new GridBagConstraints(0, 0, 1, 1, 1, 1, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            add(closeButton, new GridBagConstraints(1, 0, 1, 1, 0, 0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));

            closeButton.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    closeAction.actionPerformed(new ActionEvent(SimpleTabWithCloseButton.this, e.getID(), null));
                }

                @Override
                public void mouseEntered(MouseEvent e) {
                    closeButton.setIcon(closeActiveImageIcon);
                }

                @Override
                public void mouseExited(MouseEvent e) {
                    closeButton.setIcon(closeImageIcon);
                }
            });
        }
    }
}
