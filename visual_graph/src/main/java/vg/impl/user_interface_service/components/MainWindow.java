package vg.impl.user_interface_service.components;

import org.apache.commons.lang.Validate;
import vg.shared.gui.StorageFrame;
import vg.impl.main_service.VGMainServiceHelper;
import vg.impl.user_interface_service.UserInterfaceServiceImplCallback;
import vg.interfaces.main_service.VGMainGlobals;
import vg.interfaces.user_interface_service.*;
import vg.services.user_interface_manager.additional_swing_components.SimpleStatusBar;

import javax.swing.*;
import javax.swing.Timer;
import java.awt.*;
import java.awt.event.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

/**
 * @author Timur Zolotuhin (tzolotuhin@parallels.com)
 */
public class MainWindow extends StorageFrame {
    // Constants
    private static final int DEFAULT_UI_SERVICE_MAIN_WINDOW_WEST_CENTER_DIVIDER_LOCATION = 150;
    private static final int DEFAULT_UI_SERVICE_MAIN_WINDOW_CENTER_SOUTH_DIVIDER_LOCATION = 150;
    private static final int DEFAULT_UI_SERVICE_MAIN_WINDOW_WEST_SOUTH_EAST_SOUTH_DIVIDER_LOCATION = 150;

    // Main Components
    private JPanel outView, innerView;

    // Main Components: menu
    private JMenu fileMenu, editMenu, analyzeMenu, windowMenu, otherMenu, helpMenu;
    private JMenuItem quitMenuItem;
    private final JMenuBar menuBar;

    // Main Components: desktop and panels
    //private JSplitPane splitFirst, splitSecond, splitThird;
    private final InstrumentPanel northInstrumentPanel;
    private final InstrumentPanel southInstrumentPanel;
    private final InstrumentPanel westInstrumentPanel;
    private final InstrumentPanel eastInstrumentPanel;
    private final DesktopPanel desktopPanel;
    private final UserInterfacePanelSet westTopPanel, westSouthPanel, eastSouthPanel;

    // Main Components: status bar
    private JLabel progressName;
    private final SimpleStatusBar statusBar;

    private JProgressBar progressBar;

    // Main data
    private int westAndCenterDividerLocation;
    private int centerAndSouthDividerLocation;
    private int westSouthAndEastSouthDividerLocation;

    private UserInterfaceServiceImplCallback callback;

    public MainWindow(final UserInterfaceServiceImplCallback callback) {
        super(VGMainGlobals.UI_SERVICE_MAIN_WINDOW_POSITION_X_KEY,
                VGMainGlobals.UI_SERVICE_MAIN_WINDOW_POSITION_Y_KEY,
                VGMainGlobals.UI_SERVICE_MAIN_WINDOW_WIDTH_KEY,
                VGMainGlobals.UI_SERVICE_MAIN_WINDOW_HEIGHT_KEY);

        this.callback = callback;

        westAndCenterDividerLocation = VGMainServiceHelper.config.getIntegerProperty(VGMainGlobals.UI_SERVICE_MAIN_WINDOW_WEST_CENTER_DIVIDER_LOCATION_KEY, DEFAULT_UI_SERVICE_MAIN_WINDOW_WEST_CENTER_DIVIDER_LOCATION);
        centerAndSouthDividerLocation = VGMainServiceHelper.config.getIntegerProperty(VGMainGlobals.UI_SERVICE_MAIN_WINDOW_SOUTH_CENTER_DIVIDER_LOCATION_KEY, DEFAULT_UI_SERVICE_MAIN_WINDOW_CENTER_SOUTH_DIVIDER_LOCATION);
        westSouthAndEastSouthDividerLocation = VGMainServiceHelper.config.getIntegerProperty(VGMainGlobals.UI_SERVICE_MAIN_WINDOW_WEST_SOUTH__EAST_SOUTH_DIVIDER_LOCATION_KEY, DEFAULT_UI_SERVICE_MAIN_WINDOW_WEST_SOUTH_EAST_SOUTH_DIVIDER_LOCATION);

        frame.setTitle("Visual Graph");
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                quit();
            }
        });

        outView = new JPanel(new GridLayout(1, 1));
        innerView = new JPanel(new GridBagLayout());
        outView.add(innerView);
        frame.add(outView);

        // creating menu items
        menuBar = new JMenuBar();
        frame.setJMenuBar(menuBar);

        fileMenu = new JMenu("File");
        editMenu = new JMenu("Edit");
        analyzeMenu = new JMenu("Analyze");
        windowMenu = new JMenu("Window");
        otherMenu = new JMenu("Other");
        helpMenu = new JMenu("Help");

        quitMenuItem  = new JMenuItem("Quit");

        quitMenuItem.setIcon(new ImageIcon("./data/resources/textures/quit.png"));

        // creating components
        desktopPanel = new DesktopPanel(callback);

        northInstrumentPanel = new InstrumentPanel(InstrumentPanel.NORTH_ORIENTATION);
        southInstrumentPanel = new InstrumentPanel(InstrumentPanel.SOUTH_ORIENTATION);
        westInstrumentPanel = new InstrumentPanel(InstrumentPanel.WEST_ORIENTATION);
        eastInstrumentPanel = new InstrumentPanel(InstrumentPanel.EAST_ORIENTATION);

        westTopPanel = new UserInterfacePanelSet(new UserInterfacePanelSet.UserInterfacePanelSetCallback() {
            @Override
            public void onAddPanel(UserInterfacePanel panel) {
                callback.notifyOnAddPanel(panel, UserInterfaceService.WEST_PANEL);
            }

            @Override
            public void onRemovePanel(UserInterfacePanel panel) {
                callback.notifyOnRemovePanel(panel, UserInterfaceService.WEST_PANEL);
            }
        });
        westSouthPanel = new UserInterfacePanelSet(new UserInterfacePanelSet.UserInterfacePanelSetCallback() {
            @Override
            public void onAddPanel(UserInterfacePanel panel) {
                callback.notifyOnAddPanel(panel, UserInterfaceService.WEST_SOUTH_PANEL);
            }

            @Override
            public void onRemovePanel(UserInterfacePanel panel) {
                callback.notifyOnRemovePanel(panel, UserInterfaceService.WEST_SOUTH_PANEL);
            }
        });
        eastSouthPanel = new UserInterfacePanelSet(new UserInterfacePanelSet.UserInterfacePanelSetCallback() {
            @Override
            public void onAddPanel(UserInterfacePanel panel) {
                callback.notifyOnAddPanel(panel, UserInterfaceService.EAST_SOUTH_PANEL);
            }

            @Override
            public void onRemovePanel(UserInterfacePanel panel) {
                callback.notifyOnRemovePanel(panel, UserInterfaceService.EAST_SOUTH_PANEL);
            }
        });

        // creating status bar components
        this.statusBar = new SimpleStatusBar();
        this.progressBar = new JProgressBar(0,100);
        this.progressBar.setMaximumSize(new Dimension(100,this.statusBar.getPreferredSize().height-2));
        this.progressName = new JLabel();
        this.statusBar.add(progressName);
        this.statusBar.add(progressBar);
        Timer progressTimer = new Timer(500, new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                SwingUtilities.invokeLater(new Runnable() {
                    public void run() {
                        progressName.setText(VGMainServiceHelper.progressManager.getTaskName());
                        progressBar.setValue((int) VGMainServiceHelper.progressManager.updateProgress());
                        boolean visible = VGMainServiceHelper.progressManager.getTaskCount() > 0;
                        progressName.setVisible(visible);
                        progressBar.setVisible(visible);
                    }
                });
            }
        });
        progressTimer.start();

        // adding of menu bar
        fileMenu.add(quitMenuItem);

        menuBar.add(fileMenu);
        menuBar.add(editMenu);
        menuBar.add(analyzeMenu);
        menuBar.add(windowMenu);
        menuBar.add(helpMenu);

        // adding mnemonic for menus
        fileMenu.setMnemonic('f');
        editMenu.setMnemonic('e');
        helpMenu.setMnemonic('h');
        quitMenuItem.setMnemonic('q');
        quitMenuItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                quit();
            }
        });
        quitMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F4, KeyEvent.ALT_DOWN_MASK));

        rebuildView();
    }

    public void addMenuItem(JMenuItem item, String place) {
        Validate.notNull(item);
        Validate.notNull(place);

        VGMainServiceHelper.logger.printDebug("Add menu item  = " + item.getText() + ", place = " + place);
        switch (place) {
            case UserInterfaceService.FILE_MENU:
                fileMenu.remove(quitMenuItem);
                fileMenu.add(item);
                fileMenu.add(quitMenuItem);
                break;
            case UserInterfaceService.ANALYZE_MENU:
                analyzeMenu.add(item);
                break;
            case UserInterfaceService.EDIT_MENU:
                editMenu.add(item);
                break;
            case UserInterfaceService.WINDOW_MENU:
                windowMenu.add(item);
                break;
            case UserInterfaceService.HELP_MENU:
                helpMenu.add(item);
                break;
            default:
                otherMenu.add(item);
                VGMainServiceHelper.logger.printError("Place is not found, place: " + place);
                break;
        }
        rebuildView();
    }

    public void setPanel(final UserInterfacePanel element, final int place) {
        switch (place) {
            case UserInterfaceService.WEST_PANEL: {
                westTopPanel.setPanel(element);
                break;
            }
            case UserInterfaceService.WEST_SOUTH_PANEL: {
                westSouthPanel.setPanel(element);
                break;
            }
            case UserInterfaceService.EAST_SOUTH_PANEL: {
                eastSouthPanel.setPanel(element);
                break;
            }
            default: {
                throw new RuntimeException("Can't find place");
            }
        }
        rebuildView();
    }

    public void addTab(UserInterfaceTab tab, UserInterfaceService.FinishActionCallBack callBack) {
        desktopPanel.addTab(tab, callBack);
    }

    public void replaceTab(UserInterfaceTab oldTab, UserInterfaceTab newTab, UserInterfaceService.FinishActionCallBack callBack) {
        desktopPanel.replaceTab(oldTab, newTab, callBack);
    }

    public void selectTab(UserInterfaceTab tab) {
        desktopPanel.selectTab(tab);
    }

    public void addInstrument(UserInterfaceInstrument instrument, int place) {
        switch (place) {
            case UserInterfaceService.NORTH_INSTRUMENT_PANEL:
                northInstrumentPanel.addInstrument(instrument);
                break;
            case UserInterfaceService.EAST_INSTRUMENT_PANEL:
                eastInstrumentPanel.addInstrument(instrument);
                break;
            case UserInterfaceService.WEST_INSTRUMENT_PANEL:
                westInstrumentPanel.addInstrument(instrument);
                break;
            case UserInterfaceService.SOUTH_INSTRUMENT_PANEL:
                southInstrumentPanel.addInstrument(instrument);
                break;
        }
    }

    public JFrame getMainFrame() {
        return frame;
    }

    public void quit() {
        callback.notifyOnQuit();

        // stop services
        VGMainServiceHelper.graphDataBaseService.close();
        VGMainServiceHelper.executorService.shutdown();

        System.exit(0);
    }

    private void rebuildView() {
        //clear panel
        innerView.removeAll();

        innerView.add(northInstrumentPanel.getView(), new GridBagConstraints(0,0,  3,1,  1, 0,  GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0),  0,0));
        innerView.add(westInstrumentPanel.getView(), new GridBagConstraints(0,1,  1,1,  0, 1,  GridBagConstraints.WEST, GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0),  0,0));
        innerView.add(southInstrumentPanel.getView(), new GridBagConstraints(0,2,  3,1,  1, 0,  GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0),  0,0));

        JPanel centerPanel = new JPanel(new GridLayout(1, 1));
        if (!westTopPanel.isEmpty()) {
            JSplitPane westSplitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, westTopPanel.getView(), desktopPanel.getView());
            westSplitPane.setDividerLocation(westAndCenterDividerLocation);
            westSplitPane.addPropertyChangeListener(JSplitPane.DIVIDER_LOCATION_PROPERTY, new PropertyChangeListener() {
                @Override
                public void propertyChange(PropertyChangeEvent evt) {
                    westAndCenterDividerLocation = Integer.valueOf(evt.getNewValue().toString());
                    VGMainServiceHelper.config.setIntegerProperty(VGMainGlobals.UI_SERVICE_MAIN_WINDOW_WEST_CENTER_DIVIDER_LOCATION_KEY, westAndCenterDividerLocation);
                    VGMainServiceHelper.logger.printDebug("West<->Center slider: " + evt.getNewValue());
                }
            });
            centerPanel.add(westSplitPane);
        } else {
            centerPanel.add(desktopPanel.getView());
        }

        JPanel southPanel = new JPanel(new GridLayout(1, 1));
        if (!westSouthPanel.isEmpty() && !eastSouthPanel.isEmpty()) {
            JSplitPane westSouthEastSouthSplitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, westSouthPanel.getView(), eastSouthPanel.getView());
            westSouthEastSouthSplitPane.setDividerLocation(westSouthAndEastSouthDividerLocation);
            westSouthEastSouthSplitPane.addPropertyChangeListener(JSplitPane.DIVIDER_LOCATION_PROPERTY, new PropertyChangeListener() {
                @Override
                public void propertyChange(PropertyChangeEvent evt) {
                    westSouthAndEastSouthDividerLocation = Integer.valueOf(evt.getNewValue().toString());
                    VGMainServiceHelper.config.setIntegerProperty(VGMainGlobals.UI_SERVICE_MAIN_WINDOW_WEST_CENTER_DIVIDER_LOCATION_KEY, westSouthAndEastSouthDividerLocation);
                    VGMainServiceHelper.logger.printDebug("West<->South slider: " + evt.getNewValue());
                }
            });
            southPanel.add(westSouthEastSouthSplitPane);
        } else if (!westSouthPanel.isEmpty()) {
            southPanel.add(westSouthPanel.getView());
        } else if (!eastSouthPanel.isEmpty()) {
            southPanel.add(eastSouthPanel.getView());
        } else {
            southPanel = null;
        }

        if (southPanel != null) {
            JSplitPane betweenCenterAndSouthSplitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, centerPanel, southPanel);
            betweenCenterAndSouthSplitPane.setDividerLocation(centerAndSouthDividerLocation);
            betweenCenterAndSouthSplitPane.addPropertyChangeListener(JSplitPane.DIVIDER_LOCATION_PROPERTY, new PropertyChangeListener() {
                @Override
                public void propertyChange(PropertyChangeEvent evt) {
                    centerAndSouthDividerLocation = Integer.valueOf(evt.getNewValue().toString());
                    VGMainServiceHelper.config.setIntegerProperty(VGMainGlobals.UI_SERVICE_MAIN_WINDOW_SOUTH_CENTER_DIVIDER_LOCATION_KEY, centerAndSouthDividerLocation);
                    VGMainServiceHelper.logger.printDebug("South<->Center slider: " + evt.getNewValue());
                }
            });
            innerView.add(betweenCenterAndSouthSplitPane, new GridBagConstraints(1,1,  2,1,  1,1,  GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0,0,0,0),  0,0));
        } else {
            innerView.add(centerPanel, new GridBagConstraints(1,1,  2,1,  1,1,  GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0,0,0,0),  0,0));
        }

        innerView.add(statusBar, new GridBagConstraints(0,3, 3,1, 1,0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0,0,0,0), 0,0));

        // update ui
        innerView.updateUI();
    }

    public void refresh() {
        SwingUtilities.updateComponentTreeUI(menuBar);
        desktopPanel.refresh();
        westTopPanel.refresh();
        eastSouthPanel.refresh();
        westSouthPanel.refresh();
    }
}
