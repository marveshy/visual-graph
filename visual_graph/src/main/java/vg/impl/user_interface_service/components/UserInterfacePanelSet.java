package vg.impl.user_interface_service.components;

import vg.interfaces.user_interface_service.UserInterfacePanel;
import vg.shared.user_interface.UserInterfacePanelUtils;

import javax.swing.*;
import java.awt.*;

public class UserInterfacePanelSet {
    // Main components
    private JPanel outView, innerView;

    private JTabbedPane tabs;

    // Main data
    private UserInterfacePanel userInterfacePanel = null;

    private UserInterfacePanelSetCallback callback;

	public UserInterfacePanelSet(UserInterfacePanelSetCallback callback) {
        this.callback = callback;

        outView = new JPanel(new GridLayout(1, 1));
        innerView = new JPanel(new GridBagLayout());
        outView.add(innerView);

		tabs = new JTabbedPane();

        rebuildView();
	}

	public void setPanel(UserInterfacePanel panel) {
        if (userInterfacePanel != null)
            callback.onRemovePanel(userInterfacePanel);
        userInterfacePanel = panel;
        if (panel != null)
            callback.onAddPanel(panel);
        rebuildView();
	}

    public boolean isEmpty() {
        return userInterfacePanel == null;
    }

	public JPanel getView() {
		return outView;
	}

    public static abstract class UserInterfacePanelSetCallback {
        public void onAddPanel(UserInterfacePanel panel) {}
        public void onRemovePanel(UserInterfacePanel panel) {}
    }

//==============================================================================
//------------------PRIVATE METHODS---------------------------------------------
    private void rebuildView() {
        //clear panel
        innerView.removeAll();

        if (userInterfacePanel != null) {
            innerView.add(userInterfacePanel.getView(), new GridBagConstraints(0, 0, 1, 1, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        UserInterfacePanelUtils.createBorder(innerView);

        // update ui
        innerView.updateUI();
    }

    public void refresh() {
        SwingUtilities.updateComponentTreeUI(tabs);
        SwingUtilities.updateComponentTreeUI(innerView);
        SwingUtilities.updateComponentTreeUI(outView);
    }
}
