package vg.impl.user_interface_service.components;

import com.google.common.collect.Lists;
import vg.interfaces.user_interface_service.UserInterfaceInstrument;
import vg.interfaces.user_interface_service.UserInterfaceService;

import javax.swing.*;
import java.awt.*;
import java.util.List;

/**
 * @author Timur Zolotuhin (tzolotuhin@parallels.com)
 */
public class InstrumentPanel {
    // Constants
    public static final int SOUTH_ORIENTATION = 1;
    public static final int WEST_ORIENTATION = 2;
    public static final int EAST_ORIENTATION = 3;
    public static final int NORTH_ORIENTATION = 4;

    // Main components
    private final JPanel outView, innerView;

    // Main data
    private int type;
    private List<UserInterfaceInstrument> userInterfaceInstruments = Lists.newArrayList();

    public InstrumentPanel(int type) {
        this.type = type;

        outView = new JPanel(new GridLayout(1, 1));
        innerView = new JPanel(new GridBagLayout());
        outView.add(innerView);
    }

    public JComponent getView() {
        return outView;
    }

    public void addInstrument(final UserInterfaceInstrument userInterfaceInstrument) {
        userInterfaceInstruments.add(userInterfaceInstrument);
        rebuildView();
    }

//==============================================================================
//---------------PRIVATE METHODS------------------------------------------------
    private void rebuildView() {
        //clear panel
        innerView.removeAll();

        int index = 0;
        for (UserInterfaceInstrument userInterfaceInstrument : userInterfaceInstruments) {
            JPanel view = userInterfaceInstrument.getView();
            if (type == WEST_ORIENTATION || type == EAST_ORIENTATION) {
                view.setPreferredSize(UserInterfaceService.WEST_EAST_INSTRUMENT_PANEL_SIZE);
                view.setMaximumSize(UserInterfaceService.WEST_EAST_INSTRUMENT_PANEL_SIZE);
                view.setMinimumSize(UserInterfaceService.WEST_EAST_INSTRUMENT_PANEL_SIZE);
            } else if (type == SOUTH_ORIENTATION) {
                view.setPreferredSize(UserInterfaceService.SOUTH_INSTRUMENT_PANEL_SIZE);
                view.setMaximumSize(UserInterfaceService.SOUTH_INSTRUMENT_PANEL_SIZE);
                view.setMinimumSize(UserInterfaceService.SOUTH_INSTRUMENT_PANEL_SIZE);
            }

            int leftInset, rightInset;

            if (type == SOUTH_ORIENTATION && index == 0) {
                leftInset = UserInterfaceService.WEST_EAST_INSTRUMENT_PANEL_SIZE.width;
            } else {
                leftInset = 0;
            }
            if (type == SOUTH_ORIENTATION && index == userInterfaceInstruments.size() - 1) {
                rightInset = UserInterfaceService.WEST_EAST_INSTRUMENT_PANEL_SIZE.width;
            } else {
                rightInset = 0;
            }


            if (type == NORTH_ORIENTATION || type == SOUTH_ORIENTATION) {
                if (index == userInterfaceInstruments.size() - 1) {
                    innerView.add(view, new GridBagConstraints(index,0, 1,1,  1,0,  GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,leftInset,0,rightInset),  0,0));
                } else {
                    innerView.add(view, new GridBagConstraints(index,0, 1,1,  0,0,  GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0,leftInset,0,rightInset),  0,0));
                }
            } else {
                if (index == userInterfaceInstruments.size() - 1) {
                    innerView.add(view, new GridBagConstraints(0,index, 1,1,  0,1,  GridBagConstraints.NORTH, GridBagConstraints.NONE, new Insets(0,0,0,0),  0,0));
                } else {
                    innerView.add(view, new GridBagConstraints(0,index, 1,1,  0,0,  GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0,0,0,0),  0,0));
                }
            }
            index++;
        }

        // update ui
        innerView.updateUI();
    }
}
