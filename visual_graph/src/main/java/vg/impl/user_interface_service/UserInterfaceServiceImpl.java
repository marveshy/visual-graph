package vg.impl.user_interface_service;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import vg.shared.gui.SwingUtils;
import vg.shared.utils.MapUtils;
import vg.impl.main_service.VGMainServiceHelper;
import vg.impl.user_interface_service.components.MainWindow;
import vg.interfaces.user_interface_service.*;
import vg.shared.user_interface.DefaultSwingUtilsCallBack;

import javax.swing.*;
import java.util.List;
import java.util.Map;

/**
 * This class realizes user interface and use swing for it.
 *
 * @author tzolotuhin
 */
public class UserInterfaceServiceImpl implements UserInterfaceService, UserInterfaceServiceImplCallback {
    // Main components
    private MainWindow mainWindow;

    // Main data
    private Map<UserInterfaceListener, Integer> listeners = Maps.newLinkedHashMap();

    // Mutex
    private final Object generalMutex = new Object();

    @Override
    public void start() {
        SwingUtils.invokeInEDTIfNeeded(new Runnable() {
            @Override
            public void run() {
                synchronized (generalMutex) {
                    mainWindow = new MainWindow(UserInterfaceServiceImpl.this);
                }
            }
        }, new DefaultSwingUtilsCallBack());
    }

    @Override
	public void addMenuItem(final JMenuItem item, final String place) {
        SwingUtils.invokeInEDTIfNeeded(new Runnable() {
            @Override
            public void run() {
                synchronized (generalMutex) {
                    mainWindow.addMenuItem(item, place);
                }
            }
        }, new DefaultSwingUtilsCallBack());
	}

    @Override
    public void setPanel(final UserInterfacePanel element, final int place) {
        if (!SwingUtilities.isEventDispatchThread()) {
             SwingUtilities.invokeLater(new Runnable() {
                 @Override
                 public void run() {
                     setPanel(element, place);
                 }
             });
        } else {
            synchronized (generalMutex) {
                mainWindow.setPanel(element, place);
            }
        }
    }

    @Override
    public void addTab(final UserInterfaceTab tab, final FinishActionCallBack callBack) {
        if (!SwingUtilities.isEventDispatchThread()) {
            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    addTab(tab, callBack);
                }
            });
        } else {
            synchronized (generalMutex) {
                mainWindow.addTab(tab, callBack);
            }
        }
    }

    @Override
    public void replaceTab(final UserInterfaceTab oldTab, final UserInterfaceTab newTab, final UserInterfaceService.FinishActionCallBack callBack) {
        SwingUtils.invokeInEDTIfNeeded(new Runnable() {
            @Override
            public void run() {
                synchronized (generalMutex) {
                    mainWindow.replaceTab(oldTab, newTab, callBack);
                }
            }
        }, new DefaultSwingUtilsCallBack());
    }

    @Override
    public void selectTab(final UserInterfaceTab tab) {
        if (!SwingUtilities.isEventDispatchThread()) {
            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    selectTab(tab);
                }
            });
        } else {
            synchronized (generalMutex) {
                mainWindow.selectTab(tab);
            }
        }
    }

    @Override
    public void addObserver(UserInterfaceListener listener) {
        addObserver(listener, MIN_PRIORITY);
    }

    @Override
    public void addObserver(final UserInterfaceListener listener, final int priority) {
        if (!SwingUtilities.isEventDispatchThread()) {
            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    addObserver(listener, priority);
                }
            });
        } else {
            synchronized (generalMutex) {
                listeners.put(listener, priority);
                listeners = new MapUtils<UserInterfaceListener, Integer>().sortMapByValue(listeners);
            }
        }
    }

    @Override
    public void addInstrument(final UserInterfaceInstrument instrument, final int place) {
        SwingUtils.invokeInEDTIfNeeded(new Runnable() {
            @Override
            public void run() {
                synchronized (generalMutex) {
                    mainWindow.addInstrument(instrument, place);
                }
            }
        }, new DefaultSwingUtilsCallBack());
    }

	@Override
	public JFrame getMainFrame() {
        synchronized (generalMutex) {
            return mainWindow.getMainFrame();
        }
	}

    @Override
    public void refresh() {
        SwingUtils.invokeInEDTIfNeeded(new Runnable() {
            @Override
            public void run() {
                synchronized (generalMutex) {
                    mainWindow.refresh();
                }
            }
        }, new DefaultSwingUtilsCallBack());
    }

    @Override
	public void quit() {
        SwingUtils.invokeInEDTIfNeeded(new Runnable() {
            @Override
            public void run() {
                synchronized (generalMutex) {
                    mainWindow.quit();
                }
            }
        }, new DefaultSwingUtilsCallBack());
	}

    @Override
    public void notifyOnChangeDesktopTab(final UserInterfaceTab tab) {
        final List<UserInterfaceListener> copyListeners = syncCopyListeners();
        SwingUtils.invokeInEDTIfNeeded(new Runnable() {
            @Override
            public void run() {
                for (UserInterfaceListener listener : copyListeners) {
                    try {
                        listener.onChangeTab(tab);
                    } catch (Throwable ex) {
                        VGMainServiceHelper.logger.printException(ex);
                    }
                }
            }
        }, new DefaultSwingUtilsCallBack());
    }

    @Override
    public void notifyOnOpenTab(final UserInterfaceTab tab) {
        final List<UserInterfaceListener> copyListeners = syncCopyListeners();
        SwingUtils.invokeInEDTIfNeeded(new Runnable() {
            @Override
            public void run() {
                for (UserInterfaceListener listener : copyListeners) {
                    try {
                        listener.onOpenTab(tab);
                    } catch (Throwable ex) {
                        VGMainServiceHelper.logger.printException(ex);
                    }
                }
            }
        }, new DefaultSwingUtilsCallBack());
    }

    @Override
    public void notifyOnCloseTab(final UserInterfaceTab tab) {
        final List<UserInterfaceListener> copyListeners = syncCopyListeners();
        SwingUtils.invokeInEDTIfNeeded(new Runnable() {
            @Override
            public void run() {
                for (UserInterfaceListener listener : copyListeners) {
                    try {
                        listener.onCloseTab(tab);
                    } catch (Throwable ex) {
                        VGMainServiceHelper.logger.printException(ex);
                    }
                }
            }
        }, new DefaultSwingUtilsCallBack());
    }

    @Override
    public void notifyOnAddPanel(final UserInterfacePanel panel, final int place) {
        final List<UserInterfaceListener> copyListeners = syncCopyListeners();
        SwingUtils.invokeInEDTIfNeeded(new Runnable() {
            @Override
            public void run() {
                for (UserInterfaceListener listener : copyListeners) {
                    try {
                        listener.onAddPanel(panel, place);
                    } catch (Throwable ex) {
                        VGMainServiceHelper.logger.printException(ex);
                    }
                }
            }
        }, new DefaultSwingUtilsCallBack());
    }

    @Override
    public void notifyOnRemovePanel(final UserInterfacePanel panel, final int place) {
        final List<UserInterfaceListener> copyListeners = syncCopyListeners();
        SwingUtils.invokeInEDTIfNeeded(new Runnable() {
            @Override
            public void run() {
                for (UserInterfaceListener listener : copyListeners) {
                    try {
                        listener.onRemovePanel(panel, place);
                    } catch (Throwable ex) {
                        VGMainServiceHelper.logger.printException(ex);
                    }
                }
            }
        }, new DefaultSwingUtilsCallBack());
    }

    @Override
    public void notifyOnQuit() {
        final List<UserInterfaceListener> copyListeners = syncCopyListeners();
        SwingUtils.invokeInEDTIfNeeded(new Runnable() {
            @Override
            public void run() {
                for (UserInterfaceListener listener : copyListeners) {
                    try {
                        listener.onQuit();
                    } catch (Throwable ex) {
                        VGMainServiceHelper.logger.printException(ex);
                    }
                }
            }
        }, new DefaultSwingUtilsCallBack());
    }

//==============================================================================
//------------------PRIVATE METHODS---------------------------------------------
    private List<UserInterfaceListener> syncCopyListeners() {
        List<UserInterfaceListener> copyListeners;

        synchronized (generalMutex) {
            copyListeners = Lists.newArrayList(listeners.keySet());
        }

        return copyListeners;
    }
}
