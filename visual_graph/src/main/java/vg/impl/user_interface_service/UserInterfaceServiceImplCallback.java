package vg.impl.user_interface_service;

import vg.interfaces.user_interface_service.UserInterfacePanel;
import vg.interfaces.user_interface_service.UserInterfaceTab;

/**
 * @author Timur Zolotuhin (tzolotuhin@parallels.com)
 */
public interface UserInterfaceServiceImplCallback {
    public void notifyOnChangeDesktopTab(UserInterfaceTab tab);
    public void notifyOnOpenTab(UserInterfaceTab tab);
    public void notifyOnCloseTab(UserInterfaceTab tab);

    public void notifyOnRemovePanel(final UserInterfacePanel panel, final int place);
    public void notifyOnAddPanel(final UserInterfacePanel panel, final int place);

    public void notifyOnQuit();
}
