package vg.impl.graph_comparison_service;

import java.math.BigDecimal;

/**
 * @author Timur Zolotuhin (tzolotuhin@parallels.com)
 */
public interface GraphComparisonViewImplCallBack {
    public void notifyOnStartMatching();
    public void notifyOnFinishMatching();
    public void notifyOnUpdateMatching(BigDecimal currentCaseNumber, BigDecimal upperBound);
}
