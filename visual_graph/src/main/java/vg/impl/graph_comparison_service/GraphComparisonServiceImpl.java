package vg.impl.graph_comparison_service;

import vg.shared.gui.SwingUtils;
import vg.impl.graph_view_service.GraphViewServiceImpl;
import vg.impl.main_service.VGMainServiceHelper;
import vg.interfaces.data_base_service.data.graph.Graph;
import vg.interfaces.executor_service.ExecutorService;
import vg.interfaces.graph_comparison_service.GraphComparisonService;
import vg.interfaces.graph_comparison_service.GraphComparisonView;
import vg.interfaces.user_interface_service.UserInterfaceService;
import vg.shared.graph.utils.GraphUtils;
import vg.shared.user_interface.DefaultSwingUtilsCallBack;

/**
 * @author Timur Zolotuhin (e-mail: tzolotuhin@gmail.com)
 */
public class GraphComparisonServiceImpl implements GraphComparisonService {
    @Override
    public void asyncOpenGraphComparisonInTab(final Graph g1, final Graph g2, final GraphComparisonViewServiceCallBack callBack) {
        SwingUtils.invokeInEDTIfNeeded(new Runnable() {
            @Override
            public void run() {
                final GraphComparisonViewImpl graphComparisonView = new GraphComparisonViewImpl(g1, g2);
                ((GraphViewServiceImpl)VGMainServiceHelper.graphViewService).addGraphViewImpl(graphComparisonView);
                VGMainServiceHelper.executorService.execute(new Runnable() {
                    @Override
                    public void run() {
                        graphComparisonView.setTabTitle(GraphUtils.generateGraphTitle(graphComparisonView.getG1()) + " vs " + GraphUtils.generateGraphTitle(graphComparisonView.getG2()));
                        VGMainServiceHelper.userInterfaceService.addTab(graphComparisonView, new UserInterfaceService.FinishActionCallBack() {
                            @Override
                            public void onFinishAction() {
                                asyncCompare(graphComparisonView, callBack);
                            }
                        });
                    }
                });
            }
        }, new DefaultSwingUtilsCallBack());
    }

    @Override
    public void asyncCompare(final GraphComparisonView graphComparisonView, final GraphComparisonViewServiceCallBack callBack) {
        VGMainServiceHelper.executorService.execute(new ExecutorService.SwingExecutor() {
            @Override
            public void doInBackground() {
                graphComparisonView.compare();
            }

            @Override
            public void doInEDT() {
                if (callBack != null) {
                    try {
                        callBack.onFinishAction(graphComparisonView);
                    } catch (Throwable ex) {
                        VGMainServiceHelper.logger.printException(ex);
                    }
                }
            }
        });
    }
}
