package vg.impl.graph_comparison_service.algorithm;

/**
 * Edge in bipartite graph.
 *
 * @author Timur Zolotuhin (e-mail: tzolotuhin@gmail.com)
 */
public class BiGraphEdge {
	private int src;
	private int dst;
	
	public BiGraphEdge(int src, int dst) {
		this.src = src;
		this.dst = dst;
	}
	
	public int getSrc() {
		return src;
	}
	
	public int getDst() {
		return dst;
	}
	
	@Override
	public String toString() {
		StringBuilder str = new StringBuilder();
		str.append("(");
		str.append(src);
		str.append(",");
		str.append(dst);
		str.append(")");
		return str.toString();
	}
}
