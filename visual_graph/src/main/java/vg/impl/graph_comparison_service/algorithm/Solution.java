package vg.impl.graph_comparison_service.algorithm;

/**
 * Solution.
 *
 * @author Timur Zolotuhin (e-mail: tzolotuhin@gmail.com)
 */
public class Solution {
	private BiGraphEdge[] edges;
	
	public Solution(BiGraphEdge[] edges) {
		this.edges = edges;
	}
	
	public BiGraphEdge[] getEdges() {
		return edges;
	}
	
	@Override
	public String toString() {
		StringBuilder str = new StringBuilder();
		str.append("[");
		for (BiGraphEdge edge : edges) {
			if (edge == null) {
				str.append("null ");
			} else {
				str.append(edge.toString());
				str.append(" ");
			}
		}
		str.append("]");
		return str.toString();
	}
}
