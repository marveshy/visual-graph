package vg.impl.graph_comparison_service.algorithm;

import java.util.ArrayList;
import java.util.List;

/**
 * Group of edges of bipartite graph.
 *
 * @author Timur Zolotuhin (e-mail: tzolotuhin@gmail.com)
 */
public class BiGraphEdgeGroup {
	private List<BiGraphEdge> edges;
	private int currIndex = -1;
	
	public BiGraphEdgeGroup() {
		this.edges = new ArrayList<BiGraphEdge>();
	}
	
	public void push(BiGraphEdge e) {
		edges.add(e);
	}
	
	public BiGraphEdge next() {
		currIndex++;

        if (currIndex >= edges.size())
            return null;
        else
            return edges.get(currIndex);
	}
	
	public boolean hasNext() {
		return currIndex < edges.size() - 1;
	}
	
	public void resetIndex() {
		currIndex = -1;
	}
	
	public int size() {
		return edges.size();
	}

    public BiGraphEdge get(int index) {
        if (index < size() && index >= 0)
            return edges.get(index);
        return null;
    }
	
	public boolean contains(BiGraphEdge e) {
		for (BiGraphEdge buf : edges) {
			if ((buf != null && e != null && buf.getSrc() == e.getSrc() && buf.getDst() == e.getDst()) || (buf == null && e == null)) {
				return true;
			}
		}
		return false;
	}
	
	@Override
	public String toString() {
		StringBuilder str = new StringBuilder();
		str.append("[");
		for (BiGraphEdge buf : edges) {
			if (buf == null) {
				str.append("null ");
			} else {
				str.append(buf.toString());
				str.append(" ");
			}
		}
		str.append("]");
		return str.toString();
	}
}
