package vg.impl.graph_comparison_service.algorithm;

import com.google.common.collect.*;
import org.apache.commons.lang.Validate;
import vg.impl.graph_comparison_service.GraphComparisonViewImplCallBack;
import vg.impl.main_service.VGMainServiceHelper;
import vg.interfaces.data_base_service.data.graph.*;
import vg.interfaces.data_base_service.data.record.AttributeRecord;
import vg.impl.graph_comparison_service.algorithm.hungarian_algorithm.HungarianAlgorithm;
import vg.impl.graph_comparison_service.algorithm.hungarian_algorithm.HungarianAlgorithmOutput;
import vg.shared.graph.utils.GraphUtils;

import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Algorithm for matching two graphs.
 *
 * @author Timur Zolotuhin (e-mail: tzolotuhin@gmail.com)
 */
public class GraphComparator {
	// Defines
    private static final String INCOMING_COUNT_EDGE = "Incoming edges";
	private static final String OUTGOING_COUNT_EDGE = "Outgoing edges";

	private static final float MAX_PERCENT_VALUE = 1.0f;
	private static final float MIN_PERCENT_VALUE = 0.0f;
    private static final float MIDDLE_PERCENT_VALUE = 0.0f;
	
	// Main data
    private Graph g1, g2;

    private AttributedItem[] g1VertexAttributeItems, g2VertexAttributeItems;
    private AttributedItem[] g1EdgeAttributeItems, g2EdgeAttributeItems;

    private float vertexBarrier;
    private float edgeBarrier;

    private boolean enableHeuristic;
    private boolean enableStrongComparison;

    public BiMap<Vertex, Vertex> unitedVertices;
    public BiMap<Vertex, Vertex> separatedVertices;

    private BiMap<Vertex, Vertex> outputGraphVertices2firstOriginalGraphVertices;
    private BiMap<Vertex, Vertex> outputGraphVertices2secondOriginalGraphVertices;

    private Map<String, Float> vertexWeightsForAttributes;
    private Map<String, Float> edgeWeightsForAttributes;
    private Map<String, Float> normalizeVertexWeightsForAttributes;
    private Map<String, Float> normalizeEdgeWeightsForAttributes;

    private float[][] vertexMatchingTable = new float[0][];
    private float[][] edgeMatchingTable = new float[0][];

    private float[][] heuristicVertexMatchingTable = new float[0][];
    private float[][] heuristicEdgeMatchingTable = new float[0][];

    private AtomicBoolean inProgress = new AtomicBoolean(false);

    private Graph maximalCommonGraph;

    private List<Vertex> g1OutsideVertices, g2OutsideVertices;
    private List<Edge> g1OutsideEdges, g2OutsideEdges;

    private GraphComparisonViewImplCallBack callback;

    public GraphComparator(GraphComparisonViewImplCallBack callback, Graph g1, Graph g2) {
        this.callback = callback;

        this.g1 = g1;
        this.g2 = g2;

        Validate.notNull(g1);
        Validate.notNull(g2);

        vertexBarrier = 1.0f;
        edgeBarrier = 1.0f;

        g1VertexAttributeItems = initVertices(g1);
        g2VertexAttributeItems = initVertices(g2);

        g1EdgeAttributeItems = initEdges(g1);
        g2EdgeAttributeItems = initEdges(g2);

        unitedVertices = HashBiMap.create();
        separatedVertices = HashBiMap.create();

        // initialize vertex & edge attribute to weight map
        vertexWeightsForAttributes = Maps.newLinkedHashMap();
        edgeWeightsForAttributes = Maps.newLinkedHashMap();

        vertexWeightsForAttributes.put(INCOMING_COUNT_EDGE, MAX_PERCENT_VALUE);
        vertexWeightsForAttributes.put(OUTGOING_COUNT_EDGE, MAX_PERCENT_VALUE);

        collectVertexAttributes(g1, vertexWeightsForAttributes);
        collectVertexAttributes(g2, vertexWeightsForAttributes);

        collectEdgeAttributes(g1, edgeWeightsForAttributes);
        collectEdgeAttributes(g2, edgeWeightsForAttributes);
    }

    public void compare() {
        Validate.isTrue(!inProgress.get());

        inProgress.set(true);

        try {
            callback.notifyOnStartMatching();

            // run phases
            normalizeWeights();
            calculateMatchingTables();
            Solution solution = matching();
            maximalCommonGraph = buildMaximalCommonGraph(solution);
        } catch (Throwable ex) {
            VGMainServiceHelper.logger.printException(ex);
        } finally {
            inProgress.set(false);
            callback.notifyOnFinishMatching();
        }
    }

    public void stop() {
        inProgress.set(false);
    }

    public boolean isInProgress() {
        return inProgress.get();
    }

//    public OutputGraphComparisonAlgorithmProperties estimate(InputGraphComparisonAlgorithmProperties input) {
//        OutputGraphComparisonAlgorithmProperties output = new OutputGraphComparisonAlgorithmProperties();
//
//        output.setVertexBarrier(input.getVertexBarrier());
//        output.setEdgeBarrier(input.getEdgeBarrier());
//
//        output.setVertexMatchingTable(buildVertexMatchingTable());
//        output.setEdgeMatchingTable(buildEdgeMatchingTable());
//
//        SolutionSet solutionSet = new SolutionSet(output.getVertexMatchingTable(), output.getVertexBarrier());
//        output.setUpperBoundCountCases(solutionSet.upperBoundCountCases());
//
//        // apply united and separated vertex lists
//        if (input.getUnitedVertices() != null && input.getUnitedVertices().size() > 0) {
//            for (Vertex v1 : input.getUnitedVertices().keySet()) {
//                Vertex v2 = input.getUnitedVertices().get(v1);
//
//                int idx1 = input.getG1().getIndexByVertex(v1);
//                int idx2 = input.getG2().getIndexByVertex(v2);
//
//                for (int i = 0; i < input.getG1().getVertexCount(); i++) {
//                    output.getVertexMatchingTable()[i][idx2] = MIN_PERCENT_VALUE;
//                }
//                for (int i = 0; i < input.getG2().getVertexCount(); i++) {
//                    output.getVertexMatchingTable()[idx1][i] = MIN_PERCENT_VALUE;
//                }
//                output.getVertexMatchingTable()[idx1][idx2] = MAX_PERCENT_VALUE;
//            }
//        }
//
//        if (input.getSeparatedVertices() != null && input.getSeparatedVertices().size() > 0) {
//            for (Vertex v1 : input.getSeparatedVertices().keySet()) {
//                Vertex v2 = input.getSeparatedVertices().get(v1);
//
//                int idx1 = input.getG1().getIndexByVertex(v1);
//                int idx2 = input.getG2().getIndexByVertex(v2);
//
//                output.getVertexMatchingTable()[idx1][idx2] = MIN_PERCENT_VALUE;
//            }
//        }
//
//        // Hungarian optimization
//        float[][] cost = new float[input.getG1().getVertexCount()][input.getG2().getVertexCount()];
//        float[][] vertexMatchingTableHungarianOptimization = new float[input.getG1().getVertexCount()][input.getG2().getVertexCount()];
//
//        HungarianAlgorithmOutput hungarianAlgorithmOutput = new HungarianAlgorithm(output.getVertexMatchingTable(), HungarianAlgorithm.MAXIMIZE).execute();
//        if (hungarianAlgorithmOutput != null) {
//            cost = hungarianAlgorithmOutput.getCost();
//        }
//
//        for (int i = 0; i < cost.length; i++) {
//            for (int j = 0; j < cost[i].length; j++) {
//                if (cost[i][j] == 0 && output.getVertexMatchingTable()[i][j] >= input.getVertexBarrier()) {
//                    vertexMatchingTableHungarianOptimization[i][j] = output.getVertexMatchingTable()[i][j];
//                } else {
//                    vertexMatchingTableHungarianOptimization[i][j] = 0;
//                }
//            }
//        }
//
//        output.setHeuristicVertexMatchingTable(vertexMatchingTableHungarianOptimization);
//        output.setHeuristicEdgeMatchingTable(output.getEdgeMatchingTable());
//
//        SolutionSet hungarianSolutionSet = new SolutionSet(vertexMatchingTableHungarianOptimization, output.getVertexBarrier());
//        output.setUpperBoundHeuristicCountCases(hungarianSolutionSet.upperBoundCountCases());
//
//        return output;
//    }

    public Graph getG1() {
        return g1;
    }

    public Graph getG2() {
        return g2;
    }

    public Graph getMaximalCommonGraph() {
        return maximalCommonGraph;
    }

    public List<Vertex> getG1OutsideVertices() {
        return g1OutsideVertices;
    }

    public List<Vertex> getG2OutsideVertices() {
        return g2OutsideVertices;
    }

    public List<Edge> getG1OutsideEdges() {
        return g1OutsideEdges;
    }

    public List<Edge> getG2OutsideEdges() {
        return g2OutsideEdges;
    }

    public Map<String, Float> getVertexWeightsForAttributes() {
        return vertexWeightsForAttributes;
    }

    public Map<String, Float> getEdgeWeightsForAttributes() {
        return edgeWeightsForAttributes;
    }

    public float[][] getVertexMatchingTable() {
        return vertexMatchingTable;
    }

    public float[][] getEdgeMatchingTable() {
        return edgeMatchingTable;
    }

//==============================================================================
//-----------------PRIVATE METHODS----------------------------------------------
    private void calculateMatchingTables() {
        vertexMatchingTable = buildVertexMatchingTable();
        edgeMatchingTable = buildEdgeMatchingTable();

        // apply united and separated vertex lists
        for (Vertex v1 : unitedVertices.keySet()) {
            Vertex v2 = unitedVertices.get(v1);

            int idx1 = g1.getIdByVertex(v1);
            int idx2 = g2.getIdByVertex(v2);

            for (int i = 0; i < g1.getAllVertices().size(); i++) {
                vertexMatchingTable[i][idx2] = MIN_PERCENT_VALUE;
            }
            for (int i = 0; i < g2.getAllVertices().size(); i++) {
                vertexMatchingTable[idx1][i] = MIN_PERCENT_VALUE;
            }
            vertexMatchingTable[idx1][idx2] = MAX_PERCENT_VALUE;
        }

        for (Vertex v1 : separatedVertices.keySet()) {
            Vertex v2 = separatedVertices.get(v1);

            int idx1 = g1.getIdByVertex(v1);
            int idx2 = g2.getIdByVertex(v2);

            vertexMatchingTable[idx1][idx2] = MIN_PERCENT_VALUE;
        }

        // Hungarian optimization
        if (enableHeuristic) {
            float[][] cost = new float[g1.getAllVertices().size()][g2.getAllVertices().size()];
            heuristicVertexMatchingTable = new float[g1.getAllVertices().size()][g2.getAllVertices().size()];
            heuristicEdgeMatchingTable = new float[g1.getAllEdges().size()][g2.getAllEdges().size()];

            HungarianAlgorithmOutput hungarianAlgorithmOutput = new HungarianAlgorithm(vertexMatchingTable, HungarianAlgorithm.MAXIMIZE).execute();
            if (hungarianAlgorithmOutput != null) {
                cost = hungarianAlgorithmOutput.getCost();
            }

            for (int i = 0; i < cost.length; i++) {
                for (int j = 0; j < cost[i].length; j++) {
                    if (cost[i][j] == 0 && vertexMatchingTable[i][j] >= vertexBarrier) {
                        heuristicVertexMatchingTable[i][j] = vertexMatchingTable[i][j];
                    } else {
                        heuristicVertexMatchingTable[i][j] = 0;
                    }
                }
            }

            for (int i = 0; i < g1.getAllEdges().size(); i++) {
                System.arraycopy(edgeMatchingTable[i], 0, heuristicEdgeMatchingTable[i], 0, g2.getAllEdges().size());
            }
        }
    }

    private Solution matching() {
        if (enableHeuristic)
            return matching(new SolutionSet(heuristicVertexMatchingTable, vertexBarrier));
        else
            return matching(new SolutionSet(vertexMatchingTable, vertexBarrier));
    }

	private Graph buildMaximalCommonGraph(Solution solution) {
        // initialize output data
        outputGraphVertices2firstOriginalGraphVertices = HashBiMap.create();
        outputGraphVertices2secondOriginalGraphVertices = HashBiMap.create();

		BiGraphEdge[] edges = solution.getEdges();

		// build general vertices
        Vertex[][] newVertexMergeTable = new Vertex[g1.getAllVertices().size()][g2.getAllVertices().size()];
        Map<Integer, Vertex> newG1VertexMergeMap = Maps.newHashMap();
        Map<Integer, Vertex> newG2VertexMergeMap = Maps.newHashMap();

        Set<Integer> g1GeneralIdxSet = Sets.newHashSet();
        Set<Integer> g2GeneralIdxSet = Sets.newHashSet();
        List<Vertex> newGeneralVertices = Lists.newArrayList();
        for (BiGraphEdge edge : edges) {
            int idx1 = edge.getSrc();
            int idx2 = edge.getDst();

            g1GeneralIdxSet.add(idx1);
            g2GeneralIdxSet.add(idx2);

            Vertex v1 = g1.getVertexById(idx1);
            Vertex v2 = g2.getVertexById(idx2);

            Vertex v = new Vertex(mergingAttributeItems(v1, v2).getAttributes());
            newVertexMergeTable[idx1][idx2] = v;
            newG1VertexMergeMap.put(idx1, v);
            newG2VertexMergeMap.put(idx2, v);
            newGeneralVertices.add(v);

            outputGraphVertices2firstOriginalGraphVertices.put(v, v1);
            outputGraphVertices2secondOriginalGraphVertices.put(v, v2);
        }

        // build other vertices for g1
        g1OutsideVertices = Lists.newArrayList();
        for (Vertex vertex : g1.getAllVertices()) {
            if (!g1GeneralIdxSet.contains(g1.getIdByVertex(vertex))) {
                g1OutsideVertices.add(vertex);
                outputGraphVertices2firstOriginalGraphVertices.put(vertex, vertex);
            }
        }

        // build other vertices for g2
        g2OutsideVertices = Lists.newArrayList();
        for (Vertex vertex : g2.getAllVertices()) {
            if (!g2GeneralIdxSet.contains(g2.getIdByVertex(vertex))) {
                g2OutsideVertices.add(vertex);
                outputGraphVertices2secondOriginalGraphVertices.put(vertex, vertex);
            }
        }

        // build edges
		List<Edge> newGeneralEdges = Lists.newArrayList();
        g1OutsideEdges = Lists.newArrayList();
        g2OutsideEdges = Lists.newArrayList();
        List<Edge> newG1DeprecatedEdges = Lists.newArrayList();
        List<Edge> newG2DeprecatedEdges = Lists.newArrayList();

        for (Vertex outVertex : g1OutsideVertices) {
            for (Edge e : g1.getAllEdges()) {
                if (newG1DeprecatedEdges.contains(e))
                    continue;

                Vertex src = e.getSource();
                Vertex trg = e.getTarget();

                if (src == null || trg == null)
                    continue;

                Vertex inVertex = null;
                if (src.equals(outVertex))
                    inVertex = trg;

                if (trg.equals(outVertex))
                    inVertex = src;

                if (inVertex != null) {
                    int idx = g1.getIdByVertex(inVertex);
                    Vertex newInVertex = newG1VertexMergeMap.get(idx);

                    // add to deprecated edge list (double adding)
                    newG1DeprecatedEdges.add(e);

                    Edge outputEdge;
                    if (newInVertex != null) {
                        if (inVertex == trg)
                            outputEdge = new Edge(outVertex, newInVertex, e.getAttributes());
                        else
                            outputEdge = new Edge(newInVertex, outVertex, e.getAttributes());
                    } else {
                        if (inVertex == trg)
                            outputEdge = new Edge(outVertex, inVertex, e.getAttributes());
                        else
                            outputEdge = new Edge(inVertex, outVertex, e.getAttributes());
                    }
                    g1OutsideEdges.add(outputEdge);
                }
            }
        }

        for (Vertex outVertex : g2OutsideVertices) {
            for (Edge e : g2.getAllEdges()) {
                if (newG2DeprecatedEdges.contains(e))
                    continue;

                Vertex src = e.getSource();
                Vertex trg = e.getTarget();

                if (src == null || trg == null)
                    continue;

                Vertex inVertex = null;
                if (src.equals(outVertex))
                    inVertex = trg;

                if (trg.equals(outVertex))
                    inVertex = src;

                if (inVertex != null) {
                    int idx = g2.getIdByVertex(inVertex);
                    Vertex newInVertex = newG2VertexMergeMap.get(idx);

                    // add to deprecated edge list (double adding)
                    newG2DeprecatedEdges.add(e);

                    Edge outputEdge;
                    if (newInVertex != null) {
                        if (inVertex == trg)
                            outputEdge = new Edge(outVertex, newInVertex, e.getAttributes());
                        else
                            outputEdge = new Edge(newInVertex, outVertex, e.getAttributes());
                    } else {
                        if (inVertex == trg)
                            outputEdge = new Edge(outVertex, inVertex, e.getAttributes());
                        else
                            outputEdge = new Edge(inVertex, outVertex, e.getAttributes());
                    }
                    g2OutsideEdges.add(outputEdge);
                }
            }
        }

        // build general edges
        for (int i = 0; i < edges.length; i++) {
			for (int j = i + 1; j < edges.length; j++) {
				BiGraphEdge mcs_v1 = edges[i];
				BiGraphEdge mcs_v2 = edges[j];

				int g1Idx1 = mcs_v1.getSrc();
				int g1Idx2 = mcs_v2.getSrc();
				int g2Idx1 = mcs_v1.getDst();
				int g2Idx2 = mcs_v2.getDst();

                // edges from v1 to v2
                MergingEdgesResult mergingEdgesResultTo = mergingEdges(mcs_v1, mcs_v2, newVertexMergeTable[g1Idx1][g2Idx1], newVertexMergeTable[g1Idx2][g2Idx2]);
				for (Edge e : mergingEdgesResultTo.generalEdgesMoreBarrier) {
                    newGeneralEdges.add(e);
                }

                for (Edge e : mergingEdgesResultTo.otherG1Edges) {
                    int idx1 = g1.getIdByVertex(e.getSource());
                    int idx2 = g1.getIdByVertex(e.getTarget());
                    Vertex newSrcVertex = newG1VertexMergeMap.get(idx1);
                    Vertex newTrgVertex = newG1VertexMergeMap.get(idx2);
                    g1OutsideEdges.add(new Edge(newSrcVertex, newTrgVertex, e.getAttributes()));
                }

                for (Edge e : mergingEdgesResultTo.otherG2Edges) {
                    int idx1 = g2.getIdByVertex(e.getSource());
                    int idx2 = g2.getIdByVertex(e.getTarget());
                    Vertex newSrcVertex = newG2VertexMergeMap.get(idx1);
                    Vertex newTrgVertex = newG2VertexMergeMap.get(idx2);
                    g2OutsideEdges.add(new Edge(newSrcVertex, newTrgVertex, e.getAttributes()));
                }

                // edges from v2 to v1
                MergingEdgesResult mergingEdgesResultFrom = mergingEdges(mcs_v2, mcs_v1, newVertexMergeTable[g1Idx2][g2Idx2], newVertexMergeTable[g1Idx1][g2Idx1]);
                for (Edge e : mergingEdgesResultFrom.generalEdgesMoreBarrier) {
                    newGeneralEdges.add(e);
                }

                for (Edge e : mergingEdgesResultFrom.otherG1Edges) {
                    int idx1 = g1.getIdByVertex(e.getSource());
                    int idx2 = g1.getIdByVertex(e.getTarget());
                    Vertex newSrcVertex = newG1VertexMergeMap.get(idx1);
                    Vertex newTrgVertex = newG1VertexMergeMap.get(idx2);
                    g1OutsideEdges.add(new Edge(newSrcVertex, newTrgVertex, e.getAttributes()));
                }

                for (Edge e : mergingEdgesResultFrom.otherG2Edges) {
                    int idx1 = g2.getIdByVertex(e.getSource());
                    int idx2 = g2.getIdByVertex(e.getTarget());
                    Vertex newSrcVertex = newG2VertexMergeMap.get(idx1);
                    Vertex newTrgVertex = newG2VertexMergeMap.get(idx2);
                    g2OutsideEdges.add(new Edge(newSrcVertex, newTrgVertex, e.getAttributes()));
                }
            }
		}
		
		// build graph
        List<Vertex> newVertices = Lists.newArrayList();
        newVertices.addAll(newGeneralVertices);
        newVertices.addAll(g1OutsideVertices);
        newVertices.addAll(g2OutsideVertices);

        List<Edge> newEdges = Lists.newArrayList();
        newEdges.addAll(newGeneralEdges);
        newEdges.addAll(g1OutsideEdges);
        newEdges.addAll(g2OutsideEdges);

        return new Graph(newVertices, newEdges);
	}

    private float getVertexSimilarityPercentage4Solution(Solution solution) {
        if (solution == null || solution.getEdges() == null || solution.getEdges().length == 0)
            return 0;

        BiGraphEdge[] edges = solution.getEdges();

        // build vertices
        float vertex_result = 0;
        Vertex[][] new_vertex_Tree_table = new Vertex[g1.getAllVertices().size()][g2.getAllVertices().size()];
        List<Vertex> new_vertices = Lists.newArrayList();
        for (BiGraphEdge edge : edges) {
            int idx1 = edge.getSrc();
            int idx2 = edge.getDst();
            Vertex v1 = g1.getVertexById(idx1);
            Vertex v2 = g2.getVertexById(idx2);
            Vertex v = new Vertex(mergingAttributeItems(v1, v2).getAttributes());
            vertex_result += vertexMatchingTable[idx1][idx2];
            new_vertex_Tree_table[idx1][idx2] = v;
            new_vertices.add(v);
        }

        return vertex_result;
    }
	
	private float getEdgeSimilarityPercentage4Solution(Solution solution) {
		if (solution == null || solution.getEdges() == null || solution.getEdges().length == 0)
            return 0;

		BiGraphEdge[] edges = solution.getEdges();

		// build vertices
		float vertex_result = 0;
		Vertex[][] new_vertex_Tree_table = new Vertex[g1.getAllVertices().size()][g2.getAllVertices().size()];
		List<Vertex> new_vertices = Lists.newArrayList();
        for (BiGraphEdge edge : edges) {
            int idx1 = edge.getSrc();
            int idx2 = edge.getDst();
            Vertex v1 = g1.getVertexById(idx1);
            Vertex v2 = g2.getVertexById(idx2);
            Vertex v = new Vertex(mergingAttributeItems(v1, v2).getAttributes());
            vertex_result += vertexMatchingTable[idx1][idx2];
            new_vertex_Tree_table[idx1][idx2] = v;
            new_vertices.add(v);
        }

		// build edges
		float edge_result = 0;
		List<Edge> new_edges = Lists.newArrayList();
		for (int i = 0; i < edges.length; i++) {
			for (int j = i + 1; j < edges.length; j++) {
				BiGraphEdge mcs_v1 = edges[i];
				BiGraphEdge mcs_v2 = edges[j];
				int g1Idx1 = mcs_v1.getSrc();
				int g1Idx2 = mcs_v2.getSrc();
				int g2Idx1 = mcs_v1.getDst();
				int g2Idx2 = mcs_v2.getDst();

                MergingEdgesResult mergingEdgesResultTo = mergingEdges(mcs_v1, mcs_v2, new_vertex_Tree_table[g1Idx1][g2Idx1], new_vertex_Tree_table[g1Idx2][g2Idx2]);
				new_edges.addAll(mergingEdgesResultTo.allGeneralEdges.keySet());
				for (Float ratio : mergingEdgesResultTo.allGeneralEdges.values())
					edge_result += ratio;

                MergingEdgesResult mergingEdgesResultFrom = mergingEdges(mcs_v2, mcs_v1, new_vertex_Tree_table[g1Idx2][g2Idx2], new_vertex_Tree_table[g1Idx1][g2Idx1]);
				new_edges.addAll(mergingEdgesResultFrom.allGeneralEdges.keySet());
				for (Float ratio : mergingEdgesResultFrom.allGeneralEdges.values())
					edge_result += ratio;
			}
		}
		
		// build graph
        return edge_result;
	}

	private Solution matching(SolutionSet solutionSet) {
        BigDecimal lostCases = new BigDecimal(0);
        int loop = 0;

        Solution bestSolution = new Solution(new BiGraphEdge[0]);
        float bestVertexRatio = 0;
        float bestEdgeRatio = 0;
        while (inProgress.get()) {
            Solution solution = solutionSet.next();

            if (solution == null)
                break;

            //VGMainInitializer.logger.printDebug("solution : " + solution.toString());

            float vertexRatio = getVertexSimilarityPercentage4Solution(solution);

            int compResult = Float.compare(vertexRatio, bestVertexRatio);
            if (compResult > 0) {
                bestVertexRatio = vertexRatio;
                bestEdgeRatio = getEdgeSimilarityPercentage4Solution(solution);
                bestSolution = solution;

                //VGMainInitializer.logger.printDebug("best vertex ratio = " + bestVertexRatio);
                //VGMainInitializer.logger.printDebug("best edge ratio = " + bestEdgeRatio);
            } else if (compResult == 0) {
                float edgeRatio = getEdgeSimilarityPercentage4Solution(solution);
                if (Float.compare(edgeRatio, bestEdgeRatio) > 0) {
                    bestEdgeRatio = edgeRatio;
                    bestSolution = solution;

                    //VGMainInitializer.logger.printDebug("best edge ratio = " + bestEdgeRatio);
                }
            }

            loop++;
            if (loop >= 10000) {
                lostCases = lostCases.add(new BigDecimal(loop));
                callback.notifyOnUpdateMatching(lostCases, solutionSet.upperBoundCountCases());
                loop = 0;
            }
        }

        lostCases = lostCases.add(new BigDecimal(loop));
        callback.notifyOnUpdateMatching(lostCases, lostCases);

        // print best solution
        VGMainServiceHelper.logger.printDebug("Graph comparison, Best solution = " + bestSolution.toString());
        VGMainServiceHelper.logger.printDebug("Graph comparison, Count cases = " + lostCases);

		return bestSolution;
	}

    /**
     * Build vertex matching table.
     */
	private float[][] buildVertexMatchingTable() {
        float[][] vertexMatchingTable = new float[g1.getAllVertices().size()][g2.getAllVertices().size()];

		for (Vertex v1 : g1.getAllVertices()) {
			for(Vertex v2 : g2.getAllVertices()) {
				vertexMatchingTable[g1.getIdByVertex(v1)][g2.getIdByVertex(v2)] = getSimilarityPercentage(v1, v2);
			}
		}

        return vertexMatchingTable;
	}

    /**
     * Build edge matching table.
     */
	private float[][] buildEdgeMatchingTable() {
        float[][] edgeMatchingTable = new float[g1.getAllEdges().size()][g2.getAllEdges().size()];

		for (Edge e1 : g1.getAllEdges()) {
			for(Edge e2 : g2.getAllEdges()) {
				edgeMatchingTable[g1.getIdByEdge(e1)][g2.getIdByEdge(e2)] = getSimilarityPercentage(e1, e2);
			}
		}

        return edgeMatchingTable;
	}

    private void normalizeWeights() {
        normalizeVertexWeightsForAttributes = Maps.newLinkedHashMap();
        normalizeEdgeWeightsForAttributes = Maps.newLinkedHashMap();

        float vertexWeight = 0.0f;
        for (Float weight : vertexWeightsForAttributes.values()) {
            vertexWeight += weight;
        }

        float edgeWeight = 0.0f;
        for (Float weight : edgeWeightsForAttributes.values()) {
            edgeWeight += weight;
        }

        for (String attrName : vertexWeightsForAttributes.keySet()) {
            float weight = vertexWeightsForAttributes.get(attrName);
            if (Float.compare(vertexWeight, 0.0f) == 0)
                normalizeVertexWeightsForAttributes.put(attrName, 0.0f);
            else
                normalizeVertexWeightsForAttributes.put(attrName, weight / vertexWeight);
        }

        for (String attrName : edgeWeightsForAttributes.keySet()) {
            float weight = edgeWeightsForAttributes.get(attrName);
            if (Float.compare(edgeWeight, 0.0f) == 0)
                normalizeEdgeWeightsForAttributes.put(attrName, 0.0f);
            else
                normalizeEdgeWeightsForAttributes.put(attrName, weight / edgeWeight);
        }
    }

    /**
     * Returns percentage of similarity of two vertices in range from 0 to 1.
     */
    private float getSimilarityPercentage(Vertex g1vertex, Vertex g2vertex) {
        int g1VertexIdx = g1.getIdByVertex(g1vertex);
        int g2VertexIdx = g2.getIdByVertex(g2vertex);

        AttributedItem g1VertexAttributeItem = g1VertexAttributeItems[g1VertexIdx];
        AttributedItem g2VertexAttributeItem = g2VertexAttributeItems[g2VertexIdx];

        float result = MAX_PERCENT_VALUE;

        for (String attrName : normalizeVertexWeightsForAttributes.keySet()) {
            float match = 1.0f;
            Attribute g1Attribute = g1VertexAttributeItem.getAttribute(attrName);
            Attribute g2Attribute = g2VertexAttributeItem.getAttribute(attrName);
            if (g1Attribute != null && g2Attribute != null) {
                if (g1Attribute.isRealType() && g2Attribute.isRealType()) {
                    if (enableStrongComparison)
                        match = strongDoubleComparison(g1Attribute.getRealValue(), g2Attribute.getRealValue());
                    else
                        match = flexibleDoubleComparison(g1Attribute.getRealValue(), g2Attribute.getRealValue());
                } else if (g1Attribute.isStringType() && g2Attribute.isStringType()) {
                    if (enableStrongComparison)
                        match = strongStringComparison(g1Attribute.getStringValue(), g2Attribute.getStringValue());
                    else
                        match = flexibleStringComparison(g1Attribute.getStringValue(), g2Attribute.getStringValue());
                } else {
                    match = 0.0f;
                }
            } else if ((g1Attribute == null && g2Attribute != null) || (g1Attribute != null && g2Attribute == null)) {
                match = 0.0f;
            }
            result -= ((1 - match) * normalizeVertexWeightsForAttributes.get(attrName));
        }

		return result;
	}

    /**
     * Returns percentage of similarity of two edges in range from 0 to 1.
     */
	private float getSimilarityPercentage(Edge g1Edge, Edge g2Edge) {
        AttributedItem g1EdgeAttributeItem = g1EdgeAttributeItems[g1.getIdByEdge(g1Edge)];
        AttributedItem g2EdgeAttributeItem = g2EdgeAttributeItems[g2.getIdByEdge(g2Edge)];

        float result = MAX_PERCENT_VALUE;

        for (String attrName : normalizeEdgeWeightsForAttributes.keySet()) {
            float match = 1.0f;
            Attribute g1Attribute = g1EdgeAttributeItem.getAttribute(attrName);
            Attribute g2Attribute = g2EdgeAttributeItem.getAttribute(attrName);
            if (g1Attribute != null && g2Attribute != null) {
                if (g1Attribute.isRealType() && g2Attribute.isRealType()) {
                    if (enableStrongComparison)
                        match = strongDoubleComparison(g1Attribute.getRealValue(), g2Attribute.getRealValue());
                    else
                        match = flexibleDoubleComparison(g1Attribute.getRealValue(), g2Attribute.getRealValue());
                } else if (g1Attribute.isStringType() && g2Attribute.isStringType()) {
                    if (enableStrongComparison)
                        match = strongStringComparison(g1Attribute.getStringValue(), g2Attribute.getStringValue());
                    else
                        match = flexibleStringComparison(g1Attribute.getStringValue(), g2Attribute.getStringValue());
                } else {
                    match = 0.0f;
                }
            } else if ((g1Attribute == null && g2Attribute != null) || (g1Attribute != null && g2Attribute == null)) {
                match = 0.0f;
            }
            result -= ((1 - match) * normalizeEdgeWeightsForAttributes.get(attrName));
        }

        return result;
	}

	private AttributedItem mergingAttributeItems(AttributedItem item1, AttributedItem item2) {
		List<Attribute> attributes = Lists.newArrayList();
		for (Attribute attr1 : item1.getAttributes()) {
			Attribute attr2 = item2.getAttribute(attr1.getName());

            // skip invisible attributes
            if (!attr1.isVisible()) continue;
            if (attr2 != null && !attr2.isVisible()) continue;

            String htmlDiff;
            if (attr2 != null) {
                String value1 = attr1.getStringValue();
                String value2 = attr2.getStringValue();
                if (value1.equals(value2)) {
                    htmlDiff = "<font color=\"green\">" + value1 + "</font>" + "<b>|</b>" + "<font color=\"green\">" + value2 + "</font>";
                } else {
                    htmlDiff = "<font color=\"green\">" + value1 + "</font>" + "<b>|</b>" + "<font color=\"red\">" + value2 + "</font>";
                }
			} else {
                htmlDiff = "<font color=\"green\">" + attr1.getStringValue() + "</font>" + "<b>|</b>" + "<font color=\"red\">" + "null" + "</font>";
			}
            attributes.add(new Attribute(attr1.getName(), htmlDiff, AttributeRecord.STRING_ATTRIBUTE_TYPE, true));
        }
		return new AttributedItem(attributes);
	}
	
	/**
	 * Returns shared edges between v1 and v2.
	 */
	private MergingEdgesResult mergingEdges(BiGraphEdge v1, BiGraphEdge v2, Vertex new_v1, Vertex new_v2) {
		Map<Edge, Float> allGeneralEdges = Maps.newHashMap();
        List<Edge> generalEdgesMoreBarrier = Lists.newArrayList();
        List<Edge> generalEdgesLowBarrier = Lists.newArrayList();

		Vertex g1v1 = g1.getVertexById(v1.getSrc());
		Vertex g1v2 = g1.getVertexById(v2.getSrc());
		Vertex g2v1 = g2.getVertexById(v1.getDst());
		Vertex g2v2 = g2.getVertexById(v2.getDst());
		
		List<Edge> otherG1Edges = Lists.newArrayList();
		for (Edge e : g1.getAllEdges()) {
			Vertex src = e.getSource(), trg = e.getTarget();
			
			if (src == null || trg == null) continue;
			
			if (src.equals(g1v1) && trg.equals(g1v2)) {
				otherG1Edges.add(e);
			}
		}

		List<Edge> otherG2Edges = Lists.newArrayList();
		for (Edge e : g2.getAllEdges()) {
			Vertex src = e.getSource(), trg = e.getTarget();
			
			if (src == null || trg == null) continue;
			
			if (src.equals(g2v1) && trg.equals(g2v2)) {
				otherG2Edges.add(e);
			}
		}
		
		// merging
		if (otherG1Edges.size() > 0 && otherG2Edges.size() > 0) {
			float[][] table = new float[otherG1Edges.size()][otherG2Edges.size()];
			int i = 0, j = 0;
			for (Edge e1 : otherG1Edges) {
				for (Edge e2 : otherG2Edges) {
					table[i][j] = edgeMatchingTable[g1.getIdByEdge(e1)][g2.getIdByEdge(e2)];
					j++;
				}
				i++;
			}
			
			HungarianAlgorithmOutput hungarianAlgorithmOutput = new HungarianAlgorithm(table, HungarianAlgorithm.MAXIMIZE).execute();
            int[] assignment = hungarianAlgorithmOutput.getAssigned();
			
			for (i = 0; i < assignment.length; i++) {
				if (assignment[i] < 0)
                    continue;

                Edge e1 = otherG1Edges.get(i);
				Edge e2 = otherG2Edges.get(assignment[i]);
				float ratio = edgeMatchingTable[g1.getIdByEdge(e1)][g2.getIdByEdge(e2)];

                Edge generalEdge = new Edge(new_v1, new_v2, mergingAttributeItems(e1, e2), null);

                GraphUtils.setEdgeOrientation(generalEdge, GraphUtils.isDirectedEdge(e1) && GraphUtils.isDirectedEdge(e2));

                allGeneralEdges.put(generalEdge, ratio);
                if (ratio >= edgeBarrier) {
                    otherG1Edges.remove(e1);
                    otherG2Edges.remove(e2);
                    generalEdgesMoreBarrier.add(generalEdge);
                } else {
                    generalEdgesLowBarrier.add(generalEdge);
                }
			}
		}
		
		return new MergingEdgesResult(allGeneralEdges, generalEdgesMoreBarrier, generalEdgesLowBarrier, otherG1Edges, otherG2Edges);
	}

//==============================================================================
//-----------------STATIC METHODS-----------------------------------------------
    // Returns result in range from 0 to 1
    private static float flexibleDoubleComparison(double value1, double value2) {
        if (value1 < 0 && value2 < 0) {
            value1 = -value1;
            value2 = -value2;
        }

        if (value1 < 0 && value2 > 0) {
            value1 = -value1;
            value2 = value1 + value2;
        }

        if (value2 < 0 && value1 > 0) {
            value2 = - value2;
            value1 = value1 + value2;
        }

        // not zero
        value1++;
        value2++;


        float diff;
        if (value1 > value2)
            diff = (float)(value2 / value1);
        else
            diff = (float)(value1 / value2);

        if (diff > 1) diff = 1;
        if (diff < 0) diff = 0;

        return  diff;
    }

    // Returns result in range from 0 to 1
    private static float flexibleStringComparison(String value1, String value2) {
        float diff = 0;

        if (value1 == null || value2 == null)
            return diff;

        byte[] bytes2 = value2.getBytes();
        int i = 0;
        for (byte b : value1.getBytes()) {
            if (bytes2.length <= i || b != bytes2[i]) {
                break;
            } else {
                i++;
            }
        }
        diff = (2 * i) / (float)(value1.length() + value2.length());

        if (diff > 1) diff = 1;
        if (diff < 0) diff = 0;

        return  diff;
    }

    // Returns result which equals 0 or 1.
    private static float strongDoubleComparison(double value1, double value2) {
        if (Double.compare(value1, value2) == 0)
            return 1.0f;
        return 0.0f;
    }

    // Returns result which equals 0 or 1.
    private static float strongStringComparison(String value1, String value2) {
        if ((value1 == null && value2 == null) || (value1 != null && value2 != null && value1.equals(value2)))
            return 1.0f;

        return 0.0f;
    }

    private static AttributedItem[] initVertices(Graph graph) {
        AttributedItem[] vertices = new AttributedItem[graph.getAllVertices().size()];
        for (Vertex v : graph.getAllVertices()) {
            vertices[graph.getIdByVertex(v)] = new AttributedItem(v.getAttributes());
            vertices[graph.getIdByVertex(v)].addIntAttribute(INCOMING_COUNT_EDGE, graph.getIncomingEdgeCount(v));
            vertices[graph.getIdByVertex(v)].addIntAttribute(OUTGOING_COUNT_EDGE, graph.getOutgoingEdgeCount(v));
        }
        return vertices;
    }

    private static AttributedItem[] initEdges(Graph graph) {
        AttributedItem[] edges = new AttributedItem[graph.getAllEdges().size()];
        for (Edge e : graph.getAllEdges()) {
            edges[graph.getIdByEdge(e)] = new AttributedItem(e.getAttributes());
        }

        return edges;
    }

    /**
     * Adds vertex attributes to map with weight, if this map doesn't contains it.
     */
    private static void collectVertexAttributes(Graph graph, Map<String, Float> vertexAttribute2Weight) {
        for (Vertex v : graph.getAllVertices()) {
            for (Attribute a : v.getAttributes()) {
                if (a.isVisible()) {
                    vertexAttribute2Weight.put(a.getName(), MIDDLE_PERCENT_VALUE);
                }
            }
        }
    }

    /**
     * Adds edge attributes to map with weight, if this map doesn't contains it.
     */
    private static void collectEdgeAttributes(Graph graph, Map<String, Float> edgeAttribute2Weight) {
        for (Edge v : graph.getAllEdges()) {
            for (Attribute a : v.getAttributes()) {
                if (a.isBooleanType()) {
                    edgeAttribute2Weight.put(a.getName(), MIDDLE_PERCENT_VALUE);
                }
            }
        }
    }

    private static void normalise(float[][] array) {
        if (array == null)
            return;

        float max = 0;
        for (float[] row : array) {
            for (float col : row) {
                if (max < col)
                    max = col;
            }
        }

        if (max == 0)
            max = 1.0f;

        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                array[i][j] /= max;
            }
        }
    }

//==============================================================================
//-----------------STATIC INNER CLASSES-----------------------------------------
    private static class MergingEdgesResult {
        public Map<Edge, Float> allGeneralEdges;
        public List<Edge> generalEdgesMoreBarrier;
        public List<Edge> generalEdgesLowBarrier;
        public List<Edge> otherG1Edges;
        public List<Edge> otherG2Edges;

        private MergingEdgesResult(Map<Edge, Float> allGeneralEdges, List<Edge> generalEdgesMoreBarrier, List<Edge> generalEdgesLowBarrier, List<Edge> otherG1Edges, List<Edge> otherG2Edges) {
            this.allGeneralEdges = allGeneralEdges;
            this.generalEdgesMoreBarrier = generalEdgesMoreBarrier;
            this.generalEdgesLowBarrier = generalEdgesLowBarrier;
            this.otherG1Edges = otherG1Edges;
            this.otherG2Edges = otherG2Edges;
        }
    }
}
