package vg.impl.graph_comparison_service;

import com.google.common.collect.Lists;
import org.apache.commons.lang.Validate;
import vg.interfaces.graph_view_service.GraphView;
import vg.shared.gui.SwingUtils;
import vg.impl.graph_comparison_service.algorithm.GraphComparator;
import vg.impl.graph_view_service.GraphViewImpl;
import vg.interfaces.data_base_service.data.graph.Graph;
import vg.interfaces.graph_comparison_service.GraphComparisonListener;
import vg.interfaces.graph_comparison_service.GraphComparisonView;
import vg.shared.graph.utils.GraphUtils;
import vg.shared.user_interface.DefaultSwingUtilsCallBack;

import java.awt.*;
import java.math.BigDecimal;
import java.util.*;
import java.util.List;

/**
 * Graph comparison implementation.
 *
 * @author Timur Zolotuhin (e-mail: tzolotuhin@gmail.com)
 */
public class GraphComparisonViewImpl extends GraphViewImpl implements GraphComparisonView, GraphComparisonViewImplCallBack {
	// Constants
    private static final Color FIRST_GRAPH_OUTSIDE_COLOR = Color.decode("0xFFE4E1");
    private static final Color SECOND_GRAPH_OUTSIDE_COLOR = Color.decode("0x838B8B");

	// Main data
    private List<GraphComparisonListener> graphComparisonListeners = Lists.newArrayList();

    private GraphComparator graphComparator;

    private boolean readyForComparingFlag = true;

    // Mutex
    private final Object graphComparatorMutex = new Object();

	public GraphComparisonViewImpl(Graph g1, Graph g2) {
        graphComparator = new GraphComparator(this, g1, g2);
    }

    @Override
    public void compare() {
        synchronized (graphComparatorMutex) {
            Validate.isTrue(readyForComparingFlag);

            readyForComparingFlag = false;
        }

        try {
            lock(null);
            graphComparator.compare();
            unlock();
            synchronized (generalMutex) {
                removeAllElements();
                if (graphComparator.getMaximalCommonGraph() != null) {
                    openNewGraph(graphComparator.getMaximalCommonGraph());
                    setTabTitle(GraphUtils.generateGraphTitle(graphComparator.getMaximalCommonGraph()));
                    show(SHOW_GRAPH_INTERSECTION);
                }
            }
        } finally {
            synchronized (graphComparatorMutex) {
                readyForComparingFlag = true;
            }
        }
    }

    @Override
    public void stop() {
        synchronized (graphComparatorMutex) {
            graphComparator.stop();
        }
    }

    @Override
    public boolean isInProgress() {
        synchronized (graphComparatorMutex) {
            return graphComparator.isInProgress();
        }
    }

    @Override
    public void addGraphComparisonListener(GraphComparisonListener listener) {
        synchronized (listenerMutex) {
            graphComparisonListeners.add(listener);
        }
    }

    @Override
    public void removeGraphComparisonListener(GraphComparisonListener listener) {
        synchronized (listenerMutex) {
            graphComparisonListeners.remove(listener);
        }
    }

    @Override
    public void show(int part) {
        synchronized (generalMutex) {
            Validate.isTrue(!isInProgress());

            resetVertexColor();
            resetEdgeColor();
            resetVisibleVertices();
            resetVisibleEdges();

            setVertexColor(graphComparator.getG1OutsideVertices(), FIRST_GRAPH_OUTSIDE_COLOR);
            setEdgeColor(graphComparator.getG1OutsideEdges(), FIRST_GRAPH_OUTSIDE_COLOR);
            setVertexColor(graphComparator.getG2OutsideVertices(), SECOND_GRAPH_OUTSIDE_COLOR);
            setEdgeColor(graphComparator.getG2OutsideEdges(), SECOND_GRAPH_OUTSIDE_COLOR);
        }
    }

    @Override
    public Graph getG1() {
        synchronized (generalMutex) {
            return graphComparator.getG1();
        }
    }

    @Override
    public Graph getG2() {
        synchronized (generalMutex) {
            return graphComparator.getG2();
        }
    }

    @Override
    public Graph getMaximalCommonGraph() {
        synchronized (generalMutex) {
            return graphComparator.getMaximalCommonGraph();
        }
    }

    @Override
    public float[][] getVertexMatchingTable() {
        synchronized (generalMutex) {
            return graphComparator.getVertexMatchingTable();
        }
    }

    @Override
    public float[][] getEdgeMatchingTable() {
        synchronized (generalMutex) {
            return graphComparator.getEdgeMatchingTable();
        }
    }

    @Override
    public Map<String, Float> getWeightsForVertexAttributes() {
        synchronized (generalMutex) {
            return graphComparator.getVertexWeightsForAttributes();
        }
    }

    @Override
    public Map<String, Float> getWeightsForEdgeAttributes() {
        synchronized (generalMutex) {
            return graphComparator.getEdgeWeightsForAttributes();
        }
    }

    @Override
    public void notifyOnStartMatching() {
        syncExecute(new GraphViewRunnable() {
            private List<GraphComparisonListener> copyListeners;

            @Override
            public void run(GraphView graphView) {
                copyListeners = syncCopyListeners();
                SwingUtils.invokeInEDTIfNeeded(new Runnable() {
                    @Override
                    public void run() {
                        for (GraphComparisonListener graphComparisonListener : copyListeners) {
                            graphComparisonListener.onStartMatching();
                        }
                    }
                }, new DefaultSwingUtilsCallBack());
            }
        });
    }

    @Override
    public void notifyOnFinishMatching() {
        syncExecute(new GraphViewRunnable() {
            private List<GraphComparisonListener> copyListeners;

            @Override
            public void run(GraphView graphView) {
                copyListeners = syncCopyListeners();
                SwingUtils.invokeInEDTIfNeeded(new Runnable() {
                    @Override
                    public void run() {
                        for (GraphComparisonListener graphComparisonListener : copyListeners) {
                            graphComparisonListener.onFinishMatching();
                        }
                    }
                }, new DefaultSwingUtilsCallBack());
            }
        });
    }

    @Override
    public void notifyOnUpdateMatching(final BigDecimal currentCaseNumber, final BigDecimal upperBound) {
        syncExecute(new GraphViewRunnable() {
            private List<GraphComparisonListener> copyListeners;

            @Override
            public void run(GraphView graphView) {
                copyListeners = syncCopyListeners();
                SwingUtils.invokeInEDTIfNeeded(new Runnable() {
                    @Override
                    public void run() {
                        for (GraphComparisonListener graphComparisonListener : copyListeners) {
                            graphComparisonListener.onUpdateMatching(currentCaseNumber, upperBound);
                        }
                    }
                }, new DefaultSwingUtilsCallBack());
            }
        });
    }

//==============================================================================
//------------------PRIVATE METHODS---------------------------------------------
    private List<GraphComparisonListener> syncCopyListeners() {
        List<GraphComparisonListener> copyListeners;
        synchronized (listenerMutex) {
            copyListeners = Lists.newArrayList(graphComparisonListeners);
        }
        return copyListeners;
    }
}
