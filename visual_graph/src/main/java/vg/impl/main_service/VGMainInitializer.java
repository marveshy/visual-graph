package vg.impl.main_service;

import org.apache.log4j.PropertyConfigurator;
import vg.impl.log_service.Log4jLogger;
import vg.interfaces.config_service.ICmdLine;
import vg.interfaces.config_service.IConfig;
import vg.interfaces.log_service.ILogger;
import vg.interfaces.log_service.IWindowMessenger;
import vg.impl.config_service.CliCmdLine;
import vg.impl.config_service.FileConfig;
import vg.impl.data_base_service.SQLiteCacheGraphDataBaseService;
import vg.impl.executor_service.ExecutorServiceImpl;
import vg.impl.graph_comparison_service.GraphComparisonServiceImpl;
import vg.impl.graph_decoder_service.GraphDecoderServiceImpl;
import vg.impl.graph_layout_service.GraphLayoutServiceImpl;
import vg.impl.graph_view_service.GraphViewServiceImpl;
import vg.impl.log_service.WindowMessenger;
import vg.impl.plugin_service.PluginServiceImpl;
import vg.impl.remote_service.RemoteServiceImpl;
import vg.impl.user_interface_service.UserInterfaceServiceImpl;
import vg.interfaces.data_base_service.IGraphDataBaseService;
import vg.interfaces.executor_service.ExecutorService;
import vg.interfaces.graph_comparison_service.GraphComparisonService;
import vg.interfaces.graph_decoder_service.GraphDecoderService;
import vg.interfaces.graph_view_service.GraphViewService;
import vg.interfaces.main_service.VGMainService;
import vg.interfaces.plugin_service.PluginService;
import vg.interfaces.remote_service.RemoteService;
import vg.interfaces.user_interface_service.UserInterfaceService;
import vg.services.progress_manager.SimpleProgressManager;
import vg.services.progress_manager.interfaces.IProgressManager;

/**
 * @author Timur Zolotuhin (e-mail: tzolotuhin@gmail.com)
 */
public class VGMainInitializer {
    protected static IConfig config;
    protected static ILogger logger;
    protected static IWindowMessenger windowMessenger;
    protected static ICmdLine cmdLine;

    protected static VGMainService vgMainService;

    protected static PluginService pluginService;

    protected static RemoteService remoteService;

    protected static IGraphDataBaseService graphDataBaseService;
    protected static UserInterfaceService userInterfaceManager;

    protected static IProgressManager progressManager;
    protected static ExecutorService executorService;

    protected static GraphDecoderService graphDecoderService;
    protected static GraphLayoutServiceImpl graphLayoutService;
    protected static GraphViewService graphViewService;
    protected static GraphComparisonService graphComparisonService;

    // Mutex
    private static final Object generalMutex = new Object();

    /**
     * Executes initial preparations.
     */
    public static void executeInitialPreparations(String[] args) {
        synchronized (generalMutex) {
            try {
                // execute preparations
                windowMessenger = new WindowMessenger();
                config = new FileConfig();
                cmdLine = new CliCmdLine();

                // make changes in default services
                PropertyConfigurator.configure("data/log4j.properties");
                logger = new Log4jLogger();
                VGMainServiceHelper.logger = logger;

                vgMainService = new VGMainServiceImpl();
                pluginService = new PluginServiceImpl();

                // add options
                cmdLine.addOption(VGMainServiceHelper.CmdLineOptions.appInstanceIdOption);

                cmdLine.parseCmdLineArgs(args);

                graphDataBaseService = new SQLiteCacheGraphDataBaseService();
                userInterfaceManager = new UserInterfaceServiceImpl();

                progressManager = new SimpleProgressManager();
                executorService = new ExecutorServiceImpl();

                remoteService = new RemoteServiceImpl();

                graphDecoderService = new GraphDecoderServiceImpl();
                graphLayoutService = new GraphLayoutServiceImpl();
                graphViewService = new GraphViewServiceImpl();
                graphComparisonService = new GraphComparisonServiceImpl();

                // update helper
                updateHelper();

                pluginService.start();
            } catch (Throwable ex) {
                throw new RuntimeException(ex);
            }
        }
    }

    public static void updateHelper() {
        synchronized (generalMutex) {
            VGMainServiceHelper.logger = logger;
            VGMainServiceHelper.windowMessenger = windowMessenger;
            VGMainServiceHelper.config = config;

            VGMainServiceHelper.vgMainService = vgMainService;
            VGMainServiceHelper.pluginService = pluginService;
            VGMainServiceHelper.executorService = executorService;
            VGMainServiceHelper.progressManager = progressManager;

            VGMainServiceHelper.graphDecoderService = graphDecoderService;
            VGMainServiceHelper.graphViewService = graphViewService;
            VGMainServiceHelper.graphComparisonService = graphComparisonService;

            VGMainServiceHelper.graphDataBaseService = graphDataBaseService;
            VGMainServiceHelper.graphLayoutService = graphLayoutService;

            VGMainServiceHelper.userInterfaceService = userInterfaceManager;

            VGMainServiceHelper.remoteService = remoteService;
        }
    }
}
