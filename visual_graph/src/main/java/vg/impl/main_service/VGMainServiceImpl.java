package vg.impl.main_service;

import vg.interfaces.config_service.IConfig;
import vg.interfaces.log_service.ILogger;
import vg.interfaces.data_base_service.IGraphDataBaseService;
import vg.interfaces.main_service.VGMainService;
import vg.interfaces.user_interface_service.UserInterfaceService;

/**
 * This class contains links to all services.
 *
 * @author Timur Zolotuhin (e-mail: tzolotuhin@gmail.com)
 */
public class VGMainServiceImpl implements VGMainService {
    @Override
    public ILogger getLogger() {
        return VGMainServiceHelper.logger;
    }

    @Override
    public IConfig getConfig() {
        return VGMainServiceHelper.config;
    }

    @Override
    public IGraphDataBaseService getDataBaseService() {
        return VGMainServiceHelper.graphDataBaseService;
    }

    @Override
    public UserInterfaceService getUserInterfaceService() {
        return VGMainServiceHelper.userInterfaceService;
    }
}
