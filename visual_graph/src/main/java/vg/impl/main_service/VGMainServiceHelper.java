package vg.impl.main_service;

import vg.interfaces.config_service.IConfig;
import vg.interfaces.log_service.ILogger;
import vg.interfaces.log_service.IWindowMessenger;
import vg.impl.main_service.options.AppInstanceIdOption;
import vg.interfaces.data_base_service.IGraphDataBaseService;
import vg.interfaces.executor_service.ExecutorService;
import vg.interfaces.graph_comparison_service.GraphComparisonService;
import vg.interfaces.graph_decoder_service.GraphDecoderService;
import vg.interfaces.graph_layout_service.GraphLayoutService;
import vg.interfaces.graph_view_service.GraphViewService;
import vg.interfaces.main_service.VGMainService;
import vg.interfaces.plugin_service.PluginService;
import vg.interfaces.remote_service.RemoteService;
import vg.interfaces.user_interface_service.UserInterfaceService;
import vg.services.progress_manager.interfaces.IProgressManager;

/**
 * Contains all necessary service (or resource) links for quick access.
 *
 * Note: Don't edit this class. Read only.
 *
 * @author Timur Zolotuhin (e-mail: tzolotuhin@gmail.com)
 */
public class VGMainServiceHelper {
    public static ILogger logger;
    public static IConfig config;
    public static IWindowMessenger windowMessenger;

    public static VGMainService vgMainService;

    public static PluginService pluginService;

    public static RemoteService remoteService;

    public static IGraphDataBaseService graphDataBaseService;
    public static UserInterfaceService userInterfaceService;

    public static IProgressManager progressManager;
    public static ExecutorService executorService;

    public static GraphDecoderService graphDecoderService;
    public static GraphViewService graphViewService;
    public static GraphComparisonService graphComparisonService;
    public static GraphLayoutService graphLayoutService;

    public static class CmdLineOptions {
        public static AppInstanceIdOption appInstanceIdOption = new AppInstanceIdOption();
    }
}
