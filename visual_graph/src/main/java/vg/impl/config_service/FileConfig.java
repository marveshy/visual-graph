package vg.impl.config_service;

import vg.impl.main_service.VGMainServiceHelper;
import vg.interfaces.config_service.IConfig;
import vg.shared.utils.FileUtils;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

/**
 * Singleton class for keeping decoders (or plugins) properties.
 *
 * @author Timur Zolotuhin (e-mail: tzolotuhin@gmail.com)
 */
public class FileConfig implements IConfig {
    // Constants
    private static final String CONFIG_FILE_NAME = "config.ini";

    // Main data
    private Properties prop;
    private File configFile;
    private int behavior;

    public FileConfig() {
        this(new File(FileUtils.getWorkingDir() + CONFIG_FILE_NAME));
    }

    public FileConfig(File configFile) {
        this.behavior = SAVE_WHEN_EDIT_BEHAVIOR;
        this.configFile = configFile;

        // create log directory, if it isn't exist
        File configDir = configFile.getParentFile();
        if(!configDir.exists() && !configDir.mkdirs()) {
            throw new RuntimeException("Couldn't to create conf directory " + configDir.getAbsolutePath());
        }

        // read properties
        FileInputStream in = null;
		try {
			in = new FileInputStream(configFile);
			this.prop = new Properties();
			this.prop.load(in);
		} catch (IOException ex) {
			this.prop = new Properties();
            VGMainServiceHelper.logger.printError("Couldn't open file " + configFile + ".");
            VGMainServiceHelper.logger.printError("Try to create new config file.");
			try {
				if(configFile.createNewFile()) {
                    VGMainServiceHelper.logger.printError("Creating of config file. Successfully");
				} else {
                    VGMainServiceHelper.logger.printError("Creating of config file. Fail.");
				}
			} catch (IOException e) {
                VGMainServiceHelper.logger.printDebug("Please, ignore this exception", ex);
			}
		} finally {
			if(in != null) {
				try {
					in.close();
				} catch (IOException ex) {
                    VGMainServiceHelper.logger.printDebug("Please, ignore this exception", ex);
                }
			}
		}
		
		//finalize of services
		Runtime.getRuntime().addShutdownHook(new Thread() {
			public void run() {
				save();
			}
		});	
	}

    @Override
    public synchronized String getStringProperty(String key) {
        return prop.getProperty(key);
    }

    @Override
    public synchronized String getStringProperty(String key, String defaultValue) {
        String value = getStringProperty(key);

        if (value == null) {
            setProperty(key, defaultValue);
            return defaultValue;
        }

        return value;
    }

    @Override
    public synchronized boolean getBooleanProperty(String key) {
        String value = getStringProperty(key);

        if (value != null) {
            return Boolean.valueOf(value);
        } else {
            throw new NullPointerException("Element with key" + key + " was not found");
        }
    }

    @Override
    public synchronized boolean getBooleanProperty(String key, boolean defaultValue) {
        try {
            return getBooleanProperty(key);
        } catch (Throwable ex) {
            setProperty(key, Boolean.toString(defaultValue));
        }

        return defaultValue;
    }

    @Override
    public synchronized int getIntegerProperty(String key) {
        String value = getStringProperty(key);

        if (value != null) {
            return Integer.valueOf(value);
        } else {
            throw new NullPointerException("Element with key" + key + " was not found");
        }
    }

    @Override
    public synchronized int getIntegerProperty(String key, int defaultValue) {
        try {
            return getIntegerProperty(key);
        } catch (Throwable ex) {
            setProperty(key, Float.toString(defaultValue));
        }

        return defaultValue;
    }

    @Override
    public synchronized float getFloatProperty(String key) {
        String value = getStringProperty(key);

        if (value != null) {
            return Float.valueOf(value);
        } else {
            throw new NullPointerException("Element with key" + key + " was not found");
        }
    }

    @Override
    public synchronized float getFloatProperty(String key, float defaultValue) {
        try {
            return getFloatProperty(key);
        } catch (Throwable ex) {
            setProperty(key, Float.toString(defaultValue));
        }

        return defaultValue;
    }

    @Override
    public synchronized double getDoubleProperty(String key) {
        String value = getStringProperty(key);

        if (value != null) {
            return Double.valueOf(value);
        } else {
            throw new NullPointerException("Element with key" + key + " was not found");
        }
    }

    @Override
    public synchronized double getDoubleProperty(String key, double defaultValue) {
        try {
            return getFloatProperty(key);
        } catch (Throwable ex) {
            setProperty(key, Double.toString(defaultValue));
        }

        return defaultValue;
    }

    @Override
    public synchronized void setProperty(String key, String value) {
        prop.setProperty(key, value);
        if (behavior == SAVE_WHEN_EDIT_BEHAVIOR)
            save();
	}

    @Override
    public synchronized void setBooleanProperty(String key, boolean value) {
        setProperty(key, Boolean.toString(value));
    }

    @Override
    public synchronized void setIntegerProperty(String key, int value) {
        setProperty(key, Integer.toString(value));
    }

    @Override
    public synchronized void setDoubleProperty(String key, double value) {
        setProperty(key, Double.toString(value));
    }

    @Override
    public synchronized void save() {
        try (OutputStreamWriter out = new OutputStreamWriter(new FileOutputStream(configFile), "UTF-8")){
            List<String> keys = new ArrayList<>();
            for (Object key : prop.keySet()) {
                if (key != null)
                    keys.add(key.toString());
            }

            String[] arrayKeys = new String[keys.size()];
            keys.toArray(arrayKeys);
            Arrays.sort(arrayKeys);

            String lineSeparator = System.getProperty("line.separator");

            for (String key : arrayKeys) {
                Object value = prop.get(key);

                if (value != null) {
                    String outStr = (key + "=" + value.toString()).replace("\\", "\\\\");

                    out.write(outStr + lineSeparator);
                }
            }
		} catch (Throwable ex) {
			System.err.println(ex.getMessage());
            throw new RuntimeException(ex);
		}
	}

    @Override
    public synchronized void setSaveBehavior(int behavior) {
        this.behavior = behavior;
    }

    @Override
    public synchronized int getSaveBehavior() {
        return behavior;
    }
}
