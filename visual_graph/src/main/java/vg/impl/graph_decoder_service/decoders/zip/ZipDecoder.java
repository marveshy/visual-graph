package vg.impl.graph_decoder_service.decoders.zip;

import vg.shared.utils.FileUtils;
import vg.shared.utils.UnzipUtils;
import vg.impl.main_service.VGMainServiceHelper;
import vg.plugins.opener.GraphOpenerGlobals;
import vg.impl.graph_decoder_service.decoders.graphml.GraphMLDecoder;

import java.io.File;
import java.util.List;

/**
 * @author Timur Zolotuhin (tzolotuhin@parallels.com)
 */
public class ZipDecoder extends GraphMLDecoder {
    @Override
    public List<Integer> decode(File file, String graphName) throws Exception {
        File tmpDir = FileUtils.createTmpDir();
        UnzipUtils.unzip(file, tmpDir);

        for (String fileName : tmpDir.list()) {
            if (fileName.endsWith(GraphOpenerGlobals.GRAPHML_FORMAT)) {
                File graphmlFile = new File(tmpDir.getAbsolutePath() + File.separator + fileName);
                return VGMainServiceHelper.graphDecoderService.openFile(graphmlFile);
            }
            if (fileName.endsWith(GraphOpenerGlobals.GML_FORMAT)) {
                File gmlFile = new File(tmpDir.getAbsolutePath() + File.separator + fileName);
                return VGMainServiceHelper.graphDecoderService.openFile(gmlFile);
            }
            if (fileName.endsWith(GraphOpenerGlobals.DOT_FORMAT) || fileName.endsWith(GraphOpenerGlobals.GV_FORMAT)) {
                File dotFile = new File(tmpDir.getAbsolutePath() + File.separator + fileName);
                return VGMainServiceHelper.graphDecoderService.openFile(dotFile);
            }
        }

        throw new Exception("Zip container should contains " + GraphOpenerGlobals.GRAPHML_FORMAT + " | " +
                GraphOpenerGlobals.GML_FORMAT + "|" +
                GraphOpenerGlobals.DOT_FORMAT + "|" +
                GraphOpenerGlobals.GV_FORMAT + " file");
    }
}
