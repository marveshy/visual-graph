package vg.impl.graph_decoder_service.decoders.graphml;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.Validate;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import vg.impl.main_service.VGMainServiceHelper;
import vg.interfaces.data_base_service.data.record.*;
import vg.plugins.opener.GraphOpenerGlobals;
import vg.shared.graph.utils.GraphUtils;

import java.util.*;

class GraphMLParser extends DefaultHandler {
    // Constants
    private static final String ID = "id";
    private static final String GRAPH = "graph";
    private static final String EDGEDEF = "edgedefault";
    private static final String DIRECTED = "directed";
    private static final String UNDIRECTED = "undirected";
    private static final String PORT = "port";
    private static final String NAME = "name";
    private static final String HYPEREDGE = "hyperedge";
    private static final String ENDPOINT = "endpoint";
    private static final String KEY = "key";
    private static final String FOR = "for";
    private static final String ALL = "all";
    private static final String ATTRNAME = "attr.name";
    private static final String ATTRTYPE = "attr.type";
    private static final String DEFAULT = "default";
    private static final String NODE = "node";
    private static final String EDGE = "edge";
    private static final String SOURCE = "source";
    private static final String SOURCE_PORT = "sourceport";
    private static final String TARGET = "target";
    private static final String TARGET_PORT = "targetport";
    private static final String DATA = "data";
    private static final String GRAPHML = "graphml";

    public static final String INT = "int";
    public static final String INTEGER = "integer";
    public static final String LONG = "long";
    public static final String FLOAT = "float";
    public static final String DOUBLE = "double";
    public static final String REAL = "real";
    public static final String BOOLEAN = "boolean";
    public static final String STRING = "string";

    // Main data
    private int graphModelId;

    // schema parsing
    private String m_key;
    private String m_id;
    private String m_for;
    private String m_name;
    private String m_type;
    private String m_default;

    private StringBuffer stringBuffer = new StringBuffer();

    // node, edge, data parsing
    private boolean inSchema;
    private boolean inSubGraphSection = false;
    private boolean inHyperEdgeSection = false;
    private Map<String, GraphMLAttribute> vertexData = Maps.newHashMap();
    private Map<String, GraphMLAttribute> edgeData = Maps.newHashMap();
    private Map<String, GraphMLAttribute> graphData = Maps.newHashMap();
    
    private final Stack<GraphMLEndpoint> hyperEdgeEndpoints = new Stack<>();
    private final Stack<GraphMLGraph> lastGraph = new Stack<>();
    private final Stack<GraphMLVertex> lastVertices = new Stack<>();
    private final Stack<GraphMLEdge> lastEdges = new Stack<>();
    private final Stack<List<GraphMLAttribute>> lastAttributes = new Stack<>();

    private String attributeNameForGraphMLId;

    GraphMLParser(String graphName) {
        graphModelId = VGMainServiceHelper.graphDataBaseService.createGraph(graphName);

        attributeNameForGraphMLId = VGMainServiceHelper.config.getStringProperty(GraphOpenerGlobals.ATTRIBUTE_NAME_FOR_ID_IN_GRAPHML_FORMAT_KEY, GraphOpenerGlobals.DEFAULT_ATTRIBUTE_NAME_FOR_ID_IN_GRAPHML_FORMAT);
    }

    @Override
    public void startDocument() {
        inSchema = true;
    }

    @Override
    public void endDocument() throws SAXException {
        // if we don't reach the end of document, delete graph from model
    }

    @Override
    public void startElement(String namespaceURI, String localName, String qName, Attributes attributes) {
        // first clear the character buffer.
        stringBuffer.setLength(0);

        switch (qName) {
            case GRAPH: {
                String edef = attributes.getValue(EDGEDEF);
                if (edef != null && !edef.equalsIgnoreCase(DIRECTED) && !edef.equalsIgnoreCase(UNDIRECTED))
                    throw new RuntimeException("Unknown token: " + attributes.toString());

                boolean directed = StringUtils.equalsIgnoreCase(DIRECTED, edef) | edef == null;

                int ownerId;
                if (lastVertices.size() > 0) {
                    ownerId = lastVertices.peek().getId();
                } else {
                    ownerId = VGMainServiceHelper.graphDataBaseService.createVertex(graphModelId, VertexRecord.NO_OWNER_ID);
                }

                GraphMLGraph tmp_sg = new GraphMLGraph(ownerId, attributes.getValue(ID), directed);

                if (lastVertices.size() > 0) {
                    lastVertices.peek().setLinkToInnerGraph(tmp_sg);
                }

                lastGraph.push(tmp_sg);
                lastAttributes.add(new ArrayList<GraphMLAttribute>());
                inSubGraphSection = true;

                for (GraphMLAttribute attr : graphData.values()) {
                    if (attr.getValue() != null) {
                        lastAttributes.peek().add(new GraphMLAttribute(attr.getName(), attr.getValue(), attr.getType()));
                    }
                }

                GraphUtils.addInnerGraphNameAttribute(ownerId, tmp_sg.graphMLId);

                break;
            }

            case KEY: {
                Validate.isTrue(inSchema, "\"" + KEY + "\" element can't" + " occur after the first node or edge declaration.");
                m_for = attributes.getValue(FOR);
                m_id = attributes.getValue(ID);
                m_name = attributes.getValue(ATTRNAME);
                m_type = attributes.getValue(ATTRTYPE);
                break;
            }

            case NODE: {
                inSchema = false;
                inSubGraphSection = false;
                String graphMLNodeId = attributes.getValue(ID);
                int vertexId = VGMainServiceHelper.graphDataBaseService.createVertex(graphModelId, lastGraph.peek().getId());
                lastVertices.add(lastGraph.peek().addVertex(vertexId, graphMLNodeId));
                lastAttributes.add(new ArrayList<GraphMLAttribute>());
                for (GraphMLAttribute attr : vertexData.values()) {
                    if (attr.getValue() != null) {
                        lastAttributes.peek().add(new GraphMLAttribute(attr.getName(), attr.getValue(), attr.getType()));
                    }
                }

                break;
            }

            case PORT: {
                String graphMLPortId = attributes.getValue(NAME);
                if (inSubGraphSection) {
                    GraphMLGraph graph = lastGraph.peek();
                    graph.addPort(graphMLPortId, GraphUtils.createPort(graphModelId, graph.getId(), false, graph.getPorts().size()));
                } else {
                    GraphMLGraph graph = lastGraph.peek();
                    GraphMLVertex vertex = graph.getVertices().peek();
                    vertex.addPort(graphMLPortId, GraphUtils.createPort(graphModelId, vertex.getId(), false, vertex.getPorts().size()));
                }
                break;
            }

            case HYPEREDGE: {
                inHyperEdgeSection = true;
                hyperEdgeEndpoints.clear();
                break;
            }

            case ENDPOINT: {
                if (!inHyperEdgeSection)
                    throw new RuntimeException("Endpoint should place in hyperedge section");
                String node = attributes.getValue(NODE);
                String port = attributes.getValue(PORT);
                if (node == null)
                    throw new RuntimeException("Endpoint shouldn't contain null node");
                hyperEdgeEndpoints.add(new GraphMLEndpoint(node, port));
                break;
            }

            case EDGE: {
                inSchema = false;
                inSubGraphSection = false;
                lastAttributes.add(Lists.newArrayList());

                String graphMLEdgeId = attributes.getValue(ID);
                String srcGraphMLId = attributes.getValue(SOURCE);
                String trgGraphMLId = attributes.getValue(TARGET);
                String srcPortGraphMLId = attributes.getValue(SOURCE_PORT);
                String trgPortGraphMLId = attributes.getValue(TARGET_PORT);

                GraphMLVertex srcGraphMLVertex = null;
                GraphMLVertex trgGraphMLVertex = null;
                boolean directed = true;
                for (GraphMLGraph graphMLGraph : lastGraph) {
                    GraphMLVertex tmpSrcGraphMLVertex = graphMLGraph.getVertexByGraphMLId(srcGraphMLId);
                    GraphMLVertex tmpTrgGraphMLVertex = graphMLGraph.getVertexByGraphMLId(trgGraphMLId);
                    if (tmpSrcGraphMLVertex != null && srcGraphMLVertex == null)
                        srcGraphMLVertex = tmpSrcGraphMLVertex;
                    if (tmpTrgGraphMLVertex != null && trgGraphMLVertex == null)
                        trgGraphMLVertex = tmpTrgGraphMLVertex;

                    if (srcGraphMLVertex != null && trgGraphMLVertex != null) {
                        directed = graphMLGraph.isDirected();
                        break;
                    }
                }

                Validate.isTrue(srcGraphMLVertex != null && trgGraphMLVertex != null,
                        String.format(
                                "Wrong format. Source GraphML id is '%s', target GraphML id is '%s'",
                                srcGraphMLId,
                                trgGraphMLId));

                Integer srcId = srcGraphMLVertex.getId();
                Integer trgId = trgGraphMLVertex.getId();
                Integer srcPortId = null, trgPortId = null;
                if (srcPortGraphMLId != null) {
                    srcPortId = srcGraphMLVertex.getPorts().get(srcPortGraphMLId);

                    // if port places in graph section of node
                    if (srcPortId == null && srcGraphMLVertex.getLinkToInnerGraph() != null) {
                        srcPortId = srcGraphMLVertex.getLinkToInnerGraph().getPorts().get(srcPortGraphMLId);
                    }
                }
                if (trgPortGraphMLId != null) {
                    trgPortId = trgGraphMLVertex.getPorts().get(trgPortGraphMLId);

                    // if port places in graph section of node
                    if (trgPortId == null && trgGraphMLVertex.getLinkToInnerGraph() != null) {
                        trgPortId = trgGraphMLVertex.getLinkToInnerGraph().getPorts().get(trgPortGraphMLId);
                    }
                }

                List<Integer> edgesIds = GraphUtils.createEdge(graphModelId, srcId, trgId, srcPortId == null ? -1 : srcPortId, trgPortId == null ? -1 : trgPortId, directed);
                int edgeId = edgesIds.get(0);
                for (GraphMLAttribute attr : edgeData.values()) {
                    if (attr.getValue() != null) {
                        lastAttributes.peek().add(new GraphMLAttribute(attr.getName(), attr.getValue(), attr.getType()));
                    }
                }

                lastEdges.add(new GraphMLEdge(edgeId, graphMLEdgeId, srcGraphMLId, trgGraphMLId, srcPortGraphMLId, trgPortGraphMLId));
                break;
            }

            case DATA: {
                m_key = attributes.getValue(KEY);
                Validate.notEmpty(m_key, "Key must not be null or empty");
                break;
            }

            case GRAPHML: {
                // do nothing
                break;
            }

            default: {
                VGMainServiceHelper.logger.printInfo("Unknown token: " + qName);
            }
        }
    }

    @Override
    public void endElement(String namespaceURI, String localName, String qName) throws SAXException {
        switch (qName) {
            case DEFAULT: {
                m_default = stringBuffer.toString();
                break;
            }

            case KEY: {
                addToSchema();
                break;
            }

            case GRAPH: {
                inSubGraphSection = false;
                lastGraph.pop();
                lastAttributes.pop();
                break;
            }

            case NODE: {
                inSubGraphSection = true;

                GraphMLVertex graphMLVertex = lastVertices.pop();
                List<GraphMLAttribute> list_a = lastAttributes.pop();
                for (GraphMLAttribute buf : list_a) {
                    VGMainServiceHelper.graphDataBaseService.createVertexAttribute(graphMLVertex.getId(), buf.getName(), buf.getValue(), buf.getType(), true);
                }
                if (!StringUtils.isEmpty(graphMLVertex.getGraphMLId()))
                    VGMainServiceHelper.graphDataBaseService.createVertexAttribute(graphMLVertex.getId(), attributeNameForGraphMLId, graphMLVertex.getGraphMLId(), AttributeRecord.STRING_ATTRIBUTE_TYPE, true);
                break;
            }

            case EDGE: {
                inSubGraphSection = true;

                GraphMLEdge graphMLEdge = lastEdges.pop();
                List<GraphMLAttribute> list_a = lastAttributes.pop();
                if (graphMLEdge.getId() >= 0) {
                    for (GraphMLAttribute buf : list_a) {
                        VGMainServiceHelper.graphDataBaseService.createEdgeAttribute(graphMLEdge.getId(), buf.getName(), buf.getValue(), buf.getType(), true);
                    }

                    if (!StringUtils.isEmpty(graphMLEdge.getGraphMLId()))
                        VGMainServiceHelper.graphDataBaseService.createEdgeAttribute(graphMLEdge.getId(), attributeNameForGraphMLId, graphMLEdge.getGraphMLId(), AttributeRecord.STRING_ATTRIBUTE_TYPE, true);
                }
                break;
            }

            case HYPEREDGE: {
                inHyperEdgeSection = false;
                if (hyperEdgeEndpoints.size() > 1) {
                    GraphMLEndpoint sourceEndpoint = hyperEdgeEndpoints.get(0);
                    for (int i = 1; i < hyperEdgeEndpoints.size(); i++) {
                        createHyperEdge(sourceEndpoint, hyperEdgeEndpoints.get(i), lastGraph.peek());
                        sourceEndpoint = hyperEdgeEndpoints.get(i);
                    }
                    createHyperEdge(sourceEndpoint, hyperEdgeEndpoints.get(0), lastGraph.peek());
                }

                break;
            }

            case DATA: {
                GraphMLAttribute graphMLAttribute;
                if (inSubGraphSection)
                    graphMLAttribute = graphData.get(m_key);
                else if (vertexData.containsKey(m_key))
                    graphMLAttribute = vertexData.get(m_key);
                else if (edgeData.containsKey(m_key))
                    graphMLAttribute = edgeData.get(m_key);
                else {
                    graphMLAttribute = new GraphMLAttribute(m_key, "", graphMLAttributeType2VisualGraphAttributeType(STRING));
                }

                String strValue = StringEscapeUtils.unescapeJava(stringBuffer.toString());
                int index = lastAttributes.lastElement().indexOf(graphMLAttribute);
                if (index >= 0) {
                    lastAttributes.lastElement().get(index).setValue(strValue);
                } else {
                    lastAttributes.lastElement().add(new GraphMLAttribute(graphMLAttribute.getName(), strValue, graphMLAttribute.getType()));
                }
                break;
            }
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        stringBuffer.append(ch, start, length);
    }
    
    public int getGraphModelId() throws Exception {
        return graphModelId;
    }

//==============================================================================
//-----------------------PRIVATE METHODS----------------------------------------
    private void createHyperEdge(GraphMLEndpoint source, GraphMLEndpoint target, GraphMLGraph graph) {
        GraphMLVertex sourceVertex = graph.getVertexByGraphMLId(source.getGraphMLNodeId());
        GraphMLVertex targetVertex = graph.getVertexByGraphMLId(target.getGraphMLNodeId());

        if (sourceVertex == null || targetVertex == null) {
            VGMainServiceHelper.logger.printInfo("Skip edge between different nested graphs. Source = " + source.getGraphMLNodeId() + ", target = " + target.getGraphMLNodeId());
        } else {
            int sourceId = sourceVertex.getId();
            int targetId = targetVertex.getId();

            GraphUtils.createEdge(graphModelId, sourceId, targetId, -1, -1, graph.isDirected());
        }
    }

    private void addToSchema() throws SAXException {
        if (m_name == null || m_name.length() == 0) {
            throw new SAXException("Empty " + KEY + " name.");
        }
        if (m_type == null || m_type.length() == 0) {
            throw new SAXException("Empty " + KEY + " type.");
        }

        int type = graphMLAttributeType2VisualGraphAttributeType(m_type);
        String strValue = m_default;

        if (m_for == null || m_for.equals(ALL)) {
            vertexData.put(m_id, new GraphMLAttribute(m_name, strValue, type));
            edgeData.put(m_id, new GraphMLAttribute(m_name, strValue, type));
            graphData.put(m_id, new GraphMLAttribute(m_name, strValue, type));
        } else if (m_for.equals(NODE)) {
            vertexData.put(m_id, new GraphMLAttribute(m_name, strValue, type));
        } else if (m_for.equals(EDGE)) {
            edgeData.put(m_id, new GraphMLAttribute(m_name, strValue, type));
        } else if (m_for.equals(GRAPH)) {
            graphData.put(m_id, new GraphMLAttribute(m_name, strValue, type));
        } else {
            throw new SAXException("Unrecognized \"" + FOR + "\" value: " + m_for);
        }

        m_default = null;
    }

    private static int graphMLAttributeType2VisualGraphAttributeType(String type) {
        if (type == null || type.equals(STRING))
            return AttributeRecord.STRING_ATTRIBUTE_TYPE;

        if (type.equals(BOOLEAN))
            return AttributeRecord.BOOLEAN_ATTRIBUTE_TYPE;

        if (type.equals(DOUBLE) || type.equals(FLOAT) || type.equals(REAL))
            return AttributeRecord.DOUBLE_ATTRIBUTE_TYPE;

        if (type.equals(INT) || type.equals(INTEGER) || type.equals(LONG))
            return AttributeRecord.INTEGER_ATTRIBUTE_TYPE;

        return AttributeRecord.STRING_ATTRIBUTE_TYPE;
    }

//==============================================================================
//------------------PRIVATE CLASSES---------------------------------------------
    private static class GraphMLGraph {
        private final int id;
        private final Stack<GraphMLVertex> vertices = new Stack<>();
        private final boolean directed;
        private final String graphMLId;
        private final Map<String, Integer> ports = Maps.newLinkedHashMap();

        public GraphMLGraph(int id, String graphMLId, boolean directed) {
            this.id = id;
            this.graphMLId = graphMLId;
            this.directed = directed;
        }
        public GraphMLVertex addVertex(int id, String graphMLId) {
            GraphMLVertex graphMLVertex = new GraphMLVertex(id, graphMLId);
            vertices.add(graphMLVertex);
            return graphMLVertex;
        }

        public int getId() {
            return id;
        }

        public boolean isDirected() {
            return directed;
        }

        public Stack<GraphMLVertex> getVertices() {
            return vertices;
        }

        public GraphMLVertex getVertexByGraphMLId(String graphMLId) {
            for (GraphMLVertex vertex : vertices) {
                if (StringUtils.equals(vertex.getGraphMLId(), graphMLId))
                    return vertex;
            }
            for (GraphMLVertex vertex : vertices) {
                if (vertex.getLinkToInnerGraph() != null) {
                    GraphMLVertex result = vertex.getLinkToInnerGraph().getVertexByGraphMLId(graphMLId);
                    if (result != null) {
                        return result;
                    }
                }
            }
            return null;
        }

        public void addPort(String graphMLId, int portId) {
            ports.put(graphMLId, portId);
        }

        public Map<String, Integer> getPorts() {
            return ports;
        }
    }

    private static class GraphMLVertex {
        private final String graphMLId;
        private final int id;
        private final Map<String, Integer> ports = Maps.newLinkedHashMap();
        private GraphMLGraph linkToInnerGraph = null;

        GraphMLVertex(int id, String graphMLId) {
            this.id = id;
            this.graphMLId = graphMLId;
        }

        public int getId() {
            return id;
        }

        public String getGraphMLId() {
            return graphMLId;
        }

        public void addPort(String graphMLId, int portId) {
            ports.put(graphMLId, portId);
        }

        public Map<String, Integer> getPorts() {
            return ports;
        }

        public GraphMLGraph getLinkToInnerGraph() {
            return linkToInnerGraph;
        }

        public void setLinkToInnerGraph(GraphMLGraph linkToInnerGraph) {
            this.linkToInnerGraph = linkToInnerGraph;
        }
    }

    private static class GraphMLEdge {
        private final String graphMLId;
        private final int id;
        private final String graphMLSourceId;
        private final String graphMLTargetId;
        private final String graphMLSourcePortId;
        private final String graphMLTargetPortId;

        GraphMLEdge(int id, String graphMLId, String graphMLSourceId, String graphMLTargetId, String graphMLSourcePortId, String graphMLTargetPortId) {
            this.id = id;
            this.graphMLId = graphMLId;
            this.graphMLSourceId = graphMLSourceId;
            this.graphMLTargetId = graphMLTargetId;
            this.graphMLSourcePortId = graphMLSourcePortId;
            this.graphMLTargetPortId = graphMLTargetPortId;
        }

        public int getId() {
            return id;
        }

        String getGraphMLId() {
            return graphMLId;
        }
    }

    private static class GraphMLEndpoint {
        private final String graphMLNodeId;
        private final String graphMLPort;

        public GraphMLEndpoint(String graphMLNodeId, String graphMLPort) {
            this.graphMLNodeId = graphMLNodeId;
            this.graphMLPort = graphMLPort;
        }

        public String getGraphMLNodeId() {
            return graphMLNodeId;
        }

        public String getGraphMLPort() {
            return graphMLPort;
        }
    }

    private static class GraphMLAttribute {
        private final String name;
        private String value;
        private int type;

        public GraphMLAttribute (String name, String value, int type) {
            this.name = name;
            this.type = type;
            this.value = value;
        }

        public int getType() {
            return type;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public String getName() {
            return name;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            GraphMLAttribute that = (GraphMLAttribute) o;

            return !(name != null ? !name.equals(that.name) : that.name != null);
        }

        @Override
        public int hashCode() {
            return name != null ? name.hashCode() : 0;
        }
    }
}