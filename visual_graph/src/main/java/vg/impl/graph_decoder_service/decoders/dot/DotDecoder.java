package vg.impl.graph_decoder_service.decoders.dot;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.NotImplementedException;
import vg.impl.main_service.VGMainServiceHelper;
import vg.interfaces.data_base_service.data.record.AttributeRecord;
import vg.interfaces.data_base_service.data.record.VertexRecord;
import vg.interfaces.graph_decoder_service.GraphDecoder;
import vg.plugins.opener.GraphOpenerGlobals;
import vg.shared.graph.utils.GraphUtils;

import java.io.*;
import java.util.*;
import java.util.regex.Pattern;

/**
 * Dot decoder.
 *
 * gcc test.c -fdump-tree-all-graph
 *
 * @author Timur Zolotuhin (e-mail: tzolotuhin@gmail.com)
 */
public class DotDecoder implements GraphDecoder {
    // Constants: key words
    private static final String STRICT = "strict";
    private static final String UNDIRECTED_GRAPH = "graph";
    private static final String DIRECTED_GRAPH = "digraph";
    private static final String GRAPH = "graph";
    private static final String NODE = "node";
    private static final String EDGE = "edge";
    private static final String SUBGRAPH = "subgraph";

    private static final String BEGIN_ATTR = "[";
    private static final String END_ATTR = "]";
    private static final String BEGIN_STMT = "{";
    private static final String END_STMT = "}";
    private static final String DELIMITER = ";";
    private static final String EQUAL = "=";
    private static final String COMMA = ",";
    private static final String DIRECTED_EDGE = "->";
    private static final String UNDIRECTED_EDGE = "--";
    private static final String COLON = ":";

    // Main data
    private DotTokenizer currDotTokenizer;

    private Stack<BiMap<String, Integer>> vertexDotID2databaseID;
    private Stack<BiMap<String, Integer>> edgeDotID2databaseID;

    private BiMap<String, Integer> currVertexDotID2databaseID;
    private BiMap<String, Integer> currEdgeDotID2databaseID;

    private String attributeNameForDotId;

    @Override
    public List<Integer> decode(File file, String graphName) throws Exception {
        attributeNameForDotId = VGMainServiceHelper.config.getStringProperty(GraphOpenerGlobals.ATTRIBUTE_NAME_FOR_ID_IN_DOT_FORMAT_KEY, GraphOpenerGlobals.DEFAULT_ATTRIBUTE_NAME_FOR_ID_IN_DOT_FORMAT);

        currVertexDotID2databaseID = null;
        currEdgeDotID2databaseID = null;

        vertexDotID2databaseID = new Stack<>();
        edgeDotID2databaseID = new Stack<>();

        String content = FileUtils.readFileToString(file, "UTF-8");
        content = content.replace("\r", "");
        StringReader stringReader = new StringReader(content);
        currDotTokenizer = new DotTokenizer(stringReader);

        int graphId = VGMainServiceHelper.graphDataBaseService.createGraph(graphName);

        while (currDotTokenizer.peekToken() != null) {
            if (isNextGraph())
                parseGraph(graphId);
            else {
                throw new DotDecoderException(currDotTokenizer.getCurrentLineNumber(), currDotTokenizer.peekToken(), GRAPH);
            }
        }

        return Collections.singletonList(graphId);
    }

    private void parseGraph(int graphId) throws Exception {
        VGMainServiceHelper.logger.printDebug(String.format("parseGraph with graphId '%s'.", graphId));

        String id = null;

        if (isStrictToken(currDotTokenizer.peekToken())) {
            currDotTokenizer.nextToken();
        }

        boolean directed = isDirectedGraphToken(currDotTokenizer.peekToken());
        if (isDirectedGraphToken(currDotTokenizer.peekToken()) | isUndirectedGraphToken(currDotTokenizer.peekToken())) {
            currDotTokenizer.nextToken();
        }

        if (isNextID()) {
            id = readID(currDotTokenizer.peekToken());
            currDotTokenizer.nextToken();
        }

        if (!isBeginStmtToken(currDotTokenizer.peekToken())) {
            throw new DotDecoderException(currDotTokenizer.getCurrentLineNumber(), currDotTokenizer.peekToken(), BEGIN_STMT);
        } else {
            currDotTokenizer.nextToken();

            int vertexId = VGMainServiceHelper.graphDataBaseService.createVertex(graphId, VertexRecord.NO_OWNER_ID);

            if (id != null)
                VGMainServiceHelper.graphDataBaseService.createVertexAttribute(vertexId, attributeNameForDotId, id, AttributeRecord.STRING_ATTRIBUTE_TYPE, true);

            currVertexDotID2databaseID = HashBiMap.create();
            currEdgeDotID2databaseID = HashBiMap.create();

            vertexDotID2databaseID.add(currVertexDotID2databaseID);
            edgeDotID2databaseID.add(currEdgeDotID2databaseID);

            parseStmtList(graphId, vertexId, directed);

            currVertexDotID2databaseID = vertexDotID2databaseID.pop();
            currEdgeDotID2databaseID = edgeDotID2databaseID.pop();

            if (!isEndStmtToken(currDotTokenizer.nextToken())) {
                throw new DotDecoderException(currDotTokenizer.getCurrentLineNumber(), currDotTokenizer.peekToken(), END_STMT);
            }
        }
    }

    private void parseStmtList(int graphId, int vertexOwnerId, boolean directed) throws Exception {
        VGMainServiceHelper.logger.printDebug(String.format("parseStmtList with graphId '%s', vertexOwnerId '%s', directed '%s'.", graphId, vertexOwnerId, directed));

        while (isNextStmt()) {
            if (isDelimiterToken(currDotTokenizer.peekToken()))
                currDotTokenizer.nextToken();
            parseStmt(graphId, vertexOwnerId, directed);
        }
    }

    private void parseStmt(int graphId, int vertexOwnerId, boolean directed) throws Exception {
        VGMainServiceHelper.logger.printDebug(String.format("parseStmt with graphId '%s', vertexOwnerId '%s', directed '%s'.", graphId, vertexOwnerId, directed));

        if (!isNextStmt())
            return;

        if (isNextAttrStmt()) {
            parseAttrStmt(graphId);
        } else if (isNextSubGraph()) {
            parseSubGraph(graphId, vertexOwnerId, directed);
        } else if (isNextAttr()) {
            parseAttr();
            //AbstractMap.SimpleEntry<String, String> pair = parseAttr();
            //VGMainServiceHelper.graphDataBaseService.createGraphAttributeHeader(graphId, pair.getKey(), pair.getValue(), AttributeRecord.STRING_ATTRIBUTE_TYPE);
        } else {
            parseNodeAndEdgeStmt(graphId, vertexOwnerId, directed);
        }
    }

    private String parseNodeId() throws Exception {
        String id = readID(currDotTokenizer.nextToken());
        if (isNextPort()) {
            parsePort();
        }
        return id;
    }

    private void parsePort() throws Exception {
        if (isNextCompassPT()) {
            parseCompassPT();
        } else {
            if (!isColonToken(currDotTokenizer.nextToken()))
                throw new DotDecoderException(currDotTokenizer.getCurrentLineNumber(), currDotTokenizer.peekToken(), COLON);

            if (isNextID())
                readID(currDotTokenizer.nextToken());
            if (isNextCompassPT())
                parseCompassPT();
        }
    }

    private void parseCompassPT () throws Exception {
        currDotTokenizer.nextToken();
        currDotTokenizer.nextToken();
    }

    private void parseNodeAndEdgeStmt(int graphId, int vertexOwnerId, boolean directed) throws Exception {
        VGMainServiceHelper.logger.printDebug(String.format("parseNodeAndEdgeStmt with graphId '%s', vertexOwnerId '%s', directed '%s'.", graphId, vertexOwnerId, directed));

        int sourceVertexId;
        if (isNextSubGraph()) {
            sourceVertexId = parseSubGraph(graphId, vertexOwnerId, directed);
        } else {
            String nodeId = parseNodeId();
            if (!currVertexDotID2databaseID.containsKey(nodeId)) {
                int vertexId = VGMainServiceHelper.graphDataBaseService.createVertex(graphId, vertexOwnerId);
                currVertexDotID2databaseID.put(nodeId, vertexId);
                VGMainServiceHelper.graphDataBaseService.createVertexAttribute(vertexId, attributeNameForDotId, nodeId, AttributeRecord.STRING_ATTRIBUTE_TYPE, true);
            }
            sourceVertexId = currVertexDotID2databaseID.get(nodeId);

            if (!isNextEdgeOp(currDotTokenizer.peekToken())) {
                // note: it's node statement and next parse it as node statement
                if (isNextAttrList()) {
                    Map<String, String> result = parseAttrList();
                    for (String key : result.keySet()) {
                        VGMainServiceHelper.graphDataBaseService.createVertexAttribute(sourceVertexId, key, result.get(key), AttributeRecord.STRING_ATTRIBUTE_TYPE, true);
                    }
                    return;
                }
            }
        }

        List<Integer> dataBaseEdgeIds = parseEdgeRHS(graphId, vertexOwnerId, sourceVertexId, new ArrayList<>(), directed);

        if (isNextAttrList()) {
            Map<String, String> result = parseAttrList();
            for (Integer dataBaseEdgeId : dataBaseEdgeIds) {
                for (String key : result.keySet()) {
                    VGMainServiceHelper.graphDataBaseService.createEdgeAttribute(dataBaseEdgeId, key, result.get(key), AttributeRecord.STRING_ATTRIBUTE_TYPE, true);
                }
            }
        }
    }

    private List<Integer> parseEdgeRHS(int graphId, int vertexOwnerId, int sourceVertexId, List<Integer> edgeIds, boolean directed) throws Exception {
        VGMainServiceHelper.logger.printDebug(String.format("parseEdgeRHS with graphId '%s', vertexOwnerId '%s', sourceVertexId '%s', directed '%s'.", graphId, vertexOwnerId, sourceVertexId, directed));

        parseEdgeOP();

        int targetVertexId;
        if (isNextSubGraph()) {
            targetVertexId = parseSubGraph(graphId, vertexOwnerId, directed);
        } else {
            String nodeId = parseNodeId();
            if (!currVertexDotID2databaseID.containsKey(nodeId)) {
                int vertexId = VGMainServiceHelper.graphDataBaseService.createVertex(graphId, vertexOwnerId);
                currVertexDotID2databaseID.put(nodeId, vertexId);
                VGMainServiceHelper.graphDataBaseService.createVertexAttribute(vertexId, attributeNameForDotId, nodeId, AttributeRecord.STRING_ATTRIBUTE_TYPE, true);
            }
            targetVertexId = currVertexDotID2databaseID.get(nodeId);
        }

        // create edge in data base
        try {
            edgeIds.addAll(GraphUtils.createEdge(graphId, sourceVertexId, targetVertexId, -1, -1, directed));
        } catch (IllegalArgumentException ex) {
            VGMainServiceHelper.logger.printDebug(ex.getMessage());


        }

        if (isNextEdgeRHS()) {
            parseEdgeRHS(graphId, vertexOwnerId, targetVertexId, edgeIds, directed);
        }

        return edgeIds;
    }

    private boolean parseEdgeOP() throws Exception {
        if (!isNextEdgeRHS())
            throw new DotDecoderException(currDotTokenizer.getCurrentLineNumber(), currDotTokenizer.peekToken(), DIRECTED_EDGE + "|" + UNDIRECTED_EDGE);
        return currDotTokenizer.nextToken().equalsIgnoreCase(DIRECTED_EDGE);
    }

    private void parseAttrStmt(int graphId) throws Exception {
        VGMainServiceHelper.logger.printDebug(String.format("parseAttrStmt with graphId '%s'.", graphId));

        String token = currDotTokenizer.nextToken();
        switch (token) {
            case GRAPH:
            case NODE:
            case EDGE:
                throw new NotImplementedException("Please, implement it");
            default:
                throw new DotDecoderException(currDotTokenizer.getCurrentLineNumber(), token, GRAPH + "|" + NODE + "|" + EDGE);
        }
    }

    private Map<String, String> parseAttrList() throws Exception {
        if (!isNextAttrList())
            throw new DotDecoderException(currDotTokenizer.getCurrentLineNumber(), currDotTokenizer.peekToken(), BEGIN_ATTR);

        Map<String, String> result = Maps.newHashMap();
        while (isNextAttrList()) {
            currDotTokenizer.nextToken();

            result.putAll(parseAList());

            if (currDotTokenizer.peekToken() == null || !currDotTokenizer.peekToken().equals(END_ATTR))
                throw new DotDecoderException(currDotTokenizer.getCurrentLineNumber(), currDotTokenizer.peekToken(), END_ATTR);
            currDotTokenizer.nextToken();
        }
        return result;
    }

    private Map<String, String> parseAList() throws Exception {
        AbstractMap.SimpleEntry<String, String> pair = parseAttr();

        Map<String, String> result = Maps.newHashMap();
        result.put(pair.getKey(), pair.getValue());
        while (isDelimiterToken(currDotTokenizer.peekToken()) || isCommaToken(currDotTokenizer.peekToken())) {
            currDotTokenizer.nextToken();
            pair = parseAttr();
            result.put(pair.getKey(), pair.getValue());
        }

        return result;
    }

    private AbstractMap.SimpleEntry<String, String> parseAttr() throws Exception {
        if (!isNextAttr())
            throw new DotDecoderException(currDotTokenizer.getCurrentLineNumber(), currDotTokenizer.peekToken(), "ID" + EQUAL + "ID");

        String key = readID(currDotTokenizer.nextToken());
        currDotTokenizer.nextToken();
        String value = readID(currDotTokenizer.nextToken());
        return new AbstractMap.SimpleEntry<>(key, value);
    }

    private int parseSubGraph(int graphId, int vertexOwnerId, boolean directed) throws Exception {
        VGMainServiceHelper.logger.printDebug(String.format("parseSubGraph with graphId '%s', vertexOwnerId '%s', directed '%s'.", graphId, vertexOwnerId, directed));

        int subGraphId = -1;

        if (currDotTokenizer.hasNextToken()) {
            String token = currDotTokenizer.nextToken();

            if (token.equals(SUBGRAPH)) {
                token = currDotTokenizer.nextToken();
            }

            if (!token.equals(BEGIN_STMT)) {
                token = currDotTokenizer.nextToken();
            }

            if (token.equals(BEGIN_STMT)) {
                subGraphId = VGMainServiceHelper.graphDataBaseService.createVertex(graphId, vertexOwnerId);

                currVertexDotID2databaseID = HashBiMap.create();
                currEdgeDotID2databaseID = HashBiMap.create();

                vertexDotID2databaseID.add(currVertexDotID2databaseID);
                edgeDotID2databaseID.add(currEdgeDotID2databaseID);

                parseStmtList(graphId, subGraphId, directed);

                currVertexDotID2databaseID = vertexDotID2databaseID.pop();
                currEdgeDotID2databaseID = edgeDotID2databaseID.pop();
            }

            token = currDotTokenizer.nextToken();
            if (token == null || !token.equals(END_STMT)) {
                throw new Exception("[parseSubGraph][line number = " + currDotTokenizer.getCurrentLineNumber() + "] Unknown symbol = " + token + ". Symbol expected = " + END_STMT);
            }
        }

        return subGraphId;
    }

    private boolean isNextStmt() {
        return currDotTokenizer.peekToken() != null && !currDotTokenizer.peekToken().equals(END_STMT);
    }

    private boolean isNextSubGraph() {
        return currDotTokenizer.peekToken() != null && (currDotTokenizer.peekToken().equals(SUBGRAPH) || currDotTokenizer.peekToken().equals(BEGIN_STMT));
    }

    private boolean isNextPort() {
        String token = currDotTokenizer.peekToken();

        return  token != null && token.equals(COLON);
    }

    private boolean isNextCompassPT() {
        if (!isColonToken(currDotTokenizer.peekToken())) return false;

        Pattern pattern = Pattern.compile("^n$|^ne$|^e$|^se$|^s$|^sw$|^w$|^nw$|^c$|^_$");
        return currDotTokenizer.peekToken(1) != null && pattern.matcher(currDotTokenizer.peekToken(1)).matches();
    }

    private boolean isNextGraph() {
        return currDotTokenizer.hasNextToken() &&
                (isStrictToken(currDotTokenizer.peekToken()) ||
                        isDirectedGraphToken(currDotTokenizer.peekToken()) ||
                        isUndirectedGraphToken(currDotTokenizer.peekToken()));
    }

    private boolean isNextAttrStmt() {
        return !(currDotTokenizer.peekToken() == null || currDotTokenizer.peekToken(1) == null) &&
                (currDotTokenizer.peekToken().equals(UNDIRECTED_GRAPH) ||
                currDotTokenizer.peekToken().equals(NODE) ||
                currDotTokenizer.peekToken().equals(EDGE)) && currDotTokenizer.peekToken(1).equals(BEGIN_ATTR);
    }

    private boolean isNextAttrList() {
        return currDotTokenizer.peekToken() != null && currDotTokenizer.peekToken().equals(BEGIN_ATTR);
    }

    private boolean isNextAttr() {
        return !(currDotTokenizer.peekToken() == null || currDotTokenizer.peekToken(1) == null || currDotTokenizer.peekToken(2) == null) &&
                (isTokenID(currDotTokenizer.peekToken()) && currDotTokenizer.peekToken(1).equals(EQUAL) && isTokenID(currDotTokenizer.peekToken(2)));
    }

    private boolean isNextEdgeRHS() {
        return isNextEdgeOp(currDotTokenizer.peekToken());
    }

    private boolean isNextID() {
        return isTokenID(currDotTokenizer.peekToken());
    }

//==============================================================================
//------------------STATIC METHODS----------------------------------------------
    private static boolean isTokenID(String token) {
        if (token == null) return false;

        if (token.startsWith("\"") && token.endsWith("\""))
            return true;
        if (token.startsWith("'") && token.endsWith("'"))
            return true;
        if (token.startsWith("<") && token.endsWith(">"))
            return true;

        if (token.contains(" "))
            return false;

        Pattern idPattern = Pattern.compile("^[a-zA-Z0-9_\\x200-\\x377]+$");
        Pattern numberPattern = Pattern.compile("^[0-9]+|[0-9]+.[0-9]+");

        return idPattern.matcher(token).matches() || numberPattern.matcher(token).matches();
    }

    private static boolean isBeginStmtToken(String token) {
        return token != null && token.equalsIgnoreCase(BEGIN_STMT);
    }

    private static boolean isEndStmtToken(String token) {
        return token != null && token.equalsIgnoreCase(END_STMT);
    }

    private static boolean isDirectedGraphToken(String token) {
        return token != null && token.equalsIgnoreCase(DIRECTED_GRAPH);
    }

    private static boolean isUndirectedGraphToken(String token) {
        return token != null && token.equalsIgnoreCase(UNDIRECTED_GRAPH);
    }

    private static boolean isStrictToken(String token) {
        return token != null && token.equalsIgnoreCase(STRICT);
    }

    private static boolean isDelimiterToken(String token) {
        return token != null && token.equalsIgnoreCase(DELIMITER);
    }

    private static boolean isCommaToken(String token) {
        return token != null && token.equalsIgnoreCase(COMMA);
    }

    private static boolean isColonToken(String token) {
        return token != null && token.equalsIgnoreCase(COLON);
    }

    private static boolean isNextEdgeOp(String token) {
        return token != null && (token.equals(DIRECTED_EDGE) || token.equals(UNDIRECTED_EDGE));
    }

    private static String readID(String token) throws Exception {
        if (token.startsWith("\"") && token.endsWith("\""))
            token = token.substring(1).substring(0, token.length() - 2);

        if (token.startsWith("'") && token.endsWith("'"))
            token = token.substring(1).substring(0, token.length() - 2);

        return token;
    }

//==============================================================================
//------------------PRIVATE CLASSES---------------------------------------------
    private static class DotTokenizer {
        private String prevToken = null;
        private StreamTokenizer tokenizer;
        private List<String> tokens;
        private List<Integer> lineNumbers;

        DotTokenizer(Reader reader) {
            tokenizer = new StreamTokenizer(reader);
            tokenizer.slashSlashComments(true);
            tokenizer.slashStarComments(true);
            tokenizer.wordChars('_', '_');

            tokens = Lists.newArrayList();
            lineNumbers = Lists.newArrayList();
            readTokens(100);
        }

        boolean hasNextToken() {
            if (tokens.size() == 0)
                readTokens(100);

            return tokens.size() != 0;
        }

        String nextToken() {
            if (hasNextToken()) {
                String value = tokens.get(0);
                tokens.remove(0);
                lineNumbers.remove(0);
                return value;
            } else {
                return null;
            }
        }

        String peekToken() {
            return peekToken(0);
        }

        String peekToken(int number) {
            if (tokens.size() <= number) {
                readTokens(number - tokens.size() + 5);
                if (tokens.size() > number)
                    return tokens.get(number);
            } else {
                return tokens.get(number);
            }
            return null;
        }

        private void readTokens(int count) {
            while (count > 0) {
                try {
                    int tokenValueType;
                    String value = null;
                    if ((tokenValueType = tokenizer.nextToken()) != StreamTokenizer.TT_EOF) {
                        switch (tokenValueType) {
                            case StreamTokenizer.TT_WORD:
                                value = tokenizer.sval;
                                break;

                            case StreamTokenizer.TT_NUMBER:
                                value = Double.toString(tokenizer.nval);
                                break;

                            default:
                                if ((char)tokenValueType == '\"' || (char)tokenValueType == '\'') {
                                    value = Character.toString((char)tokenValueType) + tokenizer.sval + Character.toString((char)tokenValueType);
                                } else {
                                    value = Character.toString((char)tokenValueType);
                                }
                                break;
                        }
                    }

                    if (value != null) {
                        if (prevToken != null && prevToken.equals("-") && (value.equals(">") || value.equals("-"))) {
                            value = prevToken + value;
                            tokens.remove(tokens.size() - 1);
                            lineNumbers.remove(lineNumbers.size() - 1);
                        }

                        if (value.equals("-"))
                            count++;

                        VGMainServiceHelper.logger.printDebug(String.format("Token: '%s'.", value));
                        tokens.add(value);
                        lineNumbers.add(tokenizer.lineno());

                        prevToken = value;
                    }
                } catch (Exception ex) {
                    VGMainServiceHelper.logger.printException(ex);
                }
                count--;
            }
        }

       private int getCurrentLineNumber() {
           return lineNumbers.size() == 0 ? -1 : lineNumbers.get(0);
       }
    }

    public static class DotDecoderException extends Exception {
        DotDecoderException(int lineNumber, String token, String expectedToken) {
            super("Can't parse following token: " + token + ", expected token: " + expectedToken + ", line number: " + lineNumber);
        }
    }
}
