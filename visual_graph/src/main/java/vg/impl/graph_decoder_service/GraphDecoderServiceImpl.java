package vg.impl.graph_decoder_service;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import vg.shared.utils.FileUtils;
import vg.impl.graph_decoder_service.decoders.dot.DotDecoder;
import vg.impl.graph_decoder_service.decoders.gml.GMLDecoder;
import vg.impl.graph_decoder_service.decoders.graphml.GraphMLDecoder;
import vg.impl.graph_decoder_service.decoders.zip.ZipDecoder;
import vg.impl.main_service.VGMainServiceHelper;
import vg.interfaces.graph_decoder_service.GraphDecoder;
import vg.interfaces.graph_decoder_service.GraphDecoderService;
import vg.plugins.opener.GraphOpenerGlobals;

import java.io.*;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.util.*;

/**
 * @author Timur Zolotuhin (e-mail: tzolotuhin@gmail.com)
 */
public class GraphDecoderServiceImpl implements GraphDecoderService {
    // Main data
    private HashMap<String,GraphDecoder> decoders;
    private ArrayList<String> openedFilesList;

    // Mutex
    // TODO: Please fix sync
    //private final Object generalMutex = new Object();

    public GraphDecoderServiceImpl() {
        decoders = Maps.newLinkedHashMap();
        openedFilesList = Lists.newArrayList();

        decoders.put(GraphOpenerGlobals.GRAPHML_FORMAT, new GraphMLDecoder());
        decoders.put(GraphOpenerGlobals.DOT_FORMAT, new DotDecoder());
        decoders.put(GraphOpenerGlobals.GV_FORMAT, new DotDecoder());
        decoders.put(GraphOpenerGlobals.GML_FORMAT, new GMLDecoder());

        decoders.put(GraphOpenerGlobals.ZIP_FORMAT, new ZipDecoder());
        decoders.put(GraphOpenerGlobals.ZIP_GRAPHML_FORMAT, new ZipDecoder());
        decoders.put(GraphOpenerGlobals.ZIP_GML_FORMAT, new ZipDecoder());
        decoders.put(GraphOpenerGlobals.ZIP_DOT_FORMAT, new ZipDecoder());
        decoders.put(GraphOpenerGlobals.ZIP_GV_FORMAT, new ZipDecoder());
    }

    @Override
    public Set<String> getAvailableExtensions() {
        return decoders.keySet();
    }

    @Override
    public List<Integer> openFile(File file) throws Exception {
        //getting decoder for file
        String extension = file.getName().toLowerCase().replaceAll(".*\\.", "");

        GraphDecoder decoder = decoders.get(extension);
        if (decoder == null)
            throw new Exception("Couldn't find decoder for *." + extension + "file");

        Date d = new Date();

        List<Integer> graphModelIds = decoder.decode(file, file.getName());
        if (graphModelIds == null || graphModelIds.isEmpty()) {
            VGMainServiceHelper.logger.printError("[BAD] Cannot to add graph in graphDataBase.");
            throw new Exception("Cannot to add graph in graphDataBase.");
        }
        openedFilesList.add(file.getName());
        VGMainServiceHelper.logger.printInfo("Graph load time: " + (new Date().getTime() - d.getTime()) / 1000.0 + "sec");
        return graphModelIds;
    }

    @Override
    public List<Integer> openGraph(String name, String content, String format) throws Exception {
        String fileName = name + "." + format;
        File tmpFile = FileUtils.createTmpFileWithName(fileName);
        try {
            ReadableByteChannel rbc = Channels.newChannel(new StringBufferInputStream(content));
            FileOutputStream fos = new FileOutputStream(tmpFile);
            fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
        } catch (FileNotFoundException ex) {
            VGMainServiceHelper.logger.printException(ex);
            VGMainServiceHelper.windowMessenger.errorMessage("Can't create file: " + tmpFile.getAbsolutePath(), "Error", null);
        } catch (IOException ex) {
            VGMainServiceHelper.logger.printException(ex);
            VGMainServiceHelper.windowMessenger.errorMessage("Can't write to file: " + tmpFile.getAbsolutePath(), "Error", null);
        }

        // open graph
        return openFile(tmpFile);
    }
}
