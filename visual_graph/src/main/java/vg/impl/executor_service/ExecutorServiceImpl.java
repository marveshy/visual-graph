package vg.impl.executor_service;

import vg.impl.main_service.VGMainServiceHelper;
import vg.interfaces.executor_service.ExecutorService;

import javax.swing.*;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @author Timur Zolotuhin (e-mail: tzolotuhin@gmail.com)
 */
public class ExecutorServiceImpl extends ThreadPoolExecutor implements ExecutorService {
    // Constants
    private static final String EDT_PREFIX = "EDT";
    private static final String GENERAL_THREAD_PREFIX = "GENERAL_THREAD";
    private static final String SWING_WORKER_THREAD_PREFIX = "SWING_WORKER_THREAD";

    // Main data
    private int threadIdCounter = 0;
    private int threadCount = 0;

    // Mutex
    private static final Object generalMutex = new Object();

    public ExecutorServiceImpl() {
        super(5, 5, 0L, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<Runnable>());
    }

    @Override
    public void execute(final Runnable runnable) {
        if (runnable == null)
            return;

        final int threadId = nextThreadId();
        //VGMainServiceHelper.logger.printDebug("Thread with id: " + threadId + " will be started");
        super.execute(new Runnable() {
            @Override
            public void run() {
                incThreadCount(threadId, GENERAL_THREAD_PREFIX);
                try {
                    runnable.run();
                } catch (Throwable ex) {
                    VGMainServiceHelper.logger.printException(ex);
                }
                decThreadCount(threadId, GENERAL_THREAD_PREFIX);
            }
        });
    }

    @Override
    public void execute(final SwingExecutor swingExecutor) {
        if (swingExecutor == null)
            return;

        final int threadId = nextThreadId();
        SwingWorker worker = new SwingWorker() {
            @Override
            protected Object doInBackground() throws Exception {
                incThreadCount(threadId, SWING_WORKER_THREAD_PREFIX);
                try {
                    swingExecutor.doInBackground();
                } catch (Throwable ex) {
                    VGMainServiceHelper.logger.printException(ex);
                }
                decThreadCount(threadId, SWING_WORKER_THREAD_PREFIX);
                return null;
            }

            @Override
            protected void done() {
                incThreadCount(threadId, EDT_PREFIX);
                try {
                    swingExecutor.doInEDT();
                } catch (Throwable ex) {
                    VGMainServiceHelper.logger.printException(ex);
                }
                decThreadCount(threadId, EDT_PREFIX);
            }
        };
        worker.execute();
    }

//==============================================================================
//------------------PRIVATE METHODS---------------------------------------------
    private int nextThreadId() {
        synchronized (generalMutex) {
            return threadIdCounter++;
        }
    }

    private void incThreadCount(int threadId, String prefix) {
        synchronized (generalMutex) {
            threadCount++;
            //VGMainServiceHelper.logger.printDebug("Thread: " + prefix + ", with id: " + threadId + " was started, count of threads: " + threadCount);
        }
    }

    private void decThreadCount(int threadId, String prefix) {
        synchronized (generalMutex) {
            threadCount--;
            //VGMainServiceHelper.logger.printDebug("Thread: " + prefix + ", with id: " + threadId + " was finished, count of threads: " + threadCount);
        }
    }
}