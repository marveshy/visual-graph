package vg.impl.plugin_service;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import vg.impl.main_service.VGMainServiceHelper;
import vg.interfaces.main_service.VGMainService;

/**
 * @author Timur Zolotuhin (e-mail: tzolotuhin@gmail.com)
 */
public class Activator implements BundleActivator {
    @Override
    public void start(BundleContext bundleContext) throws Exception {
        bundleContext.registerService(VGMainService.class.getName(), VGMainServiceHelper.vgMainService, null);
    }

    @Override
    public void stop(BundleContext bundleContext) throws Exception {

    }
}
