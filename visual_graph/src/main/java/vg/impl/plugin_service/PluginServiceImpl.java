package vg.impl.plugin_service;

import org.apache.commons.lang.StringUtils;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.launch.Framework;
import org.osgi.framework.launch.FrameworkFactory;
import vg.impl.main_service.VGMainServiceHelper;
import vg.interfaces.main_service.VGMainGlobals;
import vg.interfaces.plugin_service.PluginService;

import java.util.Map;
import java.util.ServiceLoader;

/**
 * @author Timur Zolotuhin (e-mail: tzolotuhin@gmail.com)
 */
public class PluginServiceImpl implements PluginService, Runnable {
    private static Framework framework;

    // Mutex
    private final Object generalMutex = new Object();

    @Override
    public void start() {
        synchronized (generalMutex) {
            Thread t = new Thread(this);
            t.setDaemon(true);
            t.start();
        }
    }

    @Override
    public void stop() {
        synchronized (generalMutex) {
            try {
                framework.stop();
            } catch (Exception ex) {
                VGMainServiceHelper.logger.printError(ex.getMessage(), ex);
            }
        }
    }

    public void run() {
        synchronized (generalMutex) {
            try {
                Map<String, String> config = ConfigUtil.createConfig();
                framework = createFramework(config);
                framework.init();
                framework.start();
            } catch (Exception ex) {
                VGMainServiceHelper.logger.printError(ex.getMessage(), ex);
            }
        }

        try {
            installAndStartBundles(getLocations(VGMainServiceHelper.config.getStringProperty(VGMainGlobals.PLUGIN_PATHS_KEY, "")));
            framework.waitForStop(0);
        } catch (Exception ex) {
            VGMainServiceHelper.logger.printError(ex.getMessage(), ex);
        }

    }

//==============================================================================
//------------------PRIVATE METHODS---------------------------------------------
    private static String[] getLocations(String inputPaths) {
        if (StringUtils.isEmpty(inputPaths))
            return new String[0];
        return inputPaths.split(";");
    }

    private static Framework createFramework(Map<String, String> config) {
        ServiceLoader<FrameworkFactory> factoryLoader = ServiceLoader.load(FrameworkFactory.class);
        for (FrameworkFactory factory : factoryLoader) {
            return factory.newFramework(config);
        }
        throw new IllegalStateException("Unable to load FrameworkFactory service.");
    }

    private static void installAndStartBundles(String... bundleLocations) throws Exception {
        BundleContext bundleContext = framework.getBundleContext();
        Activator hostActivator = new Activator();
        hostActivator.start(bundleContext);
        for (String location : bundleLocations) {
            Bundle addition = bundleContext.installBundle(location);
            addition.start();
        }
    }
}
