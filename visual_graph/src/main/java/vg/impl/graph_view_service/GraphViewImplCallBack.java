package vg.impl.graph_view_service;

import vg.interfaces.data_base_service.data.graph.Edge;
import vg.interfaces.data_base_service.data.graph.Vertex;
import vg.interfaces.graph_layout_service.GraphLayout;
import vg.interfaces.graph_view_service.GraphViewExecutor;
import vg.interfaces.graph_view_service.data.VGVertexCell;

import java.util.Collection;

/**
 * @author Timur Zolotuhin (e-mail: tzolotuhin@gmail.com)
 */
public interface GraphViewImplCallBack {
    void notifyOnSelectElements();

    void notifyOnAddElements(final Collection<Vertex> vertices, final Collection<Edge> edges);

    void notifyOnCollapse(VGVertexCell vgVertexCell, boolean collapsed, boolean downloaded);

    void notifyOnStartLayout(GraphLayout graphLayout);

    void notifyOnStopLayout(GraphLayout graphLayout);

    void syncExecute(GraphViewExecutor.GraphViewRunnable graphViewRunnable);

    void syncExecuteInEDT(Runnable runnable);
}
