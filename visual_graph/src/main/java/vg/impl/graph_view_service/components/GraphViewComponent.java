package vg.impl.graph_view_service.components;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.mxgraph.swing.handler.mxKeyboardHandler;
import com.mxgraph.swing.handler.mxRubberband;
import com.mxgraph.swing.mxGraphComponent;
import com.mxgraph.swing.mxGraphOutline;
import com.mxgraph.util.*;
import com.mxgraph.view.mxCellState;
import com.mxgraph.view.mxGraph;
import com.mxgraph.view.mxStylesheet;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.Validate;
import org.apache.commons.lang.mutable.MutableBoolean;
import org.apache.commons.lang.mutable.MutableInt;
import vg.impl.graph_view_service.GraphViewImplCallBack;
import vg.impl.graph_view_service.components.jgraphx.VGGraphOutline;
import vg.impl.main_service.VGMainServiceHelper;
import vg.interfaces.data_base_service.data.graph.*;
import vg.interfaces.graph_view_service.*;
import vg.interfaces.graph_view_service.data.VGCell;
import vg.interfaces.graph_view_service.data.VGEdgeCell;
import vg.interfaces.graph_view_service.data.VGGraph;
import vg.interfaces.graph_view_service.data.VGVertexCell;
import vg.interfaces.main_service.VGMainGlobals;
import vg.shared.graph.utils.GraphUtils;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.geom.Point2D;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.*;
import java.util.List;

/**
 * @author Timur Zolotuhin (tzolotuhin@parallels.com)
 */
public class GraphViewComponent {
    // Constants
    private static final Color DEFAULT_VERTEX_COLOR = Color.decode("0xFFFFF0");
    private static final Color DEFAULT_EDGE_COLOR = Color.decode("0x000000");
//    private static final Color DEFAULT_BORDER_EDGE_COLOR = Color.decode("0x000000");
//    private static final Color DEFAULT_BORDER_VERTEX_COLOR = Color.decode("0x000000");

    // Icons
    private static final ImageIcon closeActiveImageIcon;
    private static final ImageIcon closeImageIcon;

    static {
        closeActiveImageIcon = new ImageIcon("./data/resources/textures/closeActive.png");
        closeImageIcon = new ImageIcon("./data/resources/textures/close.png");
    }

    // Main components
    private final JPanel outView, innerView;
    private JLayeredPane layeredPane;

    private VGGraph vgGraph;
    private Graph originalGraph;
    private VGMXGraphViewComponent jGraphView;
    private mxGraphOutline jGraphMiniMap;

    private JLabel searchLabel, searchPanelCloseButton;
    private JTextField searchTextField;

    private JPopupMenu popup;

    // Main data
    private GraphViewImplCallBack callBack;

    private boolean panning = false;
    private boolean search = false;

    private Color defaultVertexColor, defaultEdgeColor;
    private Color defaultVertexFontColor, defaultEdgeFontColor;
    //private Color defaultVertexBorderColor, defaultEdgeBorderColor;
    private String vertexShortInfoTemplate;

    private Font defaultVertexFont, defaultEdgeFont;

    public GraphViewComponent(final GraphViewImplCallBack callBack) {
        this.callBack = callBack;

        defaultVertexColor = DEFAULT_VERTEX_COLOR;
        defaultEdgeColor = DEFAULT_EDGE_COLOR;
//        defaultVertexBorderColor = DEFAULT_BORDER_VERTEX_COLOR;
//        defaultEdgeBorderColor = DEFAULT_BORDER_EDGE_COLOR;
        defaultVertexFontColor = Color.BLACK;
        defaultEdgeFontColor = Color.BLACK;
        defaultVertexFont = new Font(VGMainGlobals.UI_MONOSPACED_FONT_FAMILY, Font.BOLD, 14);
        defaultEdgeFont = new Font(VGMainGlobals.UI_MONOSPACED_FONT_FAMILY, Font.BOLD, 14);

        vertexShortInfoTemplate = "";

        // init main components
        outView = new JPanel(new GridLayout(1, 1));
        innerView = new JPanel(new GridLayout(1, 1));
        outView.add(innerView);

        searchLabel = new JLabel("Search: ");
        searchTextField = new JTextField();
        searchPanelCloseButton = new JLabel(closeImageIcon);
        searchPanelCloseButton.setPreferredSize(new Dimension(20, 20));

        searchPanelCloseButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                search = false;
                rebuildView();
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                searchPanelCloseButton.setIcon(closeActiveImageIcon);
            }

            @Override
            public void mouseExited(MouseEvent e) {
                searchPanelCloseButton.setIcon(closeImageIcon);
            }
        });

        searchTextField.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                callBack.syncExecute(new GraphViewExecutor.GraphViewRunnable() {
                    @Override
                    public void run(GraphView graphView) {
                        Set<Vertex> vertices = Sets.newHashSet();
                        Set<Edge> edges = Sets.newHashSet();
                        for (Vertex vertex : originalGraph.getAllVertices()) {
                            for (Attribute attribute : vertex.getAttributes()) {
                                if (attribute.isVisible() && (attribute.getName().matches(searchTextField.getText()) || attribute.getStringValue().matches(searchTextField.getText()))) {
                                    vertices.add(vertex);
                                    break;
                                }
                            }
                        }

                        for (Edge edge : originalGraph.getAllEdges()) {
                            for (Attribute attribute : edge.getAttributes()) {
                                if (attribute.isVisible() && (attribute.getName().matches(searchTextField.getText()) || attribute.getStringValue().matches(searchTextField.getText()))) {
                                    edges.add(edge);
                                    break;
                                }
                            }
                        }

                        selectElements(vertices, edges);
                    }
                });
            }
        });

        // build view
        buildView();

        // open new graph
        openNewGraph(null);
    }

    public void openNewGraph(final Graph graph) {
        if (graph == null) {
            originalGraph = new Graph();
        } else {
            originalGraph = graph;
        }

        // remove all elements
        removeAllElements();

        // add vertices
        final List<Vertex> vertices = Lists.newArrayList();
        originalGraph.bfs(new Graph.GraphSearchListener() {
            @Override
            public void onVertex(Vertex vertex, Vertex parent) {
                if (vertex == graph)
                    return;

                doAddVertex(vertex, parent, false, false);
                vertices.add(vertex);
            }
        });

        // add edges
        final List<Edge> edges = Lists.newArrayList();
        originalGraph.bfs(new Graph.GraphSearchListener() {
            @Override
            public void onEdge(Edge edge) {
                doAddEdge(edge, false, false);
                edges.add(edge);
            }
        });

        // refresh view and notify listeners
        callBack.syncExecuteInEDT(new Runnable() {
            @Override
            public void run() {
                refreshView();
                callBack.notifyOnAddElements(vertices, edges);
            }
        });
    }

    public void addVertex(Vertex vertex, Vertex parent) {
        doAddVertex(vertex, parent, true, true);
    }

    public void addEdge(Edge edge) {
        doAddEdge(edge, true, true);
    }

    public void removeAllElements() {
        vgGraph.removeAllCells();
    }

    public JComponent getView() {
        return outView;
    }

    public void refreshView() {
        vgGraph.bfs(new VGGraph.SearchListener() {
            @Override
            public void onVertex(VGVertexCell vertex) {
                applyVertexEffect(vertex, false);
            }

            @Override
            public void onEdge(VGEdgeCell edge) {
                applyEdgeEffect(edge);
            }
        });
        vgGraph.refresh();

        rebuildView();
    }

    public boolean isPanning() {
        return panning;
    }

    public void setSearch(boolean search) {
        this.search = search;
        rebuildView();
    }

    public boolean isSearch() {
        return search;
    }

    public void addPopupMenuItem(JMenuItem item) {
        popup.add(item);
    }

    public void removePopupMenuItem(JMenuItem item) {
        popup.remove(item);
    }

    public void selectElements(Set<Vertex> vertices, Set<Edge> edges) {
        Validate.notNull(vertices);
        Validate.notNull(edges);

        List<VGCell> VGCells = Lists.newArrayList();
        for (Vertex vertex : vertices) {
            VGCells.add(vgGraph.getVGVertexCellByVertex(vertex));
        }
        for (Edge edge : edges) {
            VGCells.add(vgGraph.getVGEdgeCellByEdge(edge));
        }

        vgGraph.setSelectionCells(VGCells.toArray());
    }

    public void setDefaultVertexColor(Color color) {
        defaultVertexColor = color;
    }

//    public Color getDefaultVertexColor() {
//        return defaultVertexColor;
//    }

    public Color getDefaultVertexFontColor() {
        return defaultVertexFontColor;
    }

    public void setDefaultVertexFontColor(Color defaultVertexFontColor) {
        this.defaultVertexFontColor = defaultVertexFontColor;
    }

    public Color getDefaultEdgeFontColor() {
        return defaultEdgeFontColor;
    }

    public void setDefaultEdgeFontColor(Color defaultEdgeFontColor) {
        this.defaultEdgeFontColor = defaultEdgeFontColor;
    }

    public void setDefaultEdgeColor(Color color) {
        defaultEdgeColor = color;
    }

//    public Color getDefaultEdgeColor() {
//        return defaultEdgeColor;
//    }

//    public Color getDefaultVertexBorderColor() {
//        return defaultVertexBorderColor;
//    }

//    public void setDefaultVertexBorderColor(Color defaultVertexBorderColor) {
//        this.defaultVertexBorderColor = defaultVertexBorderColor;
//    }

//    public Color getDefaultEdgeBorderColor() {
//        return defaultEdgeBorderColor;
//    }

//    public void setDefaultEdgeBorderColor(Color defaultEdgeBorderColor) {
//        this.defaultEdgeBorderColor = defaultEdgeBorderColor;
//    }

    public Font getDefaultVertexFont() {
        return defaultVertexFont;
    }

    public void setDefaultVertexFont(Font defaultVertexFont) {
        this.defaultVertexFont = defaultVertexFont;
    }

    public Font getDefaultEdgeFont() {
        return defaultEdgeFont;
    }

    public void setDefaultEdgeFont(Font defaultEdgeFont) {
        this.defaultEdgeFont = defaultEdgeFont;
    }

    public void setPanning(boolean panning) {
        this.panning = panning;
        rebuildView();
    }

    public void zoomIn() {
        jGraphView.zoomIn();
        refreshView();
    }

    public void zoomOut() {
        jGraphView.zoomOut();
        refreshView();
    }

    public void setCanvasSize(Point2D size) {
        jGraphView.getGraphControl().setPreferredSize(new Dimension((int)size.getX(), (int)size.getY()));
        refreshView();
    }

    public Graph getOriginalGraph() {
        return originalGraph;
    }

    public VGGraph getVGGraph() {
        return vgGraph;
    }

    public void showVertexAttributes(final Collection<Vertex> vertices, final Map<String, Boolean> vertexAttributes, final boolean useRegexp) {
        for (final String vertexAttribute : vertexAttributes.keySet()) {
            vgGraph.bfs(new VGGraph.SearchListener() {
                @Override
                public void onVertex(final VGVertexCell vgVertexCell) {
                    if (vertices.contains(vgVertexCell.getVGObject())) {
                        for (Attribute attribute : vgVertexCell.vgGetAttributes().keySet()) {
                            if ((!useRegexp && attribute.getName().equals(vertexAttribute)) ||
                                    (useRegexp && attribute.getName().matches(vertexAttribute))) {
                                vgVertexCell.vgGetAttributes().get(attribute).setValue(vertexAttributes.get(vertexAttribute));
                            }
                        }
                        callBack.syncExecuteInEDT(new Runnable() {
                            @Override
                            public void run() {
                                applyVertexEffect(vgVertexCell, true);
                            }
                        });
                    }
                }
            });
        }
    }

    public void showEdgeAttributes(final Collection<Edge> edges, final Map<String, Boolean> edgeAttributes, final boolean useRegexp) {
        for (final String vertexAttribute : edgeAttributes.keySet()) {
            vgGraph.bfs(new VGGraph.SearchListener() {
                @Override
                public void onEdge(final VGEdgeCell vgEdgeCell) {
                    if (edges.contains(vgEdgeCell.getVGObject())) {
                        for (Attribute attribute : vgEdgeCell.vgGetAttributes().keySet()) {
                            if ((!useRegexp && attribute.getName().equals(vertexAttribute)) ||
                                    (useRegexp && attribute.getName().matches(vertexAttribute))) {
                                vgEdgeCell.vgGetAttributes().get(attribute).setValue(edgeAttributes.get(vertexAttribute));
                            }
                        }
                        callBack.syncExecuteInEDT(new Runnable() {
                            @Override
                            public void run() {
                                applyEdgeEffect(vgEdgeCell);
                            }
                        });
                    }
                }
            });
        }
    }

    public void setVertexShortInfoTemplate(final String vertexShortInfoTemplate) {
        this.vertexShortInfoTemplate = vertexShortInfoTemplate;
        vgGraph.bfs(new VGGraph.SearchListener() {
            @Override
            public void onVertex(final VGVertexCell vgVertexCell) {
                List<Attribute> result = Lists.newArrayList();
                for (Attribute attribute : vgVertexCell.vgGetAttributes().keySet()) {
                    if (attribute.getName().matches(vertexShortInfoTemplate)) {
                        result.add(attribute);
                    }
                }
                vgVertexCell.vgSetShortInfoAttributes(result);
                callBack.syncExecuteInEDT(new Runnable() {
                    @Override
                    public void run() {
                        applyVertexEffect(vgVertexCell, true);
                    }
                });
            }
        });
    }

    public String getVertexShortInfoTemplate() {
        return vertexShortInfoTemplate;
    }

//==============================================================================
//-----------------PRIVATE METHODS----------------------------------------------
    private void doAddVertex(final Vertex vertex, final Vertex parent, final boolean notificationFlag, final boolean addToOriginalGraph) {
        callBack.syncExecuteInEDT(new Runnable() {
            @Override
            public void run() {
                VGVertexCell vgVertexCell = vgGraph.vgInsertVertex(vertex, vgGraph.getVGVertexCellByVertex(parent));

                if (addToOriginalGraph)
                    originalGraph.insertVertex(vertex, parent);

                // add attributes
                for (Attribute attribute : vertex.getAttributes()) {
                    vgVertexCell.vgGetAttributes().put(attribute, new MutableBoolean(false));
                }

                // apply effects
                applyVertexEffect(vgVertexCell, true);

                if (notificationFlag)
                    callBack.notifyOnAddElements(Collections.singletonList(vertex), Collections.<Edge>emptyList());
            }
        });
    }

    private void doAddEdge(final Edge edge, final boolean notificationFlag, final boolean addToOriginalGraph) {
        callBack.syncExecuteInEDT(new Runnable() {
            @Override
            public void run() {
                VGVertexCell srcVertex = vgGraph.getVGVertexCellByVertex(edge.getSource());
                VGVertexCell trgVertex = vgGraph.getVGVertexCellByVertex(edge.getTarget());

                // validating src and trg value
                Validate.notNull(srcVertex);
                Validate.notNull(trgVertex);

                // find right parent
                Object parent = VGGraph.findGeneralParent(vgGraph, srcVertex, trgVertex);

                // insert new edge
                VGEdgeCell vgEdgeCell = vgGraph.vgInsertEdge(edge, parent, srcVertex, trgVertex);

                if (addToOriginalGraph)
                    originalGraph.addEdge(edge);

                // add attributes
                for (Attribute attribute : edge.getAttributes()) {
                    vgEdgeCell.vgGetAttributes().put(attribute, new MutableBoolean(false));
                }

                vgEdgeCell.vgSetDirected(GraphUtils.isDirectedEdge(edge));

                // apply effects
                applyEdgeEffect(vgEdgeCell);

                if (notificationFlag)
                    callBack.notifyOnAddElements(Collections.<Vertex>emptyList(), Collections.singletonList(edge));
            }
        });
    }

    private void rebuildView() {
        innerView.removeAll();

        final JPanel searchPanel = new JPanel(new GridBagLayout());
        {
            searchPanel.add(searchLabel, new GridBagConstraints(0, 0, 1, 1, 0, 0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 5, 5), 0, 0));
            searchPanel.add(searchTextField, new GridBagConstraints(1, 0, 1, 1, 1, 0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5, 5, 5, 5), 0, 0));
            searchPanel.add(searchPanelCloseButton, new GridBagConstraints(2, 0, 1, 1, 0, 0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 5, 5), 0, 0));
            searchPanel.setBorder(BorderFactory.createLineBorder(Color.GRAY, 1));
        }

        jGraphMiniMap.setBorder(BorderFactory.createLineBorder(Color.BLACK, 2));

        final int SIZE_X = 150;
        final int SIZE_Y = 150;
        if (layeredPane != null)
            layeredPane.removeAll();
        layeredPane = new JLayeredPane();
        layeredPane.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
                jGraphView.setBounds(layeredPane.getBounds());
                jGraphMiniMap.setBounds(layeredPane.getBounds().width - SIZE_X - 20, layeredPane.getBounds().height - SIZE_Y - 20, SIZE_X, SIZE_Y);

                searchPanel.setBounds(3, 3, layeredPane.getWidth() - 23, 30);
            }
        });

        layeredPane.addPropertyChangeListener(new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                if (search && evt.getNewValue() != null)
                    searchPanel.setBounds(3, 3, ((JPanel)evt.getNewValue()).getWidth() - 23, 30);
            }
        });

        layeredPane.add(jGraphView, new Integer(0));
        layeredPane.add(jGraphMiniMap, new Integer(1));

        if (search) {
            layeredPane.add(searchPanel, new Integer(2));
        }

        innerView.add(layeredPane);

        innerView.updateUI();
    }

    private void buildView() {
        vgGraph = new VGGraph();

        jGraphView = new VGMXGraphViewComponent(vgGraph) {
            public boolean isPanningEvent(MouseEvent event) {
                return (event != null) && (event.getButton() == MouseEvent.BUTTON2 || panning || (event.isShiftDown() && event.isControlDown()));
            }
        };

        vgGraph.setVGGraphComponentReference(jGraphView);
        vgGraph.setGraphViewReference((GraphView)callBack);

        //vgmxGraph.setDropEnabled(false);
        //vgmxGraph.setCellsLocked(true);

        vgGraph.setAlternateEdgeStyle("edgeStyle=mxEdgeStyle.ElbowConnector;elbow=vertical");
        vgGraph.setAllowNegativeCoordinates(false);
        vgGraph.setCellsCloneable(false);
        vgGraph.setCellsDeletable(false);
        vgGraph.setHtmlLabels(false);
        vgGraph.setMultigraph(true);
        vgGraph.setCellsDisconnectable(false);
        vgGraph.setAllowDanglingEdges(false);
        //vgGraph.setCellsResizable(false);
        vgGraph.setCellsEditable(false);
        vgGraph.setConnectableEdges(false);
        //vgGraph.setCellsBendable(false);
        vgGraph.setGridEnabled(true);
        vgGraph.setGridSize(20);

        jGraphView.setConnectable(false);
        jGraphView.setToolTips(true);
        jGraphView.setOpaque(true);
        jGraphView.setGridColor(new Color(39, 58, 93));
        jGraphView.setGridVisible(true);
        jGraphView.setGridStyle(mxGraphComponent.GRID_STYLE_LINE);

        new mxRubberband(jGraphView);
        new mxKeyboardHandler(jGraphView);

        // mini map
        jGraphMiniMap = new VGGraphOutline(jGraphView);
        MouseWheelListener wheelTracker = new MouseWheelListener() {
            @Override
            public void mouseWheelMoved(MouseWheelEvent e) {
                if (e.getSource() instanceof mxGraphOutline || e.isControlDown()) {
                    if (e.getWheelRotation() < 0) {
                        jGraphView.zoomIn();
                    } else {
                        jGraphView.zoomOut();
                    }
                }
            }
        };
        jGraphMiniMap.addMouseWheelListener(wheelTracker);
        jGraphView.addMouseWheelListener(wheelTracker);

        // selection handler
        mxEventSource.mxIEventListener selectHandler = new mxEventSource.mxIEventListener() {
            public void invoke(Object sender, mxEventObject evt) {
                doSelectElements();
            }
        };
        vgGraph.getSelectionModel().addListener(mxEvent.CHANGE, selectHandler);

        // cells folded handler
        mxEventSource.mxIEventListener groupHandler = new mxEventSource.mxIEventListener() {
            public void invoke(Object sender, mxEventObject evt) {
                boolean collapsed = BooleanUtils.toBoolean(evt.getProperty("collapse").toString());
                VGVertexCell vgVertexCell = ((VGVertexCell)(((Object[])evt.getProperty("cells"))[0]));
                doFoldElements(vgVertexCell, collapsed);
            }
        };
        vgGraph.addListener(mxEvent.CELLS_FOLDED, groupHandler);

        // move handler
        mxEventSource.mxIEventListener moveHandler = new mxEventSource.mxIEventListener() {
            public void invoke(Object sender, mxEventObject evt) {}
        };

        vgGraph.addListener(mxEvent.CELLS_MOVED, moveHandler);

        // add popup menu
        popup = new JPopupMenu();

        jGraphView.getGraphControl().addMouseListener(new MouseAdapter() {
            private void popupTrigger(MouseEvent e) {
                if (e.getButton() == MouseEvent.BUTTON3) {
                    popup.show(e.getComponent(), e.getX(), e.getY());
                }
            }

            @Override
            public void mouseReleased(final MouseEvent e) {}

            @Override
            public void mousePressed(MouseEvent e) {
                popupTrigger(e);
            }
        });

        // setup default settings
        mxStylesheet styleSheet = vgGraph.getStylesheet();
        Map<String, Object> style = styleSheet.getDefaultVertexStyle();

        style.put(mxConstants.STYLE_FONTCOLOR, "#000000");
        style.put(mxConstants.STYLE_FILLCOLOR, mxUtils.hexString(defaultVertexColor));
        style.put(mxConstants.STYLE_FONTFAMILY, defaultVertexFont.getFamily());
        style.put(mxConstants.STYLE_FONTSIZE, defaultVertexFont.getSize());
        style.put(mxConstants.STYLE_FONTSTYLE, defaultVertexFont.getStyle());
        style.put(mxConstants.STYLE_SPACING_LEFT, "5");
        style.put(mxConstants.STYLE_SPACING_RIGHT, "5");
        style.put(mxConstants.STYLE_SPACING_TOP, "10");
        style.put(mxConstants.STYLE_SPACING_BOTTOM, "5");
        //style.put(mxConstants.STYLE_ROUNDED, "1");

        // update ui
        refreshView();
    }

    private Map.Entry<String, mxPoint> generateVertexLabel(VGVertexCell vgVertexCell, boolean showAttributes) {
        return generateLabel(vgVertexCell.vgGetAttributes(), vgVertexCell.vgGetShortInfoAttributes(), vgVertexCell.getHumanId(), defaultVertexFont, showAttributes);
    }

    private Map.Entry<String, mxPoint> generateEdgeLabel(VGEdgeCell vgEdgeCell, boolean showAttributes) {
        //return generateLabel(vgEdgeCell.vgGetAttributes(), vgEdgeCell.vgGetShortInfoAttributes(), vgEdgeCell.getHumanId(), defaultEdgeFont, showAttributes);
        return generateLabel(vgEdgeCell.vgGetAttributes(), vgEdgeCell.vgGetShortInfoAttributes(), "", defaultEdgeFont, showAttributes);
    }

    private static Map.Entry<String, mxPoint> generateLabel(Map<Attribute, MutableBoolean> map, List<Attribute> shortInfoAttributes, String id, Font defaultFont, boolean showAttributes) {
        String text = "";

        int lineCount = 1;
        int maxNameLength = 2, maxValueLength = id.length();

        // handle case id is null or empty
        if (StringUtils.isEmpty(id)) {
            id = "";
            maxNameLength = 0;
            maxValueLength = 0;
        }

        if (showAttributes) {
            // find max name and value lengths
            for (Attribute attr : map.keySet()) {
                if (!attr.isVisible() || (map.get(attr).isFalse() && !shortInfoAttributes.contains(attr)))
                    continue;

                maxNameLength = attr.getName().length() > maxNameLength ? attr.getName().length() : maxNameLength;
                maxValueLength = attr.getStringValue().length() > maxValueLength ? attr.getStringValue().length() : maxValueLength;
            }

            // generate label
            if (!StringUtils.isEmpty(id)) {
                text += String.format("%" + maxNameLength + "s", "ID") + " | " +
                        String.format("%-" + maxValueLength + "s", id) + "\n";
            }

            for (Attribute attr : map.keySet()) {
                if (!attr.isVisible() || (map.get(attr).isFalse() && !shortInfoAttributes.contains(attr)))
                    continue;

                text += String.format("%" + maxNameLength + "s", attr.getName()) + " | " +
                        String.format("%-" + maxValueLength + "s", attr.getStringValue()) + "\n";
                lineCount++;
            }
        } else {
            // find max name and value lengths
            for (Attribute attr : shortInfoAttributes) {
                if (!attr.isVisible())
                    continue;

                maxNameLength = attr.getName().length() > maxNameLength ? attr.getName().length() : maxNameLength;
                maxValueLength = attr.getStringValue().length() > maxValueLength ? attr.getStringValue().length() : maxValueLength;
            }

            // generate label
            if (!StringUtils.isEmpty(id)) {
                text += String.format("%" + maxNameLength + "s", "ID") + " | " +
                        String.format("%-" + maxValueLength + "s", id) + "\n";
            }
            for (Attribute attr : shortInfoAttributes) {
                if (!attr.isVisible())
                    continue;

                text += String.format("%" + maxNameLength + "s", attr.getName()) + " | " +
                        String.format("%-" + maxValueLength + "s", attr.getStringValue()) + "\n";
                lineCount++;
            }
        }

        // remove last \n
        text = text.length() > 0 ? text.substring(0, text.length() - 1) : text;

        return new AbstractMap.SimpleEntry<>(text, new mxPoint((maxNameLength + maxValueLength + 3) * 8, lineCount * defaultFont.getSize()));
    }

    private void doFoldElements(final VGVertexCell vgVertexCell, final boolean collapsed) {
        callBack.syncExecute(new GraphViewExecutor.GraphViewRunnable() {
            @Override
            public void run(GraphView graphView) {
                if (!collapsed && vgVertexCell.getVGObject().getLinkToVertexRecord() != null && vgVertexCell.getChildCount() == 0) {
                    // try download children
                    Map.Entry<List<Vertex>, Map.Entry<List<Edge>, List<Edge>>> subGraphElements = VGMainServiceHelper.graphDataBaseService.getSubGraphElements(vgVertexCell.getVGObject().getLinkToVertexRecord().getId());
                    for (Vertex vertex : subGraphElements.getKey()) {
                        addVertex(vertex, vgVertexCell.getVGObject());
                    }

                    for (Edge edge : subGraphElements.getValue().getKey()) {
                        addEdge(edge);
                    }

                    for (Edge edge : subGraphElements.getValue().getValue()) {
                        int findDbId = -1;
                        if (edge.getSource() == null)
                            findDbId = edge.getLinkToEdgeRecord().getSourceId();
                        if (edge.getTarget() == null)
                            findDbId = edge.getLinkToEdgeRecord().getTargetId();
                        if (findDbId >= 0) {
                            for (Vertex vertex : originalGraph.getAllVertices()) {
                                if (vertex.getLinkToVertexRecord() != null && vertex.getLinkToVertexRecord().getId() == findDbId) {
                                    if (edge.getSource() == null)
                                        edge.setSource(vertex);
                                    if (edge.getTarget() == null)
                                        edge.setTarget(vertex);
                                    addEdge(edge);
                                    break;
                                }
                            }
                        }
                    }

                    callBack.notifyOnCollapse(vgVertexCell, false, true);
                } else {
                    applyVertexEffect(vgVertexCell, false);
                    vgGraph.refresh();
                }
            }
        });
    }

    private void doSelectElements() {
        callBack.syncExecute(new GraphViewExecutor.GraphViewRunnable() {
            @Override
            public void run(GraphView graphView) {
                // deselect all elements
                vgGraph.bfs(new VGGraph.SearchListener() {
                    @Override
                    public void onVertex(VGVertexCell vertex) {
                        vertex.vgSetSelected(false);
                    }

                    @Override
                    public void onEdge(VGEdgeCell edge) {
                        edge.vgSetSelected(false);
                    }
                });

                // select elements
                Object[] cells = vgGraph.getSelectionCells();
                if (cells == null)
                    cells = new Object[0];

                final MutableInt index = new MutableInt();
                for (Object cell : cells) {
                    if (cell instanceof VGCell) {
                        ((VGCell) cell).vgSetSelected(true);
                        index.increment();
                        ((VGCell) cell).vgSetSelectIndex(index.intValue());
                        if (cell instanceof VGVertexCell) {
                            VGVertexCell vgVertexCell = (VGVertexCell)cell;
                            vgVertexCell.bfs(new VGGraph.SearchListener() {
                                @Override
                                public void onVertex(VGVertexCell vgVertexCell) {
                                    vgVertexCell.vgSetSelected(true);
                                    index.increment();
                                    vgVertexCell.vgSetSelectIndex(index.intValue());
                                }

                                @Override
                                public void onEdge(VGEdgeCell vgEdgeCell) {
                                    vgEdgeCell.vgSetSelected(true);
                                    index.increment();
                                    vgEdgeCell.vgSetSelectIndex(index.intValue());
                                }
                            });
                        }
                    }
                }

                callBack.notifyOnSelectElements();
                callBack.syncExecuteInEDT(new Runnable() {
                    @Override
                    public void run() {
                        refreshView();
                    }
                });
            }
        });
    }

    private void applyVertexEffect(VGVertexCell vgVertexCell, boolean updateSize) {
        Map.Entry<String, mxPoint> generateLabelResult = generateVertexLabel(vgVertexCell, vgGraph.isCellCollapsed(vgVertexCell));
        vgVertexCell.setValue(generateLabelResult.getKey());
        vgVertexCell.vgSetTextSize(generateLabelResult.getValue());

        if (vgVertexCell.getChildCount() > 0) {
            vgVertexCell.vgSetActionPanelSize(VGMXGraphViewComponent.vgGetActionPanelSize());
        } else {
            vgVertexCell.vgSetActionPanelSize(new mxPoint(0, 0));
        }

        // set effects
        String style;
        if (vgVertexCell.vgGetColor() != null)
            style = mxConstants.STYLE_FILLCOLOR + "=" + mxUtils.hexString(vgVertexCell.vgGetColor()) + ";";
        else
            style = mxConstants.STYLE_FILLCOLOR + "=" + mxUtils.hexString(defaultVertexColor) + ";";

        style += mxConstants.STYLE_STROKEWIDTH + "=" + vgVertexCell.vgGetBorder() + ";";

        if (!vgVertexCell.vgIsTransparent() || vgVertexCell.vgIsSelected()) {
            style += mxConstants.STYLE_OPACITY + "=100;";
            style += mxConstants.STYLE_TEXT_OPACITY + "=100;";

            if (vgVertexCell.vgIsSelected()) {
                style += mxConstants.STYLE_STROKECOLOR + "=" + mxUtils.hexString(Color.RED) + ";";
            }
        } else {
            style += mxConstants.STYLE_OPACITY + "=20;";
            style += mxConstants.STYLE_TEXT_OPACITY + "=20;";
        }

        if (vgVertexCell.vgGetShape() != null) {
            style += mxConstants.STYLE_SHAPE + "=" + vgVertexCell.vgGetShape();
        }

        if (vgVertexCell.getChildCount() > 0 && !vgGraph.isCellCollapsed(vgVertexCell)) {
            style += mxConstants.STYLE_ALIGN + "=" + "right;";
            style += mxConstants.STYLE_VERTICAL_ALIGN + "=" + "top;";
        }

        vgVertexCell.setStyle(style);

        // update cell
        mxCellState state = vgGraph.getView().getState(vgVertexCell);
        Map<String, Object> cellStyle = vgGraph.getCellStyle(vgVertexCell);
        if (state != null) {
            state.setStyle(cellStyle);
        }

        if (updateSize) {
            vgGraph.updateCellSize(vgVertexCell);
        }
    }

    private void applyEdgeEffect(VGEdgeCell vgEdgeCell) {
        Map.Entry<String, mxPoint> generateLabelResult = generateEdgeLabel(vgEdgeCell, true);
        vgEdgeCell.setValue(generateLabelResult.getKey());
        vgEdgeCell.vgSetTextSize(generateLabelResult.getValue());
        vgEdgeCell.vgSetActionPanelSize(new mxPoint(0, 0));

        // set effects
        String style = "";
        if (!vgEdgeCell.vgIsSelected()) {
            if (vgEdgeCell.vgGetColor() != null) {
                style = mxConstants.STYLE_STROKECOLOR + "=" + mxUtils.hexString(vgEdgeCell.vgGetColor()) + ";";
            } else {
                style = mxConstants.STYLE_STROKECOLOR + "=" + mxUtils.hexString(defaultEdgeColor) + ";";
            }
        }

        style += mxConstants.STYLE_STROKEWIDTH + "=" + vgEdgeCell.vgGetBorder() + ";";

        if (!vgEdgeCell.vgIsTransparent() || vgEdgeCell.vgIsSelected()) {
            style += mxConstants.STYLE_OPACITY + "=100;";
            style += mxConstants.STYLE_TEXT_OPACITY + "=100;";
            if (vgEdgeCell.vgIsSelected()) {
                style += mxConstants.STYLE_STROKECOLOR + "=" + mxUtils.hexString(Color.RED) + ";";
            }
        } else {
            style += mxConstants.STYLE_OPACITY + "=20;";
            style += mxConstants.STYLE_TEXT_OPACITY + "=20;";
        }

        if (vgEdgeCell.vgIsDirected()) {
            style += mxConstants.STYLE_ENDARROW + "=" + mxConstants.ARROW_CLASSIC + ";";
        } else {
            style += mxConstants.STYLE_ENDARROW + "=" + mxConstants.NONE + ";";
        }

        if (!vgEdgeCell.isNoEdgeStyle()) {
            style += mxConstants.STYLE_EDGE + "=" + vgEdgeCell.vgGetEdgeStyle() + ";";
        } else {
            style += mxConstants.STYLE_NOEDGESTYLE + "=1;";
        }

        style += mxConstants.STYLE_LABEL_BACKGROUNDCOLOR + "=" + mxUtils.hexString(Color.WHITE) + ";";

        vgEdgeCell.setStyle(style);

        // update cell
        mxCellState state = vgGraph.getView().getState(vgEdgeCell);
        Map<String, Object> cellStyle = vgGraph.getCellStyle(vgEdgeCell);
        if (state != null) {
            state.setStyle(cellStyle);
        }
        vgGraph.updateCellSize(vgEdgeCell);
    }

//==============================================================================
//------------------PRIVATE CLASSES---------------------------------------------
    private static class VGMXGraphViewComponent extends mxGraphComponent {
        private static final ImageIcon needDownloadIcon;
        private static final ImageIcon collapsedIcon;
        private static final ImageIcon expandedIcon;

        private static final mxPoint actionPanelSize = new mxPoint(30, 30);

        static {
            needDownloadIcon = new ImageIcon("./data/resources/textures/download.png");
            collapsedIcon = new ImageIcon("./data/resources/textures/expand.png");
            expandedIcon = new ImageIcon("./data/resources/textures/collapse.png");
        }


        public VGMXGraphViewComponent(mxGraph graph) {
            super(graph);
        }

        @Override
        public ImageIcon getFoldingIcon(mxCellState state) {
            if (state != null && isFoldingEnabled() && !getGraph().getModel().isEdge(state.getCell())) {
                Object cell = state.getCell();
                boolean tmp = graph.isCellCollapsed(cell);

                if (graph.isCellFoldable(cell, !tmp)) {
                    if (cell instanceof VGVertexCell) {
                        VGVertexCell vgVertexCell = (VGVertexCell) cell;
                        if (tmp && vgVertexCell.getVGObject().getLinkToVertexRecord() != null && vgVertexCell.getChildCount() == 0)
                            return needDownloadIcon;
                    }

                    return (tmp) ? collapsedIcon : expandedIcon;
                }
            }

            return null;
        }

        public static mxPoint vgGetActionPanelSize() {
            return actionPanelSize;
        }
    }
}

