package vg.impl.graph_view_service.components.jgraphx;

import com.mxgraph.swing.mxGraphComponent;
import com.mxgraph.swing.mxGraphOutline;
import com.mxgraph.view.mxGraphView;

import javax.swing.*;
import java.awt.event.MouseEvent;

/**
 * @author Timur Zolotuhin (e-mail: tzolotuhin@parallels.com)
 */
public class VGGraphOutline extends mxGraphOutline {
    public VGGraphOutline(mxGraphComponent graphComponent) {
        super(graphComponent);
        removeMouseMotionListener(tracker);
        removeMouseListener(tracker);
        tracker = new VGMouseTracker();
        addMouseMotionListener(tracker);
        addMouseListener(tracker);
    }

    public class VGMouseTracker extends MouseTracker {
        @Override
        public void mouseReleased(MouseEvent e) {
            if (start != null) {
                if (zoomGesture) {
                    double dx = e.getX() - start.getX();
                    double w = finderBounds.getWidth();

                    if (w < 10)
                        w = 10;

                    final JScrollBar hs = graphComponent
                            .getHorizontalScrollBar();
                    final double sx;

                    if (hs != null) {
                        sx = (double) hs.getValue() / hs.getMaximum();
                    } else {
                        sx = 0;
                    }

                    final JScrollBar vs = graphComponent.getVerticalScrollBar();
                    final double sy;

                    if (vs != null) {
                        sy = (double) vs.getValue() / vs.getMaximum();
                    } else {
                        sy = 0;
                    }

                    mxGraphView view = graphComponent.getGraph().getView();
                    double scale = view.getScale();
                    double newScale = scale - (dx * scale) / w;
                    double factor = newScale / scale;
                    view.setScale(newScale);

                    if (hs != null) {
                        hs.setValue((int) (sx * hs.getMaximum() * factor));
                    }

                    if (vs != null) {
                        vs.setValue((int) (sy * vs.getMaximum() * factor));
                    }
                }

                zoomGesture = false;
                start = null;
            }
        }
    }
}
