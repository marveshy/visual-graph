package vg.impl.graph_view_service;

import com.google.common.collect.Lists;
import vg.shared.gui.SwingUtils;
import vg.impl.main_service.VGMainServiceHelper;
import vg.interfaces.data_base_service.data.graph.Edge;
import vg.interfaces.data_base_service.data.graph.Graph;
import vg.interfaces.data_base_service.data.graph.Vertex;
import vg.interfaces.executor_service.ExecutorService;
import vg.interfaces.graph_view_service.GraphView;
import vg.interfaces.graph_view_service.GraphViewService;
import vg.interfaces.user_interface_service.UserInterfaceService;
import vg.shared.graph.utils.GraphUtils;
import vg.shared.user_interface.DefaultSwingUtilsCallBack;

import java.util.*;

/**
 * @author Timur Zolotuhin (e-mail: tzolotuhin@gmail.com)
 */
public class GraphViewServiceImpl implements GraphViewService {
    // Main data
    private final List<GraphViewImpl> graphViews = Lists.newArrayList();

    // Mutex
    private final Object generalMutex = new Object();

    public GraphViewServiceImpl() {
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                synchronized (generalMutex) {
                    for (final GraphViewImpl graphView : graphViews) {
                        if (graphView.isReadyForExecuting()) {
                            VGMainServiceHelper.executorService.execute(new Runnable() {
                                @Override
                                public void run() {
                                    graphView.executeNow();
                                }
                            });
                        }
                    }
                }
            }
        }, 0 , 50);
    }

    @Override
    public void asyncCopyExistingGraphView(final GraphView srcGraphView,
                                           final Collection<Vertex> vertices,
                                           final Collection<Edge> edges,
                                           final GraphViewServiceCallBack callBack) {
        asyncCreateGraphView(new Graph(), new GraphViewServiceCallBack() {
            @Override
            public void onFinishAction(final GraphView graphView) {
                VGMainServiceHelper.executorService.execute(new ExecutorService.SwingExecutor() {
                    @Override
                    public void doInBackground() {
                        graphView.openNewGraph(srcGraphView, vertices, edges);
                    }

                    @Override
                    public void doInEDT() {
                        if (callBack != null) {
                            callBack.onFinishAction(graphView);
                        }
                    }
                });
            }
        });
    }

    @Override
    public void asyncCreateGraphView(final Graph graph, final GraphViewServiceCallBack callBack) {
        SwingUtils.invokeInEDTIfNeeded(new Runnable() {
            @Override
            public void run() {
                GraphViewImpl graphViewImpl = new GraphViewImpl();
                addGraphViewImpl(graphViewImpl);
                asyncOpenGraphInGraphView(graph, graphViewImpl, callBack);
            }
        }, new DefaultSwingUtilsCallBack());
    }

    @Override
    public void asyncOpenGraphInTab(final Graph graph, final GraphViewServiceCallBack callBack) {
        SwingUtils.invokeInEDTIfNeeded(new Runnable() {
            @Override
            public void run() {
                final GraphViewImpl graphView = new GraphViewImpl();
                addGraphViewImpl(graphView);
                VGMainServiceHelper.executorService.execute(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            graphView.setTabTitle(GraphUtils.generateGraphTitle(graph));
                            VGMainServiceHelper.userInterfaceService.addTab(graphView, new UserInterfaceService.FinishActionCallBack() {
                                @Override
                                public void onFinishAction() {
                                    asyncOpenGraphInGraphView(graph, graphView, callBack);
                                }
                            });
                        } catch (Throwable ex) {
                            VGMainServiceHelper.logger.printException(ex);
                        }
                    }
                });
            }
        }, new DefaultSwingUtilsCallBack());
    }

    @Override
    public void asyncOpenGraphInGraphView(final Graph graph, final GraphView graphView, final GraphViewServiceCallBack callBack) {
        VGMainServiceHelper.executorService.execute(new ExecutorService.SwingExecutor() {
            @Override
            public void doInBackground() {
                checkCountElements(graph);
                graphView.openNewGraph(graph);
                graphView.setTabTitle(GraphUtils.generateGraphTitle(graph));
            }

            @Override
            public void doInEDT() {
                if (callBack != null) {
                    callBack.onFinishAction(graphView);
                }
            }
        });
    }

    public void addGraphViewImpl(GraphViewImpl graphView) {
        synchronized (generalMutex) {
            graphViews.add(graphView);
        }
    }

//==============================================================================
//------------------PRIVATE METHODS---------------------------------------------
    private void checkCountElements(Graph graph) {
        if (graph.getAllVertices().size() + graph.getAllEdges().size() > 1000) {
            VGMainServiceHelper.windowMessenger.warningMessage("Warning: count of elements is " +
                    graph.getAllVertices().size() + graph.getAllEdges().size() +
                    ". Max count of element should be not more than 1000", "Show graph view", null);
        }
    }
}
