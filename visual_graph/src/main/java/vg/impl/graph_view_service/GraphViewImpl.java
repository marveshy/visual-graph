package vg.impl.graph_view_service;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Queues;
import com.mxgraph.model.mxGeometry;
import com.mxgraph.util.mxPoint;
import org.apache.commons.lang.Validate;
import org.apache.commons.lang.mutable.MutableBoolean;
import org.apache.commons.lang.mutable.MutableDouble;
import org.apache.commons.lang.mutable.MutableObject;
import vg.interfaces.main_service.VGMainService;
import vg.shared.gui.SwingUtils;
import vg.shared.utils.MapUtils;
import vg.impl.graph_view_service.components.GraphViewComponent;
import vg.interfaces.graph_layout_service.GraphLayoutTemplate;
import vg.interfaces.graph_view_service.data.VGEdgeCell;
import vg.interfaces.graph_view_service.data.VGGraph;
import vg.interfaces.graph_view_service.data.VGVertexCell;
import vg.impl.main_service.VGMainServiceHelper;
import vg.interfaces.data_base_service.data.graph.*;
import vg.interfaces.graph_layout_service.GraphLayout;
import vg.interfaces.graph_view_service.*;
import vg.shared.user_interface.DefaultSwingUtilsCallBack;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.Point2D;
import java.util.*;
import java.util.List;

/**
 * This class is wrapper to jgraphx library.
 *
 * @author Timur Zolotuhin (e-mail: tzolotuhin@gmail.com)
 */
public class GraphViewImpl implements GraphView, GraphViewImplCallBack {
    // Constants
    public static final String DEFAULT_PLEASE_WAIT_TEXT = "Please wait...";

    // Main components
    protected final JPanel outView, innerView;

    protected GraphViewComponent graphViewComponent;

    protected JLabel pleaseWaitLabel;

    // Main data
    protected Queue<GraphViewRunnable> executors = Queues.newArrayDeque();
    protected boolean readyForExecutingFlag = true;

    protected List<GraphViewListener> listeners = Lists.newArrayList();

    // Main data: it's fake lock for better ui
    protected boolean lock;
    protected String lockText;

    private String title = "BLANK";

    private GraphLayoutTemplate currentGraphLayoutTemplate;
    private GraphLayout currentGraphLayout;

    // Mutex
    protected final Object generalMutex = new Object(); // simple inner mutex for using for all actions with graphViewComponent
    protected final Object listenerMutex = new Object();
    protected final Object executorMutex = new Object();
    protected final Object layoutMutex = new Object(); // personal mutex for layout

    /**
     * Note: the method must be call from EDT.
     */
    public GraphViewImpl() {
        Validate.isTrue(SwingUtilities.isEventDispatchThread());

        outView = new JPanel(new GridLayout(1, 1));
        innerView = new JPanel(new GridLayout(1, 1));
        outView.add(innerView);

        pleaseWaitLabel = new JLabel(DEFAULT_PLEASE_WAIT_TEXT);
        pleaseWaitLabel.setHorizontalAlignment(JLabel.CENTER);

        graphViewComponent = new GraphViewComponent(this);

        rebuildView();
    }

    @Override
    public void openNewGraph(final GraphView srcGraphView, final Collection<Vertex> vertices, final Collection<Edge> edges) {
        synchronized (generalMutex) {
            srcGraphView.execute(new GraphViewRunnable() {
                @Override
                public void run(GraphView graphView) {
                    final Graph srcGraph = srcGraphView.getGraph();
                    final Graph graph = srcGraph.clone();

                    // remove empty children and parents
                    graph.dfs(new Graph.GraphSearchListener() {
                        @Override
                        public void onVertex(Vertex vertex, Vertex parent) {
                            boolean check = false;
                            int id = graph.getIdByVertex(vertex);
                            for (Vertex tmpVertex : vertices) {
                                if (srcGraph.getIdByVertex(tmpVertex) == id) {
                                    check = true;
                                    break;
                                }
                            }

                            if (vertex.getChildVertices().size() == 0 && !check) {
                                graph.removeVertex(vertex, parent);
                            } else if (vertex.getChildVertices().size() != 0 && !check) {
                                graph.changeParent(vertex, parent);
                                graph.removeVertex(vertex, parent);
                            }
                        }
                    });

                    openNewGraph(graph);
                }
            });
        }
    }

    @Override
    public String getTabTitle() {
        synchronized (generalMutex) {
            return title;
        }
    }

    @Override
    public void setTabTitle(String title) {
        synchronized (generalMutex) {
            this.title = title;
        }
    }

    @Override
    public void openNewGraph(Graph graph) {
        synchronized (generalMutex) {
            graphViewComponent.openNewGraph(graph);
        }
    }

    @Override
    public void addVertex(Vertex vertex, Vertex parent) {
        synchronized (generalMutex) {
            graphViewComponent.addVertex(vertex, parent);
        }
    }

    @Override
    public void addEdge(Edge edge) {
        synchronized (generalMutex) {
            graphViewComponent.addEdge(edge);
        }
    }

    @Override
    public void removeAllElements() {
        synchronized (generalMutex) {
            graphViewComponent.removeAllElements();
        }
    }

    @Override
    public void addListener(GraphViewListener listener) {
        synchronized (listenerMutex) {
            listeners.add(listener);
        }
    }

    @Override
    public void removeListener(GraphViewListener listener) {
        synchronized (listenerMutex) {
            listeners.remove(listener);
        }
    }

    @Override
    public JComponent getView() {
        synchronized (generalMutex) {
            return outView;
        }
    }

    @Override
    public void refreshView() {
        syncExecuteInEDT(new Runnable() {
            @Override
            public void run() {
                graphViewComponent.refreshView();
            }
        });
    }

    @Override
    public Map<Edge, Map<Attribute, Boolean>> getSelectedEdgeAttributes() {
        synchronized (generalMutex) {
            final Map<Edge, Map<Attribute, Boolean>> result = Maps.newHashMap();
            graphViewComponent.getVGGraph().bfs(new VGGraph.SearchListener() {
                @Override
                public void onEdge(VGEdgeCell edge) {
                    if (edge.vgIsSelected()) {
                        result.put(edge.getVGObject(), copyAndConvertMap(edge.vgGetAttributes()));
                    }
                }
            });
            return result;
        }
    }

    @Override
    public Map<Edge, Map<Attribute, Boolean>> getOrderedSelectedEdgeAttributes() {
        synchronized (generalMutex) {
            final Map<Map.Entry<Edge, Map<Attribute, Boolean>>, Integer> unorderedMap = Maps.newHashMap();
            graphViewComponent.getVGGraph().bfs(new VGGraph.SearchListener() {
                @Override
                public void onEdge(VGEdgeCell vgmxEdgeCell) {
                    if (vgmxEdgeCell.vgIsSelected()) {
                        Map.Entry<Edge, Map<Attribute, Boolean>> entry = new AbstractMap.SimpleEntry<>(
                                vgmxEdgeCell.getVGObject(),
                                copyAndConvertMap(vgmxEdgeCell.vgGetAttributes()));

                        unorderedMap.put(entry, vgmxEdgeCell.vgGetSelectIndex());
                    }
                }
            });

            Map<Map.Entry<Edge, Map<Attribute, Boolean>>, Integer> orderedMap =
                    new MapUtils<Map.Entry<Edge, Map<Attribute, Boolean>>, Integer>().sortMapByValue(unorderedMap);

            Map<Edge, Map<Attribute, Boolean>> result = Maps.newLinkedHashMap();
            for (Map.Entry<Edge, Map<Attribute, Boolean>> entry : orderedMap.keySet()) {
                result.put(entry.getKey(), entry.getValue());
            }
            return result;
        }
    }

    @Override
    public Map<Vertex, Map<Attribute, Boolean>> getSelectedVertexAttributes() {
        synchronized (generalMutex) {
            final Map<Vertex, Map<Attribute, Boolean>> result = Maps.newHashMap();
            graphViewComponent.getVGGraph().bfs(new VGGraph.SearchListener() {
                @Override
                public void onVertex(VGVertexCell vertex) {
                    if (vertex.vgIsSelected()) {
                        result.put(vertex.getVGObject(), copyAndConvertMap(vertex.vgGetAttributes()));
                    }
                }
            });
            return result;
        }
    }

    @Override
    public Map<Vertex, Map<Attribute, Boolean>> getOrderedSelectedVertexAttributes() {
        synchronized (generalMutex) {
            final Map<Map.Entry<Vertex, Map<Attribute, Boolean>>, Integer> unorderedMap = Maps.newHashMap();
            graphViewComponent.getVGGraph().bfs(new VGGraph.SearchListener() {
                @Override
                public void onVertex(VGVertexCell vgmxVertexCell) {
                    if (vgmxVertexCell.vgIsSelected()) {
                        Map.Entry<Vertex, Map<Attribute, Boolean>> entry = new AbstractMap.SimpleEntry<>(
                                vgmxVertexCell.getVGObject(),
                                copyAndConvertMap(vgmxVertexCell.vgGetAttributes()));

                        unorderedMap.put(entry, vgmxVertexCell.vgGetSelectIndex());
                    }
                }
            });

            Map<Map.Entry<Vertex, Map<Attribute, Boolean>>, Integer> orderedMap =
                    new MapUtils<Map.Entry<Vertex, Map<Attribute, Boolean>>, Integer>().sortMapByValue(unorderedMap);

            Map<Vertex, Map<Attribute, Boolean>> result = Maps.newLinkedHashMap();
            for (Map.Entry<Vertex, Map<Attribute, Boolean>> entry : orderedMap.keySet()) {
                result.put(entry.getKey(), entry.getValue());
            }
            return result;
        }
    }

    @Override
    public Map<Edge, Map<Attribute, Boolean>> getEdgeAttributes() {
        synchronized (generalMutex) {
            final Map<Edge, Map<Attribute, Boolean>> result = Maps.newHashMap();
            graphViewComponent.getVGGraph().bfs(new VGGraph.SearchListener() {
                @Override
                public void onEdge(VGEdgeCell vgmxEdgeCell) {
                    result.put(vgmxEdgeCell.getVGObject(), copyAndConvertMap(vgmxEdgeCell.vgGetAttributes()));
                }
            });
            return result;
        }
    }

    @Override
    public Map<Vertex, Map<Attribute, Boolean>> getVertexAttributes() {
        synchronized (generalMutex) {
            final Map<Vertex, Map<Attribute, Boolean>> result = Maps.newHashMap();
            graphViewComponent.getVGGraph().bfs(new VGGraph.SearchListener() {
                @Override
                public void onVertex(VGVertexCell vgmxVertexCell) {
                    result.put(vgmxVertexCell.getVGObject(), copyAndConvertMap(vgmxVertexCell.vgGetAttributes()));
                }
            });
            return result;
        }
    }

    @Override
    public Vertex getVertexById(final String id) {
        synchronized (generalMutex) {
            final MutableObject mutableObject = new MutableObject();
            graphViewComponent.getVGGraph().bfs(new VGGraph.SearchListener() {
                @Override
                public void onVertex(VGVertexCell vgmxVertexCell) {
                    if (id.equals(vgmxVertexCell.getHumanId()))
                        mutableObject.setValue(vgmxVertexCell.getVGObject());
                }
            });
            return (Vertex)mutableObject.getValue();
        }
    }

    @Override
    public Edge getEdgeById(final String id) {
        synchronized (generalMutex) {
            final MutableObject mutableObject = new MutableObject();
            graphViewComponent.getVGGraph().bfs(new VGGraph.SearchListener() {
                @Override
                public void onEdge(VGEdgeCell vgmxEdgeCell) {
                    if (id.equals(vgmxEdgeCell.getHumanId()))
                        mutableObject.setValue(vgmxEdgeCell.getVGObject());
                }
            });
            return (Edge)mutableObject.getValue();
        }
    }

    @Override
    public String getIdByVertex(final Vertex vertex) {
        synchronized (generalMutex) {
            final MutableObject mutableObject = new MutableObject();
            graphViewComponent.getVGGraph().bfs(new VGGraph.SearchListener() {
                @Override
                public void onVertex(VGVertexCell vgmxVertexCell) {
                    if (vgmxVertexCell.getVGObject() == vertex) {
                        mutableObject.setValue(vgmxVertexCell.getHumanId());
                    }
                }
            });
            return (String)mutableObject.getValue();
        }
    }

    @Override
    public String getIdByEdge(final Edge edge) {
        synchronized (generalMutex) {
            final MutableObject mutableObject = new MutableObject();
            graphViewComponent.getVGGraph().bfs(new VGGraph.SearchListener() {
                @Override
                public void onEdge(VGEdgeCell vgmxEdgeCell) {
                    if (vgmxEdgeCell.getVGObject() == edge) {
                        mutableObject.setValue(vgmxEdgeCell.getHumanId());
                    }
                }
            });
            return (String)mutableObject.getValue();
        }
    }

    @Override
    public Graph getGraph() {
        synchronized (generalMutex) {
            return graphViewComponent.getOriginalGraph();
        }
    }

    @Override
    public Map.Entry<Collection<Vertex>, Collection<Edge>> getSelectedElements() {
        synchronized (generalMutex) {
            final Map<Vertex, Map<Attribute, Boolean>> copySelectedVertices = getSelectedVertexAttributes();
            final Map<Edge, Map<Attribute, Boolean>> copySelectedEdges = getSelectedEdgeAttributes();
            return new AbstractMap.SimpleEntry<Collection<Vertex>, Collection<Edge>>(copySelectedVertices.keySet(), copySelectedEdges.keySet());
        }
    }

    @Override
    public Map.Entry<Collection<Vertex>, Collection<Edge>> getOrderedSelectedElements() {
        synchronized (generalMutex) {
            final Map<Vertex, Map<Attribute, Boolean>> copySelectedVertices = getOrderedSelectedVertexAttributes();
            final Map<Edge, Map<Attribute, Boolean>> copySelectedEdges = getOrderedSelectedEdgeAttributes();
            return new AbstractMap.SimpleEntry<Collection<Vertex>, Collection<Edge>>(copySelectedVertices.keySet(), copySelectedEdges.keySet());
        }
    }

    @Override
    public void setLayoutTemplate(GraphLayoutTemplate graphLayoutTemplate) {
        synchronized (layoutMutex) {
            currentGraphLayoutTemplate = graphLayoutTemplate;
        }
        executeLayout();
    }

    @Override
    public GraphLayoutTemplate getLayoutTemplate() {
        synchronized (layoutMutex) {
            return currentGraphLayoutTemplate;
        }
    }

    @Override
    public void executeLayout() {
        final GraphLayout graphLayout;
        synchronized (layoutMutex) {
            if (currentGraphLayout != null)
                currentGraphLayout.stopAndWait();
            if (currentGraphLayoutTemplate != null) {
                currentGraphLayout = currentGraphLayoutTemplate.buildGraphLayout();
                graphLayout = currentGraphLayout;
                notifyOnStartLayout(currentGraphLayout);
            } else {
                graphLayout = null;
            }
        }

        if (graphLayout != null) {
            execute(new GraphViewRunnable() {
                @Override
                public void run(GraphView graphView) {
                    try {
                        lock("Please wait, applying of layout");
                        VGMainServiceHelper.logger.printDebug("Start layout");

                        graphLayout.start(graphViewComponent.getVGGraph(), new GraphLayout.GraphLayoutCallBack() {
                            @Override
                            public void onFinishAction(GraphLayout graphLayout) {
                                VGMainServiceHelper.logger.printDebug("Finish layout");

                                unlock();
                                notifyOnStopLayout(graphLayout);
                            }
                        });
                    } catch (Throwable ex){
                        notifyOnStopLayout(graphLayout);
                    }
                }
            });
        }
    }

    @Override
    public GraphLayout getCurrentLayout() {
        synchronized (layoutMutex) {
            return currentGraphLayout;
        }
    }

    @Override
    public void addPopupMenuItem(JMenuItem item) {
        synchronized (generalMutex) {
            graphViewComponent.addPopupMenuItem(item);
        }
    }

    @Override
    public void removePopupMenuItem(JMenuItem item) {
        synchronized (generalMutex) {
            graphViewComponent.removePopupMenuItem(item);
        }
    }

    @Override
    public void setVisibleEdges(Collection<Edge> edges, boolean visible) {
        synchronized (generalMutex) {
//            for (Edge edge : edges) {
//                graphViewComponent.getGraphViewGraph().getGraphViewEdgeByEdge(edge).setTransparency(visible);
//            }
        }
    }

    @Override
    public void resetVisibleEdges() {
        synchronized (generalMutex) {
//            graphViewComponent.getGraphViewGraph().bfs(new GraphViewGraph.BFSListener() {
//                @Override
//                public void onEdge(GraphViewEdge graphViewEdge) {
//                    graphViewEdge.setTransparency(true);
//                }
//            });
        }
    }

    @Override
    public void setVisibleVertices(Collection<Vertex> vertices, boolean visible) {
        synchronized (generalMutex) {
//            for (VertexTree vertexTree : vertices) {
//                graphViewComponent.getGraphViewGraph().getGraphViewVertexByVertex(vertexTree).setTransparency(visible);
//            }
        }
    }

    @Override
    public void resetVisibleVertices() {
        synchronized (generalMutex) {
//            graphViewComponent.getGraphViewGraph().bfs(new GraphViewGraph.BFSListener() {
//                @Override
//                public void onVertex(GraphViewVertex graphViewVertex) {
//                    graphViewVertex.setTransparency(true);
//                }
//
//                @Override
//                public void onGraph(GraphViewGraph graphViewGraph) {
//                    graphViewGraph.setTransparency(true);
//                }
//            });
        }
    }

    @Override
    public void selectElements(Set<Vertex> vertices, Set<Edge> edges) {
        synchronized (generalMutex) {
            graphViewComponent.selectElements(vertices, edges);
        }
    }

    @Override
    public void showVertexAttributes(Collection<Vertex> vertices, Map<String, Boolean> vertexAttributes, boolean useRegexp) {
        synchronized (generalMutex) {
            graphViewComponent.showVertexAttributes(vertices, vertexAttributes, useRegexp);
        }
    }

    @Override
    public void showEdgeAttributes(Collection<Edge> edges, Map<String, Boolean> edgeAttributes, boolean useRegexp) {
        synchronized (generalMutex) {
            graphViewComponent.showEdgeAttributes(edges, edgeAttributes, useRegexp);
        }
    }

    @Override
    public String getVertexShortInfoTemplate() {
        synchronized (generalMutex) {
            return graphViewComponent.getVertexShortInfoTemplate();
        }
    }

    @Override
    public void setVertexShortInfoTemplate(String vertexShortInfoTemplate) {
        synchronized (generalMutex) {
            graphViewComponent.setVertexShortInfoTemplate(vertexShortInfoTemplate);
        }
    }

    @Override
    public void setGridSize(int value) {
        synchronized (generalMutex) {
            graphViewComponent.getVGGraph().setGridSize(value);
        }
    }

    @Override
    public void setVertexColor(Vertex vertex, Color color) {
        synchronized (generalMutex) {
            //graphViewComponent.getGraphViewGraph().getGraphViewVertexByVertex(vertexTree).setColor(color);
        }
    }

    @Override
    public void setVertexColor(Collection<Vertex> vertices, Color color) {
        synchronized (generalMutex) {
            for (Vertex v : vertices) {
                setVertexColor(v, color);
            }
        }
    }

    @Override
    public void resetVertexColor() {
        synchronized (generalMutex) {
//            graphViewComponent.getGraphViewGraph().bfs(new GraphViewGraph.BFSListener() {
//                @Override
//                public void onVertex(GraphViewVertex graphViewVertex) {
//                    graphViewVertex.setColor(null);
//                }
//
//                @Override
//                public void onGraph(GraphViewGraph graphViewGraph) {
//                    graphViewGraph.setColor(null);
//                }
//            });
        }
    }

    @Override
    public void setVertexBorderColor(Vertex vertex, Color color) {
        synchronized (generalMutex) {
//            graphViewComponent.getGraphViewGraph().getGraphViewVertexByVertex(vertexTree).setColor(color);
        }
    }

    @Override
    public void setVertexBorderColor(Collection<Vertex> vertices, Color color) {
        synchronized (generalMutex) {
            for (Vertex v : vertices) {
                setVertexBorderColor(v, color);
            }
        }
    }

    @Override
    public void resetVertexBorderColor() {
        synchronized (generalMutex) {
//            graphViewComponent.getGraphViewGraph().bfs(new GraphViewGraph.BFSListener() {
//                @Override
//                public void onVertex(GraphViewVertex graphViewVertex) {
//                    graphViewVertex.setBorderColor(null);
//                }
//
//                @Override
//                public void onGraph(GraphViewGraph graphViewGraph) {
//                    graphViewGraph.setBorderColor(null);
//                }
//            });
        }
    }

    @Override
    public void setVertexBorderSize(Collection<Vertex> vertices, int size) {
        synchronized (generalMutex) {
            for (Vertex vertex : vertices) {
                graphViewComponent.getVGGraph().getVGVertexCellByVertex(vertex).vgSetBorder(size);
            }
        }
    }

    @Override
    public void setEdgeBorderSize(Collection<Edge> edges, int size) {
        synchronized (generalMutex) {
            for (Edge edge : edges) {
                graphViewComponent.getVGGraph().getVGEdgeCellByEdge(edge).vgSetBorder(size);
            }
        }
    }

    @Override
    public void setEdgeColor(Edge edge, Color color) {
        synchronized (generalMutex) {
//            graphViewComponent.getGraphViewGraph().getGraphViewEdgeByEdge(edge).setColor(color);
        }
    }

    @Override
    public void setEdgeColor(Collection<Edge> edges, Color color) {
        synchronized (generalMutex) {
            for (Edge e : edges) {
                setEdgeColor(e, color);
            }
        }
    }

    @Override
    public void resetEdgeColor() {
        synchronized (generalMutex) {
//            graphViewComponent.getGraphViewGraph().bfs(new GraphViewGraph.BFSListener() {
//                @Override
//                public void onEdge(GraphViewEdge graphViewEdge) {
//                    graphViewEdge.setColor(null);
//                }
//            });
        }
    }

    @Override
    public void setEdgeBorderColor(Edge edge, Color color) {
        synchronized (generalMutex) {
//            graphViewComponent.getGraphViewGraph().getGraphViewEdgeByEdge(edge).setBorderColor(color);
        }
    }

    @Override
    public void setEdgeBorderColor(Collection<Edge> edges, Color color) {
        synchronized (generalMutex) {
            for (Edge edge : edges) {
                setEdgeBorderColor(edge, color);
            }
        }
    }

    @Override
    public void resetEdgeBorderColor() {
        synchronized (generalMutex) {
//            graphViewComponent.getGraphViewGraph().bfs(new GraphViewGraph.BFSListener() {
//                @Override
//                public void onEdge(GraphViewEdge graphViewEdge) {
//                    graphViewEdge.setBorderColor(null);
//                }
//            });
        }
    }

    @Override
    public void setVertexShape(Vertex vertex, String shape) {
        synchronized (generalMutex) {
            graphViewComponent.getVGGraph().getVGVertexCellByVertex(vertex).vgSetShape(shape);
        }
    }

    @Override
    public void setEdgeStyle(Edge edge, String shape) {
        synchronized (generalMutex) {
            graphViewComponent.getVGGraph().getVGEdgeCellByEdge(edge).vgSetShape(shape);
        }
    }

    @Override
    public void setDefaultVertexColor(Color color) {
        synchronized (generalMutex) {
            graphViewComponent.setDefaultVertexColor(color);
        }
    }

    @Override
    public void setDefaultEdgeColor(Color color) {
        synchronized (generalMutex) {
            graphViewComponent.setDefaultEdgeColor(color);
        }
    }

    @Override
    public Color getDefaultVertexFontColor() {
        synchronized (generalMutex) {
            return graphViewComponent.getDefaultVertexFontColor();
        }
    }

    @Override
    public void setDefaultVertexFontColor(Color defaultVertexFontColor) {
        synchronized (generalMutex) {
            graphViewComponent.setDefaultVertexFontColor(defaultVertexFontColor);
        }
    }

    @Override
    public Color getDefaultEdgeFontColor() {
        synchronized (generalMutex) {
            return graphViewComponent.getDefaultEdgeFontColor();
        }
    }

    @Override
    public void setDefaultEdgeFontColor(Color defaultEdgeFontColor) {
        synchronized (generalMutex) {
            graphViewComponent.setDefaultEdgeFontColor(defaultEdgeFontColor);
        }
    }

    @Override
    public Font getDefaultVertexFont() {
        synchronized (generalMutex) {
            return graphViewComponent.getDefaultVertexFont();
        }
    }

    @Override
    public void setDefaultVertexFont(Font defaultVertexFont) {
        synchronized (generalMutex) {
            graphViewComponent.setDefaultVertexFont(defaultVertexFont);
        }
    }

    @Override
    public Font getDefaultEdgeFont() {
        synchronized (generalMutex) {
            return graphViewComponent.getDefaultEdgeFont();
        }
    }

    @Override
    public void setDefaultEdgeFont(Font defaultEdgeFont) {
        synchronized (generalMutex) {
            graphViewComponent.setDefaultEdgeFont(defaultEdgeFont);
        }
    }

    @Override
    public void setPanning(final boolean panning) {
        syncExecuteInEDT(new Runnable() {
            @Override
            public void run() {
                graphViewComponent.setPanning(panning);
            }
        });
    }

    @Override
    public boolean isPanning() {
        synchronized (generalMutex) {
            return graphViewComponent.isPanning();
        }
    }

    @Override
    public void setSearch(final boolean search) {
        syncExecuteInEDT(new Runnable() {
            @Override
            public void run() {
                graphViewComponent.setSearch(search);
            }
        });
    }

    @Override
    public boolean isSearch() {
        synchronized (generalMutex) {
            return graphViewComponent.isSearch();
        }
    }

    @Override
    public void zoomIn() {
        synchronized (generalMutex) {
            SwingUtils.invokeInEDTIfNeeded(new Runnable() {
                @Override
                public void run() {
                    synchronized (generalMutex) {
                        graphViewComponent.zoomIn();
                    }
                }
            }, new DefaultSwingUtilsCallBack());
        }
    }

    @Override
    public void zoomOut() {
        synchronized (generalMutex) {
            SwingUtils.invokeInEDTIfNeeded(new Runnable() {
                @Override
                public void run() {
                    synchronized (generalMutex) {
                        graphViewComponent.zoomOut();
                    }
                }
            }, new DefaultSwingUtilsCallBack());
        }
    }

    @Override
    public void setCanvasSize(final Point2D size) {
        synchronized (generalMutex) {
            SwingUtils.invokeInEDTIfNeeded(new Runnable() {
                @Override
                public void run() {
                    synchronized (generalMutex) {
                        graphViewComponent.setCanvasSize(size);
                    }
                }
            }, new DefaultSwingUtilsCallBack());
        }
    }

    @Override
    public void optimizeCanvasSize() {
        synchronized (generalMutex) {
            final MutableDouble minX = new MutableDouble(Double.MAX_VALUE);
            final MutableDouble minY = new MutableDouble(Double.MAX_VALUE);
            final MutableDouble maxX = new MutableDouble(Double.MIN_VALUE);
            final MutableDouble maxY = new MutableDouble(Double.MIN_VALUE);
            graphViewComponent.getVGGraph().bfs(new VGGraph.SearchListener() {
                @Override
                public void onVertex(VGVertexCell vgVertexCell) {
                    mxGeometry geometry = vgVertexCell.getGeometry();
                    if (geometry != null) {
                        if ((Double) minX.getValue() > geometry.getX()) {
                            minX.setValue(geometry.getX());
                        }
                        if ((Double) minY.getValue() > geometry.getY()) {
                            minY.setValue(geometry.getY());
                        }
                        if ((Double) maxX.getValue() < geometry.getX() + geometry.getWidth()) {
                            maxX.setValue(geometry.getX() + geometry.getWidth());
                        }
                        if ((Double) maxY.getValue() < geometry.getY() + geometry.getHeight()) {
                            maxY.setValue(geometry.getY() + geometry.getHeight());
                        }
                    }
                }

                @Override
                public void onEdge(VGEdgeCell vgEdgeCell) {
                    mxGeometry geometry = vgEdgeCell.getGeometry();
                    if (geometry != null && geometry.getPoints() != null) {
                        for (mxPoint point : geometry.getPoints()) {
                            if ((Double) minX.getValue() > point.getX()) {
                                minX.setValue(point.getX());
                            }
                            if ((Double) minY.getValue() > point.getY()) {
                                minY.setValue(point.getY());
                            }
                            if ((Double) maxX.getValue() < point.getX()) {
                                maxX.setValue(point.getX());
                            }
                            if ((Double) maxY.getValue() < point.getY()) {
                                maxY.setValue(point.getY());
                            }
                        }
                    }
                }
            });

            if (Double.compare((Double)minX.getValue(), Double.MAX_VALUE) == 0)
                minX.setValue(0);
            if (Double.compare((Double)minY.getValue(), Double.MAX_VALUE) == 0)
                minY.setValue(0);
            if (Double.compare((Double)maxX.getValue(), Double.MIN_VALUE) == 0)
                minX.setValue(0);
            if (Double.compare((Double)maxY.getValue(), Double.MIN_VALUE) == 0)
                minY.setValue(0);

            setCanvasSize(new Point.Double((Double) maxX.getValue(), (Double) maxY.getValue()));
        }
    }

    @Override
    public void lock(final String text) {
        synchronized (generalMutex) {
            lock = true;
            if (text != null)
                lockText = text;
            else
                lockText = DEFAULT_PLEASE_WAIT_TEXT;
            VGMainServiceHelper.logger.printDebug("Lock with text: " + lockText);
        }
        SwingUtils.invokeInEDTIfNeeded(new Runnable() {
            @Override
            public void run() {
                //synchronized (generalMutex) {
                    rebuildView();
                //}
            }
        }, new DefaultSwingUtilsCallBack());
    }

    @Override
    public void unlock() {
        synchronized (generalMutex) {
            lock = false;
            VGMainServiceHelper.logger.printDebug("Unlock with text: " + lockText);
        }
        SwingUtils.invokeInEDTIfNeeded(new Runnable() {
            @Override
            public void run() {
                synchronized (generalMutex) {
                    rebuildView();
                }
            }
        }, new DefaultSwingUtilsCallBack());
    }

    @Override
    public void execute(GraphViewRunnable graphViewRunnable) {
        synchronized (executorMutex) {
            executors.add(graphViewRunnable);
        }
    }

    @Override
    public void notifyOnAddElements(final Collection<Vertex> vertices, final Collection<Edge> edges) {
        syncExecute(new GraphViewRunnable() {
            private List<GraphViewListener> copyListeners;

            @Override
            public void run(GraphView graphView) {
                copyListeners = syncCopyListeners();
                SwingUtils.invokeInEDTIfNeeded(new Runnable() {
                    @Override
                    public void run() {
                        for (GraphViewListener graphViewListener : copyListeners) {
                            try {
                                graphViewListener.onAddElements(vertices, edges);
                            } catch (Throwable ex) {
                                VGMainServiceHelper.logger.printException(ex);
                            }
                        }
                    }
                }, new DefaultSwingUtilsCallBack());
            }
        });
    }

    @Override
    public void notifyOnCollapse(VGVertexCell vgVertexCell, boolean collapsed, boolean downloaded) {
        syncExecute(new GraphViewRunnable() {
            @Override
            public void run(GraphView graphView) {
                executeLayout();
            }
        });
    }

    @Override
    public void notifyOnSelectElements() {
        syncExecute(new GraphViewRunnable() {
            private List<GraphViewListener> copyListeners;
            private Map<Vertex, Map<Attribute, Boolean>> copySelectedVertices;
            private Map<Edge, Map<Attribute, Boolean>> copySelectedEdges;

            @Override
            public void run(GraphView graphView) {
                copyListeners = syncCopyListeners();
                copySelectedVertices = getSelectedVertexAttributes();
                copySelectedEdges = getSelectedEdgeAttributes();
                SwingUtils.invokeInEDTIfNeeded(new Runnable() {
                    @Override
                    public void run() {
                        for (GraphViewListener graphViewListener : copyListeners) {
                            try {
                                graphViewListener.onSelectElements(copySelectedVertices, copySelectedEdges);
                            } catch (Throwable ex) {
                                VGMainServiceHelper.logger.printException(ex);
                            }
                        }
                    }
                }, new DefaultSwingUtilsCallBack());
            }
        });
    }

    @Override
    public void notifyOnStartLayout(final GraphLayout graphLayout) {
        syncExecute(new GraphViewRunnable() {
            private List<GraphViewListener> copyListeners;

            @Override
            public void run(GraphView graphView) {
                copyListeners = syncCopyListeners();
                SwingUtils.invokeInEDTIfNeeded(new Runnable() {
                    @Override
                    public void run() {
                        for (GraphViewListener graphViewListener : copyListeners) {
                            try {
                                graphViewListener.onStartLayout(graphLayout);
                            } catch (Throwable ex) {
                                VGMainServiceHelper.logger.printException(ex);
                            }
                        }
                    }
                }, new DefaultSwingUtilsCallBack());
            }
        });
    }

    @Override
    public void notifyOnStopLayout(final GraphLayout graphLayout) {
        syncExecute(new GraphViewRunnable() {
            private List<GraphViewListener> copyListeners;

            @Override
            public void run(GraphView graphView) {
                copyListeners = syncCopyListeners();
                SwingUtils.invokeInEDTIfNeeded(new Runnable() {
                    @Override
                    public void run() {
                        for (GraphViewListener graphViewListener : copyListeners) {
                            try {
                                graphViewListener.onStopLayout(graphLayout);
                            } catch (Throwable ex) {
                                VGMainServiceHelper.logger.printException(ex);
                            }
                        }
                    }
                }, new DefaultSwingUtilsCallBack());
            }
        });
    }

    @Override
    public void syncExecute(final GraphViewRunnable graphViewRunnable) {
        execute(graphViewRunnable);
    }

    @Override
    public void syncExecuteInEDT(final Runnable runnable) {
        SwingUtils.invokeInEDTIfNeeded(new Runnable() {
            @Override
            public void run() {
                synchronized (generalMutex) {
                    runnable.run();
                }
            }
        }, new DefaultSwingUtilsCallBack());
    }

    public void executeNow() {
        GraphViewRunnable executor;
        boolean check = false;
        synchronized (executorMutex) {
            if (!readyForExecutingFlag)
                return;
            executor = executors.poll();
            if (executor != null) {
                readyForExecutingFlag = false;
                check = true;
            }
        }
        if (check) {
            try {
                synchronized (generalMutex) {
                    executor.run(this);
                }
            } finally {
                // wait that all swing activity were finished
                SwingUtils.invokeInEDTIfNeeded(new Runnable() {
                    @Override
                    public void run() {
                        synchronized (executorMutex) {
                            readyForExecutingFlag = true;
                        }
                    }
                }, new DefaultSwingUtilsCallBack());
            }
        }
    }

    public boolean isReadyForExecuting() {
        synchronized (executorMutex) {
            return !executors.isEmpty() && readyForExecutingFlag;
        }
    }

//==============================================================================
//------------------PRIVATE METHODS---------------------------------------------
    private void rebuildView() {
        innerView.removeAll();

        if (lock) {
            pleaseWaitLabel.setText(lockText);
            innerView.add(pleaseWaitLabel);
        } else {
            innerView.add(graphViewComponent.getView());
        }

        innerView.updateUI();
    }

    private List<GraphViewListener> syncCopyListeners() {
        List<GraphViewListener> copyListeners;
        synchronized (listenerMutex) {
            copyListeners = Lists.newArrayList(listeners);
        }
        return copyListeners;
    }

    private Map<Attribute, Boolean> copyAndConvertMap(Map<Attribute, MutableBoolean> map) {
        Map<Attribute, Boolean> result = Maps.newHashMap();
        for (Attribute key : map.keySet()) {
            result.put(key, map.get(key).booleanValue());
        }
        return result;
    }
}
