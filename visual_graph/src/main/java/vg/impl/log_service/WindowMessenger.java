package vg.impl.log_service;

import vg.impl.main_service.VGMainServiceHelper;
import vg.interfaces.log_service.IWindowMessenger;
import vg.shared.gui.SwingUtils;
import vg.shared.user_interface.DefaultSwingUtilsCallBack;

import javax.swing.*;

/**
 * @author Timur Zolotuhin (e-mail: tzolotuhin@gmail.com)
 */
public class WindowMessenger implements IWindowMessenger {
    @Override
	public void errorMessage(final String text, final String title, final WindowMessengerCallBack callBack) {
        VGMainServiceHelper.logger.printError("Title: " + title + ", text: " + text);
		SwingUtils.invokeInEDTIfNeeded(new Runnable() {
			@Override
			public void run() {
                String msg = text + "\n\nAdditional information : \n" + VGMainServiceHelper.logger.getLoggerInfo();
                Object[] options = {"Close"};
                JOptionPane.showOptionDialog(null, msg, title, JOptionPane.DEFAULT_OPTION, JOptionPane.ERROR_MESSAGE, null, options,options[0]);
                if (callBack != null)
                    callBack.execute();
			}
		}, new DefaultSwingUtilsCallBack());
	}

    @Override
    public void warningMessage(final String text, final String title, final WindowMessengerCallBack callBack) {
        VGMainServiceHelper.logger.printError(text);
		if (SwingUtilities.isEventDispatchThread()) {
			String msg;
			if(text != null) {
				msg = text + "\n\nAdditional information : \n" + VGMainServiceHelper.logger.getLoggerInfo();
			} else {
				msg = "Additional information : \n" + VGMainServiceHelper.logger.getLoggerInfo();
			}
			Object[] options = {"Close"};
			JOptionPane.showOptionDialog(null,msg,title,JOptionPane.DEFAULT_OPTION,JOptionPane.WARNING_MESSAGE,null,options,options[0]);
		} else {
			SwingUtilities.invokeLater(new Runnable() {			
				public void run() {
					warningMessage(text, title, callBack);
				}
			});
		}
	}

    @Override
    public void infoMessage(final String text, final String title) {
        VGMainServiceHelper.logger.printInfo(text);
		if (SwingUtilities.isEventDispatchThread()) {
			Object[] options = {"Close"};
			JOptionPane.showOptionDialog(null,text,title,JOptionPane.DEFAULT_OPTION,JOptionPane.INFORMATION_MESSAGE,null,options,options[0]);
		} else {
			SwingUtilities.invokeLater(new Runnable() {			
				public void run() {
					infoMessage(text, title);
				}
			});
		}
	}

    @Override
    public void infoMessage(String text) {
        infoMessage(text, "Info");
    }

    public String passwordMessage() {
        JPanel panel = new JPanel();
        JLabel label = new JLabel("Enter a password:");
        JPasswordField pass = new JPasswordField(10);
        panel.add(label);
        panel.add(pass);
        String[] options = new String[]{"Cancel", "Ok"};
        int option = JOptionPane.showOptionDialog(null, panel, "The title", JOptionPane.OK_OPTION, JOptionPane.PLAIN_MESSAGE, null, options, options[1]);
        if(option == 1) {
            return new String(pass.getPassword());
        }
        return null;
    }
}
