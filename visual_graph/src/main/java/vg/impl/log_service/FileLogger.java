package vg.impl.log_service;

import org.apache.commons.lang.exception.ExceptionUtils;
import vg.interfaces.log_service.ILogger;
import vg.shared.utils.DateUtils;
import vg.shared.utils.FileUtils;
import vg.shared.utils.IOUtils;
import vg.shared.utils.RandomUtils;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;

/**
 * This class realizes interface ILogger.
 * Messages logs to file.
 *
 * @author Timur Zolotuhin (e-mail: tzolotuhin@gmail.com)
 */
public class FileLogger implements ILogger {
    // Constants
    private static final int STACK_TRACE_DEPTH = 4;

    // Main data
    private File logFile;
    private DataOutputStream output;
    private final ConsoleLogger consoleLogger;

    private boolean enableDebugMode;

    // Mutex
    private final Object generalMutex = new Object();

    public FileLogger() {
        this(new File(FileUtils.getWorkingDir() + "log" + File.separator + DateUtils.getStartAppTimeInDefaultFormat() + "_" + RandomUtils.generateRandomUUID() + "_log.txt"));
    }

    public FileLogger(File logFile) {
		this.logFile = logFile;
        consoleLogger = new ConsoleLogger();

		// create log directory, if it isn't exist
        File logDir = logFile.getParentFile();
		if(!logDir.exists() && !logDir.mkdirs()) {
            throw new RuntimeException("Couldn't to create log directory " + logDir.getAbsolutePath());
		}

        // try open file for writing
        try {
            FileOutputStream fos = new FileOutputStream(logFile);
			output = new DataOutputStream(fos);
		} catch (IOException ex) {
			throw new RuntimeException("Couldn't to create file " + logFile);
		}
	}

    @Override
    public void enableSrcInfo(boolean enable, String srcDir) {
        synchronized (generalMutex) {
            consoleLogger.enableSrcInfo(enable, srcDir);
        }
    }

    @Override
    public void enableAdvancedInfo(boolean enable) {
        synchronized (generalMutex) {
            consoleLogger.enableAdvancedInfo(enable);
        }
    }

    @Override
    public void enableDebugMessage(boolean enable) {
        synchronized (generalMutex) {
            enableDebugMode = enable;

            consoleLogger.enableDebugMessage(enable);
        }
    }

    @Override
    public void printErrorAndThrowRuntimeException(String message) throws RuntimeException {
        synchronized (generalMutex) {
            printError(consoleLogger.getMessageFormatter().formatMessage(message, "ERROR", STACK_TRACE_DEPTH));
            throw new RuntimeException(message);
        }
    }

    @Override
	public void printDebug(String message) {
        synchronized (generalMutex) {
            if (!enableDebugMode) return;

            try {
                // print to console
                consoleLogger.printDebug(message);


                // print to file
                output.write((consoleLogger.getMessageFormatter().formatMessage(message, "DEBUG", STACK_TRACE_DEPTH) + IOUtils.getNewLineSeparator()).getBytes());
                output.flush();
            } catch (IOException ex) {
                consoleLogger.printDebug(message);
            }
        }
	}

    @Override
    public void printDebug(String message, Throwable ex) {
        synchronized (generalMutex) {
            if (!enableDebugMode) return;

            printDebug(message);
            printException(ex);
        }
    }

    @Override
    public void printError(String message) {
        synchronized (generalMutex) {
            try {
                // print to console
                consoleLogger.printError(message);

                // print to file
                output.write((consoleLogger.getMessageFormatter().formatMessage(message, "ERROR", STACK_TRACE_DEPTH) + IOUtils.getNewLineSeparator()).getBytes());
                output.flush();
            } catch (IOException ex) {
                consoleLogger.printError(message);
            }
        }
	}

    @Override
    public void printError(String message, Throwable ex) {
        synchronized (generalMutex) {
            printError(message);
            printException(ex);
        }

    }

    @Override
    public void printInfo(String message) {
        synchronized (generalMutex) {
            try {
                // print to console
                consoleLogger.printInfo(message);

                // print to file
                output.write((consoleLogger.getMessageFormatter().formatMessage(message, "INFO", STACK_TRACE_DEPTH) + IOUtils.getNewLineSeparator()).getBytes());
                output.flush();
            } catch (IOException ex) {
                consoleLogger.printInfo(message);
            }
        }
	}

    @Override
    public void printException(Throwable exception) {
        synchronized (generalMutex) {
            if (exception == null) return;

            try {
                output.write((consoleLogger.getMessageFormatter().formatMessage(ExceptionUtils.getStackTrace(exception), "TRACE", STACK_TRACE_DEPTH) + IOUtils.getNewLineSeparator()).getBytes());
            } catch (IOException ex) {
                consoleLogger.printException(ex);
            } finally {
                consoleLogger.printException(exception);
            }
        }
	}

    @Override
    public String getLoggerInfo() {
        synchronized (generalMutex) {
            return "Path to log file: " + logFile;
        }
	}
}
