package vg.impl.log_service;

import org.apache.commons.lang.exception.ExceptionUtils;
import vg.interfaces.log_service.ILogger;

/**
 * Messages are displayed on standard output.
 *
 * @author Timur Zolotuhin (e-mail: tzolotuhin@gmail.com)
 */
class ConsoleLogger implements ILogger {
    // Constants
    private static final int STACK_TRACE_DEPTH = 4;

    // Main data
    private boolean enableDebugMode;
    private MessageFormatter messageFormatter = new MessageFormatter();

    // Mutex
    private final Object generalMutex = new Object();

    @Override
    public void enableDebugMessage(boolean enable) {
        synchronized (generalMutex) {
            enableDebugMode = enable;
        }
    }

    @Override
    public void enableSrcInfo(boolean enable, String srcDir) {
        synchronized (generalMutex) {
            messageFormatter.setSrcDir(srcDir);
            messageFormatter.setSrcInfo(enable);
        }
    }

    @Override
    public void enableAdvancedInfo(boolean enable) {
        synchronized (generalMutex) {
            messageFormatter.setAdvancedInfo(enable);
        }
    }

    @Override
    public void printDebug(String message) {
        synchronized (generalMutex) {
            if (!enableDebugMode) return;

            System.out.println(messageFormatter.formatMessage(message, "DEBUG", STACK_TRACE_DEPTH));
        }
	}

    @Override
    public void printDebug(String message, Throwable ex) {
        synchronized (generalMutex) {
            if (!enableDebugMode) return;

            printDebug(message);
            printException(ex);
        }
    }

    @Override
    public void printError(String message) {
        synchronized (generalMutex) {
            System.err.println(messageFormatter.formatMessage(message, "ERROR", STACK_TRACE_DEPTH));
        }
	}

    @Override
    public void printError(String message, Throwable ex) {
        synchronized (generalMutex) {
            printError(message);
            printException(ex);
        }
    }

    @Override
    public void printInfo(String message) {
        synchronized (generalMutex) {
            System.out.println(messageFormatter.formatMessage(message, "INFO", STACK_TRACE_DEPTH));
        }
	}

    @Override
    public void printException(Throwable ex) {
        synchronized (generalMutex) {
            if (ex == null) return;

            System.out.println(messageFormatter.formatMessage(ExceptionUtils.getStackTrace(ex), "TRACE", STACK_TRACE_DEPTH));
        }
	}

    @Override
    public void printErrorAndThrowRuntimeException(String message) throws RuntimeException {
        synchronized (generalMutex) {
            printError(message);
            throw new RuntimeException(message);
        }
    }

    @Override
    public String getLoggerInfo() {
        synchronized (generalMutex) {
            return("This logger uses standard out/err channels");
        }
	}

    public MessageFormatter getMessageFormatter() {
        return messageFormatter;
    }
}
