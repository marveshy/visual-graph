package vg.impl.log_service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vg.interfaces.log_service.ILogger;

/**
 * @author Timur Zolotuhin (e-mail: tzolotuhin@gmail.com)
 */
public class Log4jLogger implements ILogger {
    public static final Logger LOGGER = LoggerFactory.getLogger(Log4jLogger.class);

    @Override
    public void enableSrcInfo(boolean enable, String srcDir) {
        // need disable in property file
    }

    @Override
    public void enableAdvancedInfo(boolean enable) {
        // need disable in property file
    }

    @Override
    public void enableDebugMessage(boolean enable) {
        // need disable in property file
    }

    @Override
    public void printDebug(String message) {
        getLogger().debug(message);
    }

    @Override
    public void printDebug(String message, Throwable ex) {
        getLogger().debug(message, ex);
    }

    @Override
    public void printInfo(String message) {
        getLogger().info(message);
    }

    @Override
    public void printError(String message) {
        LOGGER.error(message);
    }

    @Override
    public void printError(String message, Throwable ex) {
        getLogger().error(message, ex);
    }

    @Override
    public void printException(Throwable ex) {
        getLogger().trace(ex.getMessage(), ex);
    }

    @Override
    public void printErrorAndThrowRuntimeException(String message) throws RuntimeException {
        getLogger().error(message);
        throw new RuntimeException(message);
    }

    @Override
    public String getLoggerInfo() {
        return "log4j logger";
    }

    private Logger getLogger() {
        try {
            StackTraceElement[] stackTraceElements = Thread.currentThread().getStackTrace();
            Class<?> clazz = Class.forName(stackTraceElements[3].getClassName());
            return LoggerFactory.getLogger(clazz);
        } catch (ClassNotFoundException ex) {
            return LOGGER;
        }
    }
}
