package vg.impl.log_service;

import vg.shared.utils.FileUtils;
import vg.shared.utils.IOUtils;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Map;

/**
 * @author Timur Zolotuhin (e-mail: tzolotuhin@gmail.com)
 */
public class MessageFormatter {
    private boolean advancedInfo;
    private boolean srcInfo;

    private String srcDir;

    public MessageFormatter() {}

    public boolean isAdvancedInfo() {
        return advancedInfo;
    }

    public void setAdvancedInfo(boolean advancedInfo) {
        this.advancedInfo = advancedInfo;
    }

    public boolean isSrcInfo() {
        return srcInfo;
    }

    public void setSrcInfo(boolean srcInfo) {
        this.srcInfo = srcInfo;
    }

    public String getSrcDir() {
        return srcDir;
    }

    public void setSrcDir(String srcDir) {
        this.srcDir = srcDir;
    }

    public String formatMessage(String message, String severity, int stackTraceDepth) {
        message = new SimpleDateFormat("dd.M.yyyy H.mm.ss.SSS").format(System.currentTimeMillis()) + " :" + severity + ": " + message;

        StackTraceElement[] stackTraceElements = Thread.currentThread().getStackTrace();

        if (stackTraceDepth >= stackTraceElements.length) {
            return message;
        }

        StackTraceElement currentStackTraceElement = stackTraceElements[stackTraceDepth];

        if (srcInfo) {
            String srcFileName = srcDir + File.separator + currentStackTraceElement.getClassName().replace(".", File.separator) + ".java";
            String srcCodeBefore = "";
            String srcCodeAfter = "";
            try {
                Map<Integer, String> srcCodeMap = FileUtils.readLinesFromFile(new File(srcFileName), Arrays.asList(currentStackTraceElement.getLineNumber() - 1));
                for (Integer srcCodeLineNumber : srcCodeMap.keySet()) {
                    StackTraceElement stackTraceElement = new StackTraceElement(currentStackTraceElement.getClassName(),
                            currentStackTraceElement.getMethodName(),
                            currentStackTraceElement.getFileName(),
                            srcCodeLineNumber);
                    srcCodeBefore += stackTraceElement + "[SRC: " + srcCodeMap.get(srcCodeLineNumber) + "]" + IOUtils.getNewLineSeparator();
                }

                srcCodeMap = FileUtils.readLinesFromFile(new File(srcFileName), Arrays.asList(currentStackTraceElement.getLineNumber() + 1));
                for (Integer srcCodeLineNumber : srcCodeMap.keySet()) {
                    StackTraceElement stackTraceElement = new StackTraceElement(currentStackTraceElement.getClassName(),
                            currentStackTraceElement.getMethodName(),
                            currentStackTraceElement.getFileName(),
                            srcCodeLineNumber);
                    srcCodeAfter += stackTraceElement + "[SRC: " + srcCodeMap.get(srcCodeLineNumber) + "] " + IOUtils.getNewLineSeparator();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }

            return "LINK: " + currentStackTraceElement + IOUtils.getNewLineSeparator() +
                    "MESSAGE: " + message + IOUtils.getNewLineSeparator() + IOUtils.getNewLineSeparator() +
                    "BEFORE: " + srcCodeBefore +
                    "AFTER: " + srcCodeAfter +
                    "****************************************************************************************";
        } else {
            if (advancedInfo) {
                return "LINK: " + currentStackTraceElement + IOUtils.getNewLineSeparator() +
                        "MESSAGE: " + message + IOUtils.getNewLineSeparator() +
                        "****************************************************************************************";
            } else {
                return message;
            }
        }
    }
}
