package vg.impl.remote_service;

import org.apache.commons.lang.NotImplementedException;
import vg.impl.main_service.VGMainServiceHelper;
import vg.interfaces.remote_service.RemoteService;

import javax.xml.ws.Endpoint;

/**
 * @author Timur Zolotuhin (tzolotuhin@parallels.com)
 */
public class RemoteServiceImpl implements RemoteService {
    // Main data
    private boolean isStarted = false;

    // Mutex
    private final Object generalMutex = new Object();

    @Override
    public void start() {
        VGMainServiceHelper.executorService.execute(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    try {
                        synchronized (generalMutex) {
                            Endpoint.publish("http://localhost:9999/ws/vg_remote_api", new VGRemoteAPI());
                            isStarted = true;
                            return;
                        }
                    } catch (Throwable ex) {
                        VGMainServiceHelper.logger.printDebug("Can't start server: " + ex.getMessage());
                    }

                    try {
                        Thread.sleep(5000);
                    } catch (InterruptedException ex) {
                        VGMainServiceHelper.logger.printException(ex);
                    }
                }
            }
        });
    }

    @Override
    public void stop() {
        throw new NotImplementedException("Please, implement the method");
    }

    @Override
    public boolean isStarted() {
        synchronized (generalMutex) {
            return isStarted;
        }
    }
}
