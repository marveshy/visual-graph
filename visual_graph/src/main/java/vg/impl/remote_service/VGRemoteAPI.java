package vg.impl.remote_service;

import vg.impl.main_service.VGMainServiceHelper;

import javax.jws.WebService;

/**
 * @author Timur Zolotuhin (tzolotuhin@parallels.com)
 */
@WebService
public class VGRemoteAPI {
    public void openGraphFromData(String name, String format, String content) {
        try {
            VGMainServiceHelper.graphDecoderService.openGraph(name, content, format);
        } catch (Throwable ex) {
            VGMainServiceHelper.logger.printException(ex);
        }
    }

    public void ping() {
        VGMainServiceHelper.logger.printDebug("ping was called");
    }

    public String about() {
        return "Visual Graph";
    }
}
