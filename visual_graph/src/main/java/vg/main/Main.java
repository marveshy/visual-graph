package vg.main;

import com.google.common.collect.Lists;
import vg.shared.gui.SwingUtils;
import vg.impl.main_service.VGMainInitializer;
import vg.impl.main_service.VGMainServiceHelper;
import vg.plugins.attribute_manager.AttributeManagerPlugin;
import vg.plugins.graph_analyzer.GraphAnalyzerPlugin;
import vg.plugins.graph_comparator.GraphComparatorPlugin;
import vg.plugins.graph_settings.GraphSettingsPlugin;
import vg.plugins.graph_view.*;
import vg.plugins.help.about.AboutPlugin;
import vg.plugins.layout.GraphLayoutPlugin;
import vg.plugins.navigator.NavigatorPlugin;
import vg.plugins.notepad.NotepadPlugin;
import vg.plugins.opener.GraphOpenerPlugin;
import vg.plugins.skin.SkinPlugin;
import vg.interfaces.plugin_service.Plugin;
import vg.shared.user_interface.DefaultSwingUtilsCallBack;

import java.util.List;

/**
 * Enter point.
 *
 * @author Timur Zolotuhin (e-mail: tzolotuhin@gmail.com)
 */
public class Main {
	public static void main(String[] args) {
        // execute initial preparations
        VGMainInitializer.executeInitialPreparations(args);

        VGMainServiceHelper.logger.enableDebugMessage(true);
        VGMainServiceHelper.logger.enableAdvancedInfo(false);

        VGMainServiceHelper.logger.printInfo("Start program");
        VGMainServiceHelper.userInterfaceService.start();

        // try to start remote service
        try {
            VGMainServiceHelper.remoteService.start();
        } catch (Throwable ex) {
            VGMainServiceHelper.logger.printError(ex.getMessage(), ex);
        }

        // add plugins
        final List<Plugin> plugins = Lists.newArrayList();
        plugins.add(new SkinPlugin());
        plugins.add(new GraphOpenerPlugin());
        plugins.add(new GraphLayoutPlugin());
        plugins.add(new FindPlugin());
        plugins.add(new GraphSettingsPlugin());
        plugins.add(new AttributeManagerPlugin());
        plugins.add(new ZoomPlugin());
        plugins.add(new PanningButtonPlugin());
        plugins.add(new AboutPlugin());
        plugins.add(new NavigatorPlugin());
        plugins.add(new OpenSubGraphPlugin());
        plugins.add(new NeighborhoodsPlugin());
        plugins.add(new PathsSearcherPlugin());
        plugins.add(new GraphComparatorPlugin());
        plugins.add(new GraphAnalyzerPlugin());
        plugins.add(new NotepadPlugin());

        SwingUtils.invokeInEDTIfNeeded(new Runnable() {
            @Override
            public void run() {
                for (final Plugin plugin : plugins) {
                    try {
                        plugin.install();
                    } catch (Throwable ex) {
                        VGMainServiceHelper.logger.printError(ex.getMessage(), ex);
                    }
                }
            }
        }, new DefaultSwingUtilsCallBack());
	}
}
