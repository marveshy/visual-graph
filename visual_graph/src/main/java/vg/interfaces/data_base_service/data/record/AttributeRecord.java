package vg.interfaces.data_base_service.data.record;

import org.apache.commons.lang.Validate;

import java.io.Serializable;

public class AttributeRecord implements Cloneable, Serializable {
	// Defines
	public static final int VERTEX_OWNER_TYPE = 1;
	public static final int EDGE_OWNER_TYPE = 2;

    public static final int STRING_ATTRIBUTE_TYPE = 1;
    public static final int BOOLEAN_ATTRIBUTE_TYPE = 2;
    public static final int INTEGER_ATTRIBUTE_TYPE = 3;
    public static final int DOUBLE_ATTRIBUTE_TYPE = 4;

	// Main data
	private int id;
    private int ownerId;
    private int ownerType;

    private String name = null;
	private Object value = null;
    private boolean visible;
    private int type;

    public AttributeRecord(int id, int ownerId, int ownerType, String name, String value, int type, boolean visible) {
        this.id = id;
        this.ownerId = ownerId;
        this.ownerType = ownerType;
        this.name = name;
        this.visible = visible;

        setValue(value, type);
    }

    public int getId() {
        return id;
    }

    public int getOwnerId() {
        return ownerId;
    }

    public int getOwnerType() {
        return ownerType;
    }

    public String getName() {
        return name;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public int getType() {
        return type;
    }

    /**
     * Returns string value, if type is string, otherwise <b>null</b>
     */
    public String getStringValue() {
        return value.toString();
    }

    /**
     * Returns integer value, if type is integer, otherwise <b>null</b>
     */
    public Integer getIntegerValue() {
        if (type == INTEGER_ATTRIBUTE_TYPE)
            return (Integer)value;
        return null;
    }

    /**
     * Returns boolean value, if type is boolean, otherwise <b>null</b>
     */
    public Boolean getBooleanValue() {
        if (type == BOOLEAN_ATTRIBUTE_TYPE)
            return (Boolean)value;
        return null;
    }

    /**
     * Returns double value, if type is double, otherwise <b>null</b>
     */
    public Double getDoubleValue() {
        if (type == DOUBLE_ATTRIBUTE_TYPE)
            return (Double)value;
        return null;
    }

    /**
     * Returns double value, if type is real, otherwise <b>null</b>
     */
    public Double getRealValue() {
        if (isIntegerType())
            return ((Integer)value).doubleValue();

        if (isDoubleType())
            return (Double)value;

        return null;
    }

    public boolean isStringType() {
        return getType() == STRING_ATTRIBUTE_TYPE;
    }

    public boolean isBooleanType() {
        return getType() == BOOLEAN_ATTRIBUTE_TYPE;
    }

    public boolean isIntegerType() {
        return getType() == INTEGER_ATTRIBUTE_TYPE;
    }

    public boolean isDoubleType() {
        return getType() == DOUBLE_ATTRIBUTE_TYPE;
    }

    public boolean isRealType() {
        return isDoubleType() || isIntegerType();
    }

    public void setName(String newName) {
        Validate.notNull(newName);

        name = newName;
    }

    public void setStringValue(String newValue) {
        Validate.notNull(newValue);

        type = STRING_ATTRIBUTE_TYPE;
        value = newValue;
    }

    public void setBooleanValue(Boolean newValue) {
        Validate.notNull(newValue);

        type = BOOLEAN_ATTRIBUTE_TYPE;
        value = newValue;
    }

    public void setIntegerValue(Integer newValue) {
        Validate.notNull(newValue);

        type = INTEGER_ATTRIBUTE_TYPE;
        value = newValue;
    }

    public void setDoubleValue(Double newValue) {
        Validate.notNull(newValue);

        type = DOUBLE_ATTRIBUTE_TYPE;
        value = newValue;
    }

    public void setValue(String newValue, int newType) {
        switch (newType) {
            case AttributeRecord.BOOLEAN_ATTRIBUTE_TYPE:
                setBooleanValue(Boolean.valueOf(newValue));
                break;

            case AttributeRecord.DOUBLE_ATTRIBUTE_TYPE:
                setDoubleValue(Double.valueOf(newValue));
                break;

            case AttributeRecord.INTEGER_ATTRIBUTE_TYPE:
                setIntegerValue(Integer.valueOf(newValue));
                break;

            case AttributeRecord.STRING_ATTRIBUTE_TYPE:
            default:
                setStringValue(String.valueOf(newValue));
                break;
        }
    }

    @Override
	public AttributeRecord clone() {
		return new AttributeRecord(id, ownerId, ownerType, name, value.toString(), type, visible);
	}

    public static boolean isTypeAvailable(int type) {
        switch (type) {
            case AttributeRecord.BOOLEAN_ATTRIBUTE_TYPE:
            case AttributeRecord.DOUBLE_ATTRIBUTE_TYPE:
            case AttributeRecord.INTEGER_ATTRIBUTE_TYPE:
            case AttributeRecord.STRING_ATTRIBUTE_TYPE:
                return true;
            default:
                return false;
        }
    }

    public static boolean isRealType(int type) {
        return (type == INTEGER_ATTRIBUTE_TYPE || type == DOUBLE_ATTRIBUTE_TYPE);
    }
}
