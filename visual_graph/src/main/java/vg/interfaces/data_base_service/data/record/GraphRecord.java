package vg.interfaces.data_base_service.data.record;

public class GraphRecord implements Cloneable {
	// Main data
	private final int id;
	private String name;

	public GraphRecord(int id) {
		this.id = id;
		this.name = null;
	}
	
	public GraphRecord(int id, String name) {
		this.id = id;
		this.name = name;
	}

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
	public GraphRecord clone() {
		return new GraphRecord(id, name);
	}
	
	@Override
	public int hashCode() {
		return id;
	}
	
	@Override
	public boolean equals(Object o) {
		return  o != null && (o instanceof GraphRecord && ((GraphRecord)o).id == id);
	}
}
