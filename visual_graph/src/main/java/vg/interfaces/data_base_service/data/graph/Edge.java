package vg.interfaces.data_base_service.data.graph;

import vg.interfaces.data_base_service.data.record.EdgeRecord;

import java.util.ArrayList;
import java.util.List;

/**
 * This class realizes edge between vertexes.
 * Warning: No thread safety.
 *
 * @author Timur Zolotuhin (e-mail: tzolotuhin@gmail.com)
 */
public class Edge extends AttributedItem {
	// Main data
	private Vertex source;
	private Vertex target;
	private EdgeRecord linkToEdgeRecord;
	
	public Edge(Vertex source, Vertex target) {
		this(source, target, new ArrayList<Attribute>());
	}

	public Edge(Vertex source, Vertex target, List<Attribute> attributes) {
		this(source, target, new AttributedItem(attributes), null);
	}

	public Edge(Vertex source, Vertex target, List<Attribute> attributes, EdgeRecord linkToEdgeRecord) {
		this(source, target, new AttributedItem(attributes), linkToEdgeRecord);
	}

	public Edge(Vertex source, Vertex target, AttributedItem item, EdgeRecord linkToEdgeRecord) {
        super(item);

		setNodes(source, target);
		setLinkToEdgeRecord(linkToEdgeRecord);
	}

	public EdgeRecord getLinkToEdgeRecord() {
		return linkToEdgeRecord;
	}

	public void setLinkToEdgeRecord(EdgeRecord linkToEdgeRecord) {
		this.linkToEdgeRecord = linkToEdgeRecord;
	}

	public Vertex getSource() {
		return source;
	}

	public Vertex getTarget() {
		return target;
	}
	
	public void setSource(Vertex source) {
        setNodes(source, this.target);
    }
	
	public void setTarget(Vertex target) {
		setNodes(this.source, target);
	}
	
	public void setNodes(Vertex source, Vertex target) {
        this.source = source;
		this.target = target;
	}
}
