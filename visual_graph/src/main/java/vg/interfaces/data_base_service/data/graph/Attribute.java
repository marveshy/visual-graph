package vg.interfaces.data_base_service.data.graph;

import org.apache.commons.lang.Validate;
import vg.interfaces.data_base_service.data.record.AttributeRecord;

import java.io.Serializable;

/**
 * This class realizes something attribute.
 * Warning: No thread safety.
 *
 * @author Timur Zolotuhin (e-mail: tzolotuhin@gmail.com)
 */
public class Attribute implements Cloneable, Serializable {
	// Main data
	private String name = null;
	private Object value = null;
    private boolean visible = true;
	private int type = AttributeRecord.STRING_ATTRIBUTE_TYPE;
		
//=============================================================================
//-----------------Constructors------------------------------------------------
    public Attribute(String name, Object value, int type, boolean visible) {
        Validate.notNull(name);
        Validate.notNull(value);

        setName(name);
        setValue(value.toString(), type);
        setVisible(visible);
    }

    public Attribute(String name, String value, boolean visible) {
        this(name, value, AttributeRecord.STRING_ATTRIBUTE_TYPE, visible);
    }

    public Attribute(String name, Integer value) {
        this(name, value, AttributeRecord.INTEGER_ATTRIBUTE_TYPE, true);
    }

    public Attribute(String name, Float value) {
        this(name, value, AttributeRecord.DOUBLE_ATTRIBUTE_TYPE, true);
    }

    public Attribute(String name, Double value) {
        this(name, value, AttributeRecord.DOUBLE_ATTRIBUTE_TYPE, true);
    }

    public Attribute(String name, Boolean value) {
        this(name, value, AttributeRecord.BOOLEAN_ATTRIBUTE_TYPE, true);
    }

    protected Attribute(Attribute attribute) {
		this(attribute.name, attribute.value, attribute.getType(), attribute.isVisible());
	}

//==============================================================================
//------------------PUBLIC METHODS----------------------------------------------
	public String getName() {
		return name;
	}

    public int getType() {
        return type;
    }

    /**
     * Returns string value, if type is string, otherwise <b>null</b>
     */
    public String getStringValue() {
        return value.toString();
    }

    /**
     * Returns integer value, if type is integer, otherwise <b>null</b>
     */
    public Integer getIntegerValue() {
        if (type == AttributeRecord.INTEGER_ATTRIBUTE_TYPE)
            return (Integer)value;
        throw new RuntimeException("Value type isn't integer");
    }

    /**
     * Returns boolean value, if type is boolean, otherwise <b>null</b>
     */
    public Boolean getBooleanValue() {
        if (type == AttributeRecord.BOOLEAN_ATTRIBUTE_TYPE)
            return (Boolean)value;
        throw new RuntimeException("Value type isn't boolean");
    }

    /**
     * Returns double value, if type is double, otherwise <b>null</b>
     */
    public Double getDoubleValue() {
        if (type == AttributeRecord.DOUBLE_ATTRIBUTE_TYPE)
            return (Double)value;
        throw new RuntimeException("Value type isn't double");
    }

    public Double getRealValue() {
        if (isIntegerType())
            return ((Integer)value).doubleValue();

        if (isDoubleType())
            return ((Double)value);
        throw new RuntimeException("Value type isn't real");
    }

    public boolean isStringType() {
        return getType() == AttributeRecord.STRING_ATTRIBUTE_TYPE;
    }

    public boolean isBooleanType() {
        return getType() == AttributeRecord.BOOLEAN_ATTRIBUTE_TYPE;
    }

    public boolean isIntegerType() {
        return getType() == AttributeRecord.INTEGER_ATTRIBUTE_TYPE;
    }

    public boolean isDoubleType() {
        return getType() == AttributeRecord.DOUBLE_ATTRIBUTE_TYPE;
    }

    public void setName(String name) {
        Validate.notNull(name);
        this.name = name;
    }

    public boolean isRealType() {
        return isDoubleType() || isIntegerType();
    }

    public void setStringValue(String value) {
        Validate.notNull(value);
        type = AttributeRecord.STRING_ATTRIBUTE_TYPE;
        this.value = value;
    }

    public void setBooleanValue(Boolean value) {
        Validate.notNull(value);
        type = AttributeRecord.BOOLEAN_ATTRIBUTE_TYPE;
        this.value = value;
    }

    public void setIntegerValue(Integer value) {
        Validate.notNull(value);
        type = AttributeRecord.INTEGER_ATTRIBUTE_TYPE;
        this.value = value;
    }

    public void setDoubleValue(Double value) {
        Validate.notNull(value);
        type = AttributeRecord.DOUBLE_ATTRIBUTE_TYPE;
        this.value = value;
    }

    public void setValue(String strValue, int type) {
        switch (type) {
            case AttributeRecord.BOOLEAN_ATTRIBUTE_TYPE:
                setBooleanValue(Boolean.valueOf(strValue));
                break;
            case AttributeRecord.DOUBLE_ATTRIBUTE_TYPE:
                setDoubleValue(Double.valueOf(strValue));
                break;
            case AttributeRecord.INTEGER_ATTRIBUTE_TYPE:
                setIntegerValue(Integer.valueOf(strValue));
                break;
            case AttributeRecord.STRING_ATTRIBUTE_TYPE:
                setStringValue(String.valueOf(strValue));
                break;
            default:
                throw new IllegalArgumentException("Unknown attribute type");
        }
    }

    /**
     * Returns real value or null otherwise.
     */
    public Double castValueToReal() {
        if (isRealType())
            return getRealValue();

        try {
            return Double.valueOf(getStringValue());
        } catch (NumberFormatException ex) {
            return null;
        }
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

	@Override
	public Attribute clone() {
		return new Attribute(this);
	}

    @Override
    public String toString() {
        return "name = " + name + ", value = " + value + ", visible = " + visible;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Attribute attribute = (Attribute) o;

        return name != null ? name.equals(attribute.name) : attribute.name == null;

    }

    @Override
    public int hashCode() {
        return name != null ? name.hashCode() : 0;
    }
}
