package vg.interfaces.data_base_service.data.graph;

import com.google.common.collect.Lists;
import com.google.common.collect.Queues;
import vg.interfaces.data_base_service.data.record.VertexRecord;

import java.util.List;
import java.util.Queue;


/**
 * This class is bean for data of vertex.
 * Warning: No thread safety.
 *
 * @author Timur Zolotuhin (e-mail: tzolotuhin@gmail.com)
 */
public class Vertex extends AttributedItem {
    // Main data
    protected List<Vertex> vertices = Lists.newArrayList();
    private VertexRecord linkToVertexRecord;

    //=============================================================================
//-----------------Constructors------------------------------------------------
    public Vertex() {
    }

    public Vertex(List<Attribute> attributes) {
        this(attributes, null);
    }

    public Vertex(List<Attribute> attributes, VertexRecord vertexRecord) {
        super(attributes);
        setLinkToVertexRecord(vertexRecord);
    }

    //=============================================================================
//-----------------PUBLIC METHODS----------------------------------------------
    protected boolean insertVertex(Vertex vertex, Vertex parent) {
        if (this == parent || parent == null) {
            return vertices.add(vertex);
        }
        for (Vertex bufVertex : vertices) {
            if (bufVertex.insertVertex(vertex, parent))
                return true;
        }
        return false;
    }

    public VertexRecord getLinkToVertexRecord() {
        return linkToVertexRecord;
    }

    public void setLinkToVertexRecord(VertexRecord linkToVertexRecord) {
        this.linkToVertexRecord = linkToVertexRecord;
    }

    public List<Vertex> getChildVertices() {
        return vertices;
    }

    public boolean hasChildren() {
        return vertices.size() != 0;
    }
}
