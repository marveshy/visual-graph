package vg.interfaces.data_base_service.data.record;

import java.io.Serializable;

public class VertexRecord implements Cloneable, Serializable {
	// Defines
	public final static int NO_OWNER_ID = -1;
	
	// Main data
	private final int id;
    private int graphId;
    private int ownerId;

	public VertexRecord(int id, int graphId) {
        this(id, graphId, NO_OWNER_ID);
	}

    public VertexRecord(int id, int graphId, int ownerId) {
        this.id = id;
        this.graphId = graphId;
        this.ownerId = ownerId;
    }

    public int getId() {
        return id;
    }

    public int getGraphId() {
        return graphId;
    }

    public void setGraphId(int graphId) {
        this.graphId = graphId;
    }

    public int getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(int ownerId) {
        this.ownerId = ownerId;
    }

    @Override
	public VertexRecord clone() {
		return new VertexRecord(id, graphId, ownerId);
	}
}
