package vg.interfaces.data_base_service.data.record;

import java.io.Serializable;

public class EdgeRecord implements Cloneable, Serializable {
	// Main data
	private int id;
    private int graphId;
    private int sourceId;
    private int targetId;
	
	public EdgeRecord(int id, int graphId, int sourceId, int targetId) {
		this.id = id;
		this.graphId = graphId;
		this.sourceId = sourceId;
		this.targetId = targetId;
	}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getGraphId() {
        return graphId;
    }

    public void setGraphId(int graphId) {
        this.graphId = graphId;
    }

    public int getSourceId() {
        return sourceId;
    }

    public void setSourceId(int sourceId) {
        this.sourceId = sourceId;
    }

    public int getTargetId() {
        return targetId;
    }

    public void setTargetId(int targetId) {
        this.targetId = targetId;
    }

    @Override
    public EdgeRecord clone() {
        return new EdgeRecord(id, graphId, sourceId, targetId);
    }
}
