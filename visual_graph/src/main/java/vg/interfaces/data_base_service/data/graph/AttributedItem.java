package vg.interfaces.data_base_service.data.graph;

import com.google.common.collect.Lists;
import vg.interfaces.data_base_service.data.record.AttributeRecord;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

/**
 * Adds collections attributes to class which will extended.
 *
 * @author Timur Zolotuhin (e-mail: tzolotuhin@gmail.com)
 */
public class AttributedItem implements Cloneable, Serializable {
	// Main data
	protected List<Attribute> attributes;
	
//==============================================================================
//------------------Constructors------------------------------------------------
	public AttributedItem() {
		this((List<Attribute>)null);
	}
	
	public AttributedItem(List<Attribute> attributes) {
        if (attributes != null)
            this.attributes = attributes;
        else
            this.attributes = Lists.newArrayList();
	}
	
	protected AttributedItem(AttributedItem item) {
        if (item != null)
            attributes = Lists.newArrayList(item.attributes);
        else
            attributes = Lists.newArrayList();
    }
	
//==============================================================================
//------------------PUBLIC METHODS----------------------------------------------
	public void addAttributes(Collection<Attribute> attributes) {
        if (attributes == null || attributes.size() == 0)
            return;

        for (Attribute attr : attributes) {
            addAttribute(attr);
        }
    }

    /**
	 * This method adds a attribute in the vertex.
	 */
	public boolean addAttribute(String key, String value) {
		return attributes.add(new Attribute(key, value, AttributeRecord.STRING_ATTRIBUTE_TYPE, true));
	}
	
	public Attribute addAttribute(String key, String value, int type) {
		return addAttribute(key, value, type, true);
	}

    public Attribute addAttribute(String key, String value, int type, boolean visible) {
        Attribute attr = new Attribute(key, value, type, visible);
        attributes.add(attr);
        return attr;
    }
	
	public boolean addAttribute(Attribute attribute) {
		return attributes.add(attribute);
	}

    public boolean addIntAttribute(String key, int value) {
        return attributes.add(new Attribute(key, value));
    }

    public boolean addFloatAttribute(String key, float value) {
        return attributes.add(new Attribute(key, value));
    }

    public boolean addDoubleAttribute(String key, double value) {
        return attributes.add(new Attribute(key, value));
    }

    public Attribute addBooleanAttribute(String key, boolean value) {
        return addBooleanAttribute(key, value, true);
    }

    public Attribute addBooleanAttribute(String key, boolean value, boolean visible) {
        return addAttribute(key, Boolean.toString(value), AttributeRecord.BOOLEAN_ATTRIBUTE_TYPE, visible);
    }

    /**
	 * This methods returns all attributes of this vertex.
     *
     * Note: isn't null.
	 */
	public List<Attribute> getAttributes() {
        return attributes;
	}
	
	/**
	 * This method returns attribute with 
	 * name = name of parameter, else null.
	 * @param name - name of attribute.
	 * @return attribute or null.
	 */
	public Attribute getAttribute(String name) {
		Collection<Attribute>list = this.attributes;
		for(Attribute buf : list) {
			if(buf.getName().equals(name))
				return buf;
		}
		return null;
	}

	/**
	 * Returns true if the item have no attributes
     */
	public boolean isEmpty() {
		return attributes.size() == 0;
	}
	
	@Override
	public AttributedItem clone() {
		return new AttributedItem(this);
	}
}
