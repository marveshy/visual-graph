package vg.interfaces.data_base_service;

import vg.interfaces.data_base_service.data.graph.*;
import vg.interfaces.data_base_service.data.record.*;

import java.util.List;
import java.util.Map;

/**
 * Interface for graph data base.
 *
 * @author Timur Zolotuhin (e-mail: tzolotuhin@gmail.com)
 */
public interface IGraphDataBaseService {
	/**
	 * Creates new graph and returns the unique id.
     *
	 * @param name - name of the graph.
	 */
	int createGraph(String name);

	/**
	 * Creates record for the vertex and returns the unique id.
	 */
	int createVertex(int graphId, int vertexId);

	/**
	 * Creates header for the edge and returns the unique id.
     *
	 * @param graphId - id of graph.
	 * @param sourceVertexId - id of source vertex.
	 * @param targetVertexId - id of target vertex.
	 */
	int createEdge(int graphId, int sourceVertexId, int targetVertexId);
	
	/**
	 * Creates header for the attribute and returns the unique id.
     *
	 * @param edgeId - id of edge, which will contain the attribute.
     * @param name - attribute name.
     * @param value - string attribute value.
     * @param type - type of value. See AttributeRecord types: STRING_ATTRIBUTE_TYPE, INTEGER_ATTRIBUTE_TYPE, etc.
     * @param visible - for system attributes.
	 */
	int createEdgeAttribute(int edgeId, String name, String value, int type, boolean visible);
	
	/**
	 * Creates header for the attribute and returns the unique id.
     *
	 * @param vertexId - id of vertex, which will contain the attribute.
     * @param name - attribute name.
     * @param value - string attribute value.
     * @param type - type of value. See AttributeRecord types: STRING_ATTRIBUTE_TYPE, INTEGER_ATTRIBUTE_TYPE, etc.
     * @param visible - for system attributes.
     */
	int createVertexAttribute(int vertexId, String name, String value, int type, boolean visible);

    /**
     * Returns list of graph model headers.
     *
     * @param count - count of the headers, if count equals -1, that it returns all headers.
     */
    List<GraphRecord> getGraphRecords(int count);

    VertexRecord getVertexRecord(int vertexId);

    List<VertexRecord> getVertexRecordsByOwnerId(int ownerId, int count);

    EdgeRecord getEdgeRecord(int edgeId);

    List<EdgeRecord> getEdgeRecordsByVertexId(int vertexId);

    List<EdgeRecord> getEdgeRecordsByGraphId(int graphId);

    List<VertexRecord> getRoots(int graphId);

    GraphRecord getGraphRecord(int graphId);

    AttributeRecord getAttributeHeader(int attributeId);

    List<AttributeRecord> getAttributeRecordsByOwner(int ownerId, int ownerType);

    Vertex getVertex(int vertexId);

	Edge getEdge(int edgeId);

	Map.Entry<List<Vertex>, Map.Entry<List<Edge>, List<Edge>>> getSubGraphElements(int vertexOwnerId);

    Attribute getAttribute(int attributeId);

    void modifyVertexHeader(VertexRecord newVertexRecord);

    void modifyEdgeHeader(EdgeRecord newEdgeRecord);

	void close();

    void addListener(GraphDataBaseListener graphDataBaseListener);

    int getVertexCount(int graphId);
    int getGraphCount(int graphId);
    int getEdgeCount(int graphId);

	/**
	 * Calculate general parents for input vertices
	 *
	 * V1 -> [P1, P2, P3, ..., -1]
	 */
	Map<Integer, List<Integer>> findParents(List<Integer> vertexIds);
}
