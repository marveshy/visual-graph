package vg.interfaces.config_service;

import org.apache.commons.cli.ParseException;

/**
 * @author Timur Zolotuhin (tzolotuhin@parallels.com)
 */
public interface ICmdLine {
    /**
     * Parses input arguments and adds options to maps.
     */
    public void parseCmdLineArgs(String[] args) throws ParseException;

    public void addOption(CmdLineOption option);
}
