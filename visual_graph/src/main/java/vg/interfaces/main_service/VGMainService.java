package vg.interfaces.main_service;

import vg.interfaces.config_service.IConfig;
import vg.interfaces.log_service.ILogger;
import vg.interfaces.data_base_service.IGraphDataBaseService;
import vg.interfaces.user_interface_service.UserInterfaceService;

/**
 * @author Timur Zolotuhin (e-mail: tzolotuhin@gmail.com)
 */
public interface VGMainService {
    public ILogger getLogger();

    public IConfig getConfig();

    public IGraphDataBaseService getDataBaseService();

    public UserInterfaceService getUserInterfaceService();
}
