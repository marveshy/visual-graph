package vg.interfaces.log_service;

/**
 * Interface for logger.
 *
 * @author Timur Zolotuhin (e-mail: tzolotuhin@gmail.com)
 */
public interface ILogger {
	void enableDebugMessage(boolean enable);

    void enableSrcInfo(boolean enable, String srcDir);

    void enableAdvancedInfo(boolean enable);

    /**
	 * This method prints debug message.
	 */
	void printDebug(String message);

    /**
     * This method prints debug message.
     */
    void printDebug(String message, Throwable ex);

    /**
	 * This method prints info message.
	 */
	void printInfo(String message);

	/**
	 * This method prints error message.
	 */
	void printError(String message);

    /**
     * This method prints error message.
     */
    void printError(String message, Throwable ex);

    /**
     * Prints the exception.
     */
    void printException(Throwable ex);

    /**
     * This method prints message about error and after throw exception.
     * Example: method was called with illegal arguments.
     */
    void printErrorAndThrowRuntimeException(String message) throws RuntimeException;

    /**
	 * This method returns information about current logger.
	 */
	String getLoggerInfo();
}
