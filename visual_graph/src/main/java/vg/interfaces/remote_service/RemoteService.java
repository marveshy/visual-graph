package vg.interfaces.remote_service;

/**
 * @author Timur Zolotuhin (tzolotuhin@parallels.com)
 */
public interface RemoteService {
    void start();

    void stop();

    boolean isStarted();
}
