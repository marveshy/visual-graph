package vg.interfaces.user_interface_service;

/**
 * Determines methods for user interface observer.
 *
 * All methods will called from EDT.
 *
 * @author Timur Zolotuhin (e-mail: tzolotuhin@gmail.com)
 */
public class UserInterfaceListener {
    public void onChangeTab(UserInterfaceTab tab) {}
    public void onOpenTab(UserInterfaceTab tab) {}
    public void onCloseTab(UserInterfaceTab tab) {}

    public void onAddPanel(UserInterfacePanel panel, int place) {}
    public void onRemovePanel(UserInterfacePanel panel, int place) {}

    public void onQuit() {}
}
