package vg.interfaces.user_interface_service;

import javax.swing.*;

/**
 * This class determines methods for elements of user interface.
 *
 * @author Timur Zolotuhin (e-mail: tzolotuhin@gmail.com)
 */
public interface UserInterfacePanel {
	JPanel getView();
}
