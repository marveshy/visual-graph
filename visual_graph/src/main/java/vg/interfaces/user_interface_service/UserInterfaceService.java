package vg.interfaces.user_interface_service;

import vg.interfaces.main_service.VGMainGlobals;

import javax.swing.*;
import java.awt.*;

/**
 * User interface service.
 *
 * @author Timur Zolotuhin (e-mail: tzolotuhin@gmail.com)
 */
public interface UserInterfaceService {
    // Constants
    Dimension NORTH_INSTRUMENT_PANEL_SIZE = new Dimension(32, 32);
    Dimension WEST_EAST_INSTRUMENT_PANEL_SIZE = new Dimension(25, 90);
    Dimension SOUTH_INSTRUMENT_PANEL_SIZE = new Dimension(110, 25);

    Font INSTRUMENT_PANEL_FONT = new Font(VGMainGlobals.UI_STANDARD_FONT_FAMILY, 0, 12);

    int WEST_PANEL = 1;
    int WEST_SOUTH_PANEL = 2;
    int EAST_SOUTH_PANEL = 4;

    int NORTH_INSTRUMENT_PANEL = 1;
    int SOUTH_INSTRUMENT_PANEL = 2;
    int WEST_INSTRUMENT_PANEL = 3;
    int EAST_INSTRUMENT_PANEL = 4;

    int MAX_PRIORITY = 1;
    int MIN_PRIORITY = 999;

    // Constants: menu
    String FILE_MENU = "File";
    String EDIT_MENU = "Edit";
    String ANALYZE_MENU = "Analyze";
    String WINDOW_MENU = "Window";
    String HELP_MENU = "Help";

    void start();

    /**
     * Adds item to menu
     *
     * Please use constants FILE_MENU, EDIT_MENU, etc.
     */
    void addMenuItem(JMenuItem item, String menu);

    void addInstrument(UserInterfaceInstrument instrument, int place);

    /**
     * If element is null it's mean that need remove panel
     */
    void setPanel(UserInterfacePanel element, int place);

    void addTab(UserInterfaceTab tab, FinishActionCallBack callBack);
    void replaceTab(UserInterfaceTab oldTab, UserInterfaceTab newTab, FinishActionCallBack callBack);
    void selectTab(UserInterfaceTab tab);

    void addObserver(UserInterfaceListener listener);

    /**
     * Max priority is 1, min priority 999
     */
    void addObserver(UserInterfaceListener listener, int priority);

    void quit();

    void refresh();

	/**
	 * Returns main frame, which may use how parent for different dialogs.
	 */
	JFrame getMainFrame();

    interface FinishActionCallBack {
        void onFinishAction();
    }
}
