package vg.interfaces.user_interface_service;

import javax.swing.*;

/**
 * @author Timur Zolotuhin (tzolotuhin@parallels.com)
 */
public interface UserInterfaceInstrument {
    JPanel getView();
}
