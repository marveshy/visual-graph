package vg.interfaces.graph_view_service;

import vg.interfaces.data_base_service.data.graph.*;
import vg.interfaces.graph_layout_service.GraphLayout;

import java.util.Collection;
import java.util.Map;

/**
 * @author Timur Zolotuhin (e-mail: tzolotuhin@gmail.com)
 */
public class GraphViewListener {
    public void onSelectElements(Map<Vertex, Map<Attribute, Boolean>> selectedVertex, Map<Edge, Map<Attribute, Boolean>> selectedEdge) {}

    public void onAddElements(Collection<Vertex> vertices, Collection<Edge> edges) {}

    public void onStartLayout(GraphLayout layout) {};

    public void onStopLayout(GraphLayout layout) {};
}
