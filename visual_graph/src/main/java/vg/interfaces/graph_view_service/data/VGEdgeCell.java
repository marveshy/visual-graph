package vg.interfaces.graph_view_service.data;

import com.google.common.collect.Lists;
import org.apache.commons.lang.StringUtils;
import vg.interfaces.data_base_service.data.graph.Edge;
import vg.interfaces.graph_view_service.GraphView;
import vg.shared.graph.utils.GraphUtils;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author Timur Zolotuhin (e-mail: tzolotuhin@parallels.com)
 */
public class VGEdgeCell extends VGCell {
    // Constants
    private static final String LOCAL_EDGE_PREFIX = "e";

    private static AtomicInteger edgeCounter = new AtomicInteger();

    // Main data
    protected String edgeStyle;
    protected boolean directed;

    protected VGEdgeCell(VGEdgeCell vgEdgeCell) {
        super(vgEdgeCell);
    }

    public VGEdgeCell(Edge edge) {
        super(edge.getLinkToEdgeRecord() == null ? edgeCounter.incrementAndGet() : edge.getLinkToEdgeRecord().getId(), edge);
    }

    @Override
    public Edge getVGObject() {
        return (Edge)super.getVGObject();
    }

    public String vgGetEdgeStyle() {
        return edgeStyle;
    }

    public void vgSetEdgeStyle(String edgeStyle) {
        String oldStyle = getStyle();
        if (oldStyle == null)
            oldStyle = "";

        String[] oldStyles = oldStyle.split(";");
        List<String> newStyles = Lists.newArrayList();
        for (String style : oldStyles) {
            if (style.startsWith(GraphView.NO_EDGE_STYLE)) {
                newStyles.add("");
            } else if (style.startsWith(GraphView.STYLE_EDGE)) {
                newStyles.add("");
            } else {
                newStyles.add(style);
            }
        }

        String newStyle = "";
        for (String style : newStyles) {
            if (!style.isEmpty())
                newStyle += style + ";";
        }
        if (StringUtils.isEmpty(edgeStyle)) {
            newStyle += GraphView.NO_EDGE_STYLE + "=1;";
        } else {
            newStyle += GraphView.STYLE_EDGE + "=" + edgeStyle + ";";
        }

        setStyle(newStyle);

        this.edgeStyle = edgeStyle;
    }

    public boolean isNoEdgeStyle() {
        return StringUtils.isEmpty(edgeStyle);
    }

    public boolean vgIsDirected() {
        return directed;
    }

    public void vgSetDirected(boolean directed) {
        this.directed = directed;
    }

    public VGVertexCell getVGSoure() {
        return (VGVertexCell)getSource();
    }

    public VGVertexCell getVGTarget() {
        return (VGVertexCell)getTarget();
    }

    @Override
    public String getHumanId() {
        return GraphUtils.generateHumanId(id, LOCAL_EDGE_PREFIX);
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return new VGEdgeCell(this);
    }
}
