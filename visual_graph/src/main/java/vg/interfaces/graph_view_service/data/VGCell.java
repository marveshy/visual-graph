package vg.interfaces.graph_view_service.data;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.mxgraph.model.mxCell;
import com.mxgraph.model.mxGeometry;
import com.mxgraph.model.mxICell;
import com.mxgraph.util.mxPoint;
import org.apache.commons.lang.mutable.MutableBoolean;
import vg.interfaces.data_base_service.data.graph.Attribute;

import java.awt.*;
import java.util.List;
import java.util.Map;

/**
 * @author Timur Zolotuhin (tzolotuhin@parallels.com)
 */
public abstract class VGCell extends mxCell {
    protected final int id;
    protected Map<Attribute, MutableBoolean> attributes = Maps.newLinkedHashMap();
    protected List<Attribute> shortInfoAttributes = Lists.newArrayList();

    protected Object vgObject;

    protected Color color;
    protected boolean selected;
    protected int selectIndex;
    protected boolean transparent;
    protected int border;
    protected String shape;
    protected mxPoint textSize = new mxPoint(5, 5);
    protected mxPoint actionPanelSize;

    protected VGCell(VGCell vgCell) {
        this(vgCell.vgGetId(), vgCell.getVGObject());
        setValue(vgCell.cloneValue());
        setStyle(vgCell.getStyle());
        setCollapsed(vgCell.isCollapsed());
        setConnectable(vgCell.isConnectable());
        setEdge(vgCell.isEdge());
        setVertex(vgCell.isVertex());
        setVisible(vgCell.isVisible());
        setParent(null);
        setSource(null);
        setTarget(null);
        children = null;
        edges = null;
        mxGeometry var2 = vgCell.getGeometry();
        if(var2 != null) {
            setGeometry((mxGeometry) var2.clone());
        }

        vgSetAttributes(Maps.newLinkedHashMap(vgCell.vgGetAttributes()));
        vgSetShortInfoAttributes(Lists.newArrayList(vgCell.vgGetShortInfoAttributes()));

        vgSetColor(vgCell.vgGetColor());
        vgSetSelected(vgCell.vgIsSelected());
        vgSetSelectIndex(vgCell.vgGetSelectIndex());
        vgSetTransparent(vgCell.vgIsTransparent());
        vgSetBorder(vgCell.vgGetBorder());
        vgSetShape(vgGetShape());
        vgSetTextSize(vgGetTextSize());
        vgSetActionPanelSize(vgGetActionPanelSize());
    }

    public VGCell(int id, Object vgObject) {
        this.id = id;
        this.vgObject = vgObject;
    }

    public mxGeometry getGeometryInParent(mxICell parentCell) {
        mxICell curr = getParent();
        mxGeometry result = (mxGeometry)getGeometry().clone();
        while (curr != parentCell && curr != null) {
            if (result.getPoints() != null) {
                for (mxPoint point : result.getPoints()) {
                    point.setX(point.getX() + curr.getGeometry().getX());
                    point.setX(point.getY() + curr.getGeometry().getY());
                }
            }
            result.setX(result.getX() + curr.getGeometry().getX());
            result.setY(result.getY() + curr.getGeometry().getY());
            curr = curr.getParent();
        }
        return result;
    }

    public Object getVGObject() {
        return vgObject;
    }

    public void vgSetAttributes(Map<Attribute, MutableBoolean> attributes) {
        this.attributes = attributes;
    }

    public Map<Attribute, MutableBoolean> vgGetAttributes() {
        return attributes;
    }

    public List<Attribute> vgGetShortInfoAttributes() {
        return shortInfoAttributes;
    }

    public void vgSetShortInfoAttributes(List<Attribute> shortInfoAttributes) {
        this.shortInfoAttributes = shortInfoAttributes;
    }

    public Color vgGetColor() {
        return color;
    }

    public void vgSetColor(Color color) {
        this.color = color;
    }

    public boolean vgIsSelected() {
        return selected;
    }

    public void vgSetSelected(boolean selected) {
        this.selected = selected;
    }

    public int vgGetSelectIndex() {
        return selectIndex;
    }

    public void vgSetSelectIndex(int selectIndex) {
        this.selectIndex = selectIndex;
    }

    public boolean vgIsTransparent() {
        return transparent;
    }

    public void vgSetTransparent(boolean transparent) {
        this.transparent = transparent;
    }

    public int vgGetId() {
        return id;
    }

    public abstract String getHumanId();

    public int vgGetBorder() {
        return border;
    }

    public void vgSetBorder(int border) {
        this.border = border;
    }

    public String vgGetShape() {
        return shape;
    }

    public void vgSetShape(String shape) {
        this.shape = shape;
    }

    public mxPoint vgGetTextSize() {
        return textSize;
    }

    public void vgSetTextSize(mxPoint textSize) {
        this.textSize = textSize;
    }

    public mxPoint vgGetActionPanelSize() {
        return actionPanelSize;
    }

    public void vgSetActionPanelSize(mxPoint actionPanelSize) {
        this.actionPanelSize = actionPanelSize;
    }

    @Override
    public String toString() {
        return getHumanId();
    }
}
