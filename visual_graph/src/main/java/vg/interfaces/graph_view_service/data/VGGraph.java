package vg.interfaces.graph_view_service.data;

import com.google.common.collect.Lists;
import com.google.common.collect.Queues;
import com.google.common.collect.Sets;
import com.mxgraph.model.mxCell;
import com.mxgraph.model.mxGeometry;
import com.mxgraph.model.mxGraphModel;
import com.mxgraph.swing.mxGraphComponent;
import com.mxgraph.util.mxEvent;
import com.mxgraph.util.mxEventObject;
import com.mxgraph.util.mxPoint;
import com.mxgraph.view.mxGraph;
import org.apache.commons.lang.Validate;
import org.apache.commons.lang.mutable.MutableObject;
import vg.impl.main_service.VGMainServiceHelper;
import vg.interfaces.data_base_service.data.graph.Edge;
import vg.interfaces.data_base_service.data.graph.Vertex;
import vg.interfaces.data_base_service.data.record.VertexRecord;
import vg.interfaces.graph_view_service.GraphView;

import java.awt.geom.Point2D;
import java.util.*;

/**
 * @author Timur Zolotuhin (tzolotuhin@parallels.com)
 */
public class VGGraph extends mxGraph {
    // Constants
    private final Random randomCellPos = new Random();

    // Main components
    private mxGraphComponent mxGraphComponentReference;
    private GraphView graphViewReference;
    private int maxCellLevel = -1;
    private Point2D graphSize;

    public mxGraphComponent getVGMxGraphComponentReference() {
        return mxGraphComponentReference;
    }

    public void setVGGraphComponentReference(mxGraphComponent mxGraphComponentReference) {
        this.mxGraphComponentReference = mxGraphComponentReference;
    }

    public GraphView getGraphViewReference() {
        return graphViewReference;
    }

    public void setGraphViewReference(GraphView graphViewReference) {
        this.graphViewReference = graphViewReference;
    }

    public void bfs(SearchListener searchListener) {
        Queue<Object> vertexQueue = Queues.newArrayDeque(Arrays.asList(getChildCells(getDefaultParent(), true, false)));
        for (Object vertex : vertexQueue) {
            searchListener.onVertex((VGVertexCell) vertex);
        }
        Queue<Object> edgeQueue = Queues.newArrayDeque(Arrays.asList(getChildCells(getDefaultParent(), false, true)));
        for (Object edge : edgeQueue) {
            searchListener.onEdge((VGEdgeCell)edge);
        }
        while (!vertexQueue.isEmpty()) {
            Object parent = vertexQueue.poll();
            for (Object vertex : getChildVertices(parent)) {
                searchListener.onVertex((VGVertexCell)vertex);
                vertexQueue.add(vertex);
            }
            for (Object edge : getChildEdges(parent)) {
                searchListener.onEdge((VGEdgeCell)edge);
            }
        }
        searchListener.onFinish();
    }

    public VGVertexCell vgInsertVertex(Vertex vertex, Object parent) {
        if (parent == null)
            parent = getDefaultParent();

        VGVertexCell vgVertexCell = (VGVertexCell)createVertex(parent, null, vertex, randomCellPos.nextInt(5000), randomCellPos.nextInt(5000), 100, 100, null, false);

        int level = calcCellLevel(vgVertexCell);
        if (level > maxCellLevel)
            maxCellLevel = level;

        vgVertexCell.setCollapsed(true);
        if (parent instanceof VGVertexCell) {
            ((VGVertexCell)parent).setCollapsed(false);
        }

        return (VGVertexCell)addCell(vgVertexCell, parent);
    }

    public VGEdgeCell vgInsertEdge(Edge edge, Object parent, Object source, Object target) {
        if (parent == null)
            parent = getDefaultParent();

        VGEdgeCell vgEdgeCell = (VGEdgeCell)createEdge(parent, null, edge, source, target, null);

        int level = calcCellLevel(vgEdgeCell);
        if (level > maxCellLevel)
            maxCellLevel = level;

        return (VGEdgeCell)addCell(vgEdgeCell, parent);
    }

    @Override
    public Object createVertex(Object parent, String id, Object value, double x, double y, double width, double height, String style, boolean relative) {
        VGVertexCell vgVertexCell = new VGVertexCell((Vertex)value);
        vgVertexCell.setId(id);
        vgVertexCell.setVertex(true);
        vgVertexCell.setConnectable(true);

        vgVertexCell.setGeometry(new mxGeometry(x, y, width, height));
        vgVertexCell.getGeometry().setRelative(relative);

        return vgVertexCell;
    }

    @Override
    public Object createEdge(Object parent, String id, Object value, Object source, Object target, String style) {
        VGEdgeCell vgEdgeCell = new VGEdgeCell((Edge)value);

        vgEdgeCell.setId(id);
        vgEdgeCell.setEdge(true);
        vgEdgeCell.setGeometry(new mxGeometry());
        vgEdgeCell.getGeometry().setRelative(true);
        vgEdgeCell.setStyle(style);

        return addEdge(vgEdgeCell, parent, source, target, null);
    }

    public VGVertexCell getVGVertexCellByVertex(final Vertex vertex) {
        final MutableObject mutableObject = new MutableObject();
        bfs(new SearchListener() {
            @Override
            public void onVertex(VGVertexCell vgVertexCell) {
                if (vgVertexCell.getVGObject() == vertex)
                    mutableObject.setValue(vgVertexCell);

            }
        });
        return (VGVertexCell)mutableObject.getValue();
    }

    public VGEdgeCell getVGEdgeCellByEdge(final Edge edge) {
        final MutableObject mutableObject = new MutableObject();
        bfs(new SearchListener() {
            @Override
            public void onEdge(VGEdgeCell vgEdgeCell) {
                if (vgEdgeCell.getVGObject() == edge)
                    mutableObject.setValue(vgEdgeCell);

            }
        });
        return (VGEdgeCell)mutableObject.getValue();
    }

    public int getVGVertexCount() {
        final MutableObject mutableObject = new MutableObject();
        bfs(new SearchListener() {
            int vertexCount = 0;

            @Override
            public void onVertex(VGVertexCell vgVertexCell) {
                vertexCount++;
            }

            @Override
            public void onFinish() {
                mutableObject.setValue(vertexCount);
            }
        });
        return (int)mutableObject.getValue();
    }

    public int getVGEdgeCount() {
        final MutableObject mutableObject = new MutableObject();
        bfs(new SearchListener() {
            int edgeCount = 0;

            @Override
            public void onEdge(VGEdgeCell vgEdgeCell) {
                edgeCount++;
            }

            @Override
            public void onFinish() {
                mutableObject.setValue(edgeCount);
            }
        });
        return (int)mutableObject.getValue();
    }

    public VGEdgeCell getUndirectedVGEdgeCellByVertices(final VGVertexCell source, final VGVertexCell target) {
        final MutableObject mutableObject = new MutableObject();
        bfs(new SearchListener() {
            @Override
            public void onEdge(VGEdgeCell vgEdgeCell) {
                if ((vgEdgeCell.getSource() == source && vgEdgeCell.getTarget() == target) ||
                        vgEdgeCell.getSource() == target && vgEdgeCell.getTarget() == source) {
                    mutableObject.setValue(vgEdgeCell);
                }
            }
        });
        return (VGEdgeCell)mutableObject.getValue();
    }

    public void removeAllCells() {
        final List<VGVertexCell> vgVertexCells = Lists.newArrayList();
        bfs(new SearchListener() {
            @Override
            public void onVertex(VGVertexCell vgVertexCell) {
                vgVertexCells.add(vgVertexCell);

            }
        });

        for (VGVertexCell vgVertexCell : vgVertexCells) {
            getModel().remove(vgVertexCell);
        }
    }

    public void removeVGCell(List<VGCell> vgCells) {
        for (VGCell vgCell : vgCells) {
            getModel().remove(vgCell);
        }
    }

    public Set<VGEdgeCell> getVGEdgesForVertices(List<VGVertexCell> vertices) {
        // extend vertices
        Set<VGVertexCell> extendedVertices = Sets.newHashSet();
        for (VGVertexCell vertex : vertices) {
            extendedVertices.add(vertex);
            extendedVertices.addAll(vertex.vgGetAllVertexChildren());
        }

        // search edges
        final MutableObject mutableObject = new MutableObject();
        mutableObject.setValue(new HashSet<VGEdgeCell>());

        SearchListener searchListener = new SearchListener() {
            @Override
            public void onEdge(VGEdgeCell vgEdgeCell) {
                if (extendedVertices.contains(vgEdgeCell.getVGSoure()) || extendedVertices.contains(vgEdgeCell.getVGTarget())) {
                    ((Set<VGEdgeCell>)mutableObject.getValue()).add(vgEdgeCell);
                }
            }
        };

        bfs(searchListener);

        return (Set<VGEdgeCell>)mutableObject.getValue();
    }

    /**
     * If level equal -1 return all vertices
     */
    public List<VGVertexCell> getVGVerticesByLevel(final int level) {
        final List<VGVertexCell> result = Lists.newArrayList();
        bfs(new SearchListener() {
            @Override
            public void onVertex(VGVertexCell vgVertexCell) {
                if (level == -1) {
                    result.add(vgVertexCell);
                    return;
                }
                if (calcCellLevel(vgVertexCell) == level)
                    result.add(vgVertexCell);

            }
        });
        return result;
    }

    /**
     * If level equal -1 return all edges
     */
    public Set<VGEdgeCell> getVGEdgesByLevel(final int level) {
        if (level <= 1 && level != -1) {
            return vgGetEdgeCells(getDefaultParent());
        }

        final Set<VGEdgeCell> result = Sets.newHashSet();
        bfs(new SearchListener() {
            @Override
            public void onEdge(VGEdgeCell vgEdgeCell) {
                if(level == -1) {
                    result.add(vgEdgeCell);
                    return;
                }

                if (calcCellLevel(vgEdgeCell) == level)
                    result.add(vgEdgeCell);
            }
        });
        return result;
    }

    public Point2D getGraphSize() {
        return graphSize;
    }

    public void setGraphSize(Point2D graphSize) {
        this.graphSize = graphSize;
    }

    public int getMaxCellLevel() {
        return maxCellLevel;
    }

    @Override
    public void cellsFolded(Object[] cells, boolean collapse, boolean recurse, boolean checkFoldable) {
        if (cells != null && cells.length > 0) {
            model.beginUpdate();
            try {
                for (Object cell : cells) {
                    if ((!checkFoldable || isCellFoldable(cell, collapse)) && collapse != isCellCollapsed(cell)) {
                        model.setCollapsed(cell, collapse);
                        //swapBounds(cell, collapse);

                        if (isExtendParent(cell)) {
                            extendParent(cell);
                        }

                        if (recurse) {
                            Object[] children = mxGraphModel.getChildren(model, cell);
                            cellsFolded(children, collapse, true);
                        }
                    }
                }

                fireEvent(new mxEventObject(mxEvent.CELLS_FOLDED, "cells",
                        cells, "collapse", collapse, "recurse", recurse));
            } finally {
                model.endUpdate();
            }
        }
    }

    @Override
    public boolean isCellFoldable(Object cell, boolean collapse) {
        if (cell instanceof VGVertexCell) {
            VGVertexCell vgVertexCell = (VGVertexCell)cell;
            if (!super.isCellFoldable(cell, collapse) && vgVertexCell.getVGObject().getLinkToVertexRecord() != null) {
                VertexRecord vertexRecord = vgVertexCell.getVGObject().getLinkToVertexRecord();
                return VGMainServiceHelper.graphDataBaseService.getVertexRecordsByOwnerId(vertexRecord.getId(), 1).size() > 0;
            } else {
                return true;
            }
        }
        return false;
    }

//    @Override
//    public boolean isCellCollapsed(Object cell) {
//        if (cell instanceof VGVertexCell) {
//            VGVertexCell vgVertexCell = (VGVertexCell)cell;
//            if (!super.isCellCollapsed(cell) && vgVertexCell.getVGObject().getLinkToVertexRecord() != null) {
//                VertexRecord vertexRecord = vgVertexCell.getVGObject().getLinkToVertexRecord();
//                return VGMainServiceHelper.graphDataBaseService.getVertexRecordsByOwnerId(vertexRecord.getId(), 1).size() > 0;
//            }
//        }
//        return false;
//    }

    public List<VGVertexCell> getNeighborhoods(VGVertexCell vgVertexCell, boolean source, boolean target, boolean sameParent, boolean sameLevel) {
        Validate.isTrue(!(sameParent & sameLevel), "Only one may true: sameParent or sameLevel");

        List<VGVertexCell> result = Lists.newArrayList();

        List<VGEdgeCell> edges = vgVertexCell.vgGetEdges();
        for (VGEdgeCell vgEdgeCell : getVGEdgesByLevel(calcCellLevel(vgVertexCell))) {
            int vgEdgeLevel = calcCellLevel(vgEdgeCell);

            VGVertexCell neighborhoodVertex = null;

            if (edges.contains(vgEdgeCell)) {
                if (target && vgVertexCell.vgIsStartWith(vgEdgeCell))
                    neighborhoodVertex = vgEdgeCell.getVGTarget();
                if (source && vgVertexCell.vgIsEndWith(vgEdgeCell))
                    neighborhoodVertex = vgEdgeCell.getVGSoure();
            } else if (sameLevel){
                if (target && vgVertexCell.vgIsStartWith(vgEdgeCell)) {
                    mxCell tmpMxCell = getCellParentWithLevel(vgEdgeCell.getVGTarget(), vgEdgeLevel + 1);
                    if (tmpMxCell != null) {
                        neighborhoodVertex = (VGVertexCell)tmpMxCell;
                    }
                }
                if (source && vgVertexCell.vgIsEndWith(vgEdgeCell)) {
                    mxCell tmpMxCell = getCellParentWithLevel(vgEdgeCell.getVGSoure(), vgEdgeLevel + 1);
                    if (tmpMxCell != null) {
                        neighborhoodVertex = (VGVertexCell)tmpMxCell;
                    }
                }
            }

            if (neighborhoodVertex != null) {
                if (neighborhoodVertex.getParent() == vgVertexCell.getParent()) {
                    result.add(neighborhoodVertex);
                    continue;
                }

                if (sameLevel) {
                    result.add(neighborhoodVertex);
                    continue;
                }

                if (!sameParent) {
                    result.add(neighborhoodVertex);
                }
            }
        }

        return result;
    }

    public static Point2D calcMinimumGraphSize(VGGraph vgGraph, final boolean usingVertices, final boolean usingEdges) {
        //final Point2D min = new Point2D.Double(Double.MAX_VALUE, Double.MAX_VALUE);
        final Point2D max = new Point2D.Double();
        vgGraph.bfs(new SearchListener() {
            private void update(mxGeometry geometry) {
                if (max.getX() < geometry.getX() + geometry.getWidth()) {
                    max.setLocation(geometry.getX() + geometry.getWidth(), max.getY());
                }
                if (max.getY() < geometry.getY() + geometry.getHeight()) {
                    max.setLocation(max.getX(), geometry.getY() + geometry.getHeight());
                }
//                if (min.getX() > geometry.getX()) {
//                    min.setLocation(geometry.getX(), min.getY());
//                }
//                if (min.getY() > geometry.getY()) {
//                    min.setLocation(min.getX(), geometry.getY());
//                }

                if (geometry.getPoints() != null) {
                    for (mxPoint point : geometry.getPoints()) {
                        if (max.getX() < point.getX()) {
                            max.setLocation(point.getX(), max.getY());
                        }
                        if (max.getY() < point.getY()) {
                            max.setLocation(max.getX(), point.getY());
                        }
//                        if (min.getX() > point.getX()) {
//                            min.setLocation(point.getX(), min.getY());
//                        }
//                        if (min.getY() > point.getY()) {
//                            min.setLocation(min.getX(), point.getY());
//                        }
                    }
                }
            }

            @Override
            public void onVertex(VGVertexCell vgVertexCell) {
                if (usingVertices)
                    update(vgVertexCell.getGeometry());
            }

            @Override
            public void onEdge(VGEdgeCell vgEdgeCell) {
                if (usingEdges)
                    update(vgEdgeCell.getGeometry());
            }
        });

//        if (min.getX() == Double.MAX_VALUE)
        return max;
//        return new Point2D.Double(max.getX() - min.getX(), max.getY() - min.getY());
    }

    public Set<VGVertexCell> vgGetVertexCells(Object parent) {
        Object[] cells = mxGraphModel.getChildCells(model, parent, true, false);
        Set<VGVertexCell> result = new HashSet<>(cells.length);

        // Filters out the non-visible child cells
        for (Object cell : cells) {
            if (isCellVisible(cell)) {
                result.add((VGVertexCell)cell);
            }
        }

        return result;
    }

    public Set<VGEdgeCell> vgGetEdgeCells(Object parent) {
        Object[] cells = mxGraphModel.getChildCells(model, parent, false, true);
        Set<VGEdgeCell> result = new HashSet<>(cells.length);

        // Filters out the non-visible child cells
        for (Object cell : cells) {
            if (isCellVisible(cell)) {
                result.add((VGEdgeCell)cell);
            }
        }

        return result;
    }

    /**
     * Calculate max negative coordinates and shift the graph using the value.
     */
    public void removeNegativeCoordinates() {
        final Point2D min = new Point2D.Double(Double.MAX_VALUE, Double.MAX_VALUE);
        bfs(new SearchListener() {
            private void update(mxGeometry geometry) {
                if (min.getX() > geometry.getX()) {
                    min.setLocation(geometry.getX(), min.getY());
                }
                if (min.getY() > geometry.getY()) {
                    min.setLocation(min.getX(), geometry.getY());
                }

                if (geometry.getPoints() != null) {
                    for (mxPoint point : geometry.getPoints()) {
                        if (min.getX() > point.getX()) {
                            min.setLocation(point.getX(), min.getY());
                        }
                        if (min.getY() > point.getY()) {
                            min.setLocation(min.getX(), point.getY());
                        }
                    }
                }
            }

            @Override
            public void onVertex(VGVertexCell vgVertexCell) {
                update(vgVertexCell.getGeometry());
            }

            @Override
            public void onEdge(VGEdgeCell vgEdgeCell) {
                update(vgEdgeCell.getGeometry());
            }
        });

        if (min.getX() > 0)
            min.setLocation(0, min.getY());
        if (min.getY() > 0)
            min.setLocation(min.getX(), 0);

        if (min.getX() == 0 && min.getY() == 0)
            return;
        else
            VGMainServiceHelper.logger.printDebug("Negative coordinates detected: " + min.toString());

        bfs(new SearchListener() {
            private void update(mxGeometry geometry) {
                geometry.setX(geometry.getX() - min.getX());
                geometry.setY(geometry.getY() - min.getY());

                if (geometry.getPoints() != null) {
                    for (mxPoint point : geometry.getPoints()) {
                        point.setX(point.getX() - min.getX());
                        point.setY(point.getY() - min.getY());
                    }
                }
            }

            @Override
            public void onVertex(VGVertexCell vgVertexCell) {
                update(vgVertexCell.getGeometry());
            }

            @Override
            public void onEdge(VGEdgeCell vgEdgeCell) {
                update(vgEdgeCell.getGeometry());
            }
        });
    }

    /**
     * JGraph hierarchic: root->default parent->your graph
     * -1 is root
     * 0 is default parent level
     */
    public static int calcCellLevel(mxCell cell) {
        int level = -1;
        while (cell != null) {
            cell = cell.getParent() == null ? null : (mxCell)cell.getParent();
            if (cell != null)
                level++;
        }
        return level;
    }

    public static List<mxCell> getCellParents(mxCell cell) {
        List<mxCell> result = Lists.newArrayList();
        result.add(cell);
        while (cell != null) {
            cell = cell.getParent() == null ? null : (mxCell)cell.getParent();
            if (cell != null && cell.getParent() != null)
                result.add(cell);
        }
        return result;
    }

    public static mxCell getCellParentWithLevel(mxCell cell, int level) {
        List<mxCell> result = getCellParents(cell);
        if (level >= result.size())
            return null;
        return result.get(result.size() - level - 1);
    }

    public static mxCell findGeneralParent(VGGraph vgGraph, mxCell cell1, mxCell cell2) {
        List<mxCell> parents1 = VGGraph.getCellParents(cell1);
        List<mxCell> parents2 = VGGraph.getCellParents(cell2);

        parents1 = Lists.reverse(parents1);
        parents2 = Lists.reverse(parents2);

        mxCell generalParent = (mxCell) vgGraph.getDefaultParent();
        for (int i = 0; i < Math.min(parents1.size(), parents2.size()); i++) {
            if (parents1.get(i) == parents2.get(i))
                generalParent = parents1.get(i);
            else
                break;
        }

        return generalParent;
    }

    /**
     * Returns true if edge connects graph and vertex, otherwise false.
     */
    public static boolean isEdgeWithParentNode(VGEdgeCell edgeCell) {
        return edgeCell.getParent() == edgeCell.getVGSoure() || edgeCell.getParent() == edgeCell.getVGTarget();
    }

    public static void copyGeometry(VGGraph srcVGGraph, final VGGraph dstVGGraph) {
        srcVGGraph.bfs(new VGGraph.SearchListener() {
            @Override
            public void onVertex(VGVertexCell vgVertexCell) {
                dstVGGraph.getVGVertexCellByVertex(vgVertexCell.getVGObject()).setGeometry(vgVertexCell.getGeometry());
            }

            @Override
            public void onEdge(VGEdgeCell vgEdgeCell) {
                VGEdgeCell originVGEdgeCell = dstVGGraph.getVGEdgeCellByEdge(vgEdgeCell.getVGObject());
                originVGEdgeCell.setGeometry(vgEdgeCell.getGeometry());
                originVGEdgeCell.vgSetEdgeStyle(vgEdgeCell.vgGetEdgeStyle());
            }
        });
    }

//==============================================================================
//------------------PRIVATE CLASSES---------------------------------------------


//==============================================================================
//------------------INNER CLASSES-----------------------------------------------
    public static abstract class SearchListener {
        public void onVertex(VGVertexCell vgVertexCell) {}
        public void onEdge(VGEdgeCell vgEdgeCell) {}
        public void onFinish() {};
    }
}
