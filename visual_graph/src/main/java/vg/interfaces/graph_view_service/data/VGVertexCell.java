package vg.interfaces.graph_view_service.data;

import com.google.common.collect.Lists;
import com.google.common.collect.Queues;
import com.google.common.collect.Sets;
import vg.interfaces.data_base_service.data.graph.Vertex;
import vg.shared.graph.utils.GraphUtils;

import java.util.List;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author Timur Zolotuhin (e-mail: tzolotuhin@parallels.com)
 */
public class VGVertexCell extends VGCell {
    // Constants
    protected static final String LOCAL_VERTEX_PREFIX = "v";

    protected static AtomicInteger vertexCounter = new AtomicInteger();

    protected VGVertexCell(VGVertexCell vgVertexCell) {
        super(vgVertexCell);
    }

    public VGVertexCell(Vertex vertex) {
        super(vertex.getLinkToVertexRecord() == null ? vertexCounter.incrementAndGet() : vertex.getLinkToVertexRecord().getId(), vertex);
    }

    public Set<VGVertexCell> vgGetAllVertexChildren() {
        Set<VGVertexCell> result = Sets.newHashSet();
        for (int i = 0; i < getChildCount(); i++) {
            VGCell vgCell = (VGCell)getChildAt(i);
            if (vgCell.isVertex()) {
                result.add((VGVertexCell) vgCell);
                result.addAll(((VGVertexCell) vgCell).vgGetAllVertexChildren());
            }
        }
        return result;
    }

    public List<VGVertexCell> vgGetVertexChildren() {
        List<VGVertexCell> result = Lists.newArrayList();
        for (int i = 0; i < getChildCount(); i++) {
            VGCell vgCell = (VGCell)getChildAt(i);
            if (vgCell.isVertex())
                result.add((VGVertexCell)vgCell);
        }
        return result;
    }

    public List<VGEdgeCell> vgGetEdgeChildren() {
        List<VGEdgeCell> result = Lists.newArrayList();
        for (int i = 0; i < getChildCount(); i++) {
            VGCell vgCell = (VGCell)getChildAt(i);
            if (vgCell.isEdge())
                result.add((VGEdgeCell)vgCell);
        }
        return result;
    }

    public List<VGEdgeCell> vgGetEdges() {
        List<VGEdgeCell> result = Lists.newArrayList();
        for (int i = 0; i < getEdgeCount(); i++) {
            result.add((VGEdgeCell)getEdgeAt(i));
        }
        return result;
    }

    public boolean vgIsStartWith(VGEdgeCell vgEdgeCell) {
        if (this == vgEdgeCell.getSource())
            return true;

        for (VGVertexCell vgVertexCell : vgGetVertexChildren()) {
            if (vgVertexCell.vgIsStartWith(vgEdgeCell))
                return true;
        }

        return false;
    }

    public boolean vgIsEndWith(VGEdgeCell vgEdgeCell) {
        if (this == vgEdgeCell.getTarget())
            return true;

        for (VGVertexCell vgVertexCell : vgGetVertexChildren()) {
            if (vgVertexCell.vgIsEndWith(vgEdgeCell))
                return true;
        }

        return false;
    }

    public void bfs(VGGraph.SearchListener searchListener) {
        Queue<Object> vertexQueue = Queues.<Object>newArrayDeque(vgGetVertexChildren());
        for (Object vertex : vertexQueue) {
            searchListener.onVertex((VGVertexCell) vertex);
        }
        Queue<Object> edgeQueue = Queues.<Object>newArrayDeque(vgGetEdgeChildren());
        for (Object edge : edgeQueue) {
            searchListener.onEdge((VGEdgeCell)edge);
        }
        while (!vertexQueue.isEmpty()) {
            VGVertexCell vgVertexCell = (VGVertexCell)vertexQueue.poll();
            for (VGVertexCell vertexChild : vgVertexCell.vgGetVertexChildren()) {
                searchListener.onVertex(vertexChild);
                vertexQueue.add(vertexChild);
            }
            for (VGEdgeCell edgeChild : vgVertexCell.vgGetEdgeChildren()) {
                searchListener.onEdge(edgeChild);
            }
        }
        searchListener.onFinish();
    }


    @Override
    public Vertex getVGObject() {
        return (Vertex)super.getVGObject();
    }

    @Override
    public String getHumanId() {
        return GraphUtils.generateHumanId(id, LOCAL_VERTEX_PREFIX);
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return new VGVertexCell(this);
    }
}
