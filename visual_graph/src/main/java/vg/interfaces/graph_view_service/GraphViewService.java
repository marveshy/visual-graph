package vg.interfaces.graph_view_service;

import vg.interfaces.data_base_service.data.graph.Edge;
import vg.interfaces.data_base_service.data.graph.Graph;
import vg.interfaces.data_base_service.data.graph.Vertex;

import java.util.Collection;

/**
 * @author Timur Zolotuhin (e-mail: tzolotuhin@gmail.com)
 */
public interface GraphViewService {
    void asyncCopyExistingGraphView(GraphView srcGraphView, Collection<Vertex> vertices, Collection<Edge> edges, GraphViewServiceCallBack callBack);

    void asyncCreateGraphView(Graph graph, GraphViewServiceCallBack callBack);

    void asyncOpenGraphInTab(Graph graph, GraphViewServiceCallBack callBack);

    /**
     * Removes all existing elements from the graph view and adds new.
     */
    void asyncOpenGraphInGraphView(Graph graph, GraphView graphView, GraphViewServiceCallBack callBack);

    interface GraphViewServiceCallBack {
        /**
         * Will called from EDT
         */
        void onFinishAction(GraphView graphView);
    }
}
