package vg.interfaces.graph_view_service;

import com.mxgraph.util.mxConstants;
import vg.interfaces.data_base_service.data.graph.*;
import vg.interfaces.graph_layout_service.GraphLayout;
import vg.interfaces.graph_layout_service.GraphLayoutTemplate;
import vg.interfaces.user_interface_service.UserInterfaceTab;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.Point2D;
import java.util.*;

/**
 * This interface determine which methods should contains library wrapper for
 * visual presentation graph.
 *
 * @author Timur Zolotuhin (e-mail: tzolotuhin@gmail.com)
 */
public interface GraphView extends UserInterfaceTab, GraphViewExecutor {
    // Constants
    String END_ARROW_CLASSIC = mxConstants.ARROW_CLASSIC;
    String END_ARROW_NONE = mxConstants.NONE;

    String SHAPE_ELLIPSE = mxConstants.SHAPE_ELLIPSE;
    String SHAPE_DOUBLE_ELLIPSE = mxConstants.SHAPE_DOUBLE_ELLIPSE;
    String SHAPE_RECTANGLE = mxConstants.SHAPE_RECTANGLE;
    String SHAPE_TRIANGLE = mxConstants.SHAPE_TRIANGLE;

    String STYLE_EDGE = mxConstants.STYLE_EDGE;
    String NO_EDGE_STYLE = mxConstants.STYLE_NOEDGESTYLE;

    String EDGE_STYLE_ELBOW = mxConstants.EDGESTYLE_ELBOW;
    String EDGE_STYLE_ENTITY_RELATION = mxConstants.EDGESTYLE_ENTITY_RELATION;
    String EDGE_STYLE_ORTHOGONAL = mxConstants.EDGESTYLE_ORTHOGONAL;
    String EDGE_STYLE_LOOP = mxConstants.EDGESTYLE_LOOP;
    String EDGE_STYLE_SIDE_TO_SIDE = mxConstants.EDGESTYLE_SIDETOSIDE;
    String EDGE_STYLE_SEGMENT = mxConstants.EDGESTYLE_SEGMENT;
    String EDGE_STYLE_TOP_TO_BOTTOM = mxConstants.EDGESTYLE_TOPTOBOTTOM;

    void openNewGraph(GraphView srcGraphView, Collection<Vertex> vertices, Collection<Edge> edges);
    void openNewGraph(Graph graph);
    void addVertex(Vertex vertex, Vertex parent);
    void addEdge(Edge edge);

    void removeAllElements();

    Map<Edge, Map<Attribute, Boolean>> getSelectedEdgeAttributes();
    Map<Edge, Map<Attribute, Boolean>> getOrderedSelectedEdgeAttributes();
    Map<Vertex, Map<Attribute, Boolean>> getSelectedVertexAttributes();
    Map<Vertex, Map<Attribute, Boolean>> getOrderedSelectedVertexAttributes();
    Map<Edge, Map<Attribute, Boolean>> getEdgeAttributes();
    Map<Vertex, Map<Attribute, Boolean>> getVertexAttributes();
    Vertex getVertexById(String id);
    Edge getEdgeById(String id);
    String getIdByVertex(Vertex vertex);
    String getIdByEdge(Edge edge);

    Graph getGraph();
    Map.Entry<Collection<Vertex>, Collection<Edge>> getSelectedElements();
    Map.Entry<Collection<Vertex>, Collection<Edge>> getOrderedSelectedElements();

    void setPanning(boolean panning);
    boolean isPanning();

    void setSearch(boolean search);
    boolean isSearch();

    void addPopupMenuItem(JMenuItem item);
    void removePopupMenuItem(JMenuItem item);

    void addListener(GraphViewListener observer);
    void removeListener(GraphViewListener listener);

    JComponent getView();
    void refreshView();

    void setVisibleVertices(Collection<Vertex> vertices, boolean visible);
    void resetVisibleVertices();
    void setVisibleEdges(Collection<Edge> edges, boolean visible);
    void resetVisibleEdges();

    void showVertexAttributes(Collection<Vertex> vertices, Map<String, Boolean> vertexAttributes, boolean useRegexp);
    void showEdgeAttributes(Collection<Edge> edges, Map<String, Boolean> edgeAttributes, boolean useRegexp);

    String getVertexShortInfoTemplate();
    void setVertexShortInfoTemplate(String vertexShortInfoTemplate);

    void selectElements(Set<Vertex> vertices, Set<Edge> edges);

    void setGridSize(int value);

    void setVertexColor(Vertex vertex, Color color);
    void setVertexColor(Collection<Vertex> vertices, Color color);
    void resetVertexColor();

    void setVertexBorderColor(Vertex vertex, Color color);
    void setVertexBorderColor(Collection<Vertex> vertices, Color color);
    void resetVertexBorderColor();

    void setVertexBorderSize(Collection<Vertex> vertices, int size);
    void setEdgeBorderSize(Collection<Edge> edges, int size);

    void setEdgeColor(Edge e, Color color);
    void setEdgeColor(Collection<Edge> edges, Color color);
    void resetEdgeColor();

    void setEdgeBorderColor(Edge e, Color color);
    void setEdgeBorderColor(Collection<Edge> edges, Color color);
    void resetEdgeBorderColor();

    void setVertexShape(Vertex v, String shape);
    void setEdgeStyle(Edge e, String shape);

    // set default attributes
    void setDefaultVertexColor(Color color);
    void setDefaultEdgeColor(Color color);

    Color getDefaultVertexFontColor();
    void setDefaultVertexFontColor(Color defaultVertexFontColor);
    Color getDefaultEdgeFontColor();
    void setDefaultEdgeFontColor(Color defaultEdgeFontColor);
    Font getDefaultVertexFont();
    void setDefaultVertexFont(Font defaultVertexFont);
    Font getDefaultEdgeFont();
    void setDefaultEdgeFont(Font defaultEdgeFont);

    void setLayoutTemplate(GraphLayoutTemplate graphLayoutTemplate);
    GraphLayoutTemplate getLayoutTemplate();
    void executeLayout();
    GraphLayout getCurrentLayout();

    void zoomIn();
    void zoomOut();
    void setCanvasSize(Point2D size);
    void optimizeCanvasSize();

    void lock(String text);
    void unlock();
}