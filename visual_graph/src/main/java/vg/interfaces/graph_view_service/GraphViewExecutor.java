package vg.interfaces.graph_view_service;

/**
 * @author Timur Zolotuhin (e-mail: tzolotuhin@gmail.com)
 */
public interface GraphViewExecutor {
    void execute(GraphViewRunnable graphViewRunnable);

    interface GraphViewRunnable {
        void run(GraphView graphView);
    }
}
