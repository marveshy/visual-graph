package vg.interfaces.plugin_service;

/**
 * @author Timur Zolotuhin (e-mail: tzolotuhin@gmail.com)
 */
public interface PluginService {
    void start();

    void stop();
}
