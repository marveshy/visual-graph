package vg.interfaces.graph_layout_service;

/**
 * @author Timur Zolotuhin (e-mail: tzolotuhin@parallels.com)
 */
public interface GraphLayoutFactory {
    GraphLayoutTemplate buildGraphLayoutTemplate();

    String getLayoutName();

    boolean isPortsSupport();

    boolean isHierarchicalSupport();
}
