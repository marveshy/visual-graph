package vg.interfaces.graph_layout_service;


import java.util.List;

/**
 * This interface manages all layouts in decoders.
 *
 * @author Timur Zolotuhin (e-mail: tzolotuhin@gmail.com)
 */
public interface GraphLayoutService {
	/**
	 * This method registers new layout factory.
	 */
	void registerGraphLayoutFactory(GraphLayoutFactory graphLayoutFactory);
	
	/**
	 * This method returns all registered layout, where key is layout's id 
	 * and value is layout's class.
	 */	
	List<GraphLayoutFactory>  getInstalledLayoutFactories();

    GraphLayoutFactory getLayoutFactoryByName(String layoutName);
}
