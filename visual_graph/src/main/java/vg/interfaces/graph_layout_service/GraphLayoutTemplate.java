package vg.interfaces.graph_layout_service;

import java.util.List;

/**
 * @author Timur Zolotuhin (e-mail: tzolotuhin@parallels.com)
 */
public interface GraphLayoutTemplate {
    /**
     * Sets some settings for the layout template.
     */
    void setSettings(List<GraphLayoutSetting> settings);

    List<GraphLayoutSetting> getSettings();

    GraphLayout buildGraphLayout();

    GraphLayoutFactory getGraphLayoutFactoryCallBack();
}
