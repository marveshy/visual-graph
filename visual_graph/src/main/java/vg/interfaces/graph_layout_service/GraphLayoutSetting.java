package vg.interfaces.graph_layout_service;

import vg.interfaces.data_base_service.data.graph.Attribute;

import java.util.List;

/**
 * @author Timur Zolotuhin (e-mail: tzolotuhin@parallels.com)
 */
public class GraphLayoutSetting extends Attribute {
    // Constants
    public static final int VALUE_SETTING = 0;
    public static final int RANGE_SETTING = 1;
    public static final int LIST_SETTING = 2;

    // Main data
    private int settingType;
    private String text;
    private List<Object> availableValues;

    public GraphLayoutSetting(String name, String value, String text, int type, int settingType, List<Object> availableValues, boolean visible) {
        super(name, value, type, visible);
        this.text = text;
        this.settingType = settingType;
        this.availableValues = availableValues;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getSettingType() {
        return settingType;
    }

    public void setSettingType(int settingType) {
        this.settingType = settingType;
    }

    public List<Object> getAvailableValues() {
        return availableValues;
    }

    public void setAvailableValues(List<Object> availableValues) {
        this.availableValues = availableValues;
    }
}
