package vg.interfaces.graph_layout_service;

import vg.interfaces.graph_view_service.data.VGGraph;

/**
 * This interface control settings for layout.
 *
 * @author Timur Zolotuhin (e-mail: tzolotuhin@gmail.com)
 */
public interface GraphLayout {
    int STARTED_STATE = 1;
    int STOPPED_IN_PROGRESS_STATE = 2;
    int STOPPED_STATE = 3;

    /**
     * Execute layout
     */
    void start(VGGraph vgGraph);

    /**
	 * Execute layout
	 */
	void start(VGGraph vgGraph, GraphLayoutCallBack callBack);

    /**
     * Send stop signal.
     */
    void stop();

    /**
     * Send stop signal and wait for finish layout
     */
    void stopAndWait();

    /**
     * Returns state of layout
     */
    int getState();

    interface GraphLayoutCallBack {
        void onFinishAction(GraphLayout graphLayout);
    }
}
