package vg.interfaces.executor_service;

/**
 * @author Timur Zolotuhin (e-mail: tzolotuhin@gmail.com)
 */
public interface ExecutorService {
    void execute(Runnable runnable);

    void execute(SwingExecutor swingExecutor);

    void shutdown();

    abstract class SwingExecutor {
        protected boolean successfully = true;

        public void doInBackground() {}
        public void doInEDT() {}

        public boolean isSuccessfully() {
            return successfully;
        }
    }
}
