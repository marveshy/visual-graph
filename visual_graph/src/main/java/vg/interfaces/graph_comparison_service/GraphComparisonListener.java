package vg.interfaces.graph_comparison_service;

import java.math.BigDecimal;

/**
 * Observer for graph comparison algorithm.
 *
 * @author Timur Zolotuhin (e-mail: tzolotuhin@gmail.com)
 */
public abstract class GraphComparisonListener {
    public void onStartMatching() {}
    public void onFinishMatching() {}
    public void onUpdateMatching(BigDecimal currentCaseNumber, BigDecimal upperBound) {}
}
