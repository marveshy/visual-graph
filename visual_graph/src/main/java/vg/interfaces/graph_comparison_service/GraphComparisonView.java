package vg.interfaces.graph_comparison_service;

import vg.interfaces.data_base_service.data.graph.Graph;
import vg.interfaces.graph_view_service.GraphView;

import java.util.Map;

/**
 * Interface for graph comparison.
 *
 * @author Timur Zolotuhin (e-mail: tzolotuhin@gmail.com)
 */
public interface GraphComparisonView extends GraphView {
    // shows graph intersection, first graph and second graph on one view. But selects graph intersection.
    public static final int SHOW_GRAPH_INTERSECTION = 4;

    // shows graph intersection, first graph and second graph on one view. But selects first graph.
    public static final int SHOW_FIRST_GRAPH = 5;

    // shows graph intersection, first graph and second graph on one view. But selects second graph.
    public static final int SHOW_SECOND_GRAPH = 6;

    /**
     * Matches two input graphs.
     */
    public void compare();

    /**
     * Stops current matching.
     */
    public void stop();

    /**
     * Returns status of graph matching process.
     */
    public boolean isInProgress();

    public void addGraphComparisonListener(GraphComparisonListener listener);

    public void removeGraphComparisonListener(GraphComparisonListener listener);

    /**
     * This method selects one of parts.
     * @param part - one of next value: SHOW_FIRST_GRAPH_ONLY, SHOW_SECOND_GRAPH_ONLY, SHOW_GRAPH_INTERSECTION_ONLY,
     *               SHOW_GRAPH_INTERSECTION, SHOW_FIRST_GRAPH, SHOW_SECOND_GRAPH.
     */
    public void show(int part);

    public Graph getG1();
    public Graph getG2();
    public Graph getMaximalCommonGraph();

    public float[][] getVertexMatchingTable();
    public float[][] getEdgeMatchingTable();

    public Map<String, Float> getWeightsForVertexAttributes();
    public Map<String, Float> getWeightsForEdgeAttributes();
}
