package vg.interfaces.graph_comparison_service;

import vg.interfaces.data_base_service.data.graph.Graph;

/**
 * @author Timur Zolotuhin (tzolotuhin@parallels.com)
 */
public interface GraphComparisonService {
    void asyncOpenGraphComparisonInTab(Graph g1, Graph g2, GraphComparisonViewServiceCallBack callBack);

    void asyncCompare(GraphComparisonView graphComparisonView, GraphComparisonViewServiceCallBack callBack);

    interface GraphComparisonViewServiceCallBack {
        /**
         * Will called from EDT
         */
        void onFinishAction(GraphComparisonView graphComparisonView);
    }
}
