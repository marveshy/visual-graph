package vg.interfaces.graph_decoder_service;

import java.io.File;
import java.util.List;

/**
 * This interface common for all decoders.
 *
 * @author Timur Zolotuhin (tzolotuhin@parallels.com)
 */
public interface GraphDecoder {
    /**
     * Returns list of graph model indexes.
     */
	List<Integer> decode(File file, String graphName) throws Exception;
}
