package vg.interfaces.graph_decoder_service;

import java.io.File;
import java.util.List;
import java.util.Set;

/**
 * @author Timur Zolotuhin (e-mail: tzolotuhin@gmail.com)
 */
public interface GraphDecoderService {
    Set<String> getAvailableExtensions();

    /**
     * Reads a graph (or graphs) from the file and
     * returns list of graph model ids.
     */
    List<Integer> openFile(File file) throws Exception;

    List<Integer> openGraph(String name, String content, String format) throws Exception;
}
