package vg.plugins.graph_comparator;

import com.google.common.collect.Lists;
import vg.interfaces.graph_view_service.GraphView;
import vg.interfaces.graph_view_service.GraphViewExecutor;
import vg.shared.gui.SwingUtils;
import vg.shared.gui.components.CustomTable;
import vg.shared.utils.ArrayUtils;
import vg.impl.main_service.VGMainServiceHelper;
import vg.interfaces.data_base_service.data.graph.Graph;
import vg.interfaces.executor_service.ExecutorService;
import vg.interfaces.graph_comparison_service.GraphComparisonListener;
import vg.interfaces.graph_comparison_service.GraphComparisonService;
import vg.interfaces.graph_comparison_service.GraphComparisonView;
import vg.interfaces.plugin_service.Plugin;
import vg.interfaces.user_interface_service.UserInterfaceListener;
import vg.interfaces.user_interface_service.UserInterfacePanel;
import vg.interfaces.user_interface_service.UserInterfaceService;
import vg.interfaces.user_interface_service.UserInterfaceTab;
import vg.shared.graph.utils.GraphUtils;
import vg.shared.user_interface.DefaultSwingUtilsCallBack;
import vg.shared.user_interface.UserInterfacePanelUtils;

import javax.swing.*;
import javax.swing.border.BevelBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.math.BigDecimal;
import java.util.AbstractMap;
import java.util.List;
import java.util.Map;

/**
 * @author Timur Zolotuhin (tzolotuhin@parallels.com)
 */
public class GraphComparatorPlugin implements Plugin, UserInterfacePanel {
    // Constants
    private static final int NO_COMPARISON_VIEW_STATE = 0;
    private static final int COMPARISON_VIEW_STATE = 1;
    private static final int COMPARISON_VIEW_IN_PROGRESS_STATE = 2;

    // Main components
    private JPanel innerView, outView;

    private List<WeightSlider> vertexWeightSliders = Lists.newArrayList();
    private List<WeightSlider> edgeWeightSliders = Lists.newArrayList();

    private Map.Entry<JTable, JScrollPane> vertexMatchingTable;

    private JLabel infoLabel;
    private JLabel pleaseWaitInfoLabel;

    private JButton runButton;
    private JButton cancelButton;

    private int state = NO_COMPARISON_VIEW_STATE;

    // Main data
    private GraphComparisonView currentGraphComparisonView;
    private List<GraphComparisonView> graphComparisonViews;

    // Mutex
    private final Object generalMutex = new Object();

    @Override
    public void install() throws Exception {
        currentGraphComparisonView = null;
        graphComparisonViews = Lists.newArrayList();

        outView = new JPanel(new GridLayout(1,1));
        innerView = new JPanel(new GridBagLayout());
        outView.add(innerView);

        infoLabel = new JLabel("If you want to use Comparator you need select tab with matching of two graphs");
        infoLabel.setHorizontalAlignment(JLabel.CENTER);
        pleaseWaitInfoLabel = new JLabel("Please wait...");
        pleaseWaitInfoLabel.setHorizontalAlignment(JLabel.CENTER);

        runButton = new JButton("Run");
        cancelButton = new JButton("Cancel");

        VGMainServiceHelper.userInterfaceService.addObserver(new UserInterfaceListener() {
            @Override
            public void onCloseTab(UserInterfaceTab tab) {
                VGMainServiceHelper.logger.printDebug("GraphComparatorPlugin:onCloseTab action");
                if (tab instanceof GraphComparisonView) {
                    final GraphComparisonView tabGraphComparisonView = (GraphComparisonView) tab;
                    VGMainServiceHelper.executorService.execute(new Runnable() {
                        @Override
                        public void run() {
                            tabGraphComparisonView.stop();
                        }
                    });
                }
             }

            @Override
            public void onChangeTab(final UserInterfaceTab tab) {
                VGMainServiceHelper.logger.printDebug("GraphComparatorPlugin:onChangeTab action");
                final GraphComparisonView tabGraphComparisonView;
                if (tab instanceof GraphComparisonView) {
                    tabGraphComparisonView = (GraphComparisonView)tab;
                } else {
                    tabGraphComparisonView = null;
                }

                doOnChangeTab(tabGraphComparisonView);
            }
        }, 20);

        runButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                synchronized (generalMutex) {
                    if (currentGraphComparisonView != null && !currentGraphComparisonView.isInProgress()) {
                        for (WeightSlider weightSlider : vertexWeightSliders) {
                            currentGraphComparisonView.getWeightsForVertexAttributes().put(weightSlider.getName(), weightSlider.getValue());
                        }
                        for (WeightSlider weightSlider : edgeWeightSliders) {
                            currentGraphComparisonView.getWeightsForVertexAttributes().put(weightSlider.getName(), weightSlider.getValue());
                        }
                        VGMainServiceHelper.graphComparisonService.asyncCompare(currentGraphComparisonView, new GraphComparisonService.GraphComparisonViewServiceCallBack() {
                            @Override
                            public void onFinishAction(GraphComparisonView graphComparisonView) {
                                graphComparisonView.executeLayout();
                            }
                        });
                    }
                }
            }
        });

        cancelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                synchronized (generalMutex) {
                    if (currentGraphComparisonView != null && currentGraphComparisonView.isInProgress())
                        currentGraphComparisonView.stop();
                }
            }
        });

        UserInterfacePanelUtils.createPanel("Comparator", this, UserInterfaceService.SOUTH_INSTRUMENT_PANEL, UserInterfaceService.WEST_SOUTH_PANEL);
        rebuildView();
    }

    @Override
    public JPanel getView() {
        synchronized (generalMutex) {
            return outView;
        }
    }

//==============================================================================
//------------------PRIVATE METHODS---------------------------------------------
    private void buildMatchingTables(Graph g1, Graph g2, float[][] vertexMatching) {
        vertexMatchingTable = new AbstractMap.SimpleEntry<>(new JTable(0, 0), new JScrollPane());

        List<String> columnHeaders = GraphUtils.generateHumanVertexIds(g1.getAllVertices());
        List<String> rowHeaders = GraphUtils.generateHumanVertexIds(g2.getAllVertices());
        vertexMatchingTable = CustomTable.buildCustomTable(
                ArrayUtils.toObject(vertexMatching),
                columnHeaders.toArray(new String[columnHeaders.size()]),
                rowHeaders.toArray(new String[rowHeaders.size()]));
        vertexMatchingTable.getKey().setDefaultRenderer(Object.class, new GraphMatchingTableCellRenderer());
    }

    private void buildWeightSliders(Map<String, Float> vertexWeights, Map<String, Float> edgeWeights) {
        vertexWeightSliders = Lists.newArrayList();
        edgeWeightSliders = Lists.newArrayList();

        for (String attrName : vertexWeights.keySet()) {
            WeightSlider e = new WeightSlider(attrName, vertexWeights.get(attrName));
            vertexWeightSliders.add(e);
        }

        for (String attrName : edgeWeights.keySet()) {
            WeightSlider e = new WeightSlider(attrName, edgeWeights.get(attrName));
            edgeWeightSliders.add(e);
        }
    }

    private void doOnChangeTab(final GraphComparisonView graphComparisonView) {
        synchronized (generalMutex) {
            currentGraphComparisonView = graphComparisonView;
            if (currentGraphComparisonView != null && !graphComparisonViews.contains(currentGraphComparisonView)) {
                graphComparisonViews.add(currentGraphComparisonView);
                graphComparisonView.addGraphComparisonListener(new MyGraphComparisonListener(graphComparisonView));
            }

            if (graphComparisonView == null) {
                state = NO_COMPARISON_VIEW_STATE;
                rebuildView();
                return;
            }
        }
        graphComparisonView.execute(new GraphViewExecutor.GraphViewRunnable() {
            private Map<String, Float> vertexWeights, edgeWeights;
            private Graph g1, g2;
            private float[][] vertexMatching;
            boolean inProgress;

            @Override
            public void run(GraphView graphView) {
                inProgress = graphComparisonView.isInProgress();
                if (!inProgress) {
                    vertexWeights = graphComparisonView.getWeightsForVertexAttributes();
                    edgeWeights = graphComparisonView.getWeightsForEdgeAttributes();
                    vertexMatching = graphComparisonView.getVertexMatchingTable();
                    g1 = graphComparisonView.getG1();
                    g2 = graphComparisonView.getG2();
                }

                SwingUtils.invokeInEDTIfNeeded(new Runnable() {
                    @Override
                    public void run() {
                        synchronized (generalMutex) {
                            if (currentGraphComparisonView == graphComparisonView) {
                                if (!inProgress) {
                                    buildWeightSliders(vertexWeights, edgeWeights);
                                    buildMatchingTables(g1, g2, vertexMatching);
                                    state = COMPARISON_VIEW_STATE;
                                    rebuildView();
                                } else {
                                    state = COMPARISON_VIEW_IN_PROGRESS_STATE;
                                    rebuildView();
                                }
                            }
                        }
                    }
                }, new DefaultSwingUtilsCallBack());
            }
        });
    }

    private void rebuildView() {
        innerView.removeAll();

        switch (state) {
            case NO_COMPARISON_VIEW_STATE: {
                innerView.add(infoLabel, new GridBagConstraints(0, 0, 1, 1, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
                break;
            }
            case COMPARISON_VIEW_IN_PROGRESS_STATE: {
                innerView.add(pleaseWaitInfoLabel, new GridBagConstraints(0,0, 1,1, 1,1, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0,0));
                innerView.add(cancelButton, new GridBagConstraints(0,1, 1,1, 1,0, GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0,0));
                break;
            }
            case COMPARISON_VIEW_STATE: {
                JPanel inputPanel = new JPanel(new GridBagLayout());
                JPanel outputPanel = new JPanel(new GridBagLayout());

                // building sliders
                JPanel vertexWeightSlidersPanel = new JPanel(new GridLayout(vertexWeightSliders.size(), 1));
                JPanel edgeWeightSlidersPanel = new JPanel(new GridLayout(edgeWeightSliders.size(), 1));
                JPanel inputSlidersPanel = new JPanel(new GridBagLayout());
                JScrollPane inputSlidersScrollPane = new JScrollPane(inputSlidersPanel);

                for (WeightSlider weightSlider : vertexWeightSliders) {
                    vertexWeightSlidersPanel.add(weightSlider.getView());
                }
                for (WeightSlider weightSlider : edgeWeightSliders) {
                    edgeWeightSlidersPanel.add(weightSlider.getView());
                }

                inputSlidersPanel.add(new JLabel("Vertex attributes:"), new GridBagConstraints(0, 0, 1, 1, 1, 0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
                inputSlidersPanel.add(vertexWeightSlidersPanel, new GridBagConstraints(0, 1, 1, 1, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
                inputSlidersPanel.add(new JLabel("Edge attributes:"), new GridBagConstraints(0, 2, 1, 1, 1, 0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
                inputSlidersPanel.add(edgeWeightSlidersPanel, new GridBagConstraints(0, 3, 1, 1, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));

                // building input panel
                inputPanel.add(inputSlidersScrollPane, new GridBagConstraints(0, 0, 1, 1, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
                inputPanel.add(runButton, new GridBagConstraints(0, 1, 1, 1, 1, 0, GridBagConstraints.NORTH, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));

                // building output panel
                outputPanel.add(vertexMatchingTable.getValue(), new GridBagConstraints(0, 0, 1, 1, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));

                // building inner view
                innerView.add(new JLabel("Input:"), new GridBagConstraints(0, 0, 1, 1, 0.2, 0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
                innerView.add(new JLabel("Output:"), new GridBagConstraints(1, 0, 1, 1, 0.8, 0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));

                innerView.add(inputPanel, new GridBagConstraints(0, 1, 1, 1, 0.3, 1, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
                innerView.add(outputPanel, new GridBagConstraints(1, 1, 1, 1, 0.7, 1, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
                break;
            }
        }

        innerView.updateUI();
    }

    private static class WeightSlider {
        private JPanel view;
        private String name;
        private Float value;

        private WeightSlider(String name, Float value) {
            this.name = name;
            this.value = value;

            final JLabel nameLabel = new JLabel(name);

            final JSlider valueSlider = new JSlider(JSlider.HORIZONTAL, 0, 100, (int)(value * 100));
            valueSlider.setMajorTickSpacing(25);
            valueSlider.setMinorTickSpacing(5);
            //valueSlider.setPaintTicks(true);
            //valueSlider.setPaintLabels(true);

            view = new JPanel(new GridBagLayout());

            view.add(nameLabel, new GridBagConstraints(0,0,  1,1,  1,1,  GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0),  0, 0));
            view.add(valueSlider, new GridBagConstraints(1,0,  1,1,  1,1,  GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0),  0, 0));

            view.setBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED));

            valueSlider.addChangeListener(new ChangeListener() {
                @Override
                public void stateChanged(ChangeEvent e) {
                    int newValue = valueSlider.getValue();
                    WeightSlider.this.value = newValue * 0.01f;
                }
            });
        }

        public JComponent getView() {
            return view;
        }

        public String getName() {
            return name;
        }

        public Float getValue() {
            return value;
        }
    }

    private static class GraphMatchingTableCellRenderer extends DefaultTableCellRenderer {
        public static float currentBarrier = 1.0f;

        @Override
        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int col) {
            JLabel l = (JLabel) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, col);
            l.setText(String.format("%.2f", (Float)value * 100));

            if ((Float)value >= currentBarrier) {
                l.setBackground(Color.GREEN);
            } else {
                l.setBackground(Color.WHITE);
            }

            return l;
        }
    }

    private class MyGraphComparisonListener extends GraphComparisonListener {
        private int completedPercent = -1;
        private GraphComparisonView graphComparisonView;

        private MyGraphComparisonListener(GraphComparisonView graphComparisonView) {
            this.graphComparisonView = graphComparisonView;
        }

        @Override
        public void onStartMatching() {
            VGMainServiceHelper.logger.printDebug("GraphComparatorPlugin:onStartMatching action");
            synchronized (generalMutex) {
                if (graphComparisonView == currentGraphComparisonView) {
                    pleaseWaitInfoLabel.setText("Please wait...");
                    state = COMPARISON_VIEW_IN_PROGRESS_STATE;
                    completedPercent = -1;
                    rebuildView();
                }
            }
        }

        @Override
        public void onFinishMatching() {
            VGMainServiceHelper.executorService.execute(new ExecutorService.SwingExecutor() {
                private Map<String, Float> vertexWeights, edgeWeights;
                private Graph g1, g2;
                private float[][] vertexMatching;

                @Override
                public void doInBackground() {
                    if (!graphComparisonView.isInProgress()) {
                        vertexWeights = graphComparisonView.getWeightsForVertexAttributes();
                        edgeWeights = graphComparisonView.getWeightsForEdgeAttributes();
                        vertexMatching = graphComparisonView.getVertexMatchingTable();
                        g1 = graphComparisonView.getG1();
                        g2 = graphComparisonView.getG2();
                    }
                }

                @Override
                public void doInEDT() {
                    synchronized (generalMutex) {
                        if (currentGraphComparisonView == graphComparisonView) {
                            buildWeightSliders(vertexWeights, edgeWeights);
                            buildMatchingTables(g1, g2, vertexMatching);
                            state = COMPARISON_VIEW_STATE;
                            rebuildView();
                        }
                    }
                }
            });
        }

        @Override
        public void onUpdateMatching(final BigDecimal currentCaseNumber, final BigDecimal upperBound) {
            VGMainServiceHelper.logger.printInfo(completedPercent + "% completed (" + currentCaseNumber.toString() + " of " + upperBound.toString() + " cases)");

            final int localCompletedPercent = currentCaseNumber.divide(upperBound, 2, BigDecimal.ROUND_HALF_DOWN).multiply(new BigDecimal(100)).intValue();
            if (completedPercent != localCompletedPercent) {
                synchronized (generalMutex) {
                    if (currentGraphComparisonView == graphComparisonView) {
                        completedPercent = localCompletedPercent;
                        pleaseWaitInfoLabel.setText(completedPercent + "% completed (" + currentCaseNumber.toString() + " of " + upperBound.toString() + " cases)");
                        rebuildView();
                    }
                }
            }
        }
    }
}
