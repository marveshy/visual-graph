package vg.plugins.graph_comparator;

/**
 * Graph comparison globals.
 *
 * @author Timur Zolotuhin (e-mail: tzolotuhin@gmail.com)
 */
public class GraphComparisonPluginGlobals {
    public static final String ENABLING_HEURISTIC_CONFIG_PROPERTY = "GraphComparisonPlugin_ENABLING_HEURISTIC";
    public static final String ENABLING_STRONG_COMPARISON_CONFIG_PROPERTY = "GraphComparisonPlugin_ENABLING_STRONG_COMPARISON";
}
