package vg.plugins.opener;

/**
 * @author Timur Zolotuhin (tzolotuhin@parallels.com)
 */
public class GraphOpenerGlobals {
    public static final String GRAPH_OPENER_PLUGIN_PREFIX = "graph_opener_";

    public static final String ATTRIBUTE_NAME_FOR_ID_IN_DOT_FORMAT_KEY = GRAPH_OPENER_PLUGIN_PREFIX + "attribute_name_for_id_in_dot_format";
    public static final String DEFAULT_ATTRIBUTE_NAME_FOR_ID_IN_DOT_FORMAT = "dot_id";

    public static final String ATTRIBUTE_NAME_FOR_ID_IN_GML_FORMAT_KEY = GRAPH_OPENER_PLUGIN_PREFIX + "attribute_name_for_id_in_gml_format";
    public static final String DEFAULT_ATTRIBUTE_NAME_FOR_ID_IN_GML_FORMAT = "gml_id";

    public static final String ATTRIBUTE_NAME_FOR_ID_IN_GRAPHML_FORMAT_KEY = GRAPH_OPENER_PLUGIN_PREFIX + "attribute_name_for_id_in_graphml_format";
    public static final String DEFAULT_ATTRIBUTE_NAME_FOR_ID_IN_GRAPHML_FORMAT = "graphml_id";

    public static final String GRAPHML_FORMAT = "graphml";
    public static final String GML_FORMAT = "gml";
    public static final String ZIP_FORMAT = "zip";
    public static final String DOT_FORMAT = "dot";
    public static final String GV_FORMAT = "gv";

    public static final String ZIP_GRAPHML_FORMAT = "graphml.zip";
    public static final String ZIP_GML_FORMAT = "gml.zip";
    public static final String ZIP_DOT_FORMAT = "dot.zip";
    public static final String ZIP_GV_FORMAT = "gv.zip";
}
