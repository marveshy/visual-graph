package vg.plugins.opener;

import vg.shared.utils.IOUtils;
import vg.impl.main_service.VGMainServiceHelper;
import vg.interfaces.plugin_service.Plugin;
import vg.interfaces.user_interface_service.UserInterfaceService;

import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.*;
import java.util.List;

public class GraphOpenerPlugin implements Plugin {
    // Constants
    private final static String DEF_LAST_OPEN_GRAPH_DIR = "GRAPH_OPENER_LastDir";

	// Main components
    private JFileChooser fileChooser;

	public void install() {
        final JMenuItem jMenuItem = new JMenuItem("Open file");
		jMenuItem.setIcon(new ImageIcon("./data/resources/textures/openFile.png"));

		jMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				openGraph();
			}
		});
		jMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, KeyEvent.CTRL_DOWN_MASK));

        VGMainServiceHelper.userInterfaceService.addMenuItem(jMenuItem, UserInterfaceService.FILE_MENU);

        fileChooser = new JFileChooser(".");

        for (final String ext : VGMainServiceHelper.graphDecoderService.getAvailableExtensions()) {
        	fileChooser.addChoosableFileFilter(new FileFilter() {
				@Override
                public String getDescription() {
					return ext + " file (*." + ext +")";
				}

                @Override
                public boolean accept(File f) {
					return f != null && (f.isDirectory() || f.getName().toLowerCase().endsWith(ext));
				}
			});
        }
	}

//==============================================================================
//------------------PRIVATE METHODS---------------------------------------------
	private void openGraph() {
        // set last directory for file chooser
        String lastDir = VGMainServiceHelper.config.getStringProperty(GraphOpenerPlugin.DEF_LAST_OPEN_GRAPH_DIR);
        if(lastDir != null) {
            fileChooser.setCurrentDirectory(new File(lastDir));
        }

        // show it
		if (fileChooser.showOpenDialog(VGMainServiceHelper.userInterfaceService.getMainFrame()) == JFileChooser.APPROVE_OPTION) {
            VGMainServiceHelper.logger.printInfo("Open file = " + fileChooser.getSelectedFile().getPath());
            VGMainServiceHelper.config.setProperty(GraphOpenerPlugin.DEF_LAST_OPEN_GRAPH_DIR, fileChooser.getCurrentDirectory().getAbsolutePath());
            doOpenGraph(fileChooser.getSelectedFile());
		}
	}

    private void doOpenGraph(final File file) {
        VGMainServiceHelper.executorService.execute(new Runnable() {
            public void run() {
                int oldPriority = Thread.currentThread().getPriority();
                try {
                    Thread.currentThread().setPriority(Thread.MIN_PRIORITY);
                    List<Integer> newGraphModelIds = VGMainServiceHelper.graphDecoderService.openFile(file);

                    VGMainServiceHelper.logger.printDebug("File was opened. File name: " + file.getAbsolutePath());
                    if(newGraphModelIds == null || newGraphModelIds.isEmpty()) {
                        VGMainServiceHelper.windowMessenger.errorMessage("Can't open file. May be file is empty", "Error", null);
                    }
                } catch (Throwable ex) {
                    VGMainServiceHelper.logger.printException(ex);
                    VGMainServiceHelper.windowMessenger.errorMessage("Can't open file: " + file.getAbsolutePath() + IOUtils.getNewLineSeparator() + ex.getMessage(), "Error", null);
                } finally {
                    Thread.currentThread().setPriority(oldPriority);
                }
            }
        });

    }
}
