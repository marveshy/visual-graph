package vg.plugins.graph_view;

import vg.impl.main_service.VGMainServiceHelper;
import vg.interfaces.user_interface_service.UserInterfaceInstrument;
import vg.interfaces.user_interface_service.UserInterfaceListener;
import vg.interfaces.user_interface_service.UserInterfaceService;
import vg.interfaces.user_interface_service.UserInterfaceTab;
import vg.interfaces.graph_view_service.GraphView;
import vg.interfaces.plugin_service.Plugin;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class PanningButtonPlugin implements Plugin, UserInterfaceInstrument {
	// Main component
    private JPanel outView;
    private JToggleButton button;

    private static final ImageIcon pluginIcon;

    // Main data
	private GraphView currentGraphView;

	// Mutex
	private final Object generalMutex = new Object();

    static {
        pluginIcon = new ImageIcon("./data/resources/textures/graph_view/drag_hand.png");
    }
	
	@Override
	public void install() throws Exception {
        currentGraphView = null;
        button = new JToggleButton(pluginIcon);
        button.setPreferredSize(UserInterfaceService.NORTH_INSTRUMENT_PANEL_SIZE);
        button.setToolTipText("Panning graph view");
        button.setEnabled(false);

        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                doSetPanning();
            }
        });

        // build toolbar
        outView = new JPanel(new GridBagLayout());
        outView.add(button, new GridBagConstraints(0,0, 1,1, 0,0, GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0,0,0,0), 0,0));

        VGMainServiceHelper.userInterfaceService.addObserver(new UserInterfaceListener() {
            @Override
            public void onChangeTab(UserInterfaceTab tab) {
                final GraphView tabGraphView;
                if (tab != null && tab instanceof GraphView) {
                    tabGraphView = (GraphView)tab;
                } else {
                    tabGraphView = null;
                }

                synchronized (generalMutex) {
                    currentGraphView = tabGraphView;
                    button.setEnabled(currentGraphView != null);
                }

                doSetPanning();
            }
        });

        // add toolbar to instruments
        VGMainServiceHelper.userInterfaceService.addInstrument(this, UserInterfaceService.NORTH_INSTRUMENT_PANEL);
	}

	@Override
	public JPanel getView() {
		synchronized (generalMutex) {
            return outView;
        }
	}

//==============================================================================
//------------------PRIVATE METHODS---------------------------------------------
    private void doSetPanning() {
        VGMainServiceHelper.executorService.execute(new Runnable() {
            @Override
            public void run() {
                GraphView copyGraphView;
                synchronized (generalMutex) {
                    copyGraphView = currentGraphView;
                }
                if (copyGraphView != null)
                    copyGraphView.setPanning(button.isSelected());
            }
        });
    }
}
