package vg.plugins.graph_view;

import com.google.common.collect.Sets;
import org.apache.commons.lang.Validate;
import vg.shared.gui.SwingUtils;
import vg.shared.gui.components.TextFieldWithLabel;
import vg.shared.utils.IOUtils;
import vg.impl.main_service.VGMainServiceHelper;
import vg.interfaces.data_base_service.data.graph.Edge;
import vg.interfaces.data_base_service.data.graph.Graph;
import vg.interfaces.data_base_service.data.graph.Vertex;
import vg.interfaces.executor_service.ExecutorService;
import vg.interfaces.graph_view_service.GraphView;
import vg.interfaces.plugin_service.Plugin;
import vg.interfaces.user_interface_service.UserInterfaceListener;
import vg.interfaces.user_interface_service.UserInterfacePanel;
import vg.interfaces.user_interface_service.UserInterfaceService;
import vg.interfaces.user_interface_service.UserInterfaceTab;
import vg.shared.graph.utils.GraphUtils;
import vg.shared.user_interface.DefaultSwingUtilsCallBack;
import vg.shared.user_interface.UserInterfacePanelUtils;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * @author Timur Zolotuhin (tzolotuhin@parallels.com)
 */
public class PathsSearcherPlugin implements Plugin, UserInterfacePanel {
    // Constants
    private static final String SHOW_PATHS_MODE = "Show all paths";
    private static final String SHOW_CRITICAL_PATHS_MODE = "Show critical paths";
    private static final String SHOW_CYCLES_MODE = "Show all cycles";

    private static final int NO_GRAPH_VIEW_STATE = 0;
    private static final int GRAPH_VIEW_STATE = 1;
    private static final int FIND_IN_PROGRESS_STATE = 2;
    private static final int FIND_IN_PROGRESS_WRONG_GRAPH_VIEW_STATE = 3;
    private static final int FIND_COMPLETED_STATE = 4;
    private static final int FIND_COMPLETED_WRONG_GRAPH_VIEW_STATE = 5;

    // Main components
    private JPanel innerView, outView;

    private TextFieldWithLabel sourceTextField;
    private TextFieldWithLabel targetTextField;

    private TextFieldWithLabel criticalPathAttributeNameTextField;
    private JComboBox<String> modeComboBox;
    private JTextArea outputTextArea;
    private JScrollPane outputScrollPane;

    private JLabel pleaseWaitLabel;
    private JLabel infoLabel;
    private JLabel notCorrectGraphViewLabel;

    private JButton findButton;
    private JButton cancelButton;

    private JButton newSearchButton;
    private JButton goToCorrectGraphViewButton;

    // Main data
    private GraphView currentGraphView, activeGraphView;
    private String sourceId, targetId, criticalPathAttributeName;
    private String currentMode;
    private String outputResult = "";

    private int state = NO_GRAPH_VIEW_STATE;

    private final AtomicBoolean findProcessInterrupter = new AtomicBoolean(false);

    // Mutex
    private final Object generalMutex = new Object();

    @Override
    public void install() throws Exception {
        outView = new JPanel(new GridLayout(1,1));
        innerView = new JPanel(new GridBagLayout());
        outView.add(innerView);

        currentMode = SHOW_PATHS_MODE;

        modeComboBox = new JComboBox<>(new String[] {SHOW_PATHS_MODE, SHOW_CRITICAL_PATHS_MODE, SHOW_CYCLES_MODE});
        sourceTextField = new TextFieldWithLabel("Source vertex:", TextFieldWithLabel.WIDE_ALIGNMENT);
        targetTextField = new TextFieldWithLabel("Target vertex:", TextFieldWithLabel.WIDE_ALIGNMENT);
        criticalPathAttributeNameTextField = new TextFieldWithLabel("Attribute name", TextFieldWithLabel.WIDE_ALIGNMENT);

        outputTextArea = new JTextArea();
        outputTextArea.setEditable(false);
        outputScrollPane = new JScrollPane(outputTextArea);

        infoLabel = new JLabel("If you want to use Path Searcher you need open some graph");
        infoLabel.setHorizontalAlignment(JLabel.CENTER);
        pleaseWaitLabel = new JLabel("Please wait...");
        pleaseWaitLabel.setHorizontalAlignment(JLabel.CENTER);
        notCorrectGraphViewLabel = new JLabel("Current graph view isn't correct. Do you want go to correct graph view?");
        notCorrectGraphViewLabel.setHorizontalAlignment(JLabel.CENTER);

        findButton = new JButton("Find");
        cancelButton = new JButton("Cancel");
        newSearchButton = new JButton("Reset results");
        goToCorrectGraphViewButton = new JButton("Go to correct graph view");

        VGMainServiceHelper.userInterfaceService.addObserver(new UserInterfaceListener() {
            @Override
            public void onOpenTab(UserInterfaceTab tab) {
                if (tab instanceof GraphView) {
                    final GraphView tabGraphView = (GraphView)tab;

                    final JMenuItem showPathsMenuItem = new JMenuItem(SHOW_PATHS_MODE);
                    showPathsMenuItem.addActionListener(new ActionListener() {
                        public void actionPerformed(ActionEvent arg0) {
                            doShowAllPaths(tabGraphView, true);
                        }
                    });

                    final JMenuItem showCyclesMenuItem = new JMenuItem(SHOW_CYCLES_MODE);
                    showCyclesMenuItem.addActionListener(new ActionListener() {
                        public void actionPerformed(ActionEvent arg0) {
                            doShowCycles(tabGraphView, true);
                        }
                    });

                    final JMenuItem showCriticalPathsMenuItem = new JMenuItem(SHOW_CRITICAL_PATHS_MODE);
                    showCriticalPathsMenuItem.addActionListener(new ActionListener() {
                        public void actionPerformed(ActionEvent arg0) {
                            doShowCriticalPaths(tabGraphView, true);
                        }
                    });

                    VGMainServiceHelper.executorService.execute(new Runnable() {
                        @Override
                        public void run() {
                            tabGraphView.addPopupMenuItem(showPathsMenuItem);
                            tabGraphView.addPopupMenuItem(showCriticalPathsMenuItem);
                            tabGraphView.addPopupMenuItem(showCyclesMenuItem);
                        }
                    });
                }
            }

            @Override
            public void onChangeTab(UserInterfaceTab tab) {
                synchronized (generalMutex) {
                    if (tab != null && tab instanceof GraphView) {
                        currentGraphView = (GraphView)tab;
                    } else {
                        currentGraphView = null;
                    }

                    switch (state) {
                        case NO_GRAPH_VIEW_STATE:
                        case GRAPH_VIEW_STATE:
                            state = GRAPH_VIEW_STATE;
                            break;
                        case FIND_IN_PROGRESS_STATE:
                            if (activeGraphView != currentGraphView)
                                state = FIND_IN_PROGRESS_WRONG_GRAPH_VIEW_STATE;
                            break;
                        case FIND_IN_PROGRESS_WRONG_GRAPH_VIEW_STATE:
                            if (activeGraphView == currentGraphView)
                                state = FIND_IN_PROGRESS_STATE;
                            break;
                        case FIND_COMPLETED_STATE:
                            if (activeGraphView != currentGraphView)
                                state = FIND_COMPLETED_WRONG_GRAPH_VIEW_STATE;
                            break;
                        case FIND_COMPLETED_WRONG_GRAPH_VIEW_STATE:
                            if (activeGraphView == currentGraphView)
                                state = FIND_COMPLETED_STATE;
                            break;
                    }

                    if (currentGraphView == null)
                        state = NO_GRAPH_VIEW_STATE;
                    rebuildView();
                }
            }

            @Override
            public void onCloseTab(UserInterfaceTab tab) {
                synchronized (generalMutex) {
                    if (activeGraphView == tab) {
                        if (state == FIND_IN_PROGRESS_STATE || state == FIND_IN_PROGRESS_WRONG_GRAPH_VIEW_STATE) {
                            findProcessInterrupter.set(true);
                        }
                        if (state == FIND_COMPLETED_WRONG_GRAPH_VIEW_STATE) {
                            state = GRAPH_VIEW_STATE;
                            rebuildView();
                        }
                    }
                }
            }
        });

        modeComboBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                synchronized (generalMutex) {
                    String newMode = modeComboBox.getSelectedItem().toString();
                    if (newMode != null && !newMode.equals(currentMode)) {
                        currentMode = modeComboBox.getSelectedItem().toString();
                        rebuildView();
                    }
                }
            }
        });

        findButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                synchronized (generalMutex) {
                    switch (currentMode) {
                        case SHOW_PATHS_MODE:
                            doShowAllPaths(currentGraphView, false);
                            break;
                        case SHOW_CRITICAL_PATHS_MODE:
                            doShowCriticalPaths(currentGraphView, false);
                            break;
                        case SHOW_CYCLES_MODE:
                            doShowCycles(currentGraphView, false);
                            break;
                    }
                }
            }
        });

        cancelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                synchronized (generalMutex) {
                    Validate.isTrue(state == FIND_IN_PROGRESS_STATE);
                    findProcessInterrupter.set(true);
                }
            }
        });

        newSearchButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                synchronized (generalMutex) {
                    Validate.isTrue(state == FIND_COMPLETED_WRONG_GRAPH_VIEW_STATE);
                    if (state == FIND_COMPLETED_WRONG_GRAPH_VIEW_STATE) {
                        sourceId = targetId = outputResult = "";
                        state = GRAPH_VIEW_STATE;
                        rebuildView();
                    }
                }
            }
        });

        goToCorrectGraphViewButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                synchronized (generalMutex) {
                    Validate.isTrue(state == FIND_COMPLETED_WRONG_GRAPH_VIEW_STATE || state == FIND_IN_PROGRESS_WRONG_GRAPH_VIEW_STATE);
                    VGMainServiceHelper.userInterfaceService.selectTab(activeGraphView);
                }
            }
        });

        sourceTextField.getTextField().getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {copy();}
            @Override
            public void removeUpdate(DocumentEvent e) {copy();}
            @Override
            public void changedUpdate(DocumentEvent e) {copy();}

            private void copy() {
                synchronized (generalMutex) {
                    sourceId = sourceTextField.getTextField().getText();
                }
            }
        });

        targetTextField.getTextField().getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {copy();}
            @Override
            public void removeUpdate(DocumentEvent e) {copy();}
            @Override
            public void changedUpdate(DocumentEvent e) {copy();}

            private void copy() {
                synchronized (generalMutex) {
                    targetId = targetTextField.getTextField().getText();
                }
            }
        });

        criticalPathAttributeNameTextField.getTextField().getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {copy();}
            @Override
            public void removeUpdate(DocumentEvent e) {copy();}
            @Override
            public void changedUpdate(DocumentEvent e) {copy();}

            private void copy() {
                synchronized (generalMutex) {
                    criticalPathAttributeName = criticalPathAttributeNameTextField.getTextField().getText();
                }
            }
        });

        UserInterfacePanelUtils.createPanel("Paths searcher", this, UserInterfaceService.SOUTH_INSTRUMENT_PANEL, UserInterfaceService.WEST_SOUTH_PANEL);

        rebuildView();
    }

    @Override
    public JPanel getView() {
        synchronized (generalMutex) {
            return outView;
        }
    }

//==============================================================================
//------------------PRIVATE METHODS---------------------------------------------
    private boolean doCheckAccessToFindProcessAndActivateIt(GraphView graphView, String mode) {
        if (state == NO_GRAPH_VIEW_STATE) {
            VGMainServiceHelper.windowMessenger.errorMessage("You need open or select tab with graph view for using the tool", "Warning", null);
            return false;
        }

        if (currentGraphView == null)
            return false;

        if (currentGraphView != graphView)
            return false;

        if (state == FIND_IN_PROGRESS_WRONG_GRAPH_VIEW_STATE || state == FIND_IN_PROGRESS_STATE) {
            VGMainServiceHelper.windowMessenger.errorMessage("Find in progress. Please wait...", "Error", null);
            return false;
        }

        VGMainServiceHelper.userInterfaceService.setPanel(PathsSearcherPlugin.this, UserInterfaceService.WEST_SOUTH_PANEL);

        currentMode = mode;
        activeGraphView = currentGraphView;
        state = FIND_IN_PROGRESS_STATE;
        findProcessInterrupter.set(false);

        SwingUtils.invokeInEDTIfNeeded(new Runnable() {
            @Override
            public void run() {
                synchronized (generalMutex) {
                    rebuildView();
                }
            }
        }, new DefaultSwingUtilsCallBack());

        return true;
    }

    private void doShowAllPaths(final GraphView graphView, final boolean usingFromPopupMenu) {
        VGMainServiceHelper.executorService.execute(new ExecutorService.SwingExecutor() {
            @Override
            public void doInBackground() {
                // preparing data
                Map.Entry<Collection<Vertex>, Collection<Edge>> selectedElements = graphView.getOrderedSelectedElements();
                final Graph graph = graphView.getGraph();

                final Vertex source, target;
                if (usingFromPopupMenu) {
                    if (selectedElements.getKey().size() != 2) {
                        VGMainServiceHelper.windowMessenger.errorMessage("You need select two vertices", "Show all paths", null);
                        return;
                    }

                    Iterator<Vertex> iterator = selectedElements.getKey().iterator();
                    source = iterator.next();
                    target = iterator.next();
                    sourceId = graphView.getIdByVertex(source);
                    targetId = graphView.getIdByVertex(target);
                } else {
                    source = graphView.getVertexById(sourceId);
                    target = graphView.getVertexById(targetId);
                }

                if (source == null || target == null) {
                    VGMainServiceHelper.windowMessenger.errorMessage("Wrong source id or target id", "Show all paths", null);
                    return;
                }

                boolean check;
                synchronized (generalMutex) {
                    check = doCheckAccessToFindProcessAndActivateIt(graphView, SHOW_PATHS_MODE);
                }

                if (check) {
                    List<Graph> paths = GraphUtils.findAllPaths(graph, source, target, findProcessInterrupter);
                    if (findProcessInterrupter.get())
                        return;

                    synchronized (generalMutex) {
                        Set<Vertex> vertices = Sets.newHashSet();
                        Set<Edge> edges = Sets.newHashSet();
                        outputResult = "Count of paths: " + paths.size() + IOUtils.getNewLineSeparator();
                        for (int pathIndex = 0; pathIndex < paths.size(); pathIndex++) {
                            Graph path = paths.get(pathIndex);
                            outputResult += "Path " + pathIndex + ": ";
                            for (int vertexIndex = 0; vertexIndex < path.getAllVertices().size(); vertexIndex++) {
                                outputResult += activeGraphView.getIdByVertex(path.getVertexById(vertexIndex));
                                if (vertexIndex < path.getAllVertices().size() - 1) {
                                    outputResult += "->" + activeGraphView.getIdByEdge(path.getEdgeById(vertexIndex)) + "->";
                                }
                            }
                            outputResult += IOUtils.getNewLineSeparator();
                            vertices.addAll(path.getAllVertices());
                            edges.addAll(path.getAllEdges());
                        }

                        activeGraphView.selectElements(vertices, edges);
                        activeGraphView.refreshView();
                    }
                }
            }

            @Override
            public void doInEDT() {
                synchronized (generalMutex) {
                    doFindCompleted();
                }
            }
        });
   }

    private void doShowCriticalPaths(final GraphView graphView, final boolean usingFromPopupMenu) {
        VGMainServiceHelper.executorService.execute(new ExecutorService.SwingExecutor() {
            @Override
            public void doInBackground() {
                // preparing data
                Map.Entry<Collection<Vertex>, Collection<Edge>> selectedElements = graphView.getOrderedSelectedElements();

                final Vertex source, target;
                if (usingFromPopupMenu) {
                    if (selectedElements.getKey().size() != 2) {
                        VGMainServiceHelper.windowMessenger.errorMessage("You need select two vertices", "Show critical paths", null);
                        return;
                    }

                    Iterator<Vertex> iterator = selectedElements.getKey().iterator();
                    source = iterator.next();
                    target = iterator.next();
                    sourceId = graphView.getIdByVertex(source);
                    targetId = graphView.getIdByVertex(target);
                } else {
                    source = graphView.getVertexById(sourceId);
                    target = graphView.getVertexById(targetId);
                }

                if (source == null || target == null) {
                    VGMainServiceHelper.windowMessenger.errorMessage("Wrong source id or target id", "Show critical paths", null);
                    return;
                }

                boolean check;
                synchronized (generalMutex) {
                    check = doCheckAccessToFindProcessAndActivateIt(graphView, SHOW_CRITICAL_PATHS_MODE);
                }

                if (check) {
                    Map<Graph, Float> paths = GraphUtils.findCriticalPaths(currentGraphView.getGraph(), source, target, criticalPathAttributeName, findProcessInterrupter);
                    synchronized (generalMutex) {
                        if (findProcessInterrupter.get())
                            return;

                        Set<Vertex> vertices = Sets.newHashSet();
                        Set<Edge> edges = Sets.newHashSet();
                        outputResult = "Count of paths: " + paths.size() + IOUtils.getNewLineSeparator();
                        int pathIndex = 0;
                        for (Graph path : paths.keySet()) {
                            outputResult += "Path " + (pathIndex++) + " [" + paths.get(path) + "] :";
                            for (int vertexIndex = 0; vertexIndex < path.getAllVertices().size(); vertexIndex++) {
                                outputResult += currentGraphView.getIdByVertex(path.getVertexById(vertexIndex));
                                if (vertexIndex < path.getAllVertices().size() - 1) {
                                    outputResult += "->" + currentGraphView.getIdByEdge(path.getEdgeById(vertexIndex)) + "->";
                                }
                            }
                            outputResult += IOUtils.getNewLineSeparator();
                            if (pathIndex == 1) {
                                vertices.addAll(path.getAllVertices());
                                edges.addAll(path.getAllEdges());
                            }
                        }

                        currentGraphView.selectElements(vertices, edges);
                        currentGraphView.refreshView();
                    }
                }
            }

            @Override
            public void doInEDT() {
                synchronized (generalMutex) {
                    doFindCompleted();
                }
            }
        });
    }

    private void doShowCycles(final GraphView graphView, final boolean usingFromPopupMenu) {
        VGMainServiceHelper.executorService.execute(new ExecutorService.SwingExecutor() {
            @Override
            public void doInBackground() {
                // preparing data
                Map.Entry<Collection<Vertex>, Collection<Edge>> selectedElements = graphView.getOrderedSelectedElements();

                final Vertex source;
                if (usingFromPopupMenu) {
                    if (selectedElements.getKey().size() != 1) {
                        VGMainServiceHelper.windowMessenger.errorMessage("Yuo need select one vertex", "Show cycles", null);
                        return;
                    }

                    Iterator<Vertex> iterator = selectedElements.getKey().iterator();
                    source = iterator.next();
                    sourceId = graphView.getIdByVertex(source);
                    targetId = "";
                } else {
                    source = graphView.getVertexById(sourceId);
                }

                if (source == null) {
                    VGMainServiceHelper.windowMessenger.errorMessage("Wrong source id or target id", "Show cycles", null);
                    return;
                }

                boolean check;
                synchronized (generalMutex) {
                    check = doCheckAccessToFindProcessAndActivateIt(graphView, SHOW_CYCLES_MODE);
                }

                if (check) {
                    List<Graph> paths = GraphUtils.findAllCycles(currentGraphView.getGraph(), source, findProcessInterrupter);
                    synchronized (generalMutex) {
                        if (findProcessInterrupter.get())
                            return;
                        Set<Vertex> vertices = Sets.newHashSet();
                        Set<Edge> edges = Sets.newHashSet();
                        outputResult = "Count of cycles: " + paths.size() + IOUtils.getNewLineSeparator();
                        for (int pathIndex = 0; pathIndex < paths.size(); pathIndex++) {
                            Graph path = paths.get(pathIndex);
                            outputResult += "Cycle " + pathIndex + ": ";
                            for (int vertexIndex = 0; vertexIndex < path.getAllVertices().size(); vertexIndex++) {
                                outputResult += currentGraphView.getIdByVertex(path.getVertexById(vertexIndex));
                                if (vertexIndex < path.getAllVertices().size() - 1) {
                                    outputResult += "->" + currentGraphView.getIdByEdge(path.getEdgeById(vertexIndex)) + "->";
                                }
                            }
                            outputResult += IOUtils.getNewLineSeparator();
                            vertices.addAll(path.getAllVertices());
                            edges.addAll(path.getAllEdges());
                        }

                        currentGraphView.selectElements(vertices, edges);
                        currentGraphView.refreshView();
                    }
                }
            }

            @Override
            public void doInEDT() {
                synchronized (generalMutex) {
                    doFindCompleted();
                }
            }
        });
    }

    private void doFindCompleted() {
        if (findProcessInterrupter.get()) {
            sourceId = targetId = "";
            if (currentGraphView != null) {
                state = GRAPH_VIEW_STATE;
            } else {
                state = NO_GRAPH_VIEW_STATE;
            }
        } else {
            switch (state) {
                case FIND_IN_PROGRESS_STATE:
                    state = FIND_COMPLETED_STATE;
                    break;
                case FIND_IN_PROGRESS_WRONG_GRAPH_VIEW_STATE:
                    state = FIND_COMPLETED_WRONG_GRAPH_VIEW_STATE;
                    break;
            }
        }
        rebuildView();
    }

    private void rebuildView() {
        innerView.removeAll();

        switch (state) {
            case NO_GRAPH_VIEW_STATE: {
                innerView.add(infoLabel, new GridBagConstraints(0,0, 1,1, 1,1, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0,0));
                break;
            }
            case GRAPH_VIEW_STATE:
            case FIND_COMPLETED_STATE: {
                switch (currentMode) {
                    case SHOW_PATHS_MODE:
                        sourceTextField.setEnabled(true);
                        targetTextField.setEnabled(true);
                        criticalPathAttributeNameTextField.setEnabled(false);
                        break;
                    case SHOW_CRITICAL_PATHS_MODE:
                        sourceTextField.setEnabled(true);
                        targetTextField.setEnabled(true);
                        criticalPathAttributeNameTextField.setEnabled(true);
                        break;
                    case SHOW_CYCLES_MODE:
                        sourceTextField.setEnabled(true);
                        targetTextField.setEnabled(false);
                        criticalPathAttributeNameTextField.setEnabled(false);
                }

                sourceTextField.getTextField().setText(sourceId);
                targetTextField.getTextField().setText(targetId);
                modeComboBox.setSelectedItem(currentMode);

                outputTextArea.setText(outputResult);

                JPanel inputPanel = new JPanel(new GridBagLayout());
                JPanel outputPanel = new JPanel(new GridBagLayout());

                inputPanel.add(modeComboBox, new GridBagConstraints(0, 0, 1, 1, 1, 0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
                inputPanel.add(sourceTextField.getView(), new GridBagConstraints(0, 1, 1, 1, 1, 0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
                inputPanel.add(targetTextField.getView(), new GridBagConstraints(0, 2, 1, 1, 1, 0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
                inputPanel.add(criticalPathAttributeNameTextField.getView(), new GridBagConstraints(0, 3, 1, 1, 1, 0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
                inputPanel.add(findButton, new GridBagConstraints(0, 7, 1, 1, 1, 1, GridBagConstraints.NORTH, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));

                outputPanel.add(outputScrollPane, new GridBagConstraints(1, 0, 1, 1, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));

                innerView.add(new JLabel("Input:"), new GridBagConstraints(0, 0, 1, 1, 0.2, 0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
                innerView.add(new JLabel("Output:"), new GridBagConstraints(1, 0, 1, 1, 0.8, 0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));

                innerView.add(inputPanel, new GridBagConstraints(0, 1, 1, 1, 0.2, 1, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
                innerView.add(outputPanel, new GridBagConstraints(1, 1, 1, 1, 0.8, 1, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
                break;
            }
            case FIND_IN_PROGRESS_STATE: {
                innerView.add(pleaseWaitLabel, new GridBagConstraints(0,0, 1,1, 1,1, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0,0));
                innerView.add(cancelButton, new GridBagConstraints(0,1, 1,1, 1,0, GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0,0));
                break;
            }
            case FIND_IN_PROGRESS_WRONG_GRAPH_VIEW_STATE:
            case FIND_COMPLETED_WRONG_GRAPH_VIEW_STATE: {
                innerView.add(notCorrectGraphViewLabel, new GridBagConstraints(0,0, 1,1, 1,1, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0,0));
                innerView.add(goToCorrectGraphViewButton, new GridBagConstraints(0,1, 1,1, 1,0, GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0,0));
                innerView.add(newSearchButton, new GridBagConstraints(0,2, 1,1, 1,0, GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0,0));
                break;
            }
        }

        innerView.updateUI();
    }
}
