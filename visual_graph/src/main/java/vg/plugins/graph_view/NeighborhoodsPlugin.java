package vg.plugins.graph_view;

import com.google.common.collect.Sets;
import vg.impl.main_service.VGMainServiceHelper;
import vg.interfaces.data_base_service.data.graph.Edge;
import vg.interfaces.data_base_service.data.graph.Graph;
import vg.interfaces.data_base_service.data.graph.Vertex;
import vg.interfaces.graph_view_service.GraphView;
import vg.interfaces.plugin_service.Plugin;
import vg.interfaces.user_interface_service.UserInterfaceListener;
import vg.interfaces.user_interface_service.UserInterfaceTab;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collection;
import java.util.Map;
import java.util.Set;

/**
 * @author Timur Zolotuhin (tzolotuhin@parallels.com)
 */
public class NeighborhoodsPlugin implements Plugin {
    @Override
    public void install() throws Exception {
        VGMainServiceHelper.userInterfaceService.addObserver(new UserInterfaceListener() {
            @Override
            public void onOpenTab(UserInterfaceTab tab) {
                if (tab instanceof GraphView) {
                    final GraphView tabGraphView = (GraphView)tab;

                    final JMenuItem showNeighborhoodsMenuItem = new JMenuItem("Show neighborhoods");
                    showNeighborhoodsMenuItem.addActionListener(new ActionListener() {
                        public void actionPerformed(ActionEvent arg0) {
                            VGMainServiceHelper.executorService.execute(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        Map.Entry<Collection<Vertex>, Collection<Edge>> selectedElements = tabGraphView.getSelectedElements();
                                        Graph graph = tabGraphView.getGraph();
                                        Set<Vertex> vertexNeighborhoods = Sets.newHashSet();

                                        // add vertex neighborhoods
                                        vertexNeighborhoods.addAll(selectedElements.getKey());
                                        for (Vertex vertex : selectedElements.getKey()) {
                                            vertexNeighborhoods.addAll(graph.getAnyNeighborhoods(vertex));
                                        }

                                        // add edge neighborhoods
                                        Set<Edge> edgeNeighborhoods = Sets.newHashSet();
                                        for (Vertex vertex : selectedElements.getKey()) {
                                            edgeNeighborhoods.addAll(graph.getEdgeNeighborhoods(vertex));
                                        }

                                        tabGraphView.selectElements(vertexNeighborhoods, edgeNeighborhoods);
                                        tabGraphView.refreshView();
                                    } catch (Throwable ex) {
                                        VGMainServiceHelper.windowMessenger.errorMessage(ex.getMessage(), "Show neighborhoods elements", null);
                                    }
                                }
                            });
                        }
                    });

                    VGMainServiceHelper.executorService.execute(new Runnable() {
                        @Override
                        public void run() {
                            tabGraphView.addPopupMenuItem(showNeighborhoodsMenuItem);
                        }
                    });
                }
            }
        });
    }
}
