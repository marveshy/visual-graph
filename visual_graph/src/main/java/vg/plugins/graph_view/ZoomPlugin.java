package vg.plugins.graph_view;

import vg.impl.main_service.VGMainServiceHelper;
import vg.interfaces.graph_view_service.GraphView;
import vg.interfaces.user_interface_service.UserInterfaceInstrument;
import vg.interfaces.user_interface_service.UserInterfaceListener;
import vg.interfaces.user_interface_service.UserInterfaceService;
import vg.interfaces.user_interface_service.UserInterfaceTab;
import vg.interfaces.plugin_service.Plugin;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

/**
 * This plugin installs scaling panel.
 *
 * @author Timur Zolotuhin (e-mail: tzolotuhin@gmail.com)
 */
public class ZoomPlugin implements UserInterfaceInstrument, Plugin {
    // Main components
    private JPanel toolBar;

    private ZoomIn zoomIn;
    private ZoomOut zoomOut;

    private static final ImageIcon zoomInPluginIcon;
    private static final ImageIcon zoomOutPluginIcon;

    static {
        zoomInPluginIcon = new ImageIcon("./data/resources/textures/graph_view/zoom_in.png");
        zoomOutPluginIcon = new ImageIcon("./data/resources/textures/graph_view/zoom_out.png");
    }

    public void install() throws Exception {
        // initialize components
        zoomIn = new ZoomIn();
        zoomOut = new ZoomOut();

        toolBar = new JPanel(new GridBagLayout());

        toolBar.add(zoomIn.getView(), new GridBagConstraints(0,0, 1,1, 0,0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0,0,0,0), 0,0));
        toolBar.add(zoomOut.getView(), new GridBagConstraints(1,0, 1,1, 0,0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0,0,0,0), 0,0));

        VGMainServiceHelper.userInterfaceService.addInstrument(this, UserInterfaceService.NORTH_INSTRUMENT_PANEL);

        VGMainServiceHelper.userInterfaceService.addObserver(new UserInterfaceListener() {
            @Override
            public void onChangeTab(UserInterfaceTab tab) {
                GraphView graphView = null;
                if (tab != null && tab instanceof GraphView) {
                    graphView =  (GraphView)tab;
                }
                zoomIn.changeView(graphView);
                zoomOut.changeView(graphView);
            }
        });
    }

    @Override
    public JPanel getView() {
        return toolBar;
    }

// TODO: Need remove these classes and move functionality to ZoomPlugin class
//==============================================================================
//------------------PRIVATE CLASSES---------------------------------------------
    private static class ZoomIn {
        // Main components
        private final JButton button;

        // Data
        private GraphView view;

        // Mutex
        private final Object generalMutex = new Object();

        public ZoomIn() {
            // init components
            button = new JButton(zoomInPluginIcon);
            button.setPreferredSize(UserInterfaceService.NORTH_INSTRUMENT_PANEL_SIZE);
            button.setToolTipText("Zoom in");
            button.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    zoomIn();
                }
            });
            button.setEnabled(false);

            // ctrl + plus
            KeyboardFocusManager.getCurrentKeyboardFocusManager().addKeyEventDispatcher(new KeyEventDispatcher() {
                public boolean dispatchKeyEvent(KeyEvent e) {
                    // if use VK_PLUS, that it's will not work, because JDK has bug with it.
                    if (e.getID() == KeyEvent.KEY_PRESSED && (e.getKeyChar() == '+' || e.getKeyCode() == KeyEvent.VK_EQUALS) && e.isControlDown()) {
                        zoomIn();
                        return true;
                    }
                    return false;
                }
            });
        }
        public JComponent getView() {
            return(this.button);
        }

        public void changeView(final GraphView newView) {
            synchronized (generalMutex) {
                view = newView;
                button.setEnabled(view != null);
            }
        }

        private void zoomIn() {
            synchronized (generalMutex) {
                if(view != null) {
                    view.zoomIn();
                }
            }
        }
    }

    private static class ZoomOut {
        // Main components
        private final JButton button;

        // Data
        private GraphView view;

        // Mutex
        private final Object generalMutex = new Object();

        public ZoomOut() {
            // init components
            button = new JButton(zoomOutPluginIcon);
            button.setPreferredSize(UserInterfaceService.NORTH_INSTRUMENT_PANEL_SIZE);
            button.setToolTipText("Zoom out");
            button.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    zoomOut();
                }
            });
            button.setEnabled(false);

            // ctrl + minus
            KeyboardFocusManager.getCurrentKeyboardFocusManager().addKeyEventDispatcher(new KeyEventDispatcher() {
                public boolean dispatchKeyEvent(KeyEvent e) {
                    // if use VK_MINUS, that it's will not work, because JDK has bug with it.
                    if (e.getID() == KeyEvent.KEY_PRESSED && (e.getKeyChar() == '-' || e.getKeyCode() == KeyEvent.VK_MINUS) && e.isControlDown()) {
                        zoomOut();
                        return(true);
                    }
                    return(false);
                }
            });
        }

        public JComponent getView() {
            return button;
        }

        public void changeView(final GraphView newView) {
            synchronized (generalMutex) {
                view = newView;
                button.setEnabled(view != null);
            }
        }

        private void zoomOut() {
            synchronized (generalMutex) {
                if(view != null) {
                    view.zoomOut();
                }
            }
        }
    }
}
