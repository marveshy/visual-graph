package vg.plugins.graph_view;

import vg.impl.main_service.VGMainServiceHelper;
import vg.interfaces.graph_view_service.GraphView;
import vg.interfaces.plugin_service.Plugin;
import vg.interfaces.user_interface_service.UserInterfaceListener;
import vg.interfaces.user_interface_service.UserInterfaceService;
import vg.interfaces.user_interface_service.UserInterfaceTab;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

public class FindPlugin implements Plugin {
    // Main data
	private GraphView currentGraphView;

	// Mutex
	private final Object generalMutex = new Object();

	@Override
	public void install() throws Exception {
        currentGraphView = null;

        // initialize find menu item
        final JMenuItem findMenuItem = new JMenuItem("Find");
        findMenuItem.setEnabled(false);

        findMenuItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                doSearch();
            }
        });
        findMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F, KeyEvent.CTRL_DOWN_MASK));

        VGMainServiceHelper.userInterfaceService.addMenuItem(findMenuItem, UserInterfaceService.EDIT_MENU);

        VGMainServiceHelper.userInterfaceService.addObserver(new UserInterfaceListener() {
            @Override
            public void onChangeTab(UserInterfaceTab tab) {
                final GraphView tabGraphView;
                if (tab != null && tab instanceof GraphView) {
                    tabGraphView = (GraphView)tab;
                    findMenuItem.setEnabled(true);
                } else {
                    tabGraphView = null;
                    findMenuItem.setEnabled(false);
                }

                synchronized (generalMutex) {
                    currentGraphView = tabGraphView;
                }
            }
        });
	}

//==============================================================================
//------------------PRIVATE METHODS---------------------------------------------
    private void doSearch() {
        VGMainServiceHelper.executorService.execute(new Runnable() {
            @Override
            public void run() {
                GraphView copyGraphView;
                synchronized (generalMutex) {
                    copyGraphView = currentGraphView;
                }
                if (copyGraphView != null)
                    copyGraphView.setSearch(true);
            }
        });
    }
}
