package vg.plugins.graph_view;

import vg.impl.main_service.VGMainServiceHelper;
import vg.interfaces.data_base_service.data.graph.Edge;
import vg.interfaces.data_base_service.data.graph.Vertex;
import vg.interfaces.graph_view_service.GraphView;
import vg.interfaces.graph_view_service.GraphViewService;
import vg.interfaces.plugin_service.Plugin;
import vg.interfaces.user_interface_service.UserInterfaceListener;
import vg.interfaces.user_interface_service.UserInterfaceTab;
import vg.plugins.id_generator.IdGeneratorPlugin;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collection;
import java.util.Map;

/**
 * @author Timur Zolotuhin (e-mail: tzolotuhin@gmail.com)
 */
public class OpenSubGraphPlugin implements Plugin{
    @Override
    public void install() throws Exception {
        VGMainServiceHelper.userInterfaceService.addObserver(new UserInterfaceListener() {
            @Override
            public void onOpenTab(UserInterfaceTab tab) {
                if (tab instanceof GraphView) {
                    final GraphView tabGraphView = (GraphView)tab;

                    final JMenuItem openSubGraphMenuItem = new JMenuItem("Open selected graph in new tab");
                    openSubGraphMenuItem.addActionListener(new ActionListener() {
                        public void actionPerformed(ActionEvent arg0) {
                            VGMainServiceHelper.executorService.execute(new Runnable() {
                                @Override
                                public void run() {
                                    Map.Entry<Collection<Vertex>, Collection<Edge>> selectedElements = tabGraphView.getSelectedElements();
                                    VGMainServiceHelper.graphViewService.asyncCopyExistingGraphView(
                                            tabGraphView,
                                            selectedElements.getKey(),
                                            selectedElements.getValue(),
                                            new GraphViewService.GraphViewServiceCallBack() {
                                                @Override
                                                public void onFinishAction(GraphView graphView) {
                                                    graphView.setTabTitle(IdGeneratorPlugin.generateGraphTitle(graphView.getGraph()));
                                                    VGMainServiceHelper.userInterfaceService.addTab(graphView, null);
                                                }
                                            });
                                }
                            });
                        }
                    });

                    VGMainServiceHelper.executorService.execute(new Runnable() {
                        @Override
                        public void run() {
                            tabGraphView.addPopupMenuItem(openSubGraphMenuItem);
                        }
                    });
                }
            }
        });
    }
}
