package vg.plugins.attribute_manager;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.mxgraph.util.mxUtils;
import org.apache.commons.lang.StringUtils;
import vg.interfaces.graph_view_service.GraphViewExecutor;
import vg.shared.gui.ThreeStateCheckBox;
import vg.shared.gui.components.SplitPaneWithConstantDivider;
import vg.impl.main_service.VGMainServiceHelper;
import vg.interfaces.data_base_service.data.graph.Attribute;
import vg.interfaces.data_base_service.data.graph.Edge;
import vg.interfaces.data_base_service.data.graph.Vertex;
import vg.interfaces.executor_service.ExecutorService;
import vg.interfaces.graph_view_service.GraphView;
import vg.interfaces.main_service.VGMainGlobals;
import vg.interfaces.user_interface_service.UserInterfacePanel;
import vg.interfaces.user_interface_service.UserInterfaceListener;
import vg.interfaces.user_interface_service.UserInterfaceService;
import vg.interfaces.user_interface_service.UserInterfaceTab;
import vg.interfaces.graph_view_service.GraphViewListener;
import vg.interfaces.plugin_service.Plugin;
import vg.plugins.attribute_manager.data.AttributeComponent;
import vg.plugins.attribute_manager.data.InterpretationRule;
import vg.plugins.attribute_manager.data.MyTableModel;
import vg.shared.user_interface.UserInterfacePanelUtils;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Attribute panel plugin.
 *
 * @author Timur Zolotuhin (e-mail: tzolotuhin@gmail.com)
 */
public class AttributeManagerPlugin implements Plugin, UserInterfacePanel {
    // Main components
    private JPanel outView, innerView;

    private JButton applyButton;
    private JCheckBox selectAllVertexAttributesCheckBox, selectAllEdgeAttributesCheckBox;
    private JLabel vertexAttributesLabel, edgeAttributesLabel;
    private JTextField showVertexAttributesByDefaultTextField, showEdgeAttributesByDefaultTextField, vertexShortInfoTemplateTextField;
    private JButton showVertexAttributesByDefaultSaveButton, showEdgeAttributesByDefaultSaveButton, vertexShortInfoTemplateSaveButton;
    private JLabel infoLabel;
    private Font subTitleFont = new Font(VGMainGlobals.UI_STANDARD_FONT_FAMILY, Font.BOLD, 13);

    // Main data
    private List<AttributeComponent> vertexAttributeComponents = Lists.newArrayList();
    private List<AttributeComponent> edgeAttributeComponents = Lists.newArrayList();

    private GraphViewExecutor currentGraphViewExecutor;

    private String regexpVertexAttributes;
    private String regexpEdgeAttributes;
    private String regexpVertexShortInfo;
    private List<InterpretationRule> interpretationRules = Lists.newArrayList();

    // Mutex
    private final Object generalMutex = new Object();

    @Override
    public void install() throws Exception {
        outView = new JPanel(new GridLayout(1, 1));
        innerView = new JPanel(new GridBagLayout());
        outView.add(innerView);

        applyButton = new JButton("Apply");

        selectAllVertexAttributesCheckBox = new JCheckBox("Vertex attributes:");
        selectAllEdgeAttributesCheckBox = new JCheckBox("Edge attributes:");
        selectAllVertexAttributesCheckBox.setFont(subTitleFont);
        selectAllEdgeAttributesCheckBox.setFont(subTitleFont);

        showVertexAttributesByDefaultTextField = new JTextField();
        showEdgeAttributesByDefaultTextField = new JTextField();
        vertexShortInfoTemplateTextField = new JTextField();

        showVertexAttributesByDefaultSaveButton = new JButton("Save");
        showEdgeAttributesByDefaultSaveButton = new JButton("Save");
        vertexShortInfoTemplateSaveButton = new JButton("Save");

        vertexAttributesLabel = new JLabel("Vertex attributes:");
        edgeAttributesLabel = new JLabel("Edge attributes:");
        vertexAttributesLabel.setFont(subTitleFont);
        edgeAttributesLabel.setFont(subTitleFont);

        infoLabel = new JLabel("If you want to use Attribute Manager you need open some graph");
        infoLabel.setHorizontalAlignment(JLabel.CENTER);

        showVertexAttributesByDefaultSaveButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                synchronized (generalMutex) {
                    VGMainServiceHelper.config.setProperty(AttributeManagerPluginGlobals.SHOW_VERTEX_ATTRIBUTES_BY_DEFAULT_KEY, showVertexAttributesByDefaultTextField.getText());
                    initDefaultAttributes();
                }
            }
        });

        showEdgeAttributesByDefaultSaveButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                synchronized (generalMutex) {
                    VGMainServiceHelper.config.setProperty(AttributeManagerPluginGlobals.SHOW_EDGE_ATTRIBUTES_BY_DEFAULT_KEY, showVertexAttributesByDefaultTextField.getText());
                    initDefaultAttributes();
                }
            }
        });

        vertexShortInfoTemplateSaveButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                synchronized (generalMutex) {
                    VGMainServiceHelper.config.setProperty(AttributeManagerPluginGlobals.VERTEX_SHORT_INFO_TEMPLATE_BY_DEFAULT_KEY, vertexShortInfoTemplateTextField.getText());
                    initDefaultAttributes();
                }
            }
        });

        applyButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                synchronized (generalMutex) {
                    apply();
                }
            }
        });

        selectAllVertexAttributesCheckBox.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                synchronized (generalMutex) {
                    for (AttributeComponent attributeComponent : vertexAttributeComponents) {
                        attributeComponent.setShowState(true);
                    }

                    rebuildView();
                }
            }
        });

        selectAllEdgeAttributesCheckBox.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                synchronized (generalMutex) {
                    for (AttributeComponent attributeComponent : edgeAttributeComponents) {
                        attributeComponent.setShowState(true);
                    }
                    rebuildView();
                }
            }
        });

        VGMainServiceHelper.userInterfaceService.addObserver(new UserInterfaceListener() {
            @Override
            public void onOpenTab(UserInterfaceTab tab) {
                VGMainServiceHelper.logger.printDebug("AttributeManagerPlugin:onOpenTab action");
                if (tab instanceof GraphView) {
                    final GraphView tabGraphView = (GraphView)tab;

                    tabGraphView.addListener(new GraphViewListener() {
                        @Override
                        public void onAddElements(Collection<Vertex> vertices, Collection<Edge> edges) {
                            VGMainServiceHelper.logger.printDebug("AttributeManagerPlugin:onAddElements action");

                            doOnAddElements(vertices, edges, tabGraphView);
                        }

                        @Override
                        public void onSelectElements(Map<Vertex, Map<Attribute, Boolean>> selectedVertex, Map<Edge, Map<Attribute, Boolean>> selectedEdge) {
                            VGMainServiceHelper.logger.printDebug("AttributeManagerPlugin:onSelectElements action");

                            doOnSelectElements(selectedVertex, selectedEdge, tabGraphView);
                        }
                    });
                }
            }

            @Override
            public void onChangeTab(UserInterfaceTab tab) {
                VGMainServiceHelper.logger.printDebug("AttributeManagerPlugin:onChangeTab action");
                final GraphView tabGraphView;
                if (tab != null && tab instanceof GraphView) {
                    tabGraphView = (GraphView)tab;
                } else {
                    tabGraphView = null;
                }

                synchronized (generalMutex) {
                    currentGraphViewExecutor = tabGraphView;
                }

                if (tabGraphView != null) {
                    tabGraphView.execute(new GraphViewExecutor.GraphViewRunnable() {
                        private Map<Vertex, Map<Attribute, Boolean>> vertexAttributes;
                        private Map<Edge, Map<Attribute, Boolean>> edgeAttributes;

                        @Override
                        public void run(GraphView graphView) {
                            vertexAttributes = tabGraphView.getSelectedVertexAttributes();
                            edgeAttributes = tabGraphView.getSelectedEdgeAttributes();
                            doOnSelectElements(vertexAttributes, edgeAttributes, tabGraphView);
                        }
                    });
                }
            }
        }, 12);

        initDefaultAttributes();
        initInterpretationRules();

        rebuildView();

        UserInterfacePanelUtils.createPanel("Attribute manager", this, UserInterfaceService.SOUTH_INSTRUMENT_PANEL, UserInterfaceService.WEST_SOUTH_PANEL);
    }

    @Override
    public JPanel getView() {
        synchronized (generalMutex) {
            return outView;
        }
    }

//==============================================================================
//-----------------PRIVATE METHODS----------------------------------------------
    private void doOnAddElements(final Collection<Vertex> vertices, final Collection<Edge> edges, final GraphView graphView) {
        final List<InterpretationRule> copyVertexInterpretationRules;
        synchronized (generalMutex) {
            copyVertexInterpretationRules = Lists.newArrayList(interpretationRules);
        }

        graphView.execute(new GraphViewExecutor.GraphViewRunnable() {
            @Override
            public void run(GraphView graphView) {
                graphView.lock("Please wait, handle attributes for graph elements");

                VGMainServiceHelper.logger.printDebug("Try to add elements...");

                applyRules(graphView, copyVertexInterpretationRules);

                // visualize attributes
                final Map<String, Boolean> vertexAttributes = Maps.newHashMap();
                vertexAttributes.put(regexpVertexAttributes, true);
                final Map<String, Boolean> edgeAttributes = Maps.newHashMap();
                edgeAttributes.put(regexpEdgeAttributes, true);

                VGMainServiceHelper.logger.printDebug("Show vertex attributes");
                graphView.showVertexAttributes(vertices, vertexAttributes, true);

                VGMainServiceHelper.logger.printDebug("Show edge attributes");
                graphView.showEdgeAttributes(edges, edgeAttributes, true);

                VGMainServiceHelper.logger.printDebug("Set vertex short info template");
                graphView.setVertexShortInfoTemplate(regexpVertexShortInfo);

                graphView.unlock();
            }
        });
    }

    private void initDefaultAttributes() {
        regexpVertexAttributes = VGMainServiceHelper.config.getStringProperty(AttributeManagerPluginGlobals.SHOW_VERTEX_ATTRIBUTES_BY_DEFAULT_KEY, "");
        regexpEdgeAttributes = VGMainServiceHelper.config.getStringProperty(AttributeManagerPluginGlobals.SHOW_EDGE_ATTRIBUTES_BY_DEFAULT_KEY, "");
        regexpVertexShortInfo = VGMainServiceHelper.config.getStringProperty(AttributeManagerPluginGlobals.VERTEX_SHORT_INFO_TEMPLATE_BY_DEFAULT_KEY, "");
        showVertexAttributesByDefaultTextField.setText(regexpVertexAttributes);
        showEdgeAttributesByDefaultTextField.setText(regexpEdgeAttributes);
        vertexShortInfoTemplateTextField.setText(regexpVertexShortInfo);
    }

    private void initInterpretationRules() {
        interpretationRules.clear();
        interpretationRules.add(new InterpretationRule(true, VGMainServiceHelper.config.getStringProperty(AttributeManagerPluginGlobals.COLOR_INTERPRETATION_RULE_KEY, ""), InterpretationRule.COLOR_RULE));
        interpretationRules.add(new InterpretationRule(true, VGMainServiceHelper.config.getStringProperty(AttributeManagerPluginGlobals.SHAPE_INTERPRETATION_RULE_KEY, ""), InterpretationRule.SHAPE_RULE));
    }

    private void doOnSelectElements(final Map<Vertex, Map<Attribute, Boolean>> selectedVertex, final Map<Edge, Map<Attribute, Boolean>> selectedEdge, final GraphView graphView) {
        VGMainServiceHelper.executorService.execute(new ExecutorService.SwingExecutor() {
            private List<AttributeComponent> localVertexAttributeComponents = Lists.newArrayList();
            private List<AttributeComponent> localEdgeAttributeComponents = Lists.newArrayList();

            @Override
            public void doInBackground() {
                // build local attribute components
                for (Map<Attribute, Boolean> vertexAttributes : selectedVertex.values()) {
                    for (Attribute attribute : vertexAttributes.keySet()) {
                        if (!attribute.isVisible())
                            continue;

                        boolean check = false;
                        for (AttributeComponent attributeComponent : localVertexAttributeComponents) {
                            if (attribute.getName().equals(attributeComponent.getAttributeName())) {
                                attributeComponent.addValue(attribute.getStringValue());
                                check = true;
                                break;
                            }
                        }
                        if (!check) {
                            AttributeComponent newAttrComp = new AttributeComponent(AttributeComponent.NOT_STATE, attribute.getName());
                            newAttrComp.addValue(attribute.getStringValue());
                            localVertexAttributeComponents.add(newAttrComp);
                        }
                    }
                }
                for (Map<Attribute, Boolean> edgeAttributes : selectedEdge.values()) {
                    for (Attribute attribute : edgeAttributes.keySet()) {
                        if (!attribute.isVisible())
                            continue;

                        boolean check = false;
                        for (AttributeComponent attributeComponent : localEdgeAttributeComponents) {
                            if (attribute.getName().equals(attributeComponent.getAttributeName())) {
                                attributeComponent.addValue(attribute.getStringValue());
                                check = true;
                                break;
                            }
                        }
                        if (!check) {
                            AttributeComponent newAttrComp = new AttributeComponent(AttributeComponent.NOT_STATE, attribute.getName());
                            newAttrComp.addValue(attribute.getStringValue());
                            localEdgeAttributeComponents.add(newAttrComp);
                        }
                    }
                }

                // set states
                for (Map<Attribute, Boolean> vertexAttributes : selectedVertex.values()) {
                    for (Attribute attribute : vertexAttributes.keySet()) {
                        for (AttributeComponent localVertexAttributeComponent : localVertexAttributeComponents) {
                            if (localVertexAttributeComponent.getAttributeName().equals(attribute.getName())) {
                                localVertexAttributeComponent.mergeShow(vertexAttributes.get(attribute));
                            }
                        }
                    }
                }
                for (Map<Attribute, Boolean> edgeAttributes : selectedEdge.values()) {
                    for (Attribute attribute : edgeAttributes.keySet()) {
                        for (AttributeComponent localEdgeAttributeComponent : localEdgeAttributeComponents) {
                            if (localEdgeAttributeComponent.getAttributeName().equals(attribute.getName())) {
                                localEdgeAttributeComponent.mergeShow(edgeAttributes.get(attribute));
                            }
                        }
                    }
                }
            }

            @Override
            public void doInEDT() {
                synchronized (generalMutex) {
                    if (currentGraphViewExecutor == graphView) {
                        vertexAttributeComponents = localVertexAttributeComponents;
                        edgeAttributeComponents = localEdgeAttributeComponents;
                        boolean vertexSelect = true;
                        for (AttributeComponent attributeComponent : vertexAttributeComponents) {
                            vertexSelect &= attributeComponent.isShowState();
                        }
                        boolean edgeSelect = true;
                        for (AttributeComponent attributeComponent : edgeAttributeComponents) {
                            edgeSelect &= attributeComponent.isShowState();
                        }
                        selectAllVertexAttributesCheckBox.setSelected(vertexSelect);
                        selectAllEdgeAttributesCheckBox.setSelected(edgeSelect);
                        rebuildView();
                    }
                }
            }
        });
    }

    private void apply() {
        final Map<String, Boolean> vertexAttributes = Maps.newHashMap();
        final Map<String, Boolean> edgeAttributes = Maps.newHashMap();

        for (AttributeComponent attributeComponent : vertexAttributeComponents) {
            if (attributeComponent.getShowState() == AttributeComponent.UNKNOWN_SHOW_ATTRIBUTE_STATE || attributeComponent.getShowState() == AttributeComponent.NOT_STATE)
                continue;
            vertexAttributes.put(attributeComponent.getAttributeName(), attributeComponent.isShowState());
        }

        for (AttributeComponent attributeComponent : edgeAttributeComponents) {
            if (attributeComponent.getShowState() == AttributeComponent.UNKNOWN_SHOW_ATTRIBUTE_STATE || attributeComponent.getShowState() == AttributeComponent.NOT_STATE)
                continue;
            edgeAttributes.put(attributeComponent.getAttributeName(), attributeComponent.isShowState());
        }

        if (currentGraphViewExecutor != null) {
            currentGraphViewExecutor.execute(new GraphViewExecutor.GraphViewRunnable() {
                @Override
                public void run(GraphView graphView) {
                    graphView.lock("Please wait, applying of new rules for graph elements");

                    final Map.Entry<Collection<Vertex>, Collection<Edge>> selectedElements = graphView.getSelectedElements();
                    graphView.showVertexAttributes(selectedElements.getKey(), vertexAttributes, false);
                    graphView.showEdgeAttributes(selectedElements.getValue(), edgeAttributes, false);
                    graphView.refreshView();

                    graphView.unlock();

                    graphView.executeLayout();
                }
            });
        }
    }

    private void rebuildView() {
        innerView.removeAll();

        if (currentGraphViewExecutor != null) {
            // build show attributes panel
            JPanel showAttributesPanel = new JPanel(new GridBagLayout());

            // build vertex attributes
            int index = 0;
            showAttributesPanel.add(selectAllVertexAttributesCheckBox, new GridBagConstraints(0, index++, 1, 1, 1, 0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(5, 0, 0, 0), 0, 0));
            for (final AttributeComponent attributeComponent : vertexAttributeComponents) {
                final ThreeStateCheckBox checkBox = new ThreeStateCheckBox(attributeComponent.getAttributeName(), AttributeComponent.toThreeState(attributeComponent.getShowState()));
                showAttributesPanel.add(checkBox, new GridBagConstraints(0, index++, 1, 1, 1, 0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(0, 3, 0, 0), 0, 0));
                checkBox.addChangeListener(new ChangeListener() {
                    @Override
                    public void stateChanged(ChangeEvent e) {
                        synchronized (generalMutex) {
                            attributeComponent.setShowState(AttributeComponent.toAttributeState(checkBox.getState()));
                        }
                    }
                });
            }

            // build edge attributes
            showAttributesPanel.add(selectAllEdgeAttributesCheckBox, new GridBagConstraints(0, index++, 1, 1, 1, 0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(10, 0, 0, 0), 0, 0));
            for (final AttributeComponent attributeComponent : edgeAttributeComponents) {
                final ThreeStateCheckBox checkBox = new ThreeStateCheckBox(attributeComponent.getAttributeName(), AttributeComponent.toThreeState(attributeComponent.getShowState()));
                showAttributesPanel.add(checkBox, new GridBagConstraints(0, index++, 1, 1, 1, 0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(0, 3, 0, 0), 0, 0));
                checkBox.addChangeListener(new ChangeListener() {
                    @Override
                    public void stateChanged(ChangeEvent e) {
                        synchronized (generalMutex) {
                            attributeComponent.setShowState(AttributeComponent.toAttributeState(checkBox.getState()));
                        }
                    }
                });
            }

            {
                showAttributesPanel.add(new JLabel("Short info template:"), new GridBagConstraints(0, index++, 1, 1, 1, 0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(10, 0, 5, 0), 0, 0));
                JPanel panel = new JPanel(new GridBagLayout());
                panel.add(vertexShortInfoTemplateTextField, new GridBagConstraints(0, 0, 1, 1, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
                panel.add(vertexShortInfoTemplateSaveButton, new GridBagConstraints(1, 0, 1, 1, 0, 1, GridBagConstraints.CENTER, GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
                showAttributesPanel.add(panel, new GridBagConstraints(0, index++, 1, 1, 1, 0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
            }

            {
                showAttributesPanel.add(new JLabel("Default vertex attributes:"), new GridBagConstraints(0, index++, 1, 1, 1, 0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(10, 0, 5, 0), 0, 0));
                JPanel panel = new JPanel(new GridBagLayout());
                panel.add(showVertexAttributesByDefaultTextField, new GridBagConstraints(0, 0, 1, 1, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
                panel.add(showVertexAttributesByDefaultSaveButton, new GridBagConstraints(1, 0, 1, 1, 0, 1, GridBagConstraints.CENTER, GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
                showAttributesPanel.add(panel, new GridBagConstraints(0, index++, 1, 1, 1, 0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
            }

            {
                showAttributesPanel.add(new JLabel("Default edge attributes:"), new GridBagConstraints(0, index++, 1, 1, 1, 0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(10, 0, 5, 0), 0, 0));
                JPanel panel = new JPanel(new GridBagLayout());
                panel.add(showEdgeAttributesByDefaultTextField, new GridBagConstraints(0, 0, 1, 1, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
                panel.add(showEdgeAttributesByDefaultSaveButton, new GridBagConstraints(1, 0, 1, 1, 0, 1, GridBagConstraints.CENTER, GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
                showAttributesPanel.add(panel, new GridBagConstraints(0, index, 1, 1, 1, 0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
            }

            JScrollPane showAttributesScrollPane = new JScrollPane(showAttributesPanel);
            JPanel showAttributesFullPanel = new JPanel(new GridBagLayout());
            showAttributesFullPanel.add(showAttributesScrollPane, new GridBagConstraints(0, 0, 1, 1, 1, 1, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            showAttributesFullPanel.add(applyButton, new GridBagConstraints(0, 1, 1, 1, 1, 0, GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(5, 0, 5, 0), 0, 0));

            // build attributes info panel
            JPanel attributesInfoPanel = new JPanel(new GridBagLayout());

            index = 0;
            attributesInfoPanel.add(vertexAttributesLabel, new GridBagConstraints(0, index++, 1, 1, 1, 0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(5, 0, 5, 0), 0, 0));
            {
                String[][] rowData = new String[vertexAttributeComponents.size()][2];
                for (int i = 0; i < vertexAttributeComponents.size(); i++) {
                    AttributeComponent attributeComponent = vertexAttributeComponents.get(i);
                    rowData[i][0] = attributeComponent.getAttributeName();
                    rowData[i][1] = attributeComponent.convertValuesToString();

                }
                JTable table = new JTable(new MyTableModel(rowData, new String[]{"Name", "Value"}));
                JScrollPane scrollPane = new JScrollPane(table);
                scrollPane.setPreferredSize(new Dimension(table.getPreferredSize().width, table.getRowHeight() * (rowData.length + 1) + 2));
                table.getTableHeader().setReorderingAllowed(false);
                table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
                table.getColumnModel().setColumnSelectionAllowed(true);
                attributesInfoPanel.add(scrollPane, new GridBagConstraints(0, index++, 1, 1, 1, 0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
            }

            attributesInfoPanel.add(edgeAttributesLabel, new GridBagConstraints(0, index++, 1, 1, 1, 0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(10, 0, 5, 0), 0, 0));
            {
                String[][] rowData = new String[edgeAttributeComponents.size()][2];
                for (int i = 0; i < edgeAttributeComponents.size(); i++) {
                    AttributeComponent attributeComponent = edgeAttributeComponents.get(i);
                    rowData[i][0] = attributeComponent.getAttributeName();
                    rowData[i][1] = attributeComponent.convertValuesToString();

                }
                JTable table = new JTable(new MyTableModel(rowData, new String[]{"Name", "Value"}));
                JScrollPane scrollPane = new JScrollPane(table);
                scrollPane.setPreferredSize(new Dimension(table.getPreferredSize().width, table.getRowHeight() * (rowData.length + 1) + 2));
                table.getTableHeader().setReorderingAllowed(false);
                table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
                table.getColumnModel().setColumnSelectionAllowed(true);
                attributesInfoPanel.add(scrollPane, new GridBagConstraints(0, index++, 1, 1, 1, 0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
            }
            attributesInfoPanel.add(new JPanel(), new GridBagConstraints(0, index, 1, 1, 1, 1, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            JScrollPane attributesInfoScrollPane = new JScrollPane(attributesInfoPanel);

            SplitPaneWithConstantDivider splitPaneWithConstantDivider = new SplitPaneWithConstantDivider(JSplitPane.HORIZONTAL_SPLIT, showAttributesFullPanel, attributesInfoScrollPane);

            innerView.add(splitPaneWithConstantDivider, new GridBagConstraints(0, 0, 1, 1, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        } else {
            innerView.add(infoLabel, new GridBagConstraints(0, 0, 1, 1, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }

        innerView.updateUI();
    }

    private static void applyRules(GraphView graphView, List<InterpretationRule> vertexInterpretationRules) {
        for (InterpretationRule interpretationRule : vertexInterpretationRules) {
            if (interpretationRule.isEnable() && !StringUtils.isEmpty(interpretationRule.getWhatInterpret())) {
                Pattern pattern = Pattern.compile(interpretationRule.getWhatInterpret());

                // for vertices
                {
                    Map<Vertex, Map<Attribute, Boolean>> vertexAttributes = graphView.getVertexAttributes();
                    for (Vertex vertex : vertexAttributes.keySet()) {
                        Map<Attribute, Boolean> attributes = vertexAttributes.get(vertex);
                        for (Attribute attribute : attributes.keySet()) {
                            if (pattern.matcher(attribute.getName()).matches() || interpretationRule.getWhatInterpret().equalsIgnoreCase(attribute.getName())) {
                                switch (interpretationRule.getHowInterpret()) {
                                    case InterpretationRule.SHAPE_RULE: {
                                        graphView.setVertexShape(vertex, attribute.getStringValue());
                                        break;
                                    }

                                    case InterpretationRule.COLOR_RULE: {
                                        graphView.setVertexColor(vertex, mxUtils.parseColor(attribute.getStringValue()));
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }

                // for edges
                {
                    Map<Edge, Map<Attribute, Boolean>> edgeAttributes = graphView.getEdgeAttributes();
                    for (Edge edge : edgeAttributes.keySet()) {
                        Map<Attribute, Boolean> attributes = edgeAttributes.get(edge);
                        for (Attribute attribute : attributes.keySet()) {
                            if (pattern.matcher(attribute.getName()).matches() || interpretationRule.getWhatInterpret().equalsIgnoreCase(attribute.getName())) {
                                switch (interpretationRule.getHowInterpret()) {
                                    case InterpretationRule.COLOR_RULE: {
                                        graphView.setEdgeColor(edge, mxUtils.parseColor(attribute.getStringValue()));
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
