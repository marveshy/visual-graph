package vg.plugins.attribute_manager.data;

/**
 * @author Timur Zolotuhin (e-mail: tzolotuhin@parallels.com)
 */
public class InterpretationRule {
    // Constants
    public static final int COLOR_RULE = 1;
    public static final int SHAPE_RULE = 2;

    // Main data
    private final boolean enable;
    private final String whatInterpret;
    private final int howInterpret;

    /**
     * @param whatInterpret - regexp for attribute name
     */
    public InterpretationRule(boolean enable, String whatInterpret, int howInterpret) {
        this.enable = enable;
        this.whatInterpret = whatInterpret;
        this.howInterpret = howInterpret;
    }

    public boolean isEnable() {
        return enable;
    }

    public String getWhatInterpret() {
        return whatInterpret;
    }

    public int getHowInterpret() {
        return howInterpret;
    }
}