package vg.plugins.attribute_manager;

/**
 * @author Timur Zolotuhin (e-mail: tzolotuhin@gmail.com)
 */
public class AttributeManagerPluginGlobals {
    public static final String ATTRIBUTE_MANAGER_PREFIX = "attribute_manager_plugin_";

    public static final String VERTEX_SHORT_INFO_TEMPLATE_BY_DEFAULT_KEY = ATTRIBUTE_MANAGER_PREFIX + "vertex_short_info_template_by_default";
    public static final String SHOW_VERTEX_ATTRIBUTES_BY_DEFAULT_KEY = ATTRIBUTE_MANAGER_PREFIX + "show_vertex_attributes_by_default";
    public static final String SHOW_EDGE_ATTRIBUTES_BY_DEFAULT_KEY = ATTRIBUTE_MANAGER_PREFIX + "show_edge_attributes_by_default";

    public static final String COLOR_INTERPRETATION_RULE_KEY = ATTRIBUTE_MANAGER_PREFIX + "color_interpretation_rule";
    public static final String SHAPE_INTERPRETATION_RULE_KEY = ATTRIBUTE_MANAGER_PREFIX + "shape_interpretation_rule";
    public static final String PORT_INTERPRETATION_RULE_KEY = ATTRIBUTE_MANAGER_PREFIX + "port_interpretation_rule";
    public static final String PORT_PARENT_INTERPRETATION_RULE_KEY = ATTRIBUTE_MANAGER_PREFIX + "port_parent_interpretation_rule";
    public static final String PORT_BRIDGE_INTERPRETATION_RULE_KEY = ATTRIBUTE_MANAGER_PREFIX + "port_bridge_interpretation_rule";
    public static final String HYPER_EDGE_INTERPRETATION_RULE_KEY = ATTRIBUTE_MANAGER_PREFIX + "hyper_edge_interpretation_rule";
}
