package vg.plugins.id_generator;

import vg.impl.main_service.VGMainServiceHelper;
import vg.interfaces.data_base_service.data.graph.Graph;
import vg.interfaces.data_base_service.data.record.AttributeRecord;
import vg.interfaces.data_base_service.data.record.GraphRecord;
import vg.interfaces.plugin_service.Plugin;
import vg.plugins.opener.GraphOpenerGlobals;
import vg.shared.graph.utils.GraphUtils;

import java.util.List;

/**
 * Generates ids for different elements of graph model.
 * For example: graphs, vertices, edges, attributes.
 *
 * @author Timur Zolotuhin (e-mail: tzolotuhin@gmail.com)
 */
public class IdGeneratorPlugin implements Plugin {
    private static String DOT_ID = GraphOpenerGlobals.DEFAULT_ATTRIBUTE_NAME_FOR_ID_IN_DOT_FORMAT;
    private static String GRAPHML_ID = GraphOpenerGlobals.DEFAULT_ATTRIBUTE_NAME_FOR_ID_IN_GRAPHML_FORMAT;
    private static String GML_ID = GraphOpenerGlobals.DEFAULT_ATTRIBUTE_NAME_FOR_ID_IN_GML_FORMAT;

    @Override
    public void install() throws Exception {
        DOT_ID = VGMainServiceHelper.config.getStringProperty(GraphOpenerGlobals.ATTRIBUTE_NAME_FOR_ID_IN_DOT_FORMAT_KEY, GraphOpenerGlobals.DEFAULT_ATTRIBUTE_NAME_FOR_ID_IN_DOT_FORMAT);
        GRAPHML_ID = VGMainServiceHelper.config.getStringProperty(GraphOpenerGlobals.ATTRIBUTE_NAME_FOR_ID_IN_GRAPHML_FORMAT_KEY, GraphOpenerGlobals.DEFAULT_ATTRIBUTE_NAME_FOR_ID_IN_GRAPHML_FORMAT);
        GML_ID = VGMainServiceHelper.config.getStringProperty(GraphOpenerGlobals.ATTRIBUTE_NAME_FOR_ID_IN_GML_FORMAT_KEY, GraphOpenerGlobals.DEFAULT_ATTRIBUTE_NAME_FOR_ID_IN_GML_FORMAT);
    }

    public static String generateGraphTitle(Graph graph) {
        return GraphUtils.generateGraphTitle(graph);
    }

    public static String generateHumanGraphId(int graphId) {
        GraphRecord graphRecord = VGMainServiceHelper.graphDataBaseService.getGraphRecord(graphId);
        return graphRecord.getName();
    }

    public static String generateHumanVertexId(int vertexId) {
        String formatId = getFormatId(VGMainServiceHelper.graphDataBaseService.getAttributeRecordsByOwner(vertexId, AttributeRecord.VERTEX_OWNER_TYPE));
        if (formatId == null)
            return GraphUtils.generateHumanVertexId(vertexId);
        return formatId + " [" + GraphUtils.generateHumanVertexId(vertexId) + "]";
    }

    public static String generateHumanVertexWithInnerGraphId(int vertexId) {
        String formatId = GraphUtils.getInnerGraphNameAttribute(vertexId);
        if (formatId == null)
            return GraphUtils.generateHumanVertexId(vertexId);
        return formatId + " [" + GraphUtils.generateHumanVertexId(vertexId) + "]";
    }

    public static String generateHumanEdgeId(int edgeId) {
        String formatId = getFormatId(VGMainServiceHelper.graphDataBaseService.getAttributeRecordsByOwner(edgeId, AttributeRecord.EDGE_OWNER_TYPE));
        if (formatId == null)
            return GraphUtils.generateHumanEdgeId(edgeId);
        return formatId + " [" + GraphUtils.generateHumanEdgeId(edgeId) + "]";
    }

    public static String generateHumanAttributeId(int attributeId) {
        AttributeRecord attributeRecord = VGMainServiceHelper.graphDataBaseService.getAttributeHeader(attributeId);
        return attributeRecord.getName() + ":" + attributeRecord.getStringValue();
    }

    public static String getFormatId(List<AttributeRecord> attributeRecords) {
        String formatId = null;
        for (AttributeRecord attributeRecord : attributeRecords) {
            if (attributeRecord.getName().equals(DOT_ID))
                formatId = attributeRecord.getStringValue();
            if (attributeRecord.getName().equals(GRAPHML_ID))
                formatId = attributeRecord.getStringValue();
            if (attributeRecord.getName().equals(GML_ID))
                formatId = attributeRecord.getStringValue();
            if (formatId != null)
                break;
        }
        return formatId;
    }
}
