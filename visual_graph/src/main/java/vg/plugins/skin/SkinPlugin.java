package vg.plugins.skin;

import org.pushingpixels.substance.api.SubstanceLookAndFeel;
import org.pushingpixels.substance.api.skin.SkinInfo;
import vg.impl.main_service.VGMainServiceHelper;
import vg.interfaces.plugin_service.Plugin;
import vg.interfaces.user_interface_service.UserInterfaceService;

import javax.swing.*;
import javax.swing.UIManager.LookAndFeelInfo;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Map;

public class SkinPlugin implements Plugin {
    // Constants
    public static final String CONF_STYLE = "skin_plugin_skin_name";

	@Override
	public void install() throws Exception {
		final String lnf = VGMainServiceHelper.config.getStringProperty(CONF_STYLE);
		if (lnf != null) {
			for (final LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
				if (info.getName().equals(lnf))
					try {
						UIManager.setLookAndFeel(info.getClassName());
					} catch (Exception ex) {
                        VGMainServiceHelper.logger.printException(ex);
					}
			}
            Map<String, SkinInfo> skins = SubstanceLookAndFeel.getAllSkins();
			if (skins.containsKey(lnf)) {
                SubstanceLookAndFeel.setSkin(skins.get(lnf).getClassName());
			}
            //updateFrames();
		}

        JMenu LnFMenuItem = new JMenu("Skins");
        for (final LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
            JMenuItem skinMenu = new JMenuItem(info.getName());
            skinMenu.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    VGMainServiceHelper.config.setProperty(CONF_STYLE, info.getName());
                    VGMainServiceHelper.windowMessenger.infoMessage("Please, restart the program for applying new skin");
                }

            });
            LnFMenuItem.add(skinMenu);
        }

        LnFMenuItem.addSeparator();
        for (final String skin : SubstanceLookAndFeel.getAllSkins().keySet()) {
            JMenuItem skinMenu = new JMenuItem(skin);
            skinMenu.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    VGMainServiceHelper.config.setProperty(CONF_STYLE, skin);
                    VGMainServiceHelper.windowMessenger.infoMessage("Please, restart the program for applying new skin");
                }
            });
            LnFMenuItem.add(skinMenu);
        }

        VGMainServiceHelper.userInterfaceService.addMenuItem(LnFMenuItem, UserInterfaceService.WINDOW_MENU);
        VGMainServiceHelper.userInterfaceService.refresh();
	}
}
