package vg.plugins.navigator;

/**
 * @author Timur Zolotuhin (tzolotuhin@parallels.com)
 */
public class NavigatorGlobals {
    public static final String NAVIGATOR_PREFIX = "navigator_plugin_";

    public static final String USE_REGULAR_EXP_KEY = NAVIGATOR_PREFIX + "use_regular_exp";
    public static final String NODE_SET_SIZE_KEY = NAVIGATOR_PREFIX + "node_set_size";

    public static final int DEFAULT_NODE_SET_SIZE = 100;
}
