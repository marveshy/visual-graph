package vg.plugins.navigator;

import vg.impl.main_service.VGMainServiceHelper;
import vg.interfaces.data_base_service.GraphDataBaseListener;
import vg.interfaces.user_interface_service.UserInterfaceService;
import vg.plugins.navigator.components.NavigatorMainComponent;
import vg.interfaces.plugin_service.Plugin;
import vg.shared.user_interface.UserInterfacePanelUtils;

/**
 * This plugin installs navigator.
 *
 * @author Timur Zolotuhin (e-mail: tzolotuhin@gmail.com)
 */
public class NavigatorPlugin implements Plugin {
    // Main data
    private NavigatorMainComponent navigatorMainComponent;

	@Override
    public void install() throws Exception {
        navigatorMainComponent = new NavigatorMainComponent();

        VGMainServiceHelper.graphDataBaseService.addListener(new GraphDataBaseListener() {
            @Override
            public void onOpenNewGraphModel(final int graphModelId) {
                navigatorMainComponent.addGraphModel(graphModelId);
            }
        });

        UserInterfacePanelUtils.createPanel("Navigator", navigatorMainComponent, UserInterfaceService.WEST_INSTRUMENT_PANEL, UserInterfaceService.WEST_PANEL);
	}
}
