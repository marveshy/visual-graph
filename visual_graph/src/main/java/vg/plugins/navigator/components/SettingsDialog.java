package vg.plugins.navigator.components;

import com.google.common.collect.Lists;
import vg.impl.main_service.VGMainServiceHelper;
import vg.plugins.navigator.NavigatorGlobals;

import javax.swing.*;
import java.util.List;

/**
 * @author Timur Zolotuhin (tzolotuhin@parallels.com)
 */
public class SettingsDialog {
    public static void showDialog() {
        List<JComponent> dialogComponents = Lists.newArrayList();

        JCheckBox useRegularExp = new JCheckBox("Use regular expression");
        useRegularExp.setSelected(VGMainServiceHelper.config.getBooleanProperty(NavigatorGlobals.USE_REGULAR_EXP_KEY, false));
        dialogComponents.add(useRegularExp);

        JComponent[] arrayDialogComponents = new JComponent[dialogComponents.size()];
        arrayDialogComponents = dialogComponents.toArray(arrayDialogComponents);
        if (JOptionPane.showConfirmDialog(null, arrayDialogComponents, "Navigator settings", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE) == 0) {
            VGMainServiceHelper.config.setBooleanProperty(NavigatorGlobals.USE_REGULAR_EXP_KEY, useRegularExp.isSelected());
        }
    }
}