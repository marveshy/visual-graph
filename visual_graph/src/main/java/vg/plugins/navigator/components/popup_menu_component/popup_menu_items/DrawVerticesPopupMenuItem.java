package vg.plugins.navigator.components.popup_menu_component.popup_menu_items;

import com.google.common.collect.Lists;
import vg.impl.main_service.VGMainServiceHelper;
import vg.interfaces.data_base_service.data.graph.Graph;
import vg.interfaces.data_base_service.data.graph.Vertex;
import vg.plugins.navigator.components.SmartTreeComponent;

import javax.swing.*;
import javax.swing.tree.TreePath;
import java.util.Collection;
import java.util.List;

public class DrawVerticesPopupMenuItem implements IPopupMenuItem {
    @Override
    public void action(Collection<JTree> trees, JTree lastSelectedTree) {
        final List<TreePath> treePaths = Lists.newArrayList();
        for (JTree tree : trees) {
            TreePath[] paths = tree.getSelectionPaths();
            if (paths != null) {
                treePaths.addAll(Lists.newArrayList(paths));
            }
        }

        VGMainServiceHelper.executorService.execute(new Runnable() {
              @Override
              public void run() {
                  try {
                      List<Vertex> vertices = Lists.newArrayList();
                      for (TreePath path : treePaths) {
                          SmartTreeComponent.SmartTreeNode smartTreeNode = (SmartTreeComponent.SmartTreeNode) path.getLastPathComponent();
                          if (smartTreeNode != null && smartTreeNode.isVertexHeader()) {
                              vertices.add(VGMainServiceHelper.graphDataBaseService.getVertex(smartTreeNode.getVertexHeader().getId()));
                          }
                      }

                      VGMainServiceHelper.graphViewService.asyncOpenGraphInTab(new Graph(vertices, null), null);
                  } catch (Throwable ex) {
                      VGMainServiceHelper.logger.printError(ex.getMessage(), ex);
                  }
              }
        });
    }

    @Override
    public boolean isVisible(Collection<JTree> trees, JTree lastSelectedTree) {
        List<TreePath> treePaths = Lists.newArrayList();
        for (JTree tree : trees) {
            TreePath[] paths = tree.getSelectionPaths();
            if (paths != null) {
                treePaths.addAll(Lists.newArrayList(paths));
            }
        }

        for (TreePath path : treePaths) {
            SmartTreeComponent.SmartTreeNode smartTreeNode = (SmartTreeComponent.SmartTreeNode)path.getLastPathComponent();
            if (smartTreeNode != null && smartTreeNode.isVertexHeader()) {
                return true;
            }
        }

        return false;
    }

    @Override
    public Icon getIcon() {
        return null;
    }

    @Override
    public String getActionName() {
        return "Draw vertices";
    }
}
