package vg.plugins.navigator.components.popup_menu_component.popup_menu_items;

import com.google.common.collect.Lists;
import vg.impl.main_service.VGMainServiceHelper;
import vg.interfaces.data_base_service.data.graph.Edge;
import vg.interfaces.data_base_service.data.graph.Graph;
import vg.interfaces.data_base_service.data.graph.Vertex;
import vg.interfaces.graph_comparison_service.GraphComparisonService;
import vg.interfaces.graph_comparison_service.GraphComparisonView;
import vg.plugins.navigator.components.SmartTreeComponent;

import javax.swing.*;
import javax.swing.tree.TreePath;
import java.util.Collection;
import java.util.List;
import java.util.Map;

public class GraphComparisonPopupMenuItem implements IPopupMenuItem {
    @Override
    public void action(Collection<JTree> trees, JTree lastSelectedTree) {
        List<TreePath> treePaths = Lists.newArrayList();
        for (JTree tree : trees) {
            TreePath[] paths = tree.getSelectionPaths();
            if (paths != null) {
                treePaths.addAll(Lists.newArrayList(paths));
            }
        }

        if (treePaths.size() != 2) {
            VGMainServiceHelper.windowMessenger.errorMessage("Please, select two graphs for comparison", "Error", null);
            return;
        }

        final SmartTreeComponent.SmartTreeNode smartTreeNode1 = (SmartTreeComponent.SmartTreeNode)treePaths.get(0).getLastPathComponent();
        final SmartTreeComponent.SmartTreeNode smartTreeNode2 = (SmartTreeComponent.SmartTreeNode)treePaths.get(1).getLastPathComponent();

        if (smartTreeNode1 == null || smartTreeNode2 == null || !smartTreeNode1.isVertexWithInnerGraph() || !smartTreeNode2.isVertexWithInnerGraph()) {
            VGMainServiceHelper.windowMessenger.errorMessage("Please, select two graphs for comparison", "Error", null);
            return;
        }

        VGMainServiceHelper.executorService.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    Map.Entry<List<Vertex>, Map.Entry<List<Edge>, List<Edge>>> subGraph1Elements = VGMainServiceHelper.graphDataBaseService.getSubGraphElements(smartTreeNode1.getVertexWithInnerGraphRecord().getId());
                    Map.Entry<List<Vertex>, Map.Entry<List<Edge>, List<Edge>>> subGraph2Elements = VGMainServiceHelper.graphDataBaseService.getSubGraphElements(smartTreeNode2.getVertexWithInnerGraphRecord().getId());

                    final Graph graph1 = new Graph(subGraph1Elements.getKey(), subGraph1Elements.getValue().getKey());
                    final Graph graph2 = new Graph(subGraph2Elements.getKey(), subGraph2Elements.getValue().getKey());

                    VGMainServiceHelper.graphComparisonService.asyncOpenGraphComparisonInTab(graph1, graph2, new GraphComparisonService.GraphComparisonViewServiceCallBack() {
                        @Override
                        public void onFinishAction(GraphComparisonView graphComparisonView) {
                            graphComparisonView.executeLayout();
                        }
                    });
                } catch (Throwable ex) {
                    VGMainServiceHelper.logger.printError(ex.getMessage(), ex);
                }
            }
        });
    }

    @Override
    public boolean isVisible(Collection<JTree> trees, JTree lastSelectedTree) {
        List<TreePath> treePaths = Lists.newArrayList();
        for (JTree tree : trees) {
            TreePath[] paths = tree.getSelectionPaths();
            if (paths != null) {
                treePaths.addAll(Lists.newArrayList(paths));
            }
        }
        if (treePaths.size() != 2) {
            return false;
        }

        SmartTreeComponent.SmartTreeNode smartTreeNode1 = (SmartTreeComponent.SmartTreeNode)treePaths.get(0).getLastPathComponent();
        SmartTreeComponent.SmartTreeNode smartTreeNode2 = (SmartTreeComponent.SmartTreeNode)treePaths.get(1).getLastPathComponent();

        return smartTreeNode1 != null && smartTreeNode2 != null && smartTreeNode1.isVertexWithInnerGraph() && smartTreeNode2.isVertexWithInnerGraph();
    }

    @Override
    public Icon getIcon() {
        return null;
    }

    @Override
    public String getActionName() {
        return "Compare two graphs";
    }
}

