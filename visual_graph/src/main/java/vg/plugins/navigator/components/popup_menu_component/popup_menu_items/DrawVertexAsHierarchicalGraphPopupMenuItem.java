package vg.plugins.navigator.components.popup_menu_component.popup_menu_items;

import vg.impl.main_service.VGMainServiceHelper;
import vg.interfaces.data_base_service.data.graph.Graph;
import vg.plugins.navigator.components.SmartTreeComponent;
import vg.shared.graph.utils.GraphUtils;

import javax.swing.*;
import java.util.Collection;

public class DrawVertexAsHierarchicalGraphPopupMenuItem implements IPopupMenuItem {
    @Override
    public void action(Collection<JTree> trees, JTree lastSelectedTree) {
        if (lastSelectedTree == null) return;

        final SmartTreeComponent.SmartTreeNode node = (SmartTreeComponent.SmartTreeNode)lastSelectedTree.getLastSelectedPathComponent();
        if (node != null && node.isVertexHeader()) {
            VGMainServiceHelper.executorService.execute(new Runnable() {
                @Override
                public void run() {
                    try {
                        final Graph graph = new Graph();
                        graph.insertVertex(VGMainServiceHelper.graphDataBaseService.getVertex(node.getVertexHeader().getId()), null);
                        GraphUtils.downloadChildren(graph);
                        VGMainServiceHelper.graphViewService.asyncOpenGraphInTab(graph, null);
                    } catch (Throwable ex) {
                        VGMainServiceHelper.logger.printError(ex.getMessage(), ex);
                    }
                }
            });
        } else {
            VGMainServiceHelper.windowMessenger.warningMessage("Selection should be a vertex", "Opening of a vertex as a graph", null);
        }
    }

    @Override
    public boolean isVisible(Collection<JTree> trees, JTree lastSelectedTree) {
        if (lastSelectedTree == null)
            return false;

        SmartTreeComponent.SmartTreeNode node = (SmartTreeComponent.SmartTreeNode)lastSelectedTree.getLastSelectedPathComponent();
        return node != null && node.isVertexHeader();
    }

    @Override
    public Icon getIcon() {
        return null;
    }

    @Override
    public String getActionName() {
        return "Draw vertex as hierarchical graph";
    }
}
