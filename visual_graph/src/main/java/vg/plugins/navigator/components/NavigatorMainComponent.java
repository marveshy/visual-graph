package vg.plugins.navigator.components;


import com.google.common.collect.Maps;
import vg.impl.main_service.VGMainServiceHelper;
import vg.interfaces.user_interface_service.UserInterfacePanel;
import vg.plugins.navigator.NavigatorGlobals;
import vg.plugins.navigator.components.popup_menu_component.PopupMenuComponent;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Map;

public class NavigatorMainComponent implements UserInterfacePanel {
	// Main components
    private JPanel outView, innerView;

	private JButton settingsButton;
	private final JTextField searchField;
    private final PopupMenuComponent popupMenuComponent;
    private final JProgressBar progressBar;
    private Map<SmartTreeComponent, SmartTreeComponentState> smartTreeComponents = Maps.newLinkedHashMap();

	// Main data
    private boolean searchInProgress;

	// Mutex
	private final Object generalMutex = new Object();

	public NavigatorMainComponent() {
        outView = new JPanel(new GridLayout(1, 1));
        innerView = new JPanel(new GridBagLayout());
        outView.add(innerView);

        popupMenuComponent = new PopupMenuComponent();
        searchField = new JTextField();

        progressBar = new JProgressBar();
        progressBar.setVisible(false);

        // add listeners
        searchField.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                synchronized (generalMutex) {
                    search();
                }
            }
        });

        settingsButton = new JButton(new ImageIcon("./data/resources/textures/navigator/settings.png"));
        settingsButton.setToolTipText("Settings");
        settingsButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                synchronized (generalMutex) {
                    settings();
                }
            }
        });

        rebuildView();
	}

    public void addGraphModel(int graphModelId) {
        synchronized (generalMutex) {
            final SmartTreeComponent smartTreeComponent = new SmartTreeComponent(graphModelId);
            smartTreeComponents.put(smartTreeComponent, new SmartTreeComponentState());
            popupMenuComponent.addTree(smartTreeComponent.getTree());
            smartTreeComponent.addListener(new SmartTreeListener() {
                @Override
                public void onStartSearch() {
                    synchronized (generalMutex) {
                        smartTreeComponents.get(smartTreeComponent).setSearchInProgress(true);
                        doOnStartSearch();
                    }
                }

                @Override
                public void onFinishSearch() {
                    synchronized (generalMutex) {
                        smartTreeComponents.get(smartTreeComponent).setSearchInProgress(false);
                        doOnFinishSearch();
                    }
                }

                @Override
                public void onSearchProgress(int currentCount, int allCount) {
                    synchronized (generalMutex) {
                        SmartTreeComponentState state = smartTreeComponents.get(smartTreeComponent);
                        state.setAllCount(allCount);
                        state.setCurrentCount(currentCount);
                        doOnSearchProgress();
                    }
                }

                @Override
                public void onSelectNodes(boolean addSelectNodes) {
                    synchronized (generalMutex) {
                        doOnSelectNodes(smartTreeComponent, addSelectNodes);
                    }
                }
            });
            rebuildView();
        }
    }

    @Override
	public JPanel getView() {
		return outView;
	}

//=============================================================================
//------------------PRIVATE METHODS--------------------------------------------
    private void doOnStartSearch() {
        if (!SwingUtilities.isEventDispatchThread()) {
            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    synchronized (generalMutex) {
                        doOnStartSearch();
                    }
                }
            });
        } else {
            searchField.setEnabled(false);
            progressBar.setVisible(true);
            innerView.updateUI();
        }
    }

    private void doOnFinishSearch() {
        if (!SwingUtilities.isEventDispatchThread()) {
            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    synchronized (generalMutex) {
                        doOnFinishSearch();
                    }
                }
            });
        } else {
            boolean check = false;
            for (SmartTreeComponentState state : smartTreeComponents.values()) {
                if (state.isSearchInProgress()) {
                    check = true;
                    break;
                }
            }

            if (!check) {
                searchInProgress = false;
                searchField.setEnabled(true);
                progressBar.setVisible(false);
                innerView.updateUI();
            }
        }
    }

    private void doOnSearchProgress() {
        if (!SwingUtilities.isEventDispatchThread()) {
            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    synchronized (generalMutex) {
                        doOnSearchProgress();
                    }
                }
            });
        } else {
            int currentCount = 0, allCount = 0;
            for (SmartTreeComponentState state : smartTreeComponents.values()) {
                if (state.isSearchInProgress()) {
                    currentCount += state.getCurrentCount();
                    allCount += state.getAllCount();
                }
            }
            progressBar.setValue(currentCount);
            progressBar.setMinimum(0);
            progressBar.setMaximum(allCount);
            innerView.updateUI();
        }
    }

    public void doOnSelectNodes(final SmartTreeComponent currentSmartTreeComponent, final boolean addSelectNodes) {
        if (!SwingUtilities.isEventDispatchThread()) {
            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    synchronized (generalMutex) {
                        doOnSelectNodes(currentSmartTreeComponent, addSelectNodes);
                    }
                }
            });
        } else {
            for (SmartTreeComponent smartTreeComponent : smartTreeComponents.keySet()) {
                if (!addSelectNodes && smartTreeComponent != currentSmartTreeComponent) {
                    smartTreeComponent.getTree().setSelectionPaths(null);
                }
            }
            innerView.updateUI();
        }
    }

    private void rebuildView() {
        innerView.removeAll();

        JPanel treePanel = new JPanel(new GridBagLayout());
        int verticalIndex = 0;
        for (SmartTreeComponent smartTreeComponent : smartTreeComponents.keySet()) {
            treePanel.add(smartTreeComponent.getView(), new GridBagConstraints(0,verticalIndex++,  1,1,  1,0,  GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0),  0,0));
        }
        treePanel.add(new JPanel(), new GridBagConstraints(0,verticalIndex,  1,1,  1,1,  GridBagConstraints.NORTH, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0),  0,0));
        JScrollPane treeScrollPane = new JScrollPane(treePanel);
        innerView.add(treeScrollPane,
                new GridBagConstraints(0,1, 2,1, 1,1, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(5, 5, 5, 5), 0, 0));

        innerView.add(settingsButton,
                new GridBagConstraints(0,0, 1,1, 0,0, GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(5, 5, 5, 5), 0, 0));

        innerView.add(searchField,
                new GridBagConstraints(1,0, 1,1, 1,0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(5, 5, 5, 5), 0, 0));

        innerView.add(progressBar,
                new GridBagConstraints(0,2, 2,1, 1,0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(5, 5, 5, 5), 0, 0));

        innerView.updateUI();
    }

    private void search() {
        if (searchInProgress)
            return;

        searchInProgress = true;
        for (SmartTreeComponent smartTreeComponent : smartTreeComponents.keySet()) {
            smartTreeComponent.search(searchField.getText(), VGMainServiceHelper.config.getBooleanProperty(NavigatorGlobals.USE_REGULAR_EXP_KEY, false));
        }
    }

    private void settings() {
        SettingsDialog.showDialog();
    }

//=============================================================================
//------------------PRIVATE CLASSES--------------------------------------------
    private static class SmartTreeComponentState {
        private boolean searchInProgress;
        private int currentCount;
        private int allCount;

        public boolean isSearchInProgress() {
            return searchInProgress;
        }

        public void setSearchInProgress(boolean searchInProgress) {
            this.searchInProgress = searchInProgress;
        }

        public int getCurrentCount() {
            return currentCount;
        }

        public void setCurrentCount(int currentCount) {
            this.currentCount = currentCount;
        }

        public int getAllCount() {
            return allCount;
        }

        public void setAllCount(int allCount) {
            this.allCount = allCount;
        }
    }
}