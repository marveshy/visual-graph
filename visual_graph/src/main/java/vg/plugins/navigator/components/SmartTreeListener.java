package vg.plugins.navigator.components;

/**
 * @author Timur Zolotuhin (tzolotuhin@parallels.com)
 */
public class SmartTreeListener {
    public void onStartSearch() {}

    public void onFinishSearch() {}

    public void onSearchProgress(int currentCount, int allCount) {}

    public void onSelectNodes(boolean addSelectNodes) {}
}
