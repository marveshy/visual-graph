package vg.plugins.navigator.components;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.mutable.MutableInt;
import vg.shared.gui.JTreeUtils;
import vg.shared.utils.FileUtils;
import vg.impl.main_service.VGMainServiceHelper;
import vg.interfaces.data_base_service.data.record.AttributeRecord;
import vg.interfaces.data_base_service.data.record.EdgeRecord;
import vg.interfaces.data_base_service.data.record.GraphRecord;
import vg.interfaces.data_base_service.data.record.VertexRecord;
import vg.interfaces.executor_service.ExecutorService;
import vg.plugins.id_generator.IdGeneratorPlugin;
import vg.plugins.navigator.NavigatorGlobals;

import javax.swing.*;
import javax.swing.event.TreeExpansionEvent;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.event.TreeWillExpandListener;
import javax.swing.tree.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * @author Timur Zolotuhin (e-mail: tzolotuhin@gmail.com)
 */
public class SmartTreeComponent {
    // Main components
    private JPanel outView, innerView;

    private JTree tree;

    // Main data
    private String searchRequest;
    private boolean useRegularExp;
    private boolean searchInProgress = false;
    private Map<String, SearchInfo> searchInfo = Maps.newHashMap();
    private final int graphModelId;

    private boolean ctrlKeyPressed = false;

    private static int nodeSetSize;

    private List<SmartTreeListener> listeners = Lists.newArrayList();

    // Mutex
    private final Object generalMutex = new Object();

    static {
        nodeSetSize = VGMainServiceHelper.config.getIntegerProperty(NavigatorGlobals.NODE_SET_SIZE_KEY, NavigatorGlobals.DEFAULT_NODE_SET_SIZE);
    }

    public SmartTreeComponent(int graphModelId) {
        this.graphModelId = graphModelId;

        SmartTreeNode root = new SmartTreeNode(VGMainServiceHelper.graphDataBaseService.getGraphRecord(graphModelId));

        outView = new JPanel(new GridLayout(1, 1));
        innerView = new JPanel(new GridBagLayout());
        outView.add(innerView);

        final DefaultTreeModel model = new DefaultTreeModel(root);
        tree = new JTree();
        tree.setModel(model);
        tree.setCellRenderer(new SmartTreeRenderer());
        tree.setShowsRootHandles(true);
        JTreeUtils.expandAll(tree, false);

        tree.addTreeWillExpandListener(new TreeWillExpandListener() {
            @Override
            public void treeWillExpand(TreeExpansionEvent event) throws ExpandVetoException {
                synchronized (generalMutex) {
                    SmartTreeNode smartTreeNode = (SmartTreeNode)event.getPath().getLastPathComponent();
                    doExpandAndSearch(smartTreeNode);
                }
            }

            @Override
            public void treeWillCollapse(TreeExpansionEvent event) throws ExpandVetoException {
                synchronized (generalMutex) {
                    SmartTreeNode smartTreeNode = (SmartTreeNode)event.getPath().getLastPathComponent();
                    smartTreeNode.removeAllChildren();
                }
            }
        });
        tree.addTreeSelectionListener(new TreeSelectionListener() {
            @Override
            public void valueChanged(TreeSelectionEvent e) {
                if (!ArrayUtils.isEmpty(tree.getSelectionPaths()))
                    doNotifyOnSelectNodes(syncCopyListeners(), ctrlKeyPressed);
            }
        });

        KeyboardFocusManager.getCurrentKeyboardFocusManager().addKeyEventDispatcher(new KeyEventDispatcher() {
            public boolean dispatchKeyEvent(KeyEvent e) {
                // if use VK_PLUS, that it's will not work, because JDK has bug with it.
                synchronized (generalMutex) {
                    ctrlKeyPressed = e.isControlDown();
                }
                return false;
            }
        });


        rebuildView();
    }

    public JPanel getView() {
        return outView;
    }

    public void search(String searchRequest, boolean useRegularExp) {
        synchronized (generalMutex) {
            if (searchInProgress)
                throw new RuntimeException("Search in progress...");

            this.searchRequest = searchRequest;
            this.useRegularExp = useRegularExp;

            searchInProgress = true;

            doSearch();
        }
    }

    @Deprecated
    public JTree getTree() {
        return tree;
    }

    public void addListener(SmartTreeListener listener) {
        synchronized (generalMutex) {
            listeners.add(listener);
        }
    }

//==============================================================================
//---------------PRIVATE METHODS------------------------------------------------
    private List<SmartTreeListener> syncCopyListeners() {
        List<SmartTreeListener> copyListeners;

        synchronized (generalMutex) {
            copyListeners = Lists.newArrayList(listeners);
        }

        return copyListeners;
    }

    private void doNotifyOnStartSearch(List<SmartTreeListener> listeners) {
        for (SmartTreeListener listener : listeners) {
            listener.onStartSearch();
        }
    }

    private void doNotifyOnFinishSearch(List<SmartTreeListener> listeners) {
        for (SmartTreeListener listener : listeners) {
            listener.onFinishSearch();
        }
    }

    private void doNotifyOnSearchProgress(List<SmartTreeListener> listeners, int currentCount, int allCount) {
        for (SmartTreeListener listener : listeners) {
            listener.onSearchProgress(currentCount, allCount);
        }
    }

    private void doNotifyOnSelectNodes(List<SmartTreeListener> listeners, boolean addSelectNodes) {
        for (SmartTreeListener listener : listeners) {
            listener.onSelectNodes(addSelectNodes);
        }
    }

    private void rebuildView() {
        innerView.removeAll();

        innerView.add(tree, new GridBagConstraints(0,0,  1,1,  1,1,  GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0),  0,0));

        innerView.updateUI();
    }

    private int recursiveSearch(Pattern pattern, boolean useRegularExp, SmartTreeNode root, Map<String, SearchInfo> searchInfo, MutableInt count) {
        int searchNumber = 0;

        List<SmartTreeNode> children = downloadChildren4Node(root);

        for (SmartTreeNode child : children) {
            searchNumber += recursiveSearch(pattern, useRegularExp, child, searchInfo, count);
        }

        boolean satisfiesSearchCondition = false;

        // searching...
        if ((!useRegularExp && root.getNodeId().contains(pattern.pattern())) || (useRegularExp && pattern.matcher(root.getNodeId()).matches())) {
            satisfiesSearchCondition = true;
            searchNumber++;
        }

        // counting
        if (!root.isAttributeHeader()) {
            count.add(1);
            int currentCount = count.intValue();
            if (currentCount % 100 == 0) {
                int allCount = VGMainServiceHelper.graphDataBaseService.getVertexCount(graphModelId) + VGMainServiceHelper.graphDataBaseService.getEdgeCount(graphModelId) + VGMainServiceHelper.graphDataBaseService.getGraphCount(graphModelId);
                doNotifyOnSearchProgress(syncCopyListeners(), count.intValue(), allCount);
            }
        }

        // setup result values
        searchInfo.put(root.getNodeId(), new SearchInfo(searchNumber, satisfiesSearchCondition));
        root.setSatisfiesSearchCondition(satisfiesSearchCondition);
        root.setSearchNumber(searchNumber);

        return searchNumber;
    }

    private void doExpandAndSearch(final SmartTreeNode root) {
        if (root.isLoaded())
            return;

        // download node
        VGMainServiceHelper.executorService.execute(new ExecutorService.SwingExecutor() {
            List<SmartTreeNode> result;

            @Override
            public void doInBackground() {
                result = downloadChildren4Node(root);
                synchronized (generalMutex) {
                    markNodeList(result, searchInfo);
                }
            }

            @Override
            public void doInEDT() {
                synchronized (generalMutex) {
                    try {
                        root.removeAllChildren();
                        root.setChildren(result);
                        tree.updateUI();
                    } catch (Exception ex) {
                        VGMainServiceHelper.logger.printError("Unknown error", ex);
                    }
                }
            }
        });
    }

    private void doSearch() {
        VGMainServiceHelper.executorService.execute(new ExecutorService.SwingExecutor() {
            @Override
            public void doInBackground() {
                SmartTreeNode cloneRoot;
                String cloneSearchRequest;
                boolean cloneUseRegularExp;
                synchronized (generalMutex) {
                    cloneRoot = new SmartTreeNode(VGMainServiceHelper.graphDataBaseService.getGraphRecord(graphModelId));
                    cloneSearchRequest = searchRequest;
                    cloneUseRegularExp = useRegularExp;
                }

                doNotifyOnStartSearch(syncCopyListeners());

                if (StringUtils.isEmpty(cloneSearchRequest)) {
                    synchronized (generalMutex) {
                        searchInfo.clear();
                    }
                } else {
                    Pattern pattern = Pattern.compile(cloneSearchRequest);
                    Map<String, SearchInfo> cloneSearchInfo = Maps.newHashMap();
                    recursiveSearch(pattern, cloneUseRegularExp, cloneRoot, cloneSearchInfo, new MutableInt());
                    synchronized (generalMutex) {
                        searchInfo = cloneSearchInfo;
                    }
                }

                // mark current tree
                synchronized (generalMutex) {
                    markNodeRecursive(((SmartTreeNode) tree.getModel().getRoot()), searchInfo);
                }
            }

            @Override
            public void doInEDT() {
                synchronized (generalMutex) {
                    tree.updateUI();
                    searchInProgress = false;
                }
                doNotifyOnFinishSearch(syncCopyListeners());
            }
        });
    }

    private static List<SmartTreeNode> downloadChildren4Node(SmartTreeNode smartTreeNode) {
        if (smartTreeNode.isListHeader()) {
            return smartTreeNode.getListHeader();
        }

        List<SmartTreeNode> children = Lists.newArrayList();
        if (smartTreeNode.isGraphRecord()) {
            List<VertexRecord> graphHeaders = VGMainServiceHelper.graphDataBaseService.getRoots(smartTreeNode.getGraphRecord().getId());

            children = new ArrayList<>(graphHeaders.size());
            for (VertexRecord graphHeader : graphHeaders) {
                children.add(new SmartTreeNode(graphHeader));
            }
        } else  if (smartTreeNode.isVertexWithInnerGraph()) {
            List<VertexRecord> vertexRecords = VGMainServiceHelper.graphDataBaseService.getVertexRecordsByOwnerId(smartTreeNode.getVertexWithInnerGraphRecord().getId(), -1);
            //List<EdgeRecord> edgeHeaders = VGMainServiceHelper.graphDataBaseService.getEdgeHeadersByGraphId(smartTreeNode.getGraphHeader().getId(), -1);

            // add vertex headers
            List<SmartTreeNode> vertexChildren = Lists.newArrayList();
            List<SmartTreeNode> tmpChildrenSet = Lists.newArrayList();
            for (int i = 0; i < vertexRecords.size(); i++) {
                tmpChildrenSet.add(new SmartTreeNode(vertexRecords.get(i), true));
                if (i % nodeSetSize == 0 && i > 0) {
                    vertexChildren.add(new SmartTreeNode(tmpChildrenSet));
                    tmpChildrenSet = Lists.newArrayList();
                }
            }
            if (vertexChildren.size() == 0) {
                vertexChildren.addAll(tmpChildrenSet);
            } else if (tmpChildrenSet.size() > 0){
                vertexChildren.add(new SmartTreeNode(tmpChildrenSet));
            }

            // add edge headers
//            List<SmartTreeNode> edgeChildren = Lists.newArrayList();
//            tmpChildrenSet = Lists.newArrayList();
//            for (int i = 0; i < edgeHeaders.size(); i++) {
//                tmpChildrenSet.add(new SmartTreeNode(edgeHeaders.get(i)));
//                if (i % nodeSetSize == 0 && i > 0) {
//                    edgeChildren.add(new SmartTreeNode(tmpChildrenSet));
//                    tmpChildrenSet = Lists.newArrayList();
//                }
//            }
//            if (edgeChildren.size() == 0) {
//                edgeChildren.addAll(tmpChildrenSet);
//            } else if (tmpChildrenSet.size() > 0){
//                edgeChildren.add(new SmartTreeNode(tmpChildrenSet));
//            }
//
//            vertexChildren.addAll(edgeChildren);
            return vertexChildren;
        } else if (smartTreeNode.isVertexHeader()) {
            List<AttributeRecord> attributeHeaders = VGMainServiceHelper.graphDataBaseService.getAttributeRecordsByOwner(smartTreeNode.getVertexHeader().getId(), AttributeRecord.VERTEX_OWNER_TYPE);

            children = new ArrayList<>(attributeHeaders.size() + 1);
            if (VGMainServiceHelper.graphDataBaseService.getVertexRecordsByOwnerId(smartTreeNode.getVertexHeader().getId(), 1).size() > 0) {
                children.add(new SmartTreeNode(smartTreeNode.getVertexHeader()));
            }

            for (AttributeRecord attributeHeader : attributeHeaders) {
                children.add(new SmartTreeNode(attributeHeader));
            }
        } else if (smartTreeNode.isEdgeHeader()) {
            List<AttributeRecord> attributeHeaders = VGMainServiceHelper.graphDataBaseService.getAttributeRecordsByOwner(smartTreeNode.getEdgeHeader().getId(), AttributeRecord.EDGE_OWNER_TYPE);

            children = new ArrayList<>(attributeHeaders.size());
            for (AttributeRecord attributeHeader : attributeHeaders) {
                children.add(new SmartTreeNode(attributeHeader));
            }
        }

        return children;
    }

    private static void markNodeRecursive(SmartTreeNode root, Map<String, SearchInfo> searchInfo) {
        if (root.isLoaded()) {
            for (int index = 0; index < root.getChildCount(); index++) {
                if (root.getChildAt(index) instanceof SmartTreeNode) {
                    SmartTreeNode child = (SmartTreeNode)root.getChildAt(index);
                    markNodeRecursive(child, searchInfo);
                }
            }
        }

        // setup result values
        SearchInfo searchInfoValue = searchInfo.get(root.getNodeId());
        if (searchInfoValue == null) {
            root.setSatisfiesSearchCondition(false);
            root.setSearchNumber(0);
        } else {
            root.setSatisfiesSearchCondition(searchInfoValue.isSatisfiesSearchCondition());
            root.setSearchNumber(searchInfoValue.getSearchNumber());
        }
    }

    private static void markNodeList(List<SmartTreeNode> nodes, Map<String, SearchInfo> searchInfo) {
        for (SmartTreeNode node : nodes) {
            SearchInfo searchInfoValue = searchInfo.get(node.getNodeId());
            if (searchInfoValue == null) {
                node.setSatisfiesSearchCondition(false);
                node.setSearchNumber(0);
            } else {
                node.setSatisfiesSearchCondition(searchInfoValue.isSatisfiesSearchCondition());
                node.setSearchNumber(searchInfoValue.getSearchNumber());
            }
        }
    }


//==============================================================================
//------------------PRIVATE CLASSES---------------------------------------------
    private static class SmartTreeRenderer extends DefaultTreeCellRenderer {
        private static final Icon graphModelIcon;
        private static final Icon graphIcon;
        private static final Icon vertexIcon;
        private static final Icon edgeIcon;

        private static final Color selectedBackgroundColor;
        private static final Color noSelectedBackgroundColor;
        private static final Color selectedForegroundColor;
        private static final Color satisfiesSearchConditionColor;

        static {
            graphModelIcon = new ImageIcon(FileUtils.getWorkingDir() + "data/resources/textures/navigator/graph_model.png");
            graphIcon = new ImageIcon(FileUtils.getWorkingDir() + "data/resources/textures/navigator/graph.png");
            vertexIcon = new ImageIcon(FileUtils.getWorkingDir() + "data/resources/textures/navigator/vertex.png");
            edgeIcon = new ImageIcon(FileUtils.getWorkingDir() + "data/resources/textures/navigator/edge.png");

            noSelectedBackgroundColor = Color.decode("0xFFFFFF");
            selectedBackgroundColor = UIManager.getDefaults().getColor("List.selectionBackground");
            selectedForegroundColor = UIManager.getDefaults().getColor("List.selectionForeground");
            satisfiesSearchConditionColor = Color.decode("0xFFCC00");
        }

        public Component getTreeCellRendererComponent(final JTree tree, final Object value, final boolean selected, final boolean expanded, final boolean leaf, final int row, final boolean hasFocus) {
            if (!(value instanceof SmartTreeNode)) {
                TreeCellRenderer delegate = new JTree().getCellRenderer();
                return delegate.getTreeCellRendererComponent(tree, value, selected, expanded, leaf, row, hasFocus);
            }

            SmartTreeNode smartTreeNode = (SmartTreeNode)value;

            JLabel label = new JLabel();
            label.setOpaque(true);

            if (selected) {
                label.setBackground(selectedBackgroundColor);
                label.setForeground(selectedForegroundColor);
            } else {
                label.setBackground(noSelectedBackgroundColor);
            }

            label.setText(smartTreeNode.getNodeId());
            if (smartTreeNode.isGraphRecord()) {
                label.setIcon(graphModelIcon);
            }

            if (smartTreeNode.isVertexWithInnerGraph()) {
                label.setIcon(graphIcon);
            }

            if (smartTreeNode.isVertexHeader()) {
                label.setIcon(vertexIcon);
            }

            if (smartTreeNode.isEdgeHeader()) {
                label.setIcon(edgeIcon);
            }

            if (smartTreeNode.getSearchNumber() > 0)
                label.setText(label.getText() + " (" + smartTreeNode.getSearchNumber() + ")");

            if (smartTreeNode.isSatisfiesSearchCondition()) {
                label.setBackground(satisfiesSearchConditionColor);
            }

            return label;
        }
    }

    public static class SmartTreeNode extends DefaultMutableTreeNode {
        // Main data
        private boolean loaded = false;
        private int searchNumber;
        private boolean satisfiesSearchCondition;
        private Object header;
        private boolean isVertex;

        public SmartTreeNode(Object header) {
            this(header, false);
        }

        public SmartTreeNode(Object header, boolean isVertex) {
            setHeader(header);
            setAllowsChildren(true);

            this.isVertex = isVertex;

            add(new DefaultMutableTreeNode("Loading...", false));
        }

        public void setSearchNumber(int searchNumber) {
            this.searchNumber = searchNumber;
        }

        public int getSearchNumber() {
            return searchNumber;
        }

        public boolean isSatisfiesSearchCondition() {
            return satisfiesSearchCondition;
        }

        public void setSatisfiesSearchCondition(boolean satisfiesSearchCondition) {
            this.satisfiesSearchCondition = satisfiesSearchCondition;
        }

        public boolean isLoaded() {
            return loaded;
        }

        public void setHeader(Object header) {
            if (header == null) return;

            this.header = header;
        }

        public VertexRecord getVertexWithInnerGraphRecord() {
            if (isVertex)
                return null;

            if (!(header instanceof VertexRecord))
                return null;
            VertexRecord vertexRecord = (VertexRecord)header;
            if (VGMainServiceHelper.graphDataBaseService.getVertexRecordsByOwnerId(vertexRecord.getId(), 1).size() > 0)
                return vertexRecord;
            return null;
        }

        public VertexRecord getVertexHeader() {
            if (!isVertex)
                return null;

            if (header instanceof VertexRecord && getVertexWithInnerGraphRecord() == null)
                return (VertexRecord)header;
            return null;
        }

        public GraphRecord getGraphRecord() {
            if (header instanceof GraphRecord)
                return (GraphRecord)header;
            return null;
        }

        public EdgeRecord getEdgeHeader() {
            if (header instanceof EdgeRecord)
                return (EdgeRecord)header;
            return null;
        }

        public AttributeRecord getAttributeHeader() {
            if (header instanceof AttributeRecord)
                return (AttributeRecord)header;
            return null;
        }

        public List<SmartTreeNode> getListHeader() {
            if (header instanceof List) {
                return (List<SmartTreeNode>)header;
            }
            return null;
        }

        public boolean isGraphRecord() {
            return getGraphRecord() != null;
        }

        public boolean isVertexWithInnerGraph() {
            return getVertexWithInnerGraphRecord() != null;
        }

        public boolean isVertexHeader() {
            return getVertexHeader() != null;
        }

        public boolean isEdgeHeader() {
            return getEdgeHeader() != null;
        }

        public boolean isAttributeHeader() {
            return getAttributeHeader() != null;
        }

        public boolean isListHeader() {
            return getListHeader() != null;
        }

        public void setChildren(List<SmartTreeNode> children) {
            super.removeAllChildren();
            for (SmartTreeNode node : children) {
                add(node);
            }
            loaded = true;
        }

        public String getNodeId() {
            if (isGraphRecord()) return IdGeneratorPlugin.generateHumanGraphId(getGraphRecord().getId());
            if (isVertexWithInnerGraph()) return IdGeneratorPlugin.generateHumanVertexWithInnerGraphId(getVertexWithInnerGraphRecord().getId());
            if (isVertexHeader()) return IdGeneratorPlugin.generateHumanVertexId(getVertexHeader().getId());
            if (isEdgeHeader()) return IdGeneratorPlugin.generateHumanEdgeId(getEdgeHeader().getId());
            if (isAttributeHeader()) return IdGeneratorPlugin.generateHumanAttributeId(getAttributeHeader().getId());

            if (isListHeader()) {
                String ids = "[";
                int size = 3;
                for (SmartTreeNode node : getListHeader()) {
                    ids += node.getNodeId() + ",";
                    if (--size == 0) break;
                }
                ids += "...]";
                return ids;
            }

            return "unknown";
        }

        @Override
        public void removeAllChildren() {
            super.removeAllChildren();
            add(new DefaultMutableTreeNode("Loading...", false));
            loaded = false;
        }

        @Override
        public boolean isLeaf() {
            return isAttributeHeader();
        }

        @Override
        public String toString() {
            String message = "";

            if (isGraphRecord())
                message += "graph, ";
            else if (isVertexHeader())
                message += "vertex, ";
            else if (isEdgeHeader())
                message += "edge, ";
            else if (isAttributeHeader())
                message += "attribute, ";
            else if (isListHeader())
                message += "list, ";
            else
                message += "unknown, ";

            message += "loaded:" + isLoaded() + ",";
            message += "searchNumber:" + getSearchNumber() + ",";
            message += "satisfiesSearchCondition:" + isSatisfiesSearchCondition();
            return message;
        }
    }

    private static class SearchInfo {
        private int searchNumber;
        private boolean satisfiesSearchCondition;

        private SearchInfo(int searchNumber, boolean satisfiesSearchCondition) {
            this.searchNumber = searchNumber;
            this.satisfiesSearchCondition = satisfiesSearchCondition;
        }

        public int getSearchNumber() {
            return searchNumber;
        }

        public boolean isSatisfiesSearchCondition() {
            return satisfiesSearchCondition;
        }
    }
}
