package vg.plugins.navigator.components.popup_menu_component;

import com.google.common.collect.Lists;
import vg.plugins.navigator.components.popup_menu_component.popup_menu_items.*;

import javax.swing.*;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;

public class PopupMenuComponent {
    // Components
    private final JPopupMenu popup;
    private final MouseAdapter mouseAdapter;

    // Main data
    private final List<IPopupMenuItem> menuItems;
    private final List<JTree> trees;
    private JTree lastSelectedTree;

    public PopupMenuComponent() {
        trees = Lists.newArrayList();
        menuItems = Lists.newArrayList();

		popup = new JPopupMenu();

		// create items
        addMenuItem(new DrawAsHierarchicalGraphPopupMenuItem());
        addMenuItem(new DrawAsGraphPopupMenuItem());
		addMenuItem(new DrawVerticesPopupMenuItem());
        addMenuItem(new DrawVertexAsHierarchicalGraphPopupMenuItem());
        addMenuItem(null);
        addMenuItem(new GraphComparisonPopupMenuItem());

        // add listeners
        mouseAdapter = new MouseAdapter() {
            public void mousePressed(MouseEvent e) {
                mouseReleased(e);
                if (e.getButton() == MouseEvent.BUTTON1 && e.getClickCount() >= 2) {
                    doSomething();
                }
            }

            public void mouseReleased(MouseEvent e) {
                if (e.isPopupTrigger()) {
                    buildPopupMenu(e.getComponent(), e.getX(), e.getY());
                }
            }
        };
	}

    public void addMenuItem(IPopupMenuItem menuItem) {
        menuItems.add(menuItem);
    }

    public void addTree(final JTree tree) {
        trees.add(tree);
        tree.addTreeSelectionListener(new TreeSelectionListener() {
            @Override
            public void valueChanged(TreeSelectionEvent e) {
                lastSelectedTree = tree;
            }
        });
        tree.addMouseListener(mouseAdapter);
    }

//==============================================================================
//------------------PRIVATE METHODS---------------------------------------------
	private void buildPopupMenu(Component invoker, int x, int y) {
		popup.removeAll();

		for(final IPopupMenuItem menuItem : menuItems) {
            if (menuItem == null) {
                popup.addSeparator();
                continue;
            }

            JMenuItem jMenuItem = new JMenuItem(menuItem.getActionName());
            jMenuItem.setIcon(menuItem.getIcon());
            jMenuItem.setEnabled(menuItem.isVisible(trees, lastSelectedTree));
            jMenuItem.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    menuItem.action(trees, lastSelectedTree);
                }
            });
            popup.add(jMenuItem);
		}

		if(popup.getComponentCount() > 0) {
			popup.show(invoker, x, y);
		}
	}

	private void doSomething() {}
}
