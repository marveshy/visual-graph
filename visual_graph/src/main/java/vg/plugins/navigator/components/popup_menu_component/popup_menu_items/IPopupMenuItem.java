package vg.plugins.navigator.components.popup_menu_component.popup_menu_items;

import javax.swing.*;
import java.util.Collection;

public interface IPopupMenuItem {
	void action(Collection<JTree> trees, JTree lastSelectedTree);

    boolean isVisible(Collection<JTree> trees, JTree lastSelectedTree);

    Icon getIcon();

    String getActionName();
}
