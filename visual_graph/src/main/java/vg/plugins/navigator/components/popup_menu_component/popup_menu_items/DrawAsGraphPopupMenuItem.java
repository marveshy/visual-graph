package vg.plugins.navigator.components.popup_menu_component.popup_menu_items;

import vg.impl.main_service.VGMainServiceHelper;
import vg.interfaces.data_base_service.data.graph.Edge;
import vg.interfaces.data_base_service.data.graph.Graph;
import vg.interfaces.data_base_service.data.graph.Vertex;
import vg.plugins.navigator.components.SmartTreeComponent;

import javax.swing.*;
import java.util.Collection;
import java.util.List;
import java.util.Map;

public class DrawAsGraphPopupMenuItem implements IPopupMenuItem {
    @Override
    public void action(Collection<JTree> trees, JTree lastSelectedTree) {
        if (lastSelectedTree == null) return;

        final SmartTreeComponent.SmartTreeNode node = (SmartTreeComponent.SmartTreeNode)lastSelectedTree.getLastSelectedPathComponent();
        if (node != null && node.isVertexWithInnerGraph()) {
            VGMainServiceHelper.executorService.execute(new Runnable() {
                @Override
                public void run() {
                    try {
                        Map.Entry<List<Vertex>, Map.Entry<List<Edge>, List<Edge>>> subGraphElements = VGMainServiceHelper.graphDataBaseService.getSubGraphElements(node.getVertexWithInnerGraphRecord().getId());
                        Graph graph = new Graph(subGraphElements.getKey(), subGraphElements.getValue().getKey());
                        VGMainServiceHelper.graphViewService.asyncOpenGraphInTab(graph, null);
                    } catch (Throwable ex) {
                        VGMainServiceHelper.logger.printError(ex.getMessage(), ex);
                    }
                }
            });
        } else {
            VGMainServiceHelper.windowMessenger.warningMessage("Selection should be a graph", "Opening of a graph", null);
        }
    }

    @Override
    public boolean isVisible(Collection<JTree> trees, JTree lastSelectedTree) {
        if (lastSelectedTree == null)
            return false;

        SmartTreeComponent.SmartTreeNode node = (SmartTreeComponent.SmartTreeNode)lastSelectedTree.getLastSelectedPathComponent();
        return node != null && node.isVertexWithInnerGraph();
    }

    @Override
    public Icon getIcon() {
        return null;
    }

    @Override
    public String getActionName() {
        return "Draw as graph";
    }
}
