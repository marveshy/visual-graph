package vg.plugins.graph_analyzer;

import vg.shared.gui.StorageFrame;
import vg.shared.gui.components.TextFieldWithLabel;
import vg.shared.utils.TextTransfer;
import vg.shared.utils.XmlUtils;
import vg.impl.main_service.VGMainServiceHelper;
import vg.interfaces.executor_service.ExecutorService;
import vg.interfaces.main_service.VGMainGlobals;
import vg.interfaces.plugin_service.Plugin;
import vg.interfaces.user_interface_service.UserInterfaceService;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author Timur Zolotuhin (e-mail: tzolotuhin@gmail.com)
 */
public class GraphAnalyzerPlugin implements Plugin {
    // Components
    private GraphAnalyzerPanel graphAnalyzerPanel;

    // Main data
    private static final AtomicInteger graphCounter = new AtomicInteger();

    @Override
    public void install() throws Exception {
        JMenuItem analyzeGraphMenuItem = new JMenuItem("Analyze graph");
        analyzeGraphMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (graphAnalyzerPanel == null)
                    graphAnalyzerPanel = new GraphAnalyzerPanel();
                graphAnalyzerPanel.setVisible(true);
            }
        });

        VGMainServiceHelper.userInterfaceService.addMenuItem(analyzeGraphMenuItem, UserInterfaceService.ANALYZE_MENU);
    }

    public class GraphAnalyzerPanel extends StorageFrame {
        private final static String WINDOWS_SIZE_X = "graph_analyzer_width";
        private final static String WINDOWS_SIZE_Y = "graph_analyzer_height";
        private final static String WINDOWS_POS_X = "graph_analyzer_pos_x";
        private final static String WINDOWS_POS_Y = "graph_analyzer_pos_y";

        private static final String GRAPHML_FORMAT = "graphml";

        // Main components
        private JButton okButton, cancelButton, normalizeButton;
        private JTextArea graphContentTextArea;
        private TextFieldWithLabel graphNameTextFieldWithLabel;
        private JComboBox<String> formatComboBox;

        // Main data
        private String graphContent;
        private String graphName;
        private String format;

        public GraphAnalyzerPanel() {
            super(WINDOWS_POS_X, WINDOWS_POS_Y, WINDOWS_SIZE_X, WINDOWS_SIZE_Y, false);

            graphContentTextArea = new JTextArea();
            graphContentTextArea.setFont(new Font(VGMainGlobals.UI_MONOSPACED_FONT_FAMILY, 0, 14));
            final JScrollPane graphContentScrollPane = new JScrollPane(graphContentTextArea);
            graphNameTextFieldWithLabel = new TextFieldWithLabel("Name", "");
            formatComboBox = new JComboBox<>();
            formatComboBox.addItem(GRAPHML_FORMAT);

            okButton = new JButton("Ok");
            cancelButton = new JButton("Cancel");
            normalizeButton = new JButton("Normalize");

            frame.setLayout(new GridBagLayout());
            frame.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
            frame.add(formatComboBox,
                    new GridBagConstraints(0, 0, 1, 1, 1, 0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
            frame.add(graphNameTextFieldWithLabel.getView(),
                    new GridBagConstraints(0, 1, 1, 1, 1, 0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
            frame.add(graphContentScrollPane,
                    new GridBagConstraints(0, 2, 1, 1, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(3, 3, 3, 3), 0, 0));

            JPanel buttonPanel = new JPanel(new GridBagLayout());
            buttonPanel.add(normalizeButton,
                    new GridBagConstraints(0, 0, 1, 1, 1, 0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
            buttonPanel.add(okButton,
                    new GridBagConstraints(1, 0, 1, 1, 1, 0, GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
            buttonPanel.add(cancelButton,
                    new GridBagConstraints(2, 0, 1, 1, 1, 0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));

            frame.add(buttonPanel,
                    new GridBagConstraints(0, 3, 1, 1, 1, 0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));

            okButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    graphName = graphNameTextFieldWithLabel.getTextField().getText();
                    graphContent = graphContentTextArea.getText();
                    format = formatComboBox.getSelectedItem().toString();

                    VGMainServiceHelper.executorService.execute(new ExecutorService.SwingExecutor() {
                        @Override
                        public void doInBackground() {
                            try {
                                VGMainServiceHelper.graphDecoderService.openGraph(graphName, graphContent, format);
                            } catch (Throwable ex) {
                                successfully = false;
                                VGMainServiceHelper.logger.printException(ex);
                                VGMainServiceHelper.windowMessenger.errorMessage("Can't analyze the graph, error: " + ex.getMessage(), "Graph analyzer", null);
                            }
                        }

                        @Override
                        public void doInEDT() {
                            if (isSuccessfully())
                                setVisible(false);
                        }
                    });
                }
            });

            cancelButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    setVisible(false);
                }
            });

            normalizeButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    try {
                        graphContent = graphContentTextArea.getText();
                        graphContent = XmlUtils.simpleFormatXml(graphContent, 2, 120);
                        graphContentTextArea.setText(graphContent);
                        graphContentScrollPane.updateUI();
                    } catch (Throwable ex) {
                        VGMainServiceHelper.logger.printException(ex);
                        VGMainServiceHelper.windowMessenger.errorMessage("Can't normalize input xml text. Error: " + ex.getMessage(), "Graph analyzer", null);
                    }
                }
            });
        }

        public void setVisible(boolean visible) {
            if (visible) {
                TextTransfer textTransfer = new TextTransfer();
                graphContent = textTransfer.getClipboardContents();
                graphNameTextFieldWithLabel.getTextField().setText("Graph sample #" + graphCounter.incrementAndGet());

                graphContentTextArea.setText(graphContent);
            }
            frame.setVisible(visible);
        }
    }
}
