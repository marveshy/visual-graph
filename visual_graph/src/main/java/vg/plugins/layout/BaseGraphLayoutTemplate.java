package vg.plugins.layout;

import com.google.common.collect.Lists;
import vg.interfaces.graph_layout_service.GraphLayoutFactory;
import vg.interfaces.graph_layout_service.GraphLayoutSetting;
import vg.interfaces.graph_layout_service.GraphLayoutTemplate;

import java.util.Collections;
import java.util.List;

/**
 * @author Timur Zolotuhin (e-mail: tzolotuhin@parallels.com)
 */
public abstract class BaseGraphLayoutTemplate implements GraphLayoutTemplate {
    // Mutex
    protected final Object generalMutex = new Object();

    // Main data
    protected GraphLayoutFactory graphLayoutFactoryCallBack;
    protected List<GraphLayoutSetting> settings = Lists.newArrayList();

    public BaseGraphLayoutTemplate(GraphLayoutFactory graphLayoutFactoryCallBack) {
        this.graphLayoutFactoryCallBack = graphLayoutFactoryCallBack;
    }

    @Override
    public void setSettings(List<GraphLayoutSetting> settings) {
        synchronized (generalMutex) {
            // TODO
        }
    }

    @Override
    public List<GraphLayoutSetting> getSettings() {
        synchronized (generalMutex) {
            return Collections.unmodifiableList(settings);
        }
    }

    @Override
    public GraphLayoutFactory getGraphLayoutFactoryCallBack() {
        synchronized (generalMutex) {
            return graphLayoutFactoryCallBack;
        }
    }
}
