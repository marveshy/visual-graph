package vg.plugins.layout;

import com.google.common.collect.Maps;
import vg.interfaces.graph_layout_service.GraphLayoutFactory;
import vg.interfaces.graph_layout_service.GraphLayoutTemplate;
import vg.interfaces.graph_view_service.data.VGGraph;

import java.util.Map;

/**
 * @author Timur Zolotuhin (e-mail: tzolotuhin@parallels.com)
 */
public abstract class BaseGraphLayoutFactory implements GraphLayoutFactory {
    // Mutex
    protected final Object generalMutex = new Object();

    // Main data
    protected Map<String, Object> settings = Maps.newHashMap();
    protected Map<String, String> titleSettings = Maps.newHashMap();
}
