package vg.plugins.layout;

import com.google.common.collect.Maps;
import vg.interfaces.graph_view_service.GraphViewExecutor;
import vg.impl.main_service.VGMainServiceHelper;
import vg.interfaces.data_base_service.data.graph.Edge;
import vg.interfaces.data_base_service.data.graph.Vertex;
import vg.interfaces.executor_service.ExecutorService;
import vg.interfaces.graph_layout_service.GraphLayout;
import vg.interfaces.graph_layout_service.GraphLayoutFactory;
import vg.interfaces.graph_layout_service.GraphLayoutSetting;
import vg.interfaces.graph_layout_service.GraphLayoutTemplate;
import vg.interfaces.graph_view_service.GraphView;
import vg.interfaces.graph_view_service.GraphViewListener;
import vg.interfaces.main_service.VGMainGlobals;
import vg.interfaces.plugin_service.Plugin;
import vg.interfaces.user_interface_service.UserInterfaceListener;
import vg.interfaces.user_interface_service.UserInterfacePanel;
import vg.interfaces.user_interface_service.UserInterfaceService;
import vg.interfaces.user_interface_service.UserInterfaceTab;
import vg.plugins.layout.circle.CircleLayoutFactory;
import vg.plugins.layout.hierarchical_layout.HierarchicalLayoutFactory;
import vg.plugins.layout.organic.OrganicLayoutFactory;
import vg.plugins.layout.smart_layout.SmartLayoutFactory;
import vg.shared.graph.utils.GraphUtils;
import vg.shared.gui.SwingUtils;
import vg.shared.user_interface.DefaultSwingUtilsCallBack;
import vg.shared.user_interface.UserInterfacePanelUtils;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collection;
import java.util.List;
import java.util.Map;

public class GraphLayoutPlugin implements Plugin, UserInterfacePanel {
    // Constants
    private static final int NO_GRAPH_VIEW_STATE = 0;
    private static final int GRAPH_VIEW_STATE = 1;
    private static final int GRAPH_VIEW_LAYOUT_IN_PROGRESS_STATE = 2;

    private static Dimension nameLabelDimension = new Dimension(250, 20);
    private static Dimension valueLabelDimension = new Dimension(200, 20);
    private static Insets rowInsets = new Insets(2, 5, 2, 0);

    // Main components
    private JPanel outView, innerView;

    private JLabel infoLabel;
    private JLabel pleaseWaitInfoLabel;
    private JLabel currentLayoutLabel;
    private JLabel currentLayoutSettingsLabel;

    private JComboBox<String> layoutComboBox;
    private JButton runButton;
    private JButton cancelButton;

    private Font subTitleFont = new Font(VGMainGlobals.UI_STANDARD_FONT_FAMILY, Font.BOLD, 13);

    // Main data
    private GraphViewExecutor currentGraphViewExecutor;
    private GraphLayout currentGraphLayout;
    private Map<GraphLayoutSetting, String> currentSettings;

    private int state = NO_GRAPH_VIEW_STATE;

    // Mutex
    private final Object generalMutex = new Object();

    @Override
	public void install() throws Exception {
        // install layouts
        VGMainServiceHelper.graphLayoutService.registerGraphLayoutFactory(new HierarchicalLayoutFactory());
        VGMainServiceHelper.graphLayoutService.registerGraphLayoutFactory(new SmartLayoutFactory());
        VGMainServiceHelper.graphLayoutService.registerGraphLayoutFactory(new CircleLayoutFactory());
        VGMainServiceHelper.graphLayoutService.registerGraphLayoutFactory(new OrganicLayoutFactory());

        outView = new JPanel(new GridLayout(1, 1));
        innerView = new JPanel(new GridBagLayout());
        outView.add(innerView);

        infoLabel = new JLabel("If you want to use Layout Manager you need select tab with graph view");
        infoLabel.setHorizontalAlignment(JLabel.CENTER);
        pleaseWaitInfoLabel = new JLabel("Please wait...");
        pleaseWaitInfoLabel.setHorizontalAlignment(JLabel.CENTER);

        currentLayoutLabel = new JLabel("Current layout:");
        currentLayoutSettingsLabel = new JLabel("Current layout settings:");
        currentLayoutLabel.setFont(subTitleFont);
        currentLayoutSettingsLabel.setFont(subTitleFont);

        cancelButton = new JButton("Cancel");
        runButton = new JButton("Run");

        layoutComboBox = new JComboBox<>();
        for (GraphLayoutFactory graphLayoutFactory : VGMainServiceHelper.graphLayoutService.getInstalledLayoutFactories()) {
            layoutComboBox.addItem(graphLayoutFactory.getLayoutName());
        }

        VGMainServiceHelper.userInterfaceService.addObserver(new UserInterfaceListener() {
            @Override
            public void onOpenTab(UserInterfaceTab tab) {
                VGMainServiceHelper.logger.printDebug("GraphLayoutPlugin:onOpenTab action");
                if (tab instanceof GraphView) {
                    final GraphView tabGraphView = (GraphView) tab;

                    tabGraphView.addListener(new GraphViewListener() {
                        @Override
                        public void onStartLayout(GraphLayout layout) {
                            doUpdateLayout(tabGraphView);
                        }

                        @Override
                        public void onStopLayout(GraphLayout layout) {
                            doUpdateLayout(tabGraphView);
                        }

                        @Override
                        public void onAddElements(Collection<Vertex> vertices, Collection<Edge> edges) {
                            doExecuteLayout(tabGraphView);
                        }
                    });
                }
            }

            @Override
            public void onChangeTab(UserInterfaceTab tab) {
                VGMainServiceHelper.logger.printDebug("GraphLayoutPlugin:onChangeTab action");
                final GraphView tabGraphView;
                if (tab != null && tab instanceof GraphView) {
                    tabGraphView = (GraphView) tab;
                } else {
                    tabGraphView = null;
                }

                synchronized (generalMutex) {
                    currentGraphViewExecutor = tabGraphView;
                }

                if (tabGraphView != null) {
                    doUpdateLayout(tabGraphView);
                }
            }
        }, 15);

        cancelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                synchronized (generalMutex) {
                    if (currentGraphLayout != null && currentGraphLayout.getState() == GraphLayout.STARTED_STATE) {
                        currentGraphLayout.stop();
                    }
                }
            }
        });

        runButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                doExecuteLayout(currentGraphViewExecutor);
            }
        });

        layoutComboBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                doUpdateLayoutSettings(true);
            }
        });

        rebuildView();

        UserInterfacePanelUtils.createPanel("Layout manager", this, UserInterfaceService.SOUTH_INSTRUMENT_PANEL, UserInterfaceService.WEST_SOUTH_PANEL);
    }

    @Override
    public JPanel getView() {
        synchronized (generalMutex) {
            return outView;
        }
    }

//==============================================================================
//-----------------PRIVATE METHODS----------------------------------------------
	private void doExecuteLayout(final GraphViewExecutor graphViewExecutor) {
        synchronized (generalMutex) {
            if ((currentGraphLayout == null || currentGraphLayout.getState() == GraphLayout.STOPPED_STATE) && graphViewExecutor != null) {
                graphViewExecutor.execute(new GraphViewExecutor.GraphViewRunnable() {
                    @Override
                    public void run(GraphView graphView) {
                        synchronized (generalMutex) {
                            GraphLayoutFactory graphLayoutFactory = VGMainServiceHelper.graphLayoutService.getLayoutFactoryByName(layoutComboBox.getSelectedItem().toString());

                            try {
                                GraphUtils.analyzeGraphAndLayout(graphView.getGraph(), graphLayoutFactory);

                                GraphLayoutTemplate graphLayoutTemplate = graphLayoutFactory.buildGraphLayoutTemplate();
                                for (GraphLayoutSetting graphLayoutSetting : graphLayoutTemplate.getSettings()) {
                                    try {
                                        graphLayoutSetting.setValue(currentSettings.get(graphLayoutSetting), graphLayoutSetting.getType());
                                    } catch (IllegalArgumentException ex) {
                                        VGMainServiceHelper.logger.printException(ex);
                                        VGMainServiceHelper.windowMessenger.errorMessage("Wrong format for " + graphLayoutSetting.getName(), "Execute layout", null);
                                        return;
                                    }
                                }

                                VGMainServiceHelper.logger.printDebug("Try to execute layout...");
                                graphView.setLayoutTemplate(graphLayoutTemplate);
                            } catch (IllegalArgumentException ex) {
                                VGMainServiceHelper.logger.printException(ex);
                                VGMainServiceHelper.windowMessenger.errorMessage("Error: " + ex.getMessage() + "\n" +
                                        "Please use another layout", "Execute layout", null);
                            }
                        }
                    }
                });
            }
        }
    }

    private void doUpdateLayout(final GraphView graphView) {
        doUpdateLayoutSettings(false);
        VGMainServiceHelper.executorService.execute(new ExecutorService.SwingExecutor() {
            private GraphLayout graphLayout;

            @Override
            public void doInBackground() {
                graphLayout = graphView.getCurrentLayout();
            }

            @Override
            public void doInEDT() {
                synchronized (generalMutex) {
                    if (currentGraphViewExecutor == graphView) {
                        currentGraphLayout = graphLayout;
                        if (currentGraphLayout == null || currentGraphLayout.getState() == GraphLayout.STOPPED_STATE)
                            state = GRAPH_VIEW_STATE;
                        else
                            state = GRAPH_VIEW_LAYOUT_IN_PROGRESS_STATE;
                        rebuildView();
                    }
                }
            }
        });
    }

    private void doUpdateLayoutSettings(final boolean changeLayoutTemplate) {
        synchronized (generalMutex) {
            if (currentGraphViewExecutor != null) {
                currentGraphViewExecutor.execute(new GraphViewExecutor.GraphViewRunnable() {
                    @Override
                    public void run(GraphView graphView) {
                        synchronized (generalMutex) {
                            List<GraphLayoutSetting> settings;
                            if (graphView.getLayoutTemplate() == null || changeLayoutTemplate) {
                                GraphLayoutFactory graphLayoutFactory = VGMainServiceHelper.graphLayoutService.getLayoutFactoryByName(layoutComboBox.getSelectedItem().toString());
                                GraphLayoutTemplate graphLayoutTemplate = graphLayoutFactory.buildGraphLayoutTemplate();
                                settings = graphLayoutTemplate.getSettings();
                            } else {
                                settings = graphView.getLayoutTemplate().getSettings();
                            }
                            currentSettings = Maps.newHashMap();
                            for (GraphLayoutSetting graphLayoutSetting : settings) {
                                currentSettings.put(graphLayoutSetting, graphLayoutSetting.getStringValue());
                            }
                            SwingUtils.invokeInEDTIfNeeded(new Runnable() {
                                @Override
                                public void run() {
                                    synchronized (generalMutex) {
                                        rebuildView();
                                    }
                                }
                            }, new DefaultSwingUtilsCallBack());

                        }
                    }
                });
            }
        }
    }

    private void rebuildView() {
        synchronized (generalMutex) {
            innerView.removeAll();

            switch (state) {
                case NO_GRAPH_VIEW_STATE:
                    innerView.add(infoLabel, new GridBagConstraints(0, 0, 1, 1, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
                    break;
                case GRAPH_VIEW_STATE:
                    layoutComboBox.setEnabled(true);
                    int index = 0;

                    // add general settings to panel
                    innerView.add(currentLayoutLabel, new GridBagConstraints(0, index++, 1, 1, 1, 0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(5, 0, 0, 0), 0, 0));
                    JScrollPane generalSettingPanel = generateGeneralSettingPanel();
                    innerView.add(generalSettingPanel, new GridBagConstraints(0, index++, 1, 1, 1, 0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));

                    // add current layout settings to panel
                    innerView.add(currentLayoutSettingsLabel, new GridBagConstraints(0, index++, 1, 1, 1, 0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(10, 0, 0, 0), 0, 0));
                    JScrollPane settingsPanel = generateCurrentSettingsPanel();
                    innerView.add(settingsPanel, new GridBagConstraints(0, index++, 1, 1, 1, 1, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));

                    innerView.add(runButton, new GridBagConstraints(0, index, 1, 1, 1, 0, GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(10, 0, 10, 0), 0, 0));
                    break;
                case GRAPH_VIEW_LAYOUT_IN_PROGRESS_STATE:
                    layoutComboBox.setEnabled(false);
                    innerView.add(layoutComboBox, new GridBagConstraints(0, 0, 1, 1, 1, 0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
                    innerView.add(pleaseWaitInfoLabel, new GridBagConstraints(0, 1, 1, 1, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
                    innerView.add(cancelButton, new GridBagConstraints(0, 2, 1, 1, 1, 0, GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
                    break;
            }

            innerView.updateUI();
        }
    }

    private JScrollPane generateGeneralSettingPanel() {
        // parameters
        JPanel generalSettingsPanel = new JPanel(new GridBagLayout());
        int i = 0;

        // add layout name label to panel
        {
            JLabel currentLayoutNameLabel = new JLabel("Current layout:");
            currentLayoutNameLabel.setMinimumSize(nameLabelDimension);
            currentLayoutNameLabel.setPreferredSize(currentLayoutNameLabel.getMinimumSize());
            currentLayoutNameLabel.setMaximumSize(currentLayoutNameLabel.getMinimumSize());
            generalSettingsPanel.add(currentLayoutNameLabel, new GridBagConstraints(0, i, 1, 1, 0, 0, GridBagConstraints.WEST, GridBagConstraints.NONE, rowInsets, 0, 0));
        }

        // add layout value label to panel
        {
            layoutComboBox.setMinimumSize(valueLabelDimension);
            layoutComboBox.setPreferredSize(layoutComboBox.getMinimumSize());
            layoutComboBox.setMaximumSize(layoutComboBox.getMinimumSize());
            generalSettingsPanel.add(layoutComboBox, new GridBagConstraints(1, i, 1, 1, 1, 0, GridBagConstraints.WEST, GridBagConstraints.NONE, rowInsets, 0, 0));
        }

        return new JScrollPane(generalSettingsPanel);
    }

    private JScrollPane generateCurrentSettingsPanel() {
        // parameters
        JPanel currentSettingsPanel = new JPanel(new GridBagLayout());
        int i = 0;

        GraphLayoutFactory currentLayoutFactory = VGMainServiceHelper.graphLayoutService.getLayoutFactoryByName(layoutComboBox.getSelectedItem().toString());

        // add support ports name label to panel
        {
            JLabel supportPortsNameLabel = new JLabel("Support ports:");
            supportPortsNameLabel.setMinimumSize(nameLabelDimension);
            supportPortsNameLabel.setPreferredSize(supportPortsNameLabel.getMinimumSize());
            supportPortsNameLabel.setMaximumSize(supportPortsNameLabel.getMinimumSize());
            currentSettingsPanel.add(supportPortsNameLabel, new GridBagConstraints(0, i, 1, 1, 0, 0, GridBagConstraints.WEST, GridBagConstraints.NONE, rowInsets, 0, 0));
        }

        // add support ports value label to panel
        {
            JLabel supportPortsValueLabel = new JLabel((currentLayoutFactory.isPortsSupport() ? "Yes" : "No"));
            supportPortsValueLabel.setMinimumSize(valueLabelDimension);
            supportPortsValueLabel.setPreferredSize(supportPortsValueLabel.getMinimumSize());
            supportPortsValueLabel.setMaximumSize(supportPortsValueLabel.getMinimumSize());
            currentSettingsPanel.add(supportPortsValueLabel, new GridBagConstraints(1, i, 1, 1, 1, 0, GridBagConstraints.WEST, GridBagConstraints.NONE, rowInsets, 0, 0));
            i++;
        }

        // add support hierarchy name label to panel
        {
            JLabel supportHierarchicalGraphNameLabel = new JLabel("Support hierarchical graph:");
            supportHierarchicalGraphNameLabel.setMinimumSize(nameLabelDimension);
            supportHierarchicalGraphNameLabel.setPreferredSize(supportHierarchicalGraphNameLabel.getMinimumSize());
            supportHierarchicalGraphNameLabel.setMaximumSize(supportHierarchicalGraphNameLabel.getMinimumSize());
            currentSettingsPanel.add(supportHierarchicalGraphNameLabel, new GridBagConstraints(0, i, 1, 1, 0, 0, GridBagConstraints.WEST, GridBagConstraints.NONE, rowInsets, 0, 0));
        }

        // add support hierarchy value label to panel
        {
            JLabel supportHierarchicalGraphValueLabel = new JLabel((currentLayoutFactory.isHierarchicalSupport() ? "Yes" : "No"));
            supportHierarchicalGraphValueLabel.setMinimumSize(valueLabelDimension);
            supportHierarchicalGraphValueLabel.setPreferredSize(supportHierarchicalGraphValueLabel.getMinimumSize());
            supportHierarchicalGraphValueLabel.setMaximumSize(supportHierarchicalGraphValueLabel.getMinimumSize());
            currentSettingsPanel.add(supportHierarchicalGraphValueLabel, new GridBagConstraints(1, i, 1, 1, 0, 0, GridBagConstraints.WEST, GridBagConstraints.NONE, rowInsets, 0, 0));
            i++;
        }

        // add other settings to panel
        if (currentSettings != null) {
            for (final GraphLayoutSetting setting : currentSettings.keySet()) {
                // add name setting to panel
                JLabel settingNameLabel = new JLabel(setting.getText());
                settingNameLabel.setMinimumSize(nameLabelDimension);
                settingNameLabel.setPreferredSize(settingNameLabel.getMinimumSize());
                settingNameLabel.setMaximumSize(settingNameLabel.getMinimumSize());
                currentSettingsPanel.add(settingNameLabel, new GridBagConstraints(0, i, 1, 1, 0, 0, GridBagConstraints.WEST, GridBagConstraints.NONE, rowInsets, 0, 0));

                // add value setting to panel
                switch (setting.getSettingType()) {
                    case GraphLayoutSetting.VALUE_SETTING:
                        final JTextField settingValueTextField = new JTextField(currentSettings.get(setting));
                        settingValueTextField.setMinimumSize(valueLabelDimension);
                        settingValueTextField.setPreferredSize(settingNameLabel.getMinimumSize());
                        settingValueTextField.setMaximumSize(settingNameLabel.getMinimumSize());
                        currentSettingsPanel.add(settingValueTextField, new GridBagConstraints(1, i, 1, 1, 0, 0, GridBagConstraints.WEST, GridBagConstraints.NONE, rowInsets, 0, 0));

                        settingValueTextField.getDocument().addDocumentListener(new DocumentListener() {
                            public void changedUpdate(DocumentEvent e) {
                                doChangeUpdate();
                            }

                            public void removeUpdate(DocumentEvent e) {
                                doChangeUpdate();
                            }

                            public void insertUpdate(DocumentEvent e) {
                                doChangeUpdate();
                            }

                            void doChangeUpdate() {
                                synchronized (generalMutex) {
                                    currentSettings.put(setting, settingValueTextField.getText());
                                }
                            }
                        });
                        break;
                    case GraphLayoutSetting.LIST_SETTING:
                        final JComboBox<String> comboBox = new JComboBox<>();
                        comboBox.setMinimumSize(valueLabelDimension);
                        comboBox.setPreferredSize(comboBox.getMinimumSize());
                        comboBox.setMaximumSize(comboBox.getMinimumSize());

                        for (Object value : setting.getAvailableValues()) {
                            comboBox.addItem(value.toString());
                        }

                        currentSettingsPanel.add(comboBox, new GridBagConstraints(1, i, 1, 1, 0, 0, GridBagConstraints.WEST, GridBagConstraints.NONE, rowInsets, 0, 0));

                        comboBox.addActionListener(new ActionListener() {
                            @Override
                            public void actionPerformed(ActionEvent e) {
                                synchronized (generalMutex) {
                                    currentSettings.put(setting, comboBox.getSelectedItem().toString());
                                }
                            }
                        });

                        comboBox.setSelectedItem(setting.getStringValue());
                        break;
                }
                i++;
            }
        }
        return new JScrollPane(currentSettingsPanel);
    }
}
