package vg.plugins.layout.smart_layout;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.google.common.collect.Lists;
import org.jbox2d.collision.shapes.EdgeShape;
import org.jbox2d.collision.shapes.MassData;
import org.jbox2d.collision.shapes.PolygonShape;
import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.*;
import vg.shared.gui.SwingUtils;
import vg.impl.main_service.VGMainServiceHelper;
import vg.interfaces.executor_service.ExecutorService;
import vg.interfaces.graph_layout_service.GraphLayout;
import vg.interfaces.graph_layout_service.GraphLayoutFactory;
import vg.interfaces.graph_layout_service.GraphLayoutSetting;
import vg.interfaces.graph_layout_service.GraphLayoutTemplate;
import vg.interfaces.graph_view_service.data.VGEdgeCell;
import vg.interfaces.graph_view_service.data.VGGraph;
import vg.interfaces.graph_view_service.data.VGVertexCell;
import vg.plugins.layout.BaseGraphLayout;
import vg.plugins.layout.BaseGraphLayoutFactory;
import vg.plugins.layout.BaseGraphLayoutTemplate;
import vg.shared.graph.utils.GraphUtils;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author Timur Zolotuhin (e-mail: tzolotuhin@parallels.com)
 */
public class SmartLayoutFactory extends BaseGraphLayoutFactory {
    private final static String LAYOUT_NAME = "Smart layout";

    @Override
    public boolean isPortsSupport() {
        return false;
    }

    @Override
    public boolean isHierarchicalSupport() {
        return false;
    }

    @Override
    public GraphLayoutTemplate buildGraphLayoutTemplate() {
        return new SmartLayoutTemplate(this);
    }

    @Override
    public String getLayoutName() {
        return LAYOUT_NAME;
    }

    private class SmartLayoutTemplate extends BaseGraphLayoutTemplate {
        public SmartLayoutTemplate(GraphLayoutFactory graphLayoutFactoryCallBack) {
            super(graphLayoutFactoryCallBack);
        }

        @Override
        public GraphLayout buildGraphLayout() {
            synchronized (generalMutex) {
                return new SmartLayout(this, settings);
            }
        }
    }

    private static class SmartLayout extends BaseGraphLayout {
        private static final float MIN_R_LENGTH = 1;
        private static final float MAX_R_LENGTH = 400;
        private static final float MIN_EDGE_LENGTH = 200;
        private static final float MAX_EDGE_LENGTH = 300;
        private static final float MAX_F = 1000000;
        private static final float MAX_m = 1000;
        private static final int MAX_LEVEL = 100;
        private static final float MIN_IDLE_DX = 5.0f;
        private static final int MAX_IDLE_STEPS = 5;

        public SmartLayout(GraphLayoutTemplate callback, List<GraphLayoutSetting> settings) {
            super(callback, settings);
        }

        @Override
        public void start(final VGGraph vgGraph, final GraphLayoutCallBack callBack) {
            // preparing
            vgGraph.bfs(new VGGraph.SearchListener() {
                @Override
                public void onEdge(VGEdgeCell vgEdgeCell) {
                    if (vgEdgeCell.getGeometry().getPoints() != null)
                        vgEdgeCell.getGeometry().getPoints().clear();
                }
            });

            // make a world
            final Map<Integer, World> worlds = new LinkedHashMap<>();

            final BiMap<VGVertexCell, BodyExtend> vertices = HashBiMap.create();

            int vertexCount = vgGraph.getVGVertexCount();
            final int[][] adjacencyMatrix = new int[vertexCount][vertexCount];
            vgGraph.bfs(new VGGraph.SearchListener() {
                private int matrixId = 0;

                @Override
                public void onVertex(VGVertexCell vgVertexCell) {
                    int level = VGGraph.calcCellLevel(vgVertexCell);
                    World world = worlds.get(level);

                    // create new world
                    if (world == null) {
                        world = new World(new Vec2(0.0f, 0.0f));
                        Vec2 center = new Vec2(100000, 100000);
                        List<Vec2> points = calcRegularPolygonPoints(center, center.x, 4, true);
                        createBorderBody(world, points);

                        worlds.put(level, world);
                    }

                    // add cells to the world
                    float radius = (float)Math.max(vgVertexCell.getGeometry().getWidth(), vgVertexCell.getGeometry().getHeight()) / 2.0f * (float)Math.sqrt(2);
                    BodyExtend bodyExtend = createDynamicBody(
                            matrixId++,
                            new Vec2(
                                    (float) vgVertexCell.getGeometry().getCenterX() + (level > 1 ? (float)(vgVertexCell.getParent()).getGeometry().getX() : 0.0f),
                                    (float) vgVertexCell.getGeometry().getCenterY() + (level > 1 ? (float)(vgVertexCell.getParent()).getGeometry().getY() : 0.0f)),
                            radius,
                            world,
                            null);

                    vertices.put(vgVertexCell, bodyExtend);
                }
            });

            // calculate matrix
            vgGraph.bfs(new VGGraph.SearchListener() {
                @Override
                public void onEdge(VGEdgeCell vgEdgeCell) {
                    int sourceId = vertices.get(vgEdgeCell.getVGSoure()).getMatrixId();
                    int targetId = vertices.get(vgEdgeCell.getVGTarget()).getMatrixId();
                    adjacencyMatrix[sourceId][targetId] = 1;
                    //if (!GraphUtils.isDirectedEdge(vgEdgeCell.getVGObject())) {
                    adjacencyMatrix[targetId][sourceId] = 1;
                    //}
                }
            });

            GraphUtils.doIntAlgorithmFloydUorshell(adjacencyMatrix);

            // simulate the world
            float timeStep = 1.0f * 1.0f;//(1 / 45.0f);
            int velocityIterations = 10;
            int positionIterations = 10;
            while (state.get() == STARTED_STATE) {
                //updateIntersection(worlds, vgGraph, vertices);
                updateSizes(worlds, vgGraph, vertices);
                updateForces(vgGraph, vertices, adjacencyMatrix);

                boolean isIdle = true;
                for (int i = 1; i < MAX_LEVEL; i++) {
                    World world = worlds.get(i);
                    World prevWorld = worlds.get(i - 1);
                    if (world == null)
                        break;
                    List<Body> borderBodies = null;
                    if (prevWorld != null) {
                        borderBodies = createBorders(world, vgGraph, vertices, i - 1);
                    }
                    world.step(timeStep, velocityIterations, positionIterations);
                    isIdle &= updatePositions(i, worlds, vgGraph, vertices);
                    if (prevWorld != null) {
                        removeBorders(world, borderBodies);
                    }
                }

                updateGraphView(vgGraph, vertices);

                if (isIdle) {
                    break;
                }
            }

            state.set(STOPPED_STATE);
            VGMainServiceHelper.executorService.execute(new ExecutorService.SwingExecutor() {
                @Override
                public void doInEDT() {
                    if (callBack != null)
                        callBack.onFinishAction(SmartLayout.this);
                }
            });
            System.out.println("Done.");
        }

//==============================================================================
//------------------PRIVATE METHODS---------------------------------------------
        private static void updateForces(final VGGraph vgGraph, final BiMap<VGVertexCell, BodyExtend> vertices, int[][] adjacencyMatrix) {
            for (BodyExtend body1 : vertices.values()) {
                body1.getBody().m_force.setZero();
                Vec2 resultF = new Vec2();
                for (BodyExtend body2 : vertices.values()) {
                    if (body2 == body1)
                        continue;

                    VGVertexCell vgVertexCell1 = vertices.inverse().get(body1);
                    VGVertexCell vgVertexCell2 = vertices.inverse().get(body2);

                    Vec2 diff = body1.getBody().getPosition().sub(body2.getBody().getPosition());
                    Vec2 diffNorm = new Vec2(diff);
                    diffNorm.normalize();

                    float centerR = (float)Math.sqrt(diff.x * diff.x + diff.y * diff.y);
                    float length = centerR - body1.getRadius() -  body2.getRadius();

                    // calculate repulsive force
                    Vec2 repulsiveForce = new Vec2();
                    if (vgVertexCell1.getParent() == vgVertexCell2.getParent()) {
                        if (length < MIN_R_LENGTH) {
                            repulsiveForce = diffNorm.mul(MAX_F);
                            VGMainServiceHelper.logger.printInfo("warning: intersection of two bodies was detected");
                        } else {
                            int pathLength = adjacencyMatrix[body1.getMatrixId()][body2.getMatrixId()] + 1;
                            repulsiveForce = diffNorm.mul(MAX_F * (1 - (length - MIN_R_LENGTH) / (pathLength * MAX_R_LENGTH - MIN_R_LENGTH)));
                        }
                    }

                    // calculate attractive force
                    VGEdgeCell vgEdgeCell = vgGraph.getUndirectedVGEdgeCellByVertices(vertices.inverse().get(body1), vertices.inverse().get(body2));
                    Vec2 attractiveForce = new Vec2();
                    if (vgEdgeCell != null && length > MIN_EDGE_LENGTH && length > MAX_EDGE_LENGTH) {
                        attractiveForce = diffNorm.mul(MAX_F * (length - MIN_EDGE_LENGTH) / (MAX_EDGE_LENGTH - MIN_EDGE_LENGTH));
                    }

                    resultF = resultF.add(repulsiveForce).sub(attractiveForce);
                }

                Vec2 vResult = resultF.mul(1.0f / body1.getMass());

                //body1.setPrevVel(vResult);
                body1.getBody().setLinearVelocity(vResult);
            }
        }

        private static boolean updatePositions(int parentLevel, Map<Integer, World> worlds, VGGraph vgGraph, BiMap<VGVertexCell, BodyExtend> vertices) {
            boolean check = false;

            int level = parentLevel + 1;
            for (; level < MAX_LEVEL; level++) {
                if (!worlds.containsKey(level))
                    break;

                // update positions for children
                List<VGVertexCell> childVertices = vgGraph.getVGVerticesByLevel(level);
                for (VGVertexCell childVertex : childVertices) {
                    VGVertexCell parentVertexCell = (VGVertexCell)childVertex.getParent();
                    BodyExtend parentBodyExtend = vertices.get(parentVertexCell);
                    BodyExtend childBodyExtend = vertices.get(childVertex);

                    Vec2 dx = parentBodyExtend.getBody().getPosition().sub(parentBodyExtend.getLastPosition());
                    Vec2 pos = childBodyExtend.getBody().getPosition().add(dx);

                    if (pos.sub(parentBodyExtend.getBody().getPosition()).length() + childBodyExtend.getRadius() > parentBodyExtend.getRadius()) {
                        VGMainServiceHelper.logger.printInfo("warning: parent border was ignored");
                        pos = parentBodyExtend.getBody().getPosition();
                    }

                    worlds.get(level).destroyBody(childBodyExtend.getBody());
                    BodyExtend newChildBodyExtend = createDynamicBody(
                            childBodyExtend.getMatrixId(),
                            pos,
                            childBodyExtend.getRadius(),
                            worlds.get(level),
                            childBodyExtend);
                    vertices.put(childVertex, newChildBodyExtend);
                }

                // update prevent position for parent
                check |= updateBodyExtendPositions(vgGraph, level - 1, vertices);
            }

            return check | updateBodyExtendPositions(vgGraph, level - 1, vertices);
        }

        private static boolean updateBodyExtendPositions(VGGraph vgGraph, int level, BiMap<VGVertexCell, BodyExtend> vertices) {
            boolean isIdle = true;
            List<VGVertexCell> parentVertices = vgGraph.getVGVerticesByLevel(level);
            for (VGVertexCell parentVertex : parentVertices) {
                BodyExtend parentBodyExtend = vertices.get(parentVertex);
                parentBodyExtend.addLastPosition(parentBodyExtend.getBody().getPosition());
                isIdle &= parentBodyExtend.isIdle();
            }
            return isIdle;
        }

        private static void updateSizes(Map<Integer, World> worlds, final VGGraph vgGraph, final BiMap<VGVertexCell, BodyExtend> vertices) {
            for (int level = 1; level < MAX_LEVEL; level++) {
                if (!worlds.containsKey(level))
                    break;

                List<VGVertexCell> parentVertices = vgGraph.getVGVerticesByLevel(level);

                // update positions for children
                for (VGVertexCell parentVertex : parentVertices) {
                    BodyExtend parentBodyExtend = vertices.get(parentVertex);
                    boolean check = false;
                    Vec2 min = new Vec2(Float.MAX_VALUE, Float.MAX_VALUE), max = new Vec2(Float.MIN_VALUE, Float.MIN_VALUE);
                    for (VGVertexCell childVertex : parentVertex.vgGetVertexChildren()) {
                        BodyExtend childBodyExtend = vertices.get(childVertex);
                        Vec2 tmpMin = childBodyExtend.getBody().getPosition().sub(new Vec2(childBodyExtend.getRadius(), childBodyExtend.getRadius()));
                        Vec2 tmpMax = childBodyExtend.getBody().getPosition().add(new Vec2(childBodyExtend.getRadius(), childBodyExtend.getRadius()));
                        if (tmpMin.x < min.x)
                            min.x = tmpMin.x;
                        if (tmpMin.y < min.y)
                            min.y = tmpMin.y;
                        if (tmpMax.x > max.x)
                            max.x = tmpMax.x;
                        if (tmpMax.y > max.y)
                            max.y = tmpMax.y;
                        check = true;
                    }

                    if (check) {
                        float scale = 1.2f;
                        Vec2 dx = max.sub(min);
                        float radius = Math.max(dx.x, dx.y) / 2.0f * (float)Math.sqrt(2);

                        if ((Math.abs(radius * scale - parentBodyExtend.getRadius())) > 10) {
                            worlds.get(level).destroyBody(parentBodyExtend.getBody());
                            BodyExtend newParentBodyExtend = createDynamicBody(parentBodyExtend.getMatrixId(),
                                    parentBodyExtend.getBody().getPosition(),
                                    radius * scale,
                                    worlds.get(level),
                                    parentBodyExtend);
                            newParentBodyExtend.getBody().setLinearVelocity(new Vec2());
                            vertices.put(parentVertex, newParentBodyExtend);
                        }
                    }
                }
            }
        }

        private void updateGraphView(final VGGraph vgGraph, final Map<VGVertexCell, BodyExtend> vertices) {
            vgGraph.getModel().beginUpdate();
            try {
                vgGraph.bfs(new VGGraph.SearchListener() {
                    @Override
                    public void onVertex(VGVertexCell vgVertexCell) {
                        BodyExtend body = vertices.get(vgVertexCell);
                        int level = VGGraph.calcCellLevel(vgVertexCell);
                        vgVertexCell.getGeometry().setX(body.getBody().getPosition().x -
                                (float) vgVertexCell.getGeometry().getWidth() / 2 -
                                (level > 1 ? (float) (vgVertexCell.getParent()).getGeometry().getX() : 0.0f));
                        vgVertexCell.getGeometry().setY(body.getBody().getPosition().y -
                                (float) vgVertexCell.getGeometry().getHeight() / 2 -
                                (level > 1 ? (float) (vgVertexCell.getParent()).getGeometry().getY() : 0.0f));

                        vgVertexCell.getGeometry().setWidth(2 * body.getRadius() / Math.sqrt(2));
                        vgVertexCell.getGeometry().setHeight(2 * body.getRadius() / Math.sqrt(2));
                    }
                });
            } finally {
                vgGraph.getModel().endUpdate();
                SwingUtils.invokeAndWaitInEDTIfNeeded(new Runnable() {
                    @Override
                    public void run() {
                        vgGraph.getVGMxGraphComponentReference().refresh();
                        vgGraph.getVGMxGraphComponentReference().setGridVisible(true);
                    }
                }, null);
            }
        }

        private static void updateIntersection(Map<Integer, World> worlds, final VGGraph vgGraph, final BiMap<VGVertexCell, BodyExtend> vertices) {
            for (int level = 1; level < MAX_LEVEL; level++) {
                if (!worlds.containsKey(level))
                    break;

                Set<VGEdgeCell> edges = vgGraph.getVGEdgesByLevel(level);

                for (VGEdgeCell edgeCell1 : edges) {
                    for (VGEdgeCell edgeCell2 : edges) {
                        if (edgeCell1 == edgeCell2)
                            continue;

                        BodyExtend source1 = vertices.get(edgeCell1.getVGSoure());
                        BodyExtend target1 = vertices.get(edgeCell1.getVGTarget());
                        BodyExtend source2 = vertices.get(edgeCell2.getVGSoure());
                        BodyExtend target2 = vertices.get(edgeCell2.getVGTarget());

                        Vec2 intersection = calcIntersection(source1.getBody().getPosition(),
                                target1.getBody().getPosition(),
                                source2.getBody().getPosition(),
                                target2.getBody().getPosition());

                        if (intersection != null) {
                            worlds.get(level).destroyBody(source1.getBody());
                            worlds.get(level).destroyBody(source2.getBody());
                            BodyExtend newParentBodyExtend2 = createDynamicBody(
                                    source2.getMatrixId(),
                                    source1.getBody().getPosition(),
                                    source2.getRadius(),
                                    worlds.get(level),
                                    source2);
                            BodyExtend newParentBodyExtend1 = createDynamicBody(
                                    source1.getMatrixId(),
                                    source2.getBody().getPosition(),
                                    source1.getRadius(),
                                    worlds.get(level),
                                    source1);
                            vertices.put((VGVertexCell)edgeCell1.getSource(), newParentBodyExtend1);
                            vertices.put((VGVertexCell)edgeCell2.getSource(), newParentBodyExtend2);
                        }
                    }
                }

            }
        }

        private static List<Body> createBorders(final World world, final VGGraph vgGraph, final BiMap<VGVertexCell, BodyExtend> vertices, final int borderLevel) {
            final List<Body> result = Lists.newArrayList();
            vgGraph.bfs(new VGGraph.SearchListener() {
                @Override
                public void onVertex(VGVertexCell vgVertexCell) {
                    int level = VGGraph.calcCellLevel(vgVertexCell);
                    if (level == borderLevel) {
                        BodyExtend body = vertices.get(vgVertexCell);
                        List<Vec2> points = calcRegularPolygonPoints(body);
                        result.add(createBorderBody(world, points));
                    }
                }
            });
            return result;
        }

        private static void removeBorders(World world, List<Body> borderBodies) {
            for (Body body : borderBodies) {
                world.destroyBody(body);
            }
        }

        private static BodyExtend createDynamicBody(int matrixId, Vec2 center, float radius, World world, BodyExtend oldBodyExtend) {
            //VGMainServiceHelper.logger.printDebug("Create dynamic body, center: " + center.toString() + ", radius: " + radius);

            BodyDef bodyDef = new BodyDef();
            bodyDef.type = BodyType.DYNAMIC;
            bodyDef.position.set(center);
            Body body = world.createBody(bodyDef);

            //CircleShape shape = new CircleShape();
            //shape.setRadius(radius);
            PolygonShape shape = new PolygonShape();
            shape.setAsBox(radius / (float)Math.sqrt(2), radius / (float)Math.sqrt(2));

            FixtureDef fixtureDef = new FixtureDef();
            fixtureDef.shape = shape;
            fixtureDef.density = 1.0f;
            fixtureDef.friction = 0.0f;
            fixtureDef.restitution = 0.0f;
            body.createFixture(fixtureDef);

            MassData massData = new MassData();
            massData.mass = (float)Math.PI * radius * radius * 0.001f;
            if (massData.mass > MAX_m)
                massData.mass = MAX_m;

            body.setMassData(massData);

            BodyExtend bodyExtend = new BodyExtend(matrixId, body, radius, massData.mass, center);
            if (oldBodyExtend != null) {
                bodyExtend.setLastPositions(oldBodyExtend.getLastPositions());
                bodyExtend.getBody().setLinearVelocity(oldBodyExtend.getBody().getLinearVelocity());
            }

            return bodyExtend;
        }

        private static Body createBorderBody(World world, List<Vec2> points) {
            BodyDef bodyDef = new BodyDef();
            Body body = world.createBody(bodyDef);

            for (int i = 0; i < points.size() - 1; i++) {
                EdgeShape edgeShape = new EdgeShape();
                edgeShape.setRadius(10);
                edgeShape.set(points.get(i), points.get(i + 1));
                body.createFixture(edgeShape, 0.0f);
            }

            if (points.size() > 1) {
                EdgeShape edgeShape = new EdgeShape();
                edgeShape.setRadius(10);
                edgeShape.set(points.get(points.size() - 1), points.get(0));
                body.createFixture(edgeShape, 0.0f);
            }

            return body;
        }

        private static List<Vec2> calcRegularPolygonPoints(Vec2 center, float radius, int countVertices,  boolean inner) {
            float angle = (float)(Math.PI * 2.0f / countVertices);
            float defaultAngle = (float)(Math.PI * 2.0f / 8);

            if (inner)
                radius = radius * (float)Math.sqrt(2);

            List<Vec2> result = Lists.newArrayList();
            for (int i = 0; i < countVertices; i++) {
                result.add(new Vec2((float) Math.sin(i * angle + defaultAngle) * radius + center.x, (float) Math.cos(i * angle + defaultAngle) * radius + center.y));
            }

            return result;
        }

        private static List<Vec2> calcRegularPolygonPoints(BodyExtend body) {
            if (body.getBody().getFixtureList().getShape() instanceof PolygonShape) {
                PolygonShape polygonShape = (PolygonShape) body.getBody().getFixtureList().getShape();
                List<Vec2> result = Lists.newArrayList();
                for (int i = 0; i < polygonShape.getVertexCount(); i++) {
                    result.add(polygonShape.getVertex(i).add(body.getBody().getPosition()));
                }
                return result;
            } else {
                return calcRegularPolygonPoints(body.getBody().getPosition(), body.getRadius(), 4, false);
            }
        }

        private static Vec2 calcIntersection(Vec2 start1, Vec2 end1, Vec2 start2, Vec2 end2) {
            Vec2 dir1 = end1.sub(start1);
            Vec2 dir2 = end2.sub(start2);

            float a1 = -dir1.y;
            float b1 = +dir1.x;
            float d1 = -(a1*start1.x + b1*start1.y);

            float a2 = -dir2.y;
            float b2 = +dir2.x;
            float d2 = -(a2*start2.x + b2*start2.y);

            float seg1_line2_start = a2*start1.x + b2*start1.y + d2;
            float seg1_line2_end = a2*end1.x + b2*end1.y + d2;

            float seg2_line1_start = a1*start2.x + b1*start2.y + d1;
            float seg2_line1_end = a1*end2.x + b1*end2.y + d1;

            if (seg1_line2_start * seg1_line2_end >= 0 || seg2_line1_start * seg2_line1_end >= 0)
                return null;

            float u = seg1_line2_start / (seg1_line2_start - seg1_line2_end);
            return start1.add(dir1.mul(u));
        }

//==============================================================================
//------------------STATIC CLASSES----------------------------------------------
        private static class BodyExtend {
            private Body body;
            private float radius;
            private float mass;
            private List<Vec2> lastPositions;
            private int matrixId;

            public BodyExtend(int matrixId, Body body, float radius, float mass, Vec2 lastPosition) {
                this.matrixId = matrixId;
                this.body = body;
                this.radius = radius;
                this.mass = mass;
                this.lastPositions = Lists.newArrayList();
                addLastPosition(lastPosition);
            }

            public float getRadius() {
                return radius;
            }

            public Body getBody() {
                return body;
            }

            public Vec2 getLastPosition() {
                return lastPositions.listIterator().next();
            }

            public void addLastPosition(Vec2 lastPosition) {
                if (lastPositions.size() >= MAX_IDLE_STEPS) {
                    lastPositions.remove(0);
                }
                lastPositions.add(new Vec2(lastPosition));
            }

            public List<Vec2> getLastPositions() {
                return lastPositions;
            }

            public void setLastPositions(List<Vec2> lastPositions) {
                this.lastPositions = Lists.newArrayList(lastPositions);
            }

            public float getMass() {
                return mass;
            }

            public int getMatrixId() {
                return matrixId;
            }

            public boolean isIdle() {
                Vec2 min = new Vec2(Float.MAX_VALUE, Float.MAX_VALUE), max = new Vec2(Float.MIN_VALUE, Float.MIN_VALUE);
                for (Vec2 lastPosition : lastPositions) {
                    if (lastPosition.x < min.x)
                        min.x = lastPosition.x;
                    if (lastPosition.y < min.y)
                        min.y = lastPosition.y;
                    if (lastPosition.x > max.x)
                        max.x = lastPosition.x;
                    if (lastPosition.y > max.y)
                        max.y = lastPosition.y;
                }
                Vec2 dx = max.sub(min);
                return lastPositions.size() == MAX_IDLE_STEPS && dx.length() < MIN_IDLE_DX;
            }
        }
    }
}
