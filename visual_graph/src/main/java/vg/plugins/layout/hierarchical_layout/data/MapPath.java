package vg.plugins.layout.hierarchical_layout.data;

/**
 * @author Timur Zolotuhin (e-mail: tzolotuhin@parallels.com)
 */
public class MapPath {
    private MapPoint start;
    private MapPoint finish;
    private double distance;

    public MapPath(MapPoint start, MapPoint finish, double distance) {
        this.start = start;
        this.finish = finish;
        this.distance = distance;
    }

    public MapPoint getStart() {
        return start;
    }

    public MapPoint getFinish() {
        return finish;
    }

    public double getDistance() {
        return distance;
    }
}
