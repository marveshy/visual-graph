package vg.plugins.layout.hierarchical_layout.data;

import java.util.*;

/**
 * @author Timur Zolotuhin (e-mail: tzolotuhin@parallels.com)
 */
public class DijkstraGraph {
    private final List<DijkstraVertex> vertexes;
    private final List<DijkstraEdge> edges;

    public DijkstraGraph(List<DijkstraVertex> vertexes, List<DijkstraEdge> edges) {
        this.vertexes = vertexes;
        this.edges = edges;
    }

    public List<DijkstraVertex> getVertexes() {
        return vertexes;
    }

    public List<DijkstraEdge> getEdges() {
        return edges;
    }
}