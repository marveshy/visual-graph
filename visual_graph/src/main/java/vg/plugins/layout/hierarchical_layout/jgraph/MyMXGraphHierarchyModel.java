package vg.plugins.layout.hierarchical_layout.jgraph;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.mxgraph.layout.hierarchical.model.mxGraphHierarchyEdge;
import com.mxgraph.layout.hierarchical.model.mxGraphHierarchyModel;
import com.mxgraph.layout.hierarchical.model.mxGraphHierarchyNode;
import com.mxgraph.layout.hierarchical.mxHierarchicalLayout;
import vg.impl.main_service.VGMainServiceHelper;
import vg.interfaces.data_base_service.data.graph.Vertex;
import vg.interfaces.graph_view_service.data.VGEdgeCell;
import vg.interfaces.graph_view_service.data.VGGraph;
import vg.shared.utils.MapUtils;

import java.util.*;

/**
 * @author Timur Zolotuhin (e-mail: tzolotuhin@parallels.com)
 */
public class MyMXGraphHierarchyModel extends mxGraphHierarchyModel {
    protected Map<Vertex, Float> vertexToPriorityMap;
    protected VGGraph originalGraph;

    public MyMXGraphHierarchyModel(mxHierarchicalLayout layout, Object[] vertices, List<Object> roots, Object parent, Map<Vertex, Float> vertexToPriorityMap, VGGraph originalGraph) {
        super(layout, vertices, roots, parent);
        this.vertexToPriorityMap = vertexToPriorityMap;
        this.originalGraph = originalGraph;
    }

    @Override
    public void initialRank() {
        Collection<mxGraphHierarchyNode> internalNodes = vertexMapper.values();
        LinkedList<mxGraphHierarchyNode> startNodes = new LinkedList<>();

        if (roots != null) {

            for (Object root : roots) {
                mxGraphHierarchyNode internalNode = vertexMapper.get(root);

                if (internalNode != null) {
                    startNodes.add(internalNode);
                }
            }
        }

        Iterator<mxGraphHierarchyNode> iter = internalNodes.iterator();

        while (iter.hasNext()) {
            mxGraphHierarchyNode internalNode = iter.next();
            // Mark the node as not having had a layer assigned
            internalNode.temp[0] = -1;
        }

        List<mxGraphHierarchyNode> startNodesCopy = new ArrayList<>(
                startNodes);

        int SOURCESCANSTARTRANK = 100000000;
        while (!startNodes.isEmpty()) {
            mxGraphHierarchyNode internalNode = startNodes.getFirst();
            Collection<mxGraphHierarchyEdge> layerDeterminingEdges;
            Collection<mxGraphHierarchyEdge> edgesToBeMarked;

            layerDeterminingEdges = internalNode.connectsAsTarget;
            edgesToBeMarked = internalNode.connectsAsSource;

            // flag to keep track of whether or not all layer determining
            // edges have been scanned
            boolean allEdgesScanned = true;

            // Work out the layer of this node from the layer determining
            // edges
            Iterator<mxGraphHierarchyEdge> iter2 = layerDeterminingEdges
                    .iterator();

            // The minimum layer number of any node connected by one of
            // the layer determining edges variable. If we are starting
            // from sources, need to start at some huge value and
            // normalise down afterwards
            int minimumLayer = SOURCESCANSTARTRANK;

            while (allEdgesScanned && iter2.hasNext()) {
                mxGraphHierarchyEdge internalEdge = iter2.next();

                if (internalEdge.temp[0] == 5270620) {
                    // This edge has been scanned, get the layer of the
                    // node on the other end
                    mxGraphHierarchyNode otherNode = internalEdge.source;
                    minimumLayer = Math.min(minimumLayer,
                            otherNode.temp[0] - 1);
                } else {
                    allEdgesScanned = false;
                }
            }

            // If all edge have been scanned, assign the layer, mark all
            // edges in the other direction and remove from the nodes list
            if (allEdgesScanned) {
                internalNode.temp[0] = minimumLayer;
                maxRank = Math.min(maxRank, minimumLayer);

                if (edgesToBeMarked != null) {

                    for (mxGraphHierarchyEdge internalEdge : edgesToBeMarked) {
                        // Assign unique stamp ( y/m/d/h )
                        internalEdge.temp[0] = 5270620;

                        // Add node on other end of edge to LinkedList of
                        // nodes to be analysed
                        mxGraphHierarchyNode otherNode = internalEdge.target;

                        // Only add node if it hasn't been assigned a layer
                        if (otherNode.temp[0] == -1) {
                            startNodes.addLast(otherNode);

                            // Mark this other node as neither being
                            // unassigned nor assigned so it isn't
                            // added to this list again, but it's
                            // layer isn't used in any calculation.
                            otherNode.temp[0] = -2;
                        }
                    }
                }

                startNodes.removeFirst();
            } else {
                // Not all the edges have been scanned, get to the back of
                // the class and put the dunces cap on
                Object removedCell = startNodes.removeFirst();
                startNodes.addLast(internalNode);

                if (removedCell == internalNode && startNodes.size() == 1) {
                    // This is an error condition, we can't get out of
                    // this loop. It could happen for more than one node
                    // but that's a lot harder to detect. Log the error
                    // TODO make log comment
                    break;
                }
            }
        }

        // Normalize the ranks down from their large starting value to place
        // at least 1 sink on layer 0
        iter = internalNodes.iterator();
        while (iter.hasNext()) {
            mxGraphHierarchyNode internalNode = iter.next();
            // Mark the node as not having had a layer assigned
            internalNode.temp[0] -= maxRank;
        }

        // Tighten the roots as far as possible
        for (mxGraphHierarchyNode internalNode : startNodesCopy) {
            int currentMaxLayer = 0;

            for (mxGraphHierarchyEdge internalEdge : internalNode.connectsAsSource) {
                mxGraphHierarchyNode otherNode = internalEdge.target;
                internalNode.temp[0] = Math.max(currentMaxLayer,
                        otherNode.temp[0] + 1);
                currentMaxLayer = internalNode.temp[0];
            }
        }

        // Reset the maxRank to that which would be expected for a from-sink
        // scan
        maxRank = SOURCESCANSTARTRANK - maxRank;
    }

    @Override
    public void dfs(mxGraphHierarchyNode parent, mxGraphHierarchyNode root, mxGraphHierarchyEdge connectingEdge, CellVisitor visitor, Set<mxGraphHierarchyNode> seen, int layer) {
        if (root != null) {
            if (!seen.contains(root)) {
                visitor.visit(parent, root, connectingEdge, layer, 0);
                seen.add(root);

                // Copy the connects as source list so that visitors
                // can change the original for edge direction inversions
                final Object[] outgoingEdges = root.connectsAsSource.toArray();

                Map<Object, Float> unsortedEdgesMap = Maps.newHashMap();
                for (Object outgoingEdge : outgoingEdges) {
                    VGEdgeCell e = (VGEdgeCell) ((mxGraphHierarchyEdge) outgoingEdge).edges.iterator().next();
                    unsortedEdgesMap.put(outgoingEdge, vertexToPriorityMap.get(e.getVGTarget().getVGObject()));
                }
                Map<Object, Float> sortedEdgesMap = new MapUtils<Object, Float>().sortMapByValue(unsortedEdgesMap);
                List<Object> sortedEdges = Lists.reverse(Lists.newArrayList(sortedEdgesMap.keySet()));

//                for (Vertex vertex : vertexToPriorityMap.keySet()) {
//                    VGMainServiceHelper.logger.printDebug("DFS Vertex priority: " + originalGraph.getVGVertexCellByVertex(vertex).getHumanId() + ", p: " + vertexToPriorityMap.get(vertex));
//                }

                for (Object outgoingEdge : sortedEdges) {
//                    VGMainServiceHelper.logger.printDebug("DFS edge: " +
//                            originalGraph.getVGEdgeCellByEdge(((VGEdgeCell) ((mxGraphHierarchyEdge)outgoingEdge).edges.iterator().next()).getVGObject()).getHumanId() +
//                            ", p: " + sortedEdgesMap.get(outgoingEdge));

                    mxGraphHierarchyEdge internalEdge = (mxGraphHierarchyEdge) outgoingEdge;
                    mxGraphHierarchyNode targetNode = internalEdge.target;

                    // Root check is O(|roots|)
                    dfs(root, targetNode, internalEdge, visitor, seen, layer + 1);
                }
            } else {
                // Use the int field to indicate this node has been seen
                visitor.visit(parent, root, connectingEdge, layer, 1);
            }
        }
    }
}
