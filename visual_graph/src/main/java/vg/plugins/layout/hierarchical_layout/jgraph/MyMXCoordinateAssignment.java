package vg.plugins.layout.hierarchical_layout.jgraph;

import com.mxgraph.layout.hierarchical.model.*;
import com.mxgraph.layout.hierarchical.mxHierarchicalLayout;
import com.mxgraph.layout.hierarchical.stage.mxCoordinateAssignment;
import com.mxgraph.util.mxRectangle;
import com.mxgraph.view.mxGraph;
import vg.impl.main_service.VGMainServiceHelper;

import javax.swing.*;
import java.awt.*;
import java.util.*;

/**
 * @author Timur Zolotuhin (e-mail: tzolotuhin@parallels.com)
 */
class MyMXCoordinateAssignment extends mxCoordinateAssignment {
    private Point gridCellSize;
    private double initialY;
//    protected double maxX = 0;
//    protected double maxY = 0;

    MyMXCoordinateAssignment(mxHierarchicalLayout layout, double intraCellSpacing, double interRankCellSpacing, int orientation, double initialX, double initialY, double parallelEdgeSpacing, Point gridCellSize) {
        super(layout, intraCellSpacing, interRankCellSpacing, orientation, initialX, parallelEdgeSpacing);
        this.gridCellSize = gridCellSize;
        this.initialY = initialY;
    }

    private int medianXValue(Object[] connectedCells, int rankValue)
    {
        if (connectedCells.length == 0)
        {
            return 0;
        }

        int[] medianValues = new int[connectedCells.length];

        for (int i = 0; i < connectedCells.length; i++)
        {
            medianValues[i] = ((mxGraphAbstractHierarchyCell) connectedCells[i])
                    .getGeneralPurposeVariable(rankValue);
        }

        Arrays.sort(medianValues);

        if (connectedCells.length % 2 == 1)
        {
            // For odd numbers of adjacent vertices return the median
            return medianValues[connectedCells.length / 2];
        }
        else
        {
            int medianPoint = connectedCells.length / 2;
            int leftMedian = medianValues[medianPoint - 1];
            int rightMedian = medianValues[medianPoint];

            return ((leftMedian + rightMedian) / 2);
        }
    }

    @Override
    protected void calculateWidestRank(mxGraph graph, mxGraphHierarchyModel model) {
        // Starting y co-ordinate
        double y = -interRankCellSpacing + initialY;

        // CHANGES IN ORIGINAL CODE
        y = y + gridCellSize.y;
        // END CHANGES IN ORIGINAL CODE

        // Track the widest cell on the last rank since the y
        // difference depends on it
        double lastRankMaxCellHeight = 0.0;
        rankWidths = new double[model.maxRank + 1];
        rankY = new double[model.maxRank + 1];

        for (int rankValue = model.maxRank; rankValue >= 0; rankValue--) {
            // Keep track of the widest cell on this rank
            double maxCellHeight = 0.0;
            mxGraphHierarchyRank rank = model.ranks.get(rankValue);
            double localX = initialX;

            // Store whether or not any of the cells' bounds were unavailable so
            // to only issue the warning once for all cells
            boolean boundsWarning = false;
            Iterator<mxGraphAbstractHierarchyCell> iter = rank.iterator();

            while (iter.hasNext()) {
                mxGraphAbstractHierarchyCell cell = iter.next();

                if (cell.isVertex()) {
                    mxGraphHierarchyNode node = (mxGraphHierarchyNode) cell;
                    mxRectangle bounds = layout.getVertexBounds(node.cell);

                    if (bounds != null) {
                        if (orientation == SwingConstants.NORTH
                                || orientation == SwingConstants.SOUTH) {
                            cell.width = bounds.getWidth();
                            cell.height = bounds.getHeight();
                        } else {
                            cell.width = bounds.getHeight();
                            cell.height = bounds.getWidth();
                        }
                    } else {
                        boundsWarning = true;
                    }

                    maxCellHeight = Math.max(maxCellHeight, cell.height);
                } else if (cell.isEdge()) {
                    mxGraphHierarchyEdge edge = (mxGraphHierarchyEdge) cell;
                    // The width is the number of additional parallel edges
                    // time the parallel edge spacing
                    int numEdges = 1;

                    if (edge.edges != null) {
                        numEdges = edge.edges.size();
                    } else {
                        VGMainServiceHelper.logger.printDebug("edge.edges is null");
                    }

                    cell.width = (numEdges - 1) * parallelEdgeSpacing;
                }

                // Set the initial x-value as being the best result so far
                localX += cell.width / 2.0;
                cell.setX(rankValue, localX);
                cell.setGeneralPurposeVariable(rankValue, (int) localX);
                localX += cell.width / 2.0;
                localX += intraCellSpacing;

                if (localX > widestRankValue) {
                    widestRankValue = localX;
                    widestRank = rankValue;
                }

                rankWidths[rankValue] = localX;
            }

            if (boundsWarning) {
                VGMainServiceHelper.logger.printDebug("At least one cell has no bounds");
            }

            rankY[rankValue] = y;
            double distanceToNextRank = maxCellHeight / 2.0
                    + lastRankMaxCellHeight / 2.0 + interRankCellSpacing;
            lastRankMaxCellHeight = maxCellHeight;

            if (orientation == SwingConstants.NORTH
                    || orientation == SwingConstants.WEST) {
                y += distanceToNextRank;
            } else {
                y -= distanceToNextRank;
            }

            iter = rank.iterator();

            while (iter.hasNext()) {
                mxGraphAbstractHierarchyCell cell = iter.next();
                cell.setY(rankValue, y);
            }
        }
    }

    private void minNode(mxGraphHierarchyModel model)
    {
        // Queue all nodes
        LinkedList<WeightedCellSorter> nodeList = new LinkedList<>();

        // Need to be able to map from cell to cellWrapper
        Map<mxGraphAbstractHierarchyCell, WeightedCellSorter> map = new Hashtable<>();
        mxGraphAbstractHierarchyCell[][] rank = new mxGraphAbstractHierarchyCell[model.maxRank + 1][];

        for (int i = 0; i <= model.maxRank; i++)
        {
            mxGraphHierarchyRank rankSet = model.ranks.get(i);
            rank[i] = rankSet.toArray(new mxGraphAbstractHierarchyCell[rankSet
                    .size()]);

            for (int j = 0; j < rank[i].length; j++)
            {
                // Use the weight to store the rank and visited to store whether
                // or not the cell is in the list
                mxGraphAbstractHierarchyCell cell = rank[i][j];
                WeightedCellSorter cellWrapper = new WeightedCellSorter(cell, i);
                cellWrapper.rankIndex = j;
                cellWrapper.visited = true;
                nodeList.add(cellWrapper);
                map.put(cell, cellWrapper);
            }
        }

        // Set a limit of the maximum number of times we will access the queue
        // in case a loop appears
        int maxTries = nodeList.size() * 10;
        int count = 0;

        // Don't move cell within this value of their median
        int tolerance = 1;

        while (!nodeList.isEmpty() && count <= maxTries)
        {
            WeightedCellSorter cellWrapper = nodeList.getFirst();
            mxGraphAbstractHierarchyCell cell = cellWrapper.cell;

            int rankValue = cellWrapper.weightedValue;
            int rankIndex = cellWrapper.rankIndex;

            Object[] nextLayerConnectedCells = cell.getNextLayerConnectedCells(
                    rankValue).toArray();
            Object[] previousLayerConnectedCells = cell
                    .getPreviousLayerConnectedCells(rankValue).toArray();

            int numNextLayerConnected = nextLayerConnectedCells.length;
            int numPreviousLayerConnected = previousLayerConnectedCells.length;

            int medianNextLevel = medianXValue(nextLayerConnectedCells,
                    rankValue + 1);
            int medianPreviousLevel = medianXValue(previousLayerConnectedCells,
                    rankValue - 1);

            int numConnectedNeighbours = numNextLayerConnected
                    + numPreviousLayerConnected;
            int currentPosition = cell.getGeneralPurposeVariable(rankValue);
            double cellMedian = currentPosition;

            if (numConnectedNeighbours > 0)
            {
                cellMedian = (medianNextLevel * numNextLayerConnected + medianPreviousLevel
                        * numPreviousLayerConnected)
                        / numConnectedNeighbours;
            }

            // Flag storing whether or not position has changed
            boolean positionChanged = false;

            if (cellMedian < currentPosition - tolerance)
            {
                if (rankIndex == 0)
                {
                    cell.setGeneralPurposeVariable(rankValue, (int) cellMedian);
                    positionChanged = true;
                }
                else
                {
                    mxGraphAbstractHierarchyCell leftCell = rank[rankValue][rankIndex - 1];
                    int leftLimit = leftCell
                            .getGeneralPurposeVariable(rankValue);
                    leftLimit = leftLimit + (int) leftCell.width / 2
                            + (int) intraCellSpacing + (int) cell.width / 2;

                    if (leftLimit < cellMedian)
                    {
                        cell.setGeneralPurposeVariable(rankValue,
                                (int) cellMedian);
                        positionChanged = true;
                    }
                    else if (leftLimit < cell
                            .getGeneralPurposeVariable(rankValue) - tolerance)
                    {
                        cell.setGeneralPurposeVariable(rankValue, leftLimit);
                        positionChanged = true;
                    }
                }
            }
            else if (cellMedian > currentPosition + tolerance)
            {
                int rankSize = rank[rankValue].length;

                if (rankIndex == rankSize - 1)
                {
                    cell.setGeneralPurposeVariable(rankValue, (int) cellMedian);
                    positionChanged = true;
                }
                else
                {
                    mxGraphAbstractHierarchyCell rightCell = rank[rankValue][rankIndex + 1];
                    int rightLimit = rightCell
                            .getGeneralPurposeVariable(rankValue);
                    rightLimit = rightLimit - (int) rightCell.width / 2
                            - (int) intraCellSpacing - (int) cell.width / 2;

                    if (rightLimit > cellMedian)
                    {
                        cell.setGeneralPurposeVariable(rankValue,
                                (int) cellMedian);
                        positionChanged = true;
                    }
                    else if (rightLimit > cell
                            .getGeneralPurposeVariable(rankValue) + tolerance)
                    {
                        cell.setGeneralPurposeVariable(rankValue, rightLimit);
                        positionChanged = true;
                    }
                }
            }

            if (positionChanged)
            {
                // Add connected nodes to map and list
                for (Object nextLayerConnectedCell : nextLayerConnectedCells) {
                    mxGraphAbstractHierarchyCell connectedCell = (mxGraphAbstractHierarchyCell) nextLayerConnectedCell;
                    WeightedCellSorter connectedCellWrapper = map
                            .get(connectedCell);

                    if (connectedCellWrapper != null) {
                        if (!connectedCellWrapper.visited) {
                            connectedCellWrapper.visited = true;
                            nodeList.add(connectedCellWrapper);
                        }
                    }
                }

                // Add connected nodes to map and list
                for (Object previousLayerConnectedCell : previousLayerConnectedCells) {
                    mxGraphAbstractHierarchyCell connectedCell = (mxGraphAbstractHierarchyCell) previousLayerConnectedCell;
                    WeightedCellSorter connectedCellWrapper = map
                            .get(connectedCell);

                    if (connectedCellWrapper != null) {
                        if (!connectedCellWrapper.visited) {
                            connectedCellWrapper.visited = true;
                            nodeList.add(connectedCellWrapper);
                        }
                    }
                }
            }

            nodeList.removeFirst();
            cellWrapper.visited = false;
            count++;
        }
    }

    private void initialCoords(mxGraph facade, mxGraphHierarchyModel model) {
        calculateWidestRank(facade, model);

        // Sweep up and down from the widest rank
        for (int i = widestRank; i >= 0; i--) {
            if (i < model.maxRank) {
                rankCoordinates(i, facade, model);
            }
        }

        for (int i = widestRank + 1; i <= model.maxRank; i++) {
            if (i > 0) {
                rankCoordinates(i, facade, model);
            }
        }
    }

    private void medianPos(int i, mxGraphHierarchyModel model) {
        // Reverse sweep direction each time through this method
        boolean downwardSweep = (i % 2 == 0);

        if (downwardSweep) {
            for (int j = model.maxRank; j > 0; j--) {
                rankMedianPosition(j - 1, model, j);
            }
        } else {
            for (int j = 0; j < model.maxRank - 1; j++) {
                rankMedianPosition(j + 1, model, j);
            }
        }
    }

    @Override
    public void execute(Object parent) {
        mxGraphHierarchyModel model = layout.getModel();
        currentXDelta = 0.0;

        initialCoords(layout.getGraph(), model);

        if (fineTuning) {
            minNode(model);
        }

        double bestXDelta = 100000000.0;

        if (fineTuning) {
            for (int i = 0; i < maxIterations; i++) {
                // Median Heuristic
                if (i != 0) {
                    medianPos(i, model);
                    minNode(model);
                }

                // if the total offset is less for the current positioning,
                // there are less heavily angled edges and so the current
                // positioning is used
                if (currentXDelta < bestXDelta) {
                    for (int j = 0; j < model.ranks.size(); j++) {
                        mxGraphHierarchyRank rank = model.ranks.get(j);

                        for (mxGraphAbstractHierarchyCell cell : rank) {
                            // CHANGES IN ORIGINAL CODE
                            double x = cell.getGeneralPurposeVariable(j) - cell.width / 2;
                            if (((int)(x + 0.5)) % gridCellSize.x != 0) {
                                x = (((int)(x + 0.5)) / gridCellSize.x) * gridCellSize.x + gridCellSize.x;
                                cell.setGeneralPurposeVariable(j, (int)(x + cell.width / 2 + 0.5));
                            }
                            cell.setGeneralPurposeVariable(j, cell.getGeneralPurposeVariable(j) + gridCellSize.x);
//                            if (cell.y[0] + cell.height / 2 > maxY)
//                                maxY = cell.y[0] + cell.height / 2;
//                            if (cell.x[0] + cell.width / 2 > maxX)
//                                maxX = cell.x[0] + cell.width / 2;
                            // END CHANGES IN ORIGINAL CODE
                            cell.setX(j, cell.getGeneralPurposeVariable(j));
                        }
                    }

                    bestXDelta = currentXDelta;
                } else {
                    // Restore the best positions
                    for (int j = 0; j < model.ranks.size(); j++) {
                        mxGraphHierarchyRank rank = model.ranks.get(j);

                        for (mxGraphAbstractHierarchyCell cell : rank) {
                            cell.setGeneralPurposeVariable(j, (int) cell.getX(j));
                        }
                    }
                }

                minPath(model);

                currentXDelta = 0;
            }
        }

        setCellLocations(layout.getGraph(), model);
    }
}
