package vg.plugins.layout.hierarchical_layout.data;

import org.apache.commons.lang.Validate;

/**
 * @author Timur Zolotuhin (e-mail: tzolotuhin@parallels.com)
 */
public class MapPoint {
    private final int x, y;
    private Object value;
    private double cost;

    private final int hashCode;

    public MapPoint(int x, int y) {
        this(x, y, Integer.MAX_VALUE, null);
    }

    public MapPoint(int x, int y, int cost, Object value) {
        this.x = x;
        this.y = y;
        this.hashCode = 31 * x + y;
        this.value = value;
        setCost(cost);
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        Validate.isTrue(cost > 0);
        this.cost = cost;
    }

    @Override
    public String toString() {
        return "(" + x + "," + y + ")";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MapPoint mapPoint = (MapPoint) o;

        return x == mapPoint.x && y == mapPoint.y;
    }

    @Override
    public int hashCode() {
        return hashCode;
    }
}
