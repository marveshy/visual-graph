package vg.plugins.layout.hierarchical_layout.jgraph;

import vg.interfaces.data_base_service.data.graph.Vertex;
import vg.interfaces.graph_view_service.data.VGVertexCell;

import java.util.List;
import java.util.Map;

/**
 * @author Timur Zolotuhin (e-mail: tzolotuhin@parallels.com)
 */
public class MyMXHierarchicalLayoutUtils {
    public static void initVertexToPriority(Map<Vertex, Float> vertexToPriorityMap, List<VGVertexCell> vertices) {
        int p = 1;
        for (VGVertexCell vgVertexCell : vertices) {
            vertexToPriorityMap.put(vgVertexCell.getVGObject(), (float)(p++));
        }
    }
}
