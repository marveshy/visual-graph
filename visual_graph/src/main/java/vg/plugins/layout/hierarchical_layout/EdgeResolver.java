package vg.plugins.layout.hierarchical_layout;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.mxgraph.util.mxPoint;
import vg.impl.main_service.VGMainServiceHelper;
import vg.interfaces.graph_view_service.data.VGEdgeCell;
import vg.interfaces.graph_view_service.data.VGGraph;
import vg.interfaces.graph_view_service.data.VGVertexCell;
import vg.plugins.layout.hierarchical_layout.data.*;
import vg.shared.graph.utils.GraphUtils;
import vg.shared.utils.MapUtils;

import java.awt.*;
import java.awt.geom.Point2D;
import java.util.*;
import java.util.List;

/**
 * @author Timur Zolotuhin (e-mail: tzolotuhin@parallels.com)
 */
class EdgeResolver {
    static List<Map.Entry<VGEdgeCell, VGEdgeCell>> resolveEdgeIntersection(VGGraph graph, VGGraph originalGraph, Map<VGGraph, Object[][]> tmpGraphToMap, Point cellSize, boolean ignoreEdgeIntersection) {
        Point2D graphSize = graph.getGraphSize();
        Set<VGEdgeCell> edges = graph.getVGEdgesByLevel(-1);
        List<VGVertexCell> vertices = graph.getVGVerticesByLevel(-1);

        int generalMapSizeX = (int) (graphSize.getX() / cellSize.x);
        int generalMapSizeY = (int) (graphSize.getY() / cellSize.y);
        boolean[][] generalMap = new boolean[generalMapSizeX][generalMapSizeY];
        Object[][] generalMapEdges = new Object[generalMapSizeX][generalMapSizeY];
        MapPoint[][] map = new MapPoint[generalMapSizeX][generalMapSizeY];

        for (int x = 0; x < generalMapSizeX; x++) {
            for (int y = 0; y < generalMapSizeY; y++) {
                generalMapEdges[x][y] = Lists.<VGEdgeCell>newArrayList();
                map[x][y] = new MapPoint(x, y, 1, null);
            }
        }

        // initialize map: setup vertices
        for (VGVertexCell vgVertexCell : vertices) {
            int x1 = (int) (vgVertexCell.getGeometry().getX() / cellSize.x);
            int y1 = (int) (vgVertexCell.getGeometry().getY() / cellSize.y);
            int x2 = (int) ((vgVertexCell.getGeometry().getX() + vgVertexCell.getGeometry().getWidth() - 0.5) / cellSize.x);
            int y2 = (int) ((vgVertexCell.getGeometry().getY() + vgVertexCell.getGeometry().getHeight() - 0.5) / cellSize.y);
            for (int x = x1; x <= x2; x++) {
                for (int y = y1; y <= y2; y++) {
                    map[x][y].setValue(vgVertexCell);
                    map[x][y].setCost(Integer.MAX_VALUE);
                }
            }
        }

        // sort edges
        edges = doSortAndColorizeEdges(edges, graph, originalGraph, cellSize);

        // handle edges
        for (VGEdgeCell vgEdgeCell : edges) {
            // initialize start and finish points
            MapPath mapPath = doFindShortPath(graph, originalGraph, vgEdgeCell, cellSize);

            DijkstraGraph dijkstraGraph = doConvertMapToDijkstraGraph(map, vgEdgeCell, cellSize);
            DijkstraAlgorithm dijkstraAlgorithm = new DijkstraAlgorithm(dijkstraGraph);

            dijkstraAlgorithm.execute(mapPath.getFinish());

            List<MapPoint> shortPath = dijkstraAlgorithm.getPath(mapPath.getStart());

            // update map
            doUpdateMap(map, shortPath);

            // log map
            //logMap(generalMapSizeX, generalMapSizeY, map, vgEdgeCell, mapPath.getStart(), mapPath.getFinish());

            // set points to the edge
            vgEdgeCell.getGeometry().setPoints(doHandleMapPath(cellSize, generalMap, generalMapEdges, vgEdgeCell, shortPath));
        }

        // add graph->map
        tmpGraphToMap.put(graph, map);

        // count intersections
        List<Map.Entry<VGEdgeCell, VGEdgeCell>> intersections = Lists.newArrayList();
        for (int x = 0; x < generalMapSizeX; x++) {
            for (int y = 0; y < generalMapSizeY; y++) {
                List<Map.Entry<VGEdgeCell, VGEdgeCell>> allLocalIntersections = countLocalIntersections((List<VGEdgeCell>)generalMapEdges[x][y]);
                List<Map.Entry<VGEdgeCell, VGEdgeCell>> newLocalIntersections = Lists.newArrayList(allLocalIntersections);

                // check same intersections
                for (Map.Entry<VGEdgeCell, VGEdgeCell> intersection : intersections) {
                    boolean check = false;
                    for (Map.Entry<VGEdgeCell, VGEdgeCell> localIntersection : allLocalIntersections) {
                        if ((intersection.getKey() == localIntersection.getKey() && intersection.getValue() == localIntersection.getValue()) ||
                                (intersection.getKey() == localIntersection.getValue() && intersection.getValue() == localIntersection.getKey())) {
                            check = true;
                            break;
                        }
                    }
                    if (check) {
                        newLocalIntersections.remove(intersection);
                    }
                }

                intersections.addAll(newLocalIntersections);
            }
        }

        return intersections;
    }

//==============================================================================
//------------------PRIVATE METHODS---------------------------------------------
    private static DijkstraGraph doConvertMapToDijkstraGraph(MapPoint[][] map, VGEdgeCell vgEdgeCell, Point cellSize) {
        // initialize vertices
        List<DijkstraEdge> edges = Lists.newArrayList();
        List<DijkstraVertex> vertices = Lists.newArrayList();
        DijkstraVertex[][] tmpMap = new DijkstraVertex[map.length][map[0].length];
        for (int x = 0; x < map.length; x++) {
            for (int y = 0; y < map[x].length; y++) {
                DijkstraVertex vertex = new DijkstraVertex(map[x][y], vertices.size());
                vertices.add(vertex);
                tmpMap[x][y] = vertex;
            }
        }

        // calculate top left and bottom right corner for optimization
        Point topLeftCorner = new Point(0, 0);
        Point bottomRightCorner = new Point(map.length, map[0].length);

        topLeftCorner.x = (int)Math.round(Math.min(vgEdgeCell.getVGSoure().getGeometry().getX(), vgEdgeCell.getVGTarget().getGeometry().getX()));
        topLeftCorner.y = (int)Math.round(Math.min(vgEdgeCell.getVGSoure().getGeometry().getY(), vgEdgeCell.getVGTarget().getGeometry().getY()));

        bottomRightCorner.x = (int)Math.round(Math.max(vgEdgeCell.getVGSoure().getGeometry().getX() + vgEdgeCell.getVGSoure().getGeometry().getWidth(), vgEdgeCell.getVGTarget().getGeometry().getX() + vgEdgeCell.getVGTarget().getGeometry().getWidth()));
        bottomRightCorner.y = (int)Math.round(Math.max(vgEdgeCell.getVGSoure().getGeometry().getY() + vgEdgeCell.getVGSoure().getGeometry().getHeight(), vgEdgeCell.getVGTarget().getGeometry().getY() + vgEdgeCell.getVGTarget().getGeometry().getHeight()));

        topLeftCorner.x = topLeftCorner.x / cellSize.x - 2;
        topLeftCorner.y = topLeftCorner.y / cellSize.y - 2;

        bottomRightCorner.x = bottomRightCorner.x / cellSize.x + 2;
        bottomRightCorner.y = bottomRightCorner.y / cellSize.y + 2;

        // initialize edges
        for (int x = 0; x < map.length; x++) {
            for (int y = 0; y < map[x].length; y++) {
                if (map[x][y].getValue() != null)
                    continue;

                if (x > 0 && x > topLeftCorner.x)
                    edges.add(new DijkstraEdge(tmpMap[x][y].getIndex(), tmpMap[x - 1][y].getIndex(), map[x - 1][y].getCost()));
                if (x < map.length - 1 && x < bottomRightCorner.x)
                    edges.add(new DijkstraEdge(tmpMap[x][y].getIndex(), tmpMap[x + 1][y].getIndex(), map[x + 1][y].getCost()));
                if (y > 0 && y > topLeftCorner.y)
                    edges.add(new DijkstraEdge(tmpMap[x][y].getIndex(), tmpMap[x][y - 1].getIndex(), map[x][y - 1].getCost()));
                if (y < map[x].length - 1 && y < bottomRightCorner.y)
                    edges.add(new DijkstraEdge(tmpMap[x][y].getIndex(), tmpMap[x][y + 1].getIndex(), map[x][y + 1].getCost()));
            }
        }

        return new DijkstraGraph(vertices, edges);
    }

    private static List<mxPoint> doHandleMapPath(Point cellSize, boolean[][] generalMap, Object[][] generalMapEdges, VGEdgeCell vgEdgeCell, List<MapPoint> mapPoints) {
        List<mxPoint> points = Lists.newArrayList();
        for (MapPoint mapPoint : mapPoints) {
            if (mapPoints.indexOf(mapPoint) == 0) {
                points.add(new mxPoint(mapPoint.getX() * cellSize.x + cellSize.x / 2, mapPoint.getY() * cellSize.y));
            }

            points.add(new mxPoint(mapPoint.getX() * cellSize.x + cellSize.x / 2, mapPoint.getY() * cellSize.y + cellSize.y / 2));

            if (mapPoints.indexOf(mapPoint) == mapPoints.size() - 1) {
                points.add(new mxPoint(mapPoint.getX() * cellSize.x + cellSize.x / 2, mapPoint.getY() * cellSize.y + cellSize.y));
            }

            generalMap[mapPoint.getX()][mapPoint.getY()] = true;
            ((List<VGEdgeCell>)generalMapEdges[mapPoint.getX()][mapPoint.getY()]).add(vgEdgeCell);
        }

        return points;
    }

    private static MapPath doFindShortPath(VGGraph graph, VGGraph originalGraph, VGEdgeCell vgEdgeCell, Point cellSize) {
        List<MapPoint> startMapPoints = Lists.newArrayList(), finishMapPoints = Lists.newArrayList();
        //int xStartCenter = -1;
        int xFinishCenter = -1;

        // initialize finish points
        VGEdgeCell originalVGEdgeCell = originalGraph.getVGEdgeCellByEdge(vgEdgeCell.getVGObject());
        if (GraphUtils.isPort(originalVGEdgeCell.getVGSoure().getVGObject()) && graph.getVGVertexCellByVertex(originalVGEdgeCell.getVGSoure().getVGObject()) == null) {
            MapPoint tmpMapPoint = doCalcPortPoint(originalVGEdgeCell.getVGSoure(), vgEdgeCell.getVGSoure(), cellSize, true);
            finishMapPoints.add(new MapPoint(tmpMapPoint.getX(), tmpMapPoint.getY() + 1));
        } else {
            int x1 = (int)Math.round(vgEdgeCell.getVGSoure().getGeometry().getX()) / cellSize.x;
            int x2 = (int)Math.round(vgEdgeCell.getVGSoure().getGeometry().getX() + vgEdgeCell.getVGSoure().getGeometry().getWidth()) / cellSize.x;
            int y = (int)Math.round(vgEdgeCell.getVGSoure().getGeometry().getY() + vgEdgeCell.getVGSoure().getGeometry().getHeight()) / cellSize.y;
            xFinishCenter = x1 + (x2 - x1) / 2;
            for (int x = x1; x < x2; x++) {
                finishMapPoints.add(new MapPoint(x, y));
            }
        }

        // initialize start points
        if (GraphUtils.isPort(originalVGEdgeCell.getVGTarget().getVGObject()) && graph.getVGVertexCellByVertex(originalVGEdgeCell.getVGTarget().getVGObject()) == null) {
            MapPoint tmpMapPoint = doCalcPortPoint(originalVGEdgeCell.getVGTarget(), vgEdgeCell.getVGTarget(), cellSize, false);
            startMapPoints.add(new MapPoint(tmpMapPoint.getX(), tmpMapPoint.getY() - 1));
        } else {
            int x1 = (int)Math.round(vgEdgeCell.getVGTarget().getGeometry().getX()) / cellSize.x;
            int x2 = (int)Math.round(vgEdgeCell.getVGTarget().getGeometry().getX() + vgEdgeCell.getVGTarget().getGeometry().getWidth()) / cellSize.x;
            int y = (int)Math.round(vgEdgeCell.getVGTarget().getGeometry().getY()) / cellSize.y;
            //xStartCenter = x1 + (x2 - x1) / 2;
            for (int x = x1; x < x2; x++) {
                startMapPoints.add(new MapPoint(x, y - 1));
            }
        }

        // check back edge
//        if (startMapPoint.getY() < finishMapPoint.getY()) {
//            if (GraphUtils.isPort(originalVGEdgeCell.getVGSoure().getVGObject()) && graph.getVGVertexCellByVertex(originalVGEdgeCell.getVGSoure().getVGObject()) == null) {
//                finishMapPoint = doCalcPortPoint(originalVGEdgeCell.getVGSoure(), vgEdgeCell.getVGSoure(), cellSize, true);
//                finishMapPoint = new MapPoint(finishMapPoint.getX() + 1, finishMapPoint.getY(), Integer.MAX_VALUE);
//            } else {
//                finishMapPoint = new MapPoint(
//                        ((int)(vgEdgeCell.getVGSoure().getGeometry().getX() + vgEdgeCell.getVGSoure().getGeometry().getWidth() + 0.5)) / cellSize.x,
//                        ((int)(vgEdgeCell.getVGSoure().getGeometry().getCenterY()  + 0.5)) / cellSize.y,
//                        Integer.MAX_VALUE);
//            }
//            currentDirection = GridBagConstraints.EAST;
//        }

        // find minimum distance
        double minDistance = Float.MAX_VALUE;
        MapPoint startMapPoint = startMapPoints.get(0);
        MapPoint finishMapPoint = finishMapPoints.get(0);
        for (MapPoint tmpStartMapPoint : startMapPoints) {
            for (MapPoint tmpFinishMapPoint : finishMapPoints) {
                double tmpDistance = Math.sqrt(Math.pow(tmpStartMapPoint.getX() - tmpFinishMapPoint.getX(), 2) + Math.pow(tmpStartMapPoint.getY() - tmpFinishMapPoint.getY(), 2));

                if (tmpDistance < minDistance ||
                        (Math.abs(tmpDistance - minDistance) < 0.000001 &&
                                (xFinishCenter == tmpFinishMapPoint.getX() || (finishMapPoint.getX() != xFinishCenter && tmpFinishMapPoint.getX() > xFinishCenter)))) {
                    minDistance = tmpDistance;
                    startMapPoint = tmpStartMapPoint;
                    finishMapPoint = tmpFinishMapPoint;
                }
            }
        }

        return new MapPath(startMapPoint, finishMapPoint, minDistance);
    }

    private static void logMap(int generalMapSizeX, int generalMapSizeY, MapPoint[][] map, VGEdgeCell vgEdgeCell, MapPoint startMapPoint, MapPoint finishMapPoint) {
        String log = "Edge: " + vgEdgeCell.getHumanId() + " start xy=" + startMapPoint.getX() + ":" + startMapPoint.getY() +
                ", finish xy=" + finishMapPoint.getX() + ":" + finishMapPoint.getY() + "\n";
        for (int y = 0; y < generalMapSizeY; y++) {
            // draw grid x:y
            if (y == 0) {
                for (int x = -1; x < generalMapSizeX; x++) {
                    log += String.format("%3d", x) + "|";
                }
                log += "\n" + String.format("%3d", 0) + "|";
            } else {
                log += String.format("%3d", y) + "|";
            }

            // draw values
            for (int x = 0; x < generalMapSizeX; x++) {
                if (map[x][y].getValue() == null)
                    log += "   |";
                    //log += String.format("%2d", ((Integer) map[x][y].getValue())) + "|";
                else
                    log += "###|";
            }
            log += "\n";
        }
        VGMainServiceHelper.logger.printDebug(log);
    }

    private static Set<VGEdgeCell> doSortAndColorizeEdges(Set<VGEdgeCell> edges, VGGraph graph, VGGraph originalGraph, Point cellSize) {
        // sort edges
        Map<VGEdgeCell, Double> unsortedEdges = Maps.newLinkedHashMap();
        double minDistance = Double.MAX_VALUE, maxDistance = Double.MIN_VALUE;
        for (VGEdgeCell edge : edges) {
            MapPath shortPath = doFindShortPath(graph, originalGraph, edge, cellSize);
            unsortedEdges.put(edge, shortPath.getDistance());
            if (minDistance > shortPath.getDistance()) {
                minDistance = shortPath.getDistance();
            }
            if (maxDistance < shortPath.getDistance()) {
                maxDistance = shortPath.getDistance();
            }
        }
        
        // colorize edges
        for (VGEdgeCell edge : edges) {
            double normDistance = (unsortedEdges.get(edge) - minDistance) / (maxDistance - minDistance);
            edge.vgSetColor(new Color(0, (int)((1 - normDistance) * 200), (int)(normDistance * 255)));
        }
        
        MapUtils<VGEdgeCell, Double> mapUtils = new MapUtils<>();
        return mapUtils.sortMapByValue(unsortedEdges).keySet();
    }

    private static void doUpdateMap(MapPoint[][] map, List<MapPoint> path) {
        for (MapPoint mapPoint : path) {
            double currCost = map[mapPoint.getX()][mapPoint.getY()].getCost();
            map[mapPoint.getX()][mapPoint.getY()].setCost(currCost + 25);
        }
    }

    private static MapPoint doCalcPortPoint(VGVertexCell port, VGVertexCell parentForPort, Point cellSize, boolean isSource) {
        int xParent = ((int)(parentForPort.getGeometry().getX() + 0.5)) / cellSize.x;
        int yParent = ((int)(parentForPort.getGeometry().getY() + 0.5)) / cellSize.y;
        int xPort = ((int)(port.getGeometry().getX() + port.getGeometry().getWidth() / 2 + 0.5)) / cellSize.x;
        int yPort = ((int)(port.getGeometry().getY() + 0.5)) / cellSize.y;
        if (isSource) {
            xPort = ((int)(port.getGeometry().getX() + port.getGeometry().getWidth() / 2 + 0.5)) / cellSize.x;
            yPort = ((int)(port.getGeometry().getY() + port.getGeometry().getHeight() + 0.5)) / cellSize.y - 1;
        }
        return new MapPoint(xParent + xPort, yParent + yPort);
    }

    private static List<Map.Entry<VGEdgeCell, VGEdgeCell>> countLocalIntersections(List<VGEdgeCell> vgEdgeCells) {
        List<Map.Entry<VGEdgeCell, VGEdgeCell>> result = Lists.newArrayList();
        for (int i = 0; i < vgEdgeCells.size(); i++) {
            for (int j = i + 1; j < vgEdgeCells.size(); j++) {
                VGEdgeCell vgEdgeCell1 = vgEdgeCells.get(i);
                VGEdgeCell vgEdgeCell2 = vgEdgeCells.get(j);
                result.add(new AbstractMap.SimpleEntry<>(vgEdgeCell1, vgEdgeCell2));
            }
        }
        return result;
    }
}
