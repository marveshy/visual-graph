package vg.plugins.layout.hierarchical_layout;

import com.google.common.collect.Lists;
import org.apache.commons.lang.Validate;
import vg.impl.main_service.VGMainServiceHelper;
import vg.plugins.layout.hierarchical_layout.data.DijkstraEdge;
import vg.plugins.layout.hierarchical_layout.data.DijkstraGraph;
import vg.plugins.layout.hierarchical_layout.data.DijkstraVertex;
import vg.plugins.layout.hierarchical_layout.data.MapPoint;

import java.util.*;

/**
 * @author Timur Zolotuhin (e-mail: tzolotuhin@parallels.com)
 */
class DijkstraAlgorithm {
    // Main data
    private final List<DijkstraVertex> nodes;
    private final List<DijkstraEdge> edges;
    private boolean[] settledNodes;

    private boolean[] unSettledNodes;
    private int unSettledNodesCounter;

    private int[] predecessors;

    private double[] distance;

    DijkstraAlgorithm(DijkstraGraph graph) {
        // create a copy of the array so that we can operate on this array
        this.nodes = Lists.newArrayList(graph.getVertexes());
        this.edges = Lists.newArrayList(graph.getEdges());
    }

    public void execute(MapPoint source) {
        VGMainServiceHelper.logger.printDebug("Dijkstra start");

        // prepare input parameters
        int sourceIndex = -1;
        for (DijkstraVertex vertex : nodes) {
            if (vertex.getMapPoint().equals(source)) {
                sourceIndex = vertex.getIndex();
            }
        }
        Validate.isTrue(sourceIndex >= 0, "Source index should be more or equals 0");

        // initialize settled nodes
        settledNodes = new boolean[nodes.size()];

        // initialize unsettled nodes
        unSettledNodes = new boolean[nodes.size()];
        unSettledNodes[sourceIndex] = true;
        unSettledNodesCounter = 1;

        // initialize distance array
        distance = new double[nodes.size()];
        for (int i = 0; i < nodes.size(); i++) {
            distance[i] = Double.MAX_VALUE;
        }
        distance[sourceIndex] = 0;

        // initialize predecessors
        predecessors = new int[nodes.size()];
        for (int i = 0; i < nodes.size(); i++) {
            predecessors[i] = -1;
        }

        int counter = 0;

        VGMainServiceHelper.logger.printDebug("Start iterations in Dijkstra's algorithm");

        long findMinimumNodeTimer = 0;
        long findDistancesTimer = 0;
        long delta = 0;

        while (unSettledNodesCounter > 0) {
            // get minimum node
            delta = System.currentTimeMillis();

            int minimumNodeIndex = -1;
            for (int i = 0; i < unSettledNodes.length; i++) {
                if (unSettledNodes[i]) {
                    minimumNodeIndex = i;
                }
            }
            Validate.isTrue(minimumNodeIndex >= 0, "minimumNodeIndex index should be more or equals 0");

            for (int i = 0; i < unSettledNodes.length; i++) {
                if (unSettledNodes[i] && distance[i] < distance[minimumNodeIndex]) {
                    minimumNodeIndex = i;
                }
            }

            findMinimumNodeTimer += System.currentTimeMillis() - delta;

            // find minimum distances
            delta = System.currentTimeMillis();

            settledNodes[minimumNodeIndex] = true;
            unSettledNodes[minimumNodeIndex] = false;
            unSettledNodesCounter--;
            findMinimalDistances(minimumNodeIndex);

            findDistancesTimer += System.currentTimeMillis() - delta;

            counter++;
            if (counter % 100 == 0) {
                VGMainServiceHelper.logger.printDebug("Find next minimum node: " + findMinimumNodeTimer + " ms");
                VGMainServiceHelper.logger.printDebug("Find next minimum distances: " + findDistancesTimer + " ms");
            }
        }

        VGMainServiceHelper.logger.printDebug("Find next minimum node: " + findMinimumNodeTimer + " ms");
        VGMainServiceHelper.logger.printDebug("Find next minimum distances: " + findDistancesTimer + " ms");
        VGMainServiceHelper.logger.printDebug("Finish iterations in Dijkstra's algorithm, count of iterations: " + counter + ", vertices: " + nodes.size() + ", edges: " + edges.size());
        VGMainServiceHelper.logger.printDebug("Dijkstra finish");
    }

    /*
     * This method returns the path from the source to the selected target and
     * Empty list if no path exists
     */
    public LinkedList<MapPoint> getPath(MapPoint target) {
        // prepare input parameters
        int targetIndex = -1;
        for (DijkstraVertex vertex : nodes) {
            if (vertex.getMapPoint().equals(target)) {
                targetIndex = vertex.getIndex();
            }
        }
        Validate.isTrue(targetIndex >= 0, "targetIndex index should be more or equals 0");

        // initialize path variable
        LinkedList<MapPoint> path = Lists.newLinkedList();
        int step = targetIndex;

        // check if a path exists
        if (predecessors[step] == -1) {
            return Lists.newLinkedList();
        }

        // build result path
        path.add(nodes.get(step).getMapPoint());
        while (predecessors[step] != -1) {
            step = predecessors[step];
            path.add(nodes.get(step).getMapPoint());
        }

        // put it into the correct order
        Collections.reverse(path);

        return path;
    }

    private void findMinimalDistances(int nodeIndex) {
        for (DijkstraEdge edge : edges) {
            if (edge.getSource() == nodeIndex && !settledNodes[edge.getDestination()]) {
                double shortestDistance = distance[nodeIndex];
                double tmpDistance = getDistance(nodeIndex, edge.getDestination());
                if (distance[edge.getDestination()] > shortestDistance + tmpDistance) {
                    distance[edge.getDestination()] = shortestDistance + tmpDistance;
                    predecessors[edge.getDestination()] = nodeIndex;
                    unSettledNodesCounter++;
                    unSettledNodes[edge.getDestination()] = true;
                }
            }
        }
    }

    private double getDistance(int nodeIndex, int targetIndex) {
        for (DijkstraEdge edge : edges) {
            if (edge.getSource() == nodeIndex && edge.getDestination() == targetIndex) {
                return edge.getWeight();
            }
        }
        throw new RuntimeException("Should not happen");
    }
}
