package vg.plugins.layout.hierarchical_layout;

import com.google.common.collect.Maps;
import vg.impl.main_service.VGMainServiceHelper;
import vg.interfaces.data_base_service.data.graph.Vertex;
import vg.interfaces.graph_view_service.data.VGEdgeCell;
import vg.interfaces.graph_view_service.data.VGGraph;
import vg.interfaces.graph_view_service.data.VGVertexCell;
import vg.plugins.layout.hierarchical_layout.jgraph.MyMXHierarchicalLayout;
import vg.plugins.layout.hierarchical_layout.jgraph.MyMXHierarchicalLayoutUtils;

import java.awt.*;
import java.util.List;
import java.util.Map;

/**
 * @author Timur Zolotuhin (e-mail: tzolotuhin@parallels.com)
 */
public class AnnealingAlgorithm {
    public static void optimize(VGGraph originalGraph,
                                List<VGVertexCell> vertices,
                                Map<VGGraph, Object[][]> tmpGraphToMap,
                                Point portSize,
                                Point gridCellSize,
                                int intraCellSpacing,
                                int interRankCellSpacing,
                                int interHierarchySpacing,
                                boolean ignoreEdgeIntersection,
                                boolean resolveEdgeIntersection,
                                VGVertexCell vgParentVertexCell,
                                VGGraph tmpGraph,
                                List<VGVertexCell> sourcePortsInTmpGraph,
                                List<VGVertexCell> targetPortsInTmpGraph) {
        int prevCountIntersection = Integer.MAX_VALUE;
        Map<Vertex, Float> currVertexToPriorityMap = Maps.newHashMap();
        Map<Vertex, Float> prevVertexToPriorityMap = Maps.newHashMap();
        MyMXHierarchicalLayoutUtils.initVertexToPriority(currVertexToPriorityMap, vertices);
        MyMXHierarchicalLayoutUtils.initVertexToPriority(prevVertexToPriorityMap, vertices);
        boolean finish = false;
        do {
            VGMainServiceHelper.logger.printDebug(">>>Nex Iteration: count intersection: " + prevCountIntersection);

//            for (Vertex vertex : currVertexToPriorityMap.keySet()) {
//                VGMainServiceHelper.logger.printDebug("Original vertex priority: " + originalGraph.getVGVertexCellByVertex(vertex).getHumanId() + ", p: " + currVertexToPriorityMap.get(vertex));
//            }

            MyMXHierarchicalLayout layout = new MyMXHierarchicalLayout(tmpGraph,
                    originalGraph,
                    sourcePortsInTmpGraph,
                    targetPortsInTmpGraph,
                    currVertexToPriorityMap,
                    portSize,
                    gridCellSize,
                    vgParentVertexCell);
            layout.setIntraCellSpacing(intraCellSpacing);
            layout.setInterRankCellSpacing(interRankCellSpacing);
            layout.setInterHierarchySpacing(interHierarchySpacing);

            // prepare ports
            for (VGVertexCell vertexCell : sourcePortsInTmpGraph) {
                vertexCell.setVisible(false);
            }
            for (VGVertexCell vertexCell : targetPortsInTmpGraph) {
                vertexCell.setVisible(false);
            }

            // execute layout
            layout.execute(tmpGraph.getDefaultParent());
            VGMainServiceHelper.logger.printDebug("Layout size: " + layout.getSize());

            // back ports to initial state
            for (VGVertexCell tmpGraphSourcePort : sourcePortsInTmpGraph) {
                tmpGraphSourcePort.setVisible(true);
            }
            for (VGVertexCell tmpGraphTargetPort : targetPortsInTmpGraph) {
                tmpGraphTargetPort.setVisible(true);
            }

            tmpGraph.removeNegativeCoordinates();

            // calculate graph size
            tmpGraph.setGraphSize(layout.getSize());

            // resolve edge and vertices intersection
            //List<Map.Entry<VGEdgeCell, VGEdgeCell>> intersections = Lists.newArrayList();
            List<Map.Entry<VGEdgeCell, VGEdgeCell>> intersections = EdgeResolver.resolveEdgeIntersection(tmpGraph, originalGraph, tmpGraphToMap, gridCellSize, ignoreEdgeIntersection);

            if (finish || !resolveEdgeIntersection)
                break;

            if (prevCountIntersection <= intersections.size()) {
                currVertexToPriorityMap = prevVertexToPriorityMap;
                finish = true;
            } else {
                prevCountIntersection = intersections.size();
                prevVertexToPriorityMap = currVertexToPriorityMap;
                currVertexToPriorityMap = Maps.newHashMap();
                MyMXHierarchicalLayoutUtils.initVertexToPriority(currVertexToPriorityMap, vertices);

                for (Map.Entry<VGEdgeCell, VGEdgeCell> intersection : intersections) {
                    Vertex src1 = intersection.getKey().getVGSoure().getVGObject();
                    Vertex src2 = intersection.getValue().getVGSoure().getVGObject();

                    if (src1 != src2) {
                        float tmp = currVertexToPriorityMap.get(src1);
                        currVertexToPriorityMap.put(src1, currVertexToPriorityMap.get(src2));
                        currVertexToPriorityMap.put(src2, tmp);
                    } else {
                        Vertex trg1 = intersection.getKey().getVGTarget().getVGObject();
                        Vertex trg2 = intersection.getValue().getVGTarget().getVGObject();
                        float tmp = currVertexToPriorityMap.get(trg1);
                        currVertexToPriorityMap.put(trg1, currVertexToPriorityMap.get(trg2));
                        currVertexToPriorityMap.put(trg2, tmp);
                    }
                }
            }
        } while (true);
    }
}
