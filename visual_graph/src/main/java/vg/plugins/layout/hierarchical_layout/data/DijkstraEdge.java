package vg.plugins.layout.hierarchical_layout.data;

/**
 * @author Timur Zolotuhin (e-mail: tzolotuhin@parallels.com)
 */
public class DijkstraEdge {
    private final int sourceIndex;
    private final int destinationIndex;
    private final double weight;

    public DijkstraEdge(int sourceIndex, int destinationIndex, double weight) {
        this.sourceIndex = sourceIndex;
        this.destinationIndex = destinationIndex;
        this.weight = weight;
    }

    public int getDestination() {
        return destinationIndex;
    }

    public int getSource() {
        return sourceIndex;
    }
    public double getWeight() {
        return weight;
    }

    @Override
    public String toString() {
        return sourceIndex + " " + destinationIndex;
    }
}