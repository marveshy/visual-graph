package vg.plugins.layout.hierarchical_layout.data;

/**
 * @author Timur Zolotuhin (e-mail: tzolotuhin@parallels.com)
 */
public class DijkstraVertex {
    private final MapPoint mapPoint;
    private final int index;

    public DijkstraVertex(MapPoint mapPoint, int index) {
        this.mapPoint = mapPoint;
        this.index = index;
    }

    public int getIndex() {
        return index;
    }

    public MapPoint getMapPoint() {
        return mapPoint;
    }

    @Override
    public boolean equals(Object o) {
        return this == o;
    }

    @Override
    public int hashCode() {
        return mapPoint.hashCode();
    }

    @Override
    public String toString() {
        return mapPoint.toString();
    }
}
