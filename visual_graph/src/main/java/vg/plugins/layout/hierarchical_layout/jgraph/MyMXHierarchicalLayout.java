package vg.plugins.layout.hierarchical_layout.jgraph;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.mxgraph.layout.hierarchical.mxHierarchicalLayout;
import com.mxgraph.model.mxIGraphModel;
import com.mxgraph.util.mxPoint;
import com.mxgraph.view.mxGraphView;
import vg.interfaces.graph_view_service.data.VGEdgeCell;
import vg.interfaces.graph_view_service.data.VGGraph;
import vg.shared.utils.MapUtils;
import vg.interfaces.data_base_service.data.graph.Vertex;
import vg.interfaces.graph_view_service.data.VGVertexCell;

import java.awt.*;
import java.util.*;
import java.util.List;

/**
 * @author Timur Zolotuhin (e-mail: tzolotuhin@parallels.com)
 */
public class MyMXHierarchicalLayout extends mxHierarchicalLayout {
    protected Map<Vertex, Float> vertexToPriorityMap;

    protected Point gridCellSize;
    protected Point portSize;

    protected Point actionPanelSize;
    protected Point shortInfoSize;

    protected Point graphLeftPoint;
    protected Point targetPortsLeftPoint;
    protected Point sourcePortsLeftPoint;

    protected List<VGVertexCell> sourcePorts;
    protected List<VGVertexCell> targetPorts;
    protected mxPoint graphSize;
    protected VGVertexCell vgParentVertexCell;
    protected VGGraph originalGraph;

    public MyMXHierarchicalLayout(VGGraph graph,
                                  VGGraph originalGraph,
                                  List<VGVertexCell> sourcePorts,
                                  List<VGVertexCell> targetPorts,
                                  Map<Vertex, Float> vertexToPriorityMap,
                                  Point portSize,
                                  Point gridCellSize,
                                  VGVertexCell vgParentVertexCell) {
        super(graph);
        this.originalGraph = originalGraph;
        this.sourcePorts = sourcePorts;
        this.targetPorts = targetPorts;
        this.vertexToPriorityMap = vertexToPriorityMap;
        this.gridCellSize = gridCellSize;
        this.portSize = portSize;
        this.vgParentVertexCell = vgParentVertexCell;
    }

    @Override
    public List<Object> findRoots(Object parent, Set<Object> vertices) {
        List<Object> roots = new ArrayList<>();

        Object best = null;
        int maxDiff = -100000;
        mxIGraphModel model = graph.getModel();

        for (Object vertex : vertices) {
            if (model.isVertex(vertex) && graph.isCellVisible(vertex)) {
                Object[] conns = this.getEdges(vertex);
                int fanOut = 0;
                int fanIn = 0;

                for (Object conn : conns) {
                    Object src = graph.getView().getVisibleTerminal(conn, true);

                    if (src == vertex) {
                        fanOut++;
                    } else {
                        fanIn++;
                    }
                }

                if (fanIn == 0 && fanOut > 0) {
                    roots.add(vertex);
                }

                int diff = fanOut - fanIn;

                if (diff > maxDiff) {
                    maxDiff = diff;
                    best = vertex;
                }
            }
        }

        if (roots.isEmpty() && best != null) {
            roots.add(best);
        }

        return roots;
    }

    @Override
    public Set<Object> filterDescendants(Object cell) {
        mxIGraphModel model = graph.getModel();
        Set<Object> result = new LinkedHashSet<>();

        if (model.isVertex(cell) && cell != this.parent && model.isVisible(cell)) {
            result.add(cell);
        }

        if (this.traverseAncestors || cell == this.parent && model.isVisible(cell)) {
            int childCount = model.getChildCount(cell);

            for (int i = 0; i < childCount; i++) {
                Object child = model.getChildAt(cell, i);
                result.addAll(filterDescendants(child));
            }
        }

        return result;
    }

    @Override
    public void layeringStage() {
        model.initialRank();
        model.fixRanks();
    }

    @Override
    public void run(Object parent) {
        // calculate short info size and action panel size aligned per grid size
        {
            Point originShortInfoSize = new Point(0, 0);
            if (vgParentVertexCell != null)
                originShortInfoSize = vgParentVertexCell.vgGetTextSize().getPoint();

            Point originActionPanelSize = new Point(0, 0);
            if (vgParentVertexCell != null)
                originActionPanelSize = vgParentVertexCell.vgGetActionPanelSize().getPoint();

            Point shortInfoCellsCount = new Point(Math.round(originShortInfoSize.x) / gridCellSize.x, Math.round(originShortInfoSize.y) / gridCellSize.y);
            if (Math.round(originShortInfoSize.x) % gridCellSize.x != 0) {
                shortInfoCellsCount.x++;
            }
            if (Math.round(originShortInfoSize.y) % gridCellSize.y != 0) {
                shortInfoCellsCount.y++;
            }

            Point actionPanelCellsCount = new Point(Math.round(originActionPanelSize.x) / gridCellSize.x, Math.round(originActionPanelSize.y) / gridCellSize.y);
            if (Math.round(originActionPanelSize.x) % gridCellSize.x != 0) {
                actionPanelCellsCount.x++;
            }
            if (Math.round(originActionPanelSize.y) % gridCellSize.y != 0) {
                actionPanelCellsCount.y++;
            }

            actionPanelSize = new Point(actionPanelCellsCount.x * gridCellSize.x, actionPanelCellsCount.y * gridCellSize.y);
            shortInfoSize = new Point(shortInfoCellsCount.x * gridCellSize.x, shortInfoCellsCount.y * gridCellSize.y);
        }

        // Separate out unconnected hierarchies
        List<Set<Object>> hierarchyVertices = Lists.newArrayList();
        Set<Object> allVertexSet = Sets.newLinkedHashSet();
        Set<Object> filledVertexSet = filterDescendants(parent);

        this.roots = Lists.newArrayList();

        while (!filledVertexSet.isEmpty()) {
            List<Object> candidateRoots = findRoots(parent, filledVertexSet);

            // CHANGES IN ORIGINAL CODE
            // filter roots by priority
            Map<VGVertexCell, Float> newFilteredVertex = Maps.newLinkedHashMap();
            for (Object candidateRoot : candidateRoots) {
                VGVertexCell vgVertexCell = (VGVertexCell)candidateRoot;
                newFilteredVertex.put(vgVertexCell, vertexToPriorityMap.get(vgVertexCell.getVGObject()));
            }
            newFilteredVertex = new MapUtils<VGVertexCell, Float>().sortMapByValue(newFilteredVertex);
            candidateRoots = Lists.reverse(Lists.<Object>newArrayList(newFilteredVertex.keySet()));
            // END CHANGES IN ORIGINAL CODE

            for (Object root : candidateRoots) {
                Set<Object> vertexSet = Sets.newLinkedHashSet();
                hierarchyVertices.add(vertexSet);

                traverse(root, true, null, allVertexSet, vertexSet, hierarchyVertices, filledVertexSet);
            }

            this.roots.addAll(candidateRoots);
        }

        // Iterate through the result removing parents who have children in this layout
        // Perform a layout for each separate hierarchy
        // Track initial coordinate x-positioning
        {
            mxPoint initialPos = new mxPoint(0, actionPanelSize.y);

            if (!targetPorts.isEmpty() && initialPos.getY() < portSize.y) {
                initialPos.setY(portSize.y);
            }

            if (initialPos.getY() < shortInfoSize.y) {
                initialPos.setY(shortInfoSize.y);
            }

            if (initialPos.getY() > 0)
                initialPos.setY(initialPos.getY() + getInterRankCellSpacing());

            graphLeftPoint = new Point(initialPos.getPoint());
            graphSize = new mxPoint(initialPos);

            // placement stage for graph
            for (Set<Object> vertexSet : hierarchyVertices) {
                this.model = new MyMXGraphHierarchyModel(this, vertexSet.toArray(), roots, parent, vertexToPriorityMap, originalGraph);

                cycleStage(parent);
                layeringStage();
                crossingStage(parent);
                initialPos.setX(placementStage(initialPos.getX(), initialPos.getY(), parent));
            }
        }

        // calculate graphSize
        for (VGVertexCell vgVertexCell : ((VGGraph)graph).getVGVerticesByLevel(-1)) {
            if (vgVertexCell.getGeometry().getX() + vgVertexCell.getGeometry().getWidth() > graphSize.getX())
                graphSize.setX(vgVertexCell.getGeometry().getX() + vgVertexCell.getGeometry().getWidth());
            if (vgVertexCell.getGeometry().getY() + vgVertexCell.getGeometry().getHeight() > graphSize.getY())
                graphSize.setY(vgVertexCell.getGeometry().getY() + vgVertexCell.getGeometry().getHeight());
        }
        graphSize.setX(graphSize.getX() + gridCellSize.getX());

        // calculate target ports size
        int targetPortCellsCount = targetPorts.size() + (targetPorts.size() == 0 ? 0 : (targetPorts.size() - 1));
        int targetPortSize = targetPortCellsCount * portSize.x;

        // calculate source ports size
        int sourcePortCellsCount = sourcePorts.size() + (sourcePorts.size() == 0 ? 0 : (sourcePorts.size() - 1));
        int sourcePortSize = sourcePortCellsCount * portSize.x;

        // calculate top and bottom sizes, also correct maxX and maxY
        // calculate parameters for placement stage for ports
        {
            int topSize = targetPortSize + shortInfoSize.x + actionPanelSize.x;

            if (graphSize.getX() < topSize)
                graphSize.setX(topSize);
            if (graphSize.getX() < sourcePortSize)
                graphSize.setX(sourcePortSize);

            int topX = (graphSize.getPoint().x - topSize) / 2;
            if (topX % gridCellSize.x != 0)
                topX += gridCellSize.x;

            int bottomX = (graphSize.getPoint().x - sourcePortSize) / 2;
            if (bottomX % gridCellSize.x != 0)
                bottomX += gridCellSize.x;

            targetPortsLeftPoint = new Point(actionPanelSize.x + (topX / gridCellSize.x) * gridCellSize.x, 0);
            if (Math.abs(graphLeftPoint.getY() - graphSize.getY()) < 0.01)
                sourcePortsLeftPoint = new Point((bottomX / gridCellSize.x) * gridCellSize.x, graphSize.getPoint().y);
            else
                sourcePortsLeftPoint = new Point((bottomX / gridCellSize.x) * gridCellSize.x, graphSize.getPoint().y + (int) Math.round(getInterRankCellSpacing()));

            graphSize.setY(sourcePortsLeftPoint.y + (sourcePorts.size() == 0 ? 0 : portSize.y));
        }

        portPlacementStage();
    }

    public Point getSize() {
        return graphSize.getPoint();
    }

    @Override
    protected void traverse(Object vertex, boolean directed, Object edge, Set<Object> allVertices, Set<Object> currentComp, List<Set<Object>> hierarchyVertices, Set<Object> filledVertexSet) {
        mxGraphView view = graph.getView();
        mxIGraphModel model = graph.getModel();

        if (vertex != null && allVertices != null) {
            // Has this vertex been seen before in any traversal
            // And if the filled vertex set is populated, only
            // process vertices in that it contains
            if (!allVertices.contains(vertex) && (filledVertexSet == null || filledVertexSet.contains(vertex))) {
                currentComp.add(vertex);
                allVertices.add(vertex);

                if (filledVertexSet != null) {
                    filledVertexSet.remove(vertex);
                }

                int edgeCount = model.getEdgeCount(vertex);

                if (edgeCount > 0) {
                    Map<VGEdgeCell, Float> unsortedEdgesMap = Maps.newHashMap();
                    for (int i = 0; i < edgeCount; i++) {
                        VGEdgeCell e = (VGEdgeCell) model.getEdgeAt(vertex, i);
                        unsortedEdgesMap.put(e, vertexToPriorityMap.get(e.getVGTarget().getVGObject()));
                    }
                    Map<VGEdgeCell, Float> sortedEdgesMap = new MapUtils<VGEdgeCell, Float>().sortMapByValue(unsortedEdgesMap);
                    List<VGEdgeCell> sortedEdges = Lists.reverse(Lists.newArrayList(sortedEdgesMap.keySet()));

                    for (VGEdgeCell e : sortedEdges) {
                        boolean isSource = view.getVisibleTerminal(e, true) == vertex;

                        if (!directed || isSource) {
                            //VGMainServiceHelper.logger.printDebug("Layout traverse edge: " + originalGraph.getVGEdgeCellByEdge(e.getVGObject()).getHumanId() + ", p: " + sortedEdgesMap.get(e));
                            Object next = view.getVisibleTerminal(e, !isSource);
                            traverse(next, directed, e, allVertices, currentComp, hierarchyVertices, filledVertexSet);
                        }
                    }
                }
            } else {
                if (!currentComp.contains(vertex)) {
                    // We've seen this vertex before, but not in the current component
                    // This component and the one it's in need to be merged
                    Set<Object> matchComp = null;

                    for (Set<Object> comp : hierarchyVertices) {
                        if (comp.contains(vertex)) {
                            currentComp.addAll(comp);
                            matchComp = comp;
                            break;
                        }
                    }

                    if (matchComp != null) {
                        hierarchyVertices.remove(matchComp);
                    }
                }
            }
        }
    }

//==============================================================================
//------------------PRIVATE METHODS---------------------------------------------
    private double placementStage(double initialX, double initialY, Object parent) {
        MyMXCoordinateAssignment placementStage = new MyMXCoordinateAssignment(this, intraCellSpacing, interRankCellSpacing, orientation, initialX, initialY, parallelEdgeSpacing, gridCellSize);
        placementStage.setFineTuning(fineTuning);
        placementStage.execute(parent);
        return placementStage.getLimitX() + interHierarchySpacing;
    }

    private void portPlacementStage() {
        // draw target ports
        {
            int x = targetPortsLeftPoint.x;
            int y = targetPortsLeftPoint.y;
            for (VGVertexCell targetPortVertexCell : targetPorts) {
                targetPortVertexCell.getGeometry().setX(x);
                targetPortVertexCell.getGeometry().setY(y);
                targetPortVertexCell.getGeometry().setWidth(portSize.x);
                targetPortVertexCell.getGeometry().setHeight(portSize.y);
                x += portSize.x * 2;
            }
        }

        // draw source ports
        {
            int x = sourcePortsLeftPoint.x;
            int y = sourcePortsLeftPoint.y;

            for (VGVertexCell sourcePortVertexCell : sourcePorts) {
                sourcePortVertexCell.getGeometry().setX(x);
                sourcePortVertexCell.getGeometry().setY(y);
                sourcePortVertexCell.getGeometry().setWidth(portSize.x);
                sourcePortVertexCell.getGeometry().setHeight(portSize.y);
                x += portSize.x * 2;
            }
        }
    }
}