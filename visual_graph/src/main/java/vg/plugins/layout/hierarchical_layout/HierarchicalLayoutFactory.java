package vg.plugins.layout.hierarchical_layout;

import com.google.common.collect.*;
import vg.impl.main_service.VGMainServiceHelper;
import vg.interfaces.data_base_service.data.graph.Attribute;
import vg.interfaces.data_base_service.data.record.AttributeRecord;
import vg.interfaces.executor_service.ExecutorService;
import vg.interfaces.graph_layout_service.GraphLayout;
import vg.interfaces.graph_layout_service.GraphLayoutFactory;
import vg.interfaces.graph_layout_service.GraphLayoutSetting;
import vg.interfaces.graph_layout_service.GraphLayoutTemplate;
import vg.interfaces.graph_view_service.data.VGEdgeCell;
import vg.interfaces.graph_view_service.data.VGGraph;
import vg.interfaces.graph_view_service.data.VGVertexCell;
import vg.plugins.layout.BaseGraphLayout;
import vg.plugins.layout.BaseGraphLayoutFactory;
import vg.plugins.layout.BaseGraphLayoutTemplate;
import vg.plugins.layout.GraphLayoutPluginGlobals;
import vg.shared.graph.utils.GraphUtils;

import java.awt.*;
import java.awt.geom.Point2D;
import java.util.*;
import java.util.List;

/**
 * @author Timur Zolotuhin (e-mail: tzolotuhin@parallels.com)
 */
public class HierarchicalLayoutFactory extends BaseGraphLayoutFactory {
    // Constants
    private final static String HIERARCHICAL_LAYOUT_PREFIX = GraphLayoutPluginGlobals.LAYOUT_PLUGIN_SETTINGS_PREFIX + "hierarchical_layout_";
//
//    private final static String PERFORMANCE_PRIORITY = "Performance";
//    private final static String QUALITY_PRIORITY = "Quality";
//
//    private final static String VERTEX_BORDER_KEY = HIERARCHICAL_LAYOUT_PREFIX + "vertex_border";
//    private final static String EDGE_BORDER_KEY = HIERARCHICAL_LAYOUT_PREFIX + "edge_border";
//    private final static String EDGE_STYLE_KEY = HIERARCHICAL_LAYOUT_PREFIX + "edge_style";
//    private final static String LAYOUT_QUALITY_PRIORITY_KEY = HIERARCHICAL_LAYOUT_PREFIX + "layout_quality_priority";
    private final static String GRID_CELL_SIZE_KEY = HIERARCHICAL_LAYOUT_PREFIX + "grid_cell_size";
    private final static String INTRA_CELL_SPACING_KEY = HIERARCHICAL_LAYOUT_PREFIX + "intra_cell_spacing";
    private final static String INTER_RANK_CELL_SPACING_KEY = HIERARCHICAL_LAYOUT_PREFIX + "inter_rank_cell_spacing";
    private final static String INTER_HIERARCHY_SPACING_KEY = HIERARCHICAL_LAYOUT_PREFIX + "inter_hierarchy_spacing";

    private final static String LAYOUT_NAME = "Hierarchical layout";

    private static Color portColor = new Color(252,251,227);
    private static Color[] colors = new Color[] {new Color(152, 251, 152), new Color(175, 238, 238), new Color(240, 230, 140), new Color(255, 182, 193)};

    @Override
    public boolean isPortsSupport() {
        return true;
    }

    @Override
    public boolean isHierarchicalSupport() {
        return true;
    }

    @Override
    public GraphLayoutTemplate buildGraphLayoutTemplate() {
        return new HierarchicalLayoutTemplate(this);
    }

    @Override
    public String getLayoutName() {
        return LAYOUT_NAME;
    }

    private class HierarchicalLayoutTemplate extends BaseGraphLayoutTemplate {
        public HierarchicalLayoutTemplate(GraphLayoutFactory graphLayoutFactoryCallBack) {
            super(graphLayoutFactoryCallBack);
            settings.add(new GraphLayoutSetting(GRID_CELL_SIZE_KEY, "40", "Grid cell size:", AttributeRecord.INTEGER_ATTRIBUTE_TYPE, GraphLayoutSetting.VALUE_SETTING, null, true));
            settings.add(new GraphLayoutSetting(INTRA_CELL_SPACING_KEY, "3", "Intra cell spacing in grid:", AttributeRecord.INTEGER_ATTRIBUTE_TYPE, GraphLayoutSetting.VALUE_SETTING, null, true));
            settings.add(new GraphLayoutSetting(INTER_RANK_CELL_SPACING_KEY, "2", "Inter rank cell spacing in grid:", AttributeRecord.INTEGER_ATTRIBUTE_TYPE, GraphLayoutSetting.VALUE_SETTING, null, true));
            settings.add(new GraphLayoutSetting(INTER_HIERARCHY_SPACING_KEY, "5", "Inter hierarchy spacing in grid:", AttributeRecord.INTEGER_ATTRIBUTE_TYPE, GraphLayoutSetting.VALUE_SETTING, null, true));
            //settings.add(new GraphLayoutSetting(EDGE_BORDER_KEY, "3", "Edge border:", AttributeRecord.INTEGER_ATTRIBUTE_TYPE, GraphLayoutSetting.VALUE_SETTING, null, true));
            //settings.add(new GraphLayoutSetting(EDGE_STYLE_KEY, GraphView.EDGE_STYLE_NONE, "Edge style", AttributeRecord.STRING_ATTRIBUTE_TYPE, GraphLayoutSetting.LIST_SETTING, Arrays.<Object>asList(GraphView.EDGE_STYLE_NONE, GraphView.EDGE_STYLE_ORTHOGONAL, GraphView.EDGE_STYLE_ELBOW), true));
            //settings.add(new GraphLayoutSetting(LAYOUT_QUALITY_PRIORITY_KEY, PERFORMANCE_PRIORITY, "Priority", AttributeRecord.STRING_ATTRIBUTE_TYPE, GraphLayoutSetting.LIST_SETTING, Arrays.<Object>asList(PERFORMANCE_PRIORITY, QUALITY_PRIORITY), true));
        }

        @Override
        public GraphLayout buildGraphLayout() {
            synchronized (generalMutex) {
                return new HierarchicalLayout(this, settings);
            }
        }
    }

    private static class HierarchicalLayout extends BaseGraphLayout {
        public HierarchicalLayout(GraphLayoutTemplate callback, List<GraphLayoutSetting> settings) {
            super(callback, settings);
        }

        @Override
        public void start(final VGGraph vgGraph, final GraphLayoutCallBack callBack) {
            try {
                // initialize parameters
                int vertexBorder = 3; //getSettingByName(VERTEX_BORDER_KEY).getIntegerValue();
                int edgeBorder = 3; //getSettingByName(EDGE_BORDER_KEY).getIntegerValue();
                final String edgeStyle = null; //getSettingByName(EDGE_STYLE_KEY).getStringValue();
                final int gridCellSizeValue = getSettingByName(GRID_CELL_SIZE_KEY).getIntegerValue();
                final Point gridCellSize = new Point(gridCellSizeValue, gridCellSizeValue);
                int intraCellSpacing = getSettingByName(INTRA_CELL_SPACING_KEY).getIntegerValue() * gridCellSizeValue;
                int interRankCellSpacing = getSettingByName(INTER_RANK_CELL_SPACING_KEY).getIntegerValue() * gridCellSizeValue;
                int interHierarchySpacing = getSettingByName(INTER_HIERARCHY_SPACING_KEY).getIntegerValue() * gridCellSizeValue;
                final Point vertexAlign = new Point(1, 1);
                final boolean ignoreEdgeIntersection = false;
                final boolean resolveEdgeIntersection = false;

                vgGraph.getGraphViewReference().setVertexBorderSize(vgGraph.getGraphViewReference().getVertexAttributes().keySet(), vertexBorder);
                vgGraph.getGraphViewReference().setEdgeBorderSize(vgGraph.getGraphViewReference().getEdgeAttributes().keySet(), edgeBorder);
                vgGraph.setGridSize(gridCellSizeValue);

                // execute layout
                doJGraphLayout(
                        vgGraph,
                        vgGraph.getVGVerticesByLevel(1),
                        edgeStyle,
                        Maps.newHashMap(),
                        gridCellSize,
                        intraCellSpacing,
                        interRankCellSpacing,
                        interHierarchySpacing,
                        vertexAlign,
                        ignoreEdgeIntersection,
                        resolveEdgeIntersection,
                        null);

                VGMainServiceHelper.logger.printDebug("Finish doJGraphLayout");

                // refresh view
                vgGraph.getGraphViewReference().optimizeCanvasSize();
                vgGraph.getGraphViewReference().refreshView();
            } catch (Throwable ex) {
                VGMainServiceHelper.logger.printException(ex);
                VGMainServiceHelper.windowMessenger.errorMessage("Something went wrong...", "Hierarchical layout error", null);
            } finally {
                state.set(STOPPED_STATE);
                VGMainServiceHelper.executorService.execute(new ExecutorService.SwingExecutor() {
                    @Override
                    public void doInEDT() {
                        if (callBack != null)
                            callBack.onFinishAction(HierarchicalLayout.this);
                    }
                });
            }
        }

        private static Point2D doJGraphLayout(VGGraph originalGraph,
                                              List<VGVertexCell> vertices,
                                              String edgeStyle,
                                              Map<VGGraph, Object[][]> tmpGraphToMap,
                                              Point gridCellSize,
                                              int intraCellSpacing,
                                              int interRankCellSpacing,
                                              int interHierarchySpacing,
                                              Point vertexAlign,
                                              boolean ignoreEdgeIntersection,
                                              boolean resolveEdgeIntersection,
                                              VGVertexCell vgParentVertexCell) {
            // check that all vertices are leafs
            for (VGVertexCell vgVertexCell : vertices) {
                if (vgVertexCell.getChildCount() != 0) {
                    // execute layout for children
                    Point2D size = doJGraphLayout(
                            originalGraph,
                            vgVertexCell.vgGetVertexChildren(),
                            edgeStyle,
                            tmpGraphToMap,
                            gridCellSize,
                            intraCellSpacing,
                            interRankCellSpacing,
                            interHierarchySpacing,
                            vertexAlign,
                            ignoreEdgeIntersection,
                            resolveEdgeIntersection,
                            vgVertexCell);

                    // set result parameters for parent
                    vgVertexCell.getGeometry().setWidth(size.getX());
                    vgVertexCell.getGeometry().setHeight(size.getY());
                    vgVertexCell.vgSetColor(colors[VGGraph.calcCellLevel(vgVertexCell) % colors.length]);
                }
            }

            // find ports
            Map.Entry<List<VGVertexCell>, List<VGVertexCell>> portsInOriginalGraph = GraphUtils.findPorts(vertices, originalGraph.getVGEdgesByLevel(-1));

            // calculate port size
            Point maxPortSize = new Point();
            for (VGVertexCell vgVertexCell : portsInOriginalGraph.getKey()) {
                if (maxPortSize.getX() < vgVertexCell.vgGetTextSize().getX())
                    maxPortSize.setLocation(vgVertexCell.vgGetTextSize().getX(), maxPortSize.getY());
                if (maxPortSize.getY() < vgVertexCell.vgGetTextSize().getY())
                    maxPortSize.setLocation(maxPortSize.getX(), vgVertexCell.vgGetTextSize().getY());
            }
            for (VGVertexCell vgVertexCell : portsInOriginalGraph.getValue()) {
                if (maxPortSize.getX() < vgVertexCell.vgGetTextSize().getX())
                    maxPortSize.setLocation(vgVertexCell.vgGetTextSize().getX(), maxPortSize.getY());
                if (maxPortSize.getY() < vgVertexCell.vgGetTextSize().getY())
                    maxPortSize.setLocation(maxPortSize.getX(), vgVertexCell.vgGetTextSize().getY());
            }

            if (maxPortSize.x % gridCellSize.x != 0)
                maxPortSize.x = maxPortSize.x + gridCellSize.x;
            if (maxPortSize.y % gridCellSize.y != 0)
                maxPortSize.y = maxPortSize.y + gridCellSize.y;
            if ((maxPortSize.x / gridCellSize.x) % 2 != 1)
                maxPortSize.x = maxPortSize.x + gridCellSize.x;

            maxPortSize.x = (maxPortSize.x / gridCellSize.x) * gridCellSize.x;
            maxPortSize.y = (maxPortSize.y / gridCellSize.y) * gridCellSize.y;

            // build new temp graph for layout
            VGGraph tmpGraph = new VGGraph();
            List<VGVertexCell> sourcePortsInTmpGraph = Lists.newArrayList();
            List<VGVertexCell> targetPortsInTmpGraph = Lists.newArrayList();

            // insert vertices to tmp graph
            VGMainServiceHelper.logger.printDebug("Insert vertices to temp graph");

            for (VGVertexCell vertex : vertices) {
                VGVertexCell vertexCell = tmpGraph.vgInsertVertex(vertex.getVGObject(), tmpGraph.getDefaultParent());
                vertexCell.getGeometry().setWidth(vertex.getGeometry().getWidth());
                vertexCell.getGeometry().setHeight(vertex.getGeometry().getHeight());
                if (portsInOriginalGraph.getKey().contains(vertex)) {
                    sourcePortsInTmpGraph.add(vertexCell);
                }
                if (portsInOriginalGraph.getValue().contains(vertex)) {
                    targetPortsInTmpGraph.add(vertexCell);
                }
            }

            // insert edges to tmp graph
            VGMainServiceHelper.logger.printDebug("Insert edges to temp graph");

            for (VGEdgeCell edge : originalGraph.getVGEdgesForVertices(vertices)) {
                if (VGGraph.isEdgeWithParentNode(edge)) {
                    VGMainServiceHelper.logger.printDebug(String.format("Edge with id '%s' connects vertex and graph so will be skipped.", edge.getId()));

                    continue;
                }

                VGVertexCell srcParent = (VGVertexCell)VGGraph.getCellParentWithLevel(edge.getVGSoure(), VGGraph.calcCellLevel(edge));
                VGVertexCell trgParent = (VGVertexCell)VGGraph.getCellParentWithLevel(edge.getVGTarget(), VGGraph.calcCellLevel(edge));

                if (srcParent == null || trgParent == null) {
                    VGMainServiceHelper.logger.printDebug(String.format("Edge with id '%s' has null parent for some nodes so will be skipped.", edge.getId()));
                    continue;
                }

                if (vertices.contains(srcParent) && vertices.contains(trgParent)) {
                    VGEdgeCell tmpEdgeCell = tmpGraph.vgInsertEdge(
                            edge.getVGObject(),
                            tmpGraph.getDefaultParent(),
                            tmpGraph.getVGVertexCellByVertex(srcParent.getVGObject()),
                            tmpGraph.getVGVertexCellByVertex(trgParent.getVGObject()));
                    tmpEdgeCell.vgSetEdgeStyle(edgeStyle);
                } else {
                    VGMainServiceHelper.logger.printDebug(String.format("Edge with id '%s' has some nodes in no vertices so will be skipped.", edge.getId()));
                }
            }

            // prepare graph to layout
            tmpGraph.setStylesheet(originalGraph.getStylesheet());
            tmpGraph.setAllowNegativeCoordinates(false);

            doAlignVertices(tmpGraph, gridCellSize, vertexAlign);

            // make layout
            AnnealingAlgorithm.optimize(originalGraph,
                    vertices,
                    tmpGraphToMap,
                    maxPortSize,
                    gridCellSize,
                    intraCellSpacing,
                    interRankCellSpacing,
                    interHierarchySpacing,
                    ignoreEdgeIntersection,
                    resolveEdgeIntersection,
                    vgParentVertexCell,
                    tmpGraph,
                    sourcePortsInTmpGraph,
                    targetPortsInTmpGraph);

            // copy positions and sizes to the origin graph
            tmpGraph.bfs(new VGGraph.SearchListener() {
                @Override
                public void onVertex(VGVertexCell vgVertexCell) {
                    VGVertexCell srcVGVertexCell = originalGraph.getVGVertexCellByVertex(vgVertexCell.getVGObject());

                    srcVGVertexCell.setGeometry(vgVertexCell.getGeometry());

                    // handle ports
                    Attribute attr = vgVertexCell.getVGObject().getAttribute(GraphUtils.ATTRIBUTE_NAME_FOR_PORT);
                    if (attr != null) {
                        if (attr.getBooleanValue()) {
                            srcVGVertexCell.setVisible(false);
                        } else {
                            srcVGVertexCell.vgSetColor(portColor);
                        }
                    }
                }

                @Override
                public void onEdge(VGEdgeCell vgEdgeCell) {
                    VGEdgeCell originVGEdgeCell = originalGraph.getVGEdgeCellByEdge(vgEdgeCell.getVGObject());
                    originVGEdgeCell.setGeometry(vgEdgeCell.getGeometry());
                    originVGEdgeCell.vgSetEdgeStyle(vgEdgeCell.vgGetEdgeStyle());
                    originVGEdgeCell.vgSetColor(vgEdgeCell.vgGetColor());

                    Attribute attr = vgEdgeCell.getVGObject().getAttribute(GraphUtils.ATTRIBUTE_NAME_FOR_COMPOSITE_EDGE_ORDER);
                    if (attr != null) {
                        if (attr.getIntegerValue() != -1) {
                            originVGEdgeCell.vgSetDirected(false);
                        }
                    }
                }
            });

            return tmpGraph.getGraphSize();
        }

        private static void doAlignVertices(VGGraph vgGraph, Point cellSize, Point vertexAlign) {
            VGMainServiceHelper.logger.printDebug("Do align vertices");

            List<VGVertexCell> vertices = vgGraph.getVGVerticesByLevel(-1);
            for (VGVertexCell vgVertexCell : vertices) {
                if (((int)(vgVertexCell.getGeometry().getWidth() + 0.5)) % cellSize.x != 0)
                    vgVertexCell.getGeometry().setWidth((((int)(vgVertexCell.getGeometry().getWidth() + 0.5)) / cellSize.x) * cellSize.x + cellSize.x);
                if (((int)(vgVertexCell.getGeometry().getHeight() + 0.5)) % cellSize.y != 0)
                    vgVertexCell.getGeometry().setHeight((((int)(vgVertexCell.getGeometry().getHeight() + 0.5)) / cellSize.y) * cellSize.y + cellSize.y);

                int x = ((int)(vgVertexCell.getGeometry().getWidth() + 0.5)) / cellSize.x;
                if (x == 0 || x % vertexAlign.x != 0) {
                    vgVertexCell.getGeometry().setWidth(((x / vertexAlign.x) + 1) * vertexAlign.x * cellSize.x);
                }
                int y = ((int)(vgVertexCell.getGeometry().getHeight() + 0.5)) / cellSize.y;
                if (y == 0 || y % vertexAlign.y != 0) {
                    vgVertexCell.getGeometry().setHeight(((y / vertexAlign.y) + 1) * vertexAlign.y * cellSize.y);
                }

                // make an odd number of cells for X coordinate
                if (!vgVertexCell.getVGObject().hasChildren()) {
                    if (((int) (vgVertexCell.getGeometry().getWidth() + 0.5) / cellSize.x) % 2 == 0)
                        vgVertexCell.getGeometry().setWidth(vgVertexCell.getGeometry().getWidth() + cellSize.x);
                }
            }
        }
    }
}
