package vg.plugins.layout.circle;

import com.mxgraph.layout.mxCircleLayout;
import vg.impl.main_service.VGMainServiceHelper;
import vg.interfaces.executor_service.ExecutorService;
import vg.interfaces.graph_layout_service.GraphLayout;
import vg.interfaces.graph_layout_service.GraphLayoutFactory;
import vg.interfaces.graph_layout_service.GraphLayoutSetting;
import vg.interfaces.graph_layout_service.GraphLayoutTemplate;
import vg.interfaces.graph_view_service.data.VGEdgeCell;
import vg.interfaces.graph_view_service.data.VGGraph;
import vg.interfaces.graph_view_service.data.VGVertexCell;
import vg.plugins.layout.BaseGraphLayout;
import vg.plugins.layout.BaseGraphLayoutFactory;
import vg.plugins.layout.BaseGraphLayoutTemplate;

import java.util.List;

/**
 * @author Timur Zolotuhin (e-mail: tzolotuhin@parallels.com)
 */
public class CircleLayoutFactory extends BaseGraphLayoutFactory {
    // Constants
    private final static String LAYOUT_NAME = "Circle layout";

    @Override
    public boolean isPortsSupport() {
        return false;
    }

    @Override
    public boolean isHierarchicalSupport() {
        return false;
    }

    @Override
    public GraphLayoutTemplate buildGraphLayoutTemplate() {
        return new CircleLayoutTemplate(this);
    }

    @Override
    public String getLayoutName() {
        return LAYOUT_NAME;
    }

    private class CircleLayoutTemplate extends BaseGraphLayoutTemplate {
        public CircleLayoutTemplate(GraphLayoutFactory graphLayoutFactoryCallBack) {
            super(graphLayoutFactoryCallBack);
        }

        @Override
        public GraphLayout buildGraphLayout() {
            synchronized (generalMutex) {
                return new CircleLayout(this, settings);
            }
        }
    }

    private static class CircleLayout extends BaseGraphLayout {
        public CircleLayout(GraphLayoutTemplate callback, List<GraphLayoutSetting> settings) {
            super(callback, settings);
        }

        @Override
        public void start(final VGGraph vgGraph, final GraphLayoutCallBack callBack) {
            try {
                int vertexBorder = 3; //getSettingByName(VERTEX_BORDER_KEY).getIntegerValue();
                int edgeBorder = 3; //getSettingByName(EDGE_BORDER_KEY).getIntegerValue();
                final String edgeStyle = null; //getSettingByName(EDGE_STYLE_KEY).getStringValue();
                final int gridCellSizeValue = 60;

                vgGraph.getGraphViewReference().setVertexBorderSize(vgGraph.getGraphViewReference().getVertexAttributes().keySet(), vertexBorder);
                vgGraph.getGraphViewReference().setEdgeBorderSize(vgGraph.getGraphViewReference().getEdgeAttributes().keySet(), edgeBorder);
                vgGraph.setGridSize(gridCellSizeValue);

                // execute layout
                doJGraphLayout(vgGraph, vgGraph.getVGVerticesByLevel(1), edgeStyle);

                // refresh view
                vgGraph.getGraphViewReference().optimizeCanvasSize();
                vgGraph.getGraphViewReference().refreshView();
            } catch (Throwable ex) {
                VGMainServiceHelper.logger.printException(ex);
                VGMainServiceHelper.windowMessenger.errorMessage("Something went wrong...", "Circle layout error", null);
            } finally {
                state.set(STOPPED_STATE);
                VGMainServiceHelper.executorService.execute(new ExecutorService.SwingExecutor() {
                    @Override
                    public void doInEDT() {
                        if (callBack != null)
                            callBack.onFinishAction(CircleLayout.this);
                    }
                });
            }
        }

//=============================================================================
//-----------------PRIVATE METHODS---------------------------------------------
        private static void doJGraphLayout(final VGGraph graph,
                                              List<VGVertexCell> vertices,
                                              String edgeStyle) {
            // build new temp graph for layout
            VGGraph tmpGraph = new VGGraph();
            for (VGVertexCell vertex : vertices) {
                VGVertexCell vertexCell = tmpGraph.vgInsertVertex(vertex.getVGObject(), tmpGraph.getDefaultParent());
                vertexCell.getGeometry().setWidth(vertex.getGeometry().getWidth());
                vertexCell.getGeometry().setHeight(vertex.getGeometry().getHeight());
            }

            int level = -1;
            if (!vertices.isEmpty()) {
                level = VGGraph.calcCellLevel(vertices.get(0));
            }

            for (VGEdgeCell edge : graph.getVGEdgesByLevel(level)) {
                if (VGGraph.isEdgeWithParentNode(edge))
                    continue;

                VGVertexCell srcParent = (VGVertexCell)VGGraph.getCellParentWithLevel(edge.getVGSoure(), VGGraph.calcCellLevel(edge));
                VGVertexCell trgParent = (VGVertexCell)VGGraph.getCellParentWithLevel(edge.getVGTarget(), VGGraph.calcCellLevel(edge));

                if (srcParent == null || trgParent == null)
                    continue;

                if (vertices.contains(srcParent) && vertices.contains(trgParent)) {
                    VGEdgeCell tmpEdgeCell = tmpGraph.vgInsertEdge(edge.getVGObject(),
                            tmpGraph.getDefaultParent(),
                            tmpGraph.getVGVertexCellByVertex(srcParent.getVGObject()),
                            tmpGraph.getVGVertexCellByVertex(trgParent.getVGObject()));
                    tmpEdgeCell.vgSetEdgeStyle(edgeStyle);
                }
            }

            // make layout for temp graph
            tmpGraph.setStylesheet(graph.getStylesheet());
            tmpGraph.setAllowNegativeCoordinates(false);

            mxCircleLayout layout = new mxCircleLayout(tmpGraph);
            layout.setResetEdges(true);
            layout.setDisableEdgeStyle(false);

            // execute layout
            layout.execute(tmpGraph.getDefaultParent());

            tmpGraph.removeNegativeCoordinates();

            // copy positions and sizes to the origin graph
            VGGraph.copyGeometry(tmpGraph, graph);
        }
    }
}
