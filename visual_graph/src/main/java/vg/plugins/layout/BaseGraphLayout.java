package vg.plugins.layout;

import com.google.common.collect.Lists;
import vg.shared.utils.RunAttemptsWithTimeout;
import vg.interfaces.graph_layout_service.GraphLayout;
import vg.interfaces.graph_layout_service.GraphLayoutSetting;
import vg.interfaces.graph_layout_service.GraphLayoutTemplate;
import vg.interfaces.graph_view_service.data.VGGraph;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Abstract class for layout with a lot of standard methods.
 *
 * @author Timur Zolotuhin (tzolotuhin@parallels.com)
 */
public abstract class BaseGraphLayout implements GraphLayout {
    // Mutex
    protected final Object generalMutex = new Object();

    // Main data
    protected List<GraphLayoutSetting> settings = Lists.newArrayList();

    protected AtomicInteger state = new AtomicInteger(STARTED_STATE);

    protected BaseGraphLayout(GraphLayoutTemplate callback, List<GraphLayoutSetting> settings) {
        this.settings = settings;
    }

    @Override
    public void start(VGGraph vgGraph) {
        start(vgGraph, null);
    }

    @Override
    public void stop() {
        state.set(STOPPED_IN_PROGRESS_STATE);
    }

    @Override
    public void stopAndWait() {
        stop();
        new RunAttemptsWithTimeout(Integer.MAX_VALUE, 10) {
            @Override
            public boolean attempt() {
                return state.get() == STOPPED_STATE;
            }
        };
    }

    @Override
    public int getState() {
        return state.get();
    }

    public GraphLayoutSetting getSettingByName(String name) {
        for (GraphLayoutSetting setting : settings) {
            if (setting.getName().equals(name))
                return setting;
        }
        return null;
    }
}
