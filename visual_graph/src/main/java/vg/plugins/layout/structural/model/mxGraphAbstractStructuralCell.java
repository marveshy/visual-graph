package vg.plugins.layout.structural.model;

import java.awt.*;

/**
 * Created by sunveil on 13.07.2014.
 */
public abstract class mxGraphAbstractStructuralCell {

    public Rectangle rectangle = new Rectangle();

    public final int hDist = 70;

    public final int wDist = 120;

    /**
     * Temporary variable for general use.
     */
    public int[] temp = new int[1];

    /**
     *
     * @return whether or not this cell is an edge
     */
    public abstract boolean isEdge();

    /**
     *
     * @return whether or not this cell is a node
     */
    public abstract boolean isVertex();

    /**
     *
     * @return geometry
     */
    public Rectangle getRectangle(){
        return getRectangle();
    };

    public int getWidth(){
        return this.rectangle.width;
    };

    public int getHeight(){
        return this.rectangle.height;
    };

    public int getCenterX() {
        return Math.round(this.getWidth() / 2 + Math.round(this.getX()));
    }

    public int getCenterY() {
        return Math.round(this.getHeight() / 2 + Math.round(this.getY()));
    }

    public double getX(){
        return this.rectangle.getX();
    };

    public double getY(){
        return this.rectangle.getY();
    };

    public void setLocation(int x, int y){
        this.rectangle.setLocation(x, y);
        //this.rectangle.width += x;
        //this.rectangle.height += y;
    };
    /**
     *
     * @return whether or not this cell is a node
     */
    public abstract String show();



}
