package vg.plugins.layout.structural.model;

import com.mxgraph.model.mxCell;

import java.awt.*;
import java.util.*;
import java.util.List;

/**
 * Created by sunveil on 13.07.2014.
 */
public class mxGraphStructuralNode extends mxGraphAbstractStructuralCell {

    public int xPosition = 0;

    public int yPosition = 0;

    public static int xLeft = 0;

    public static int yTop = 0;

    /**
     * Assigns a unique hashcode for each node. Used by the model dfs instead
     * of copying HashSets
     */
    public int[] hashCode;

    /**
     * Shared empty connection map to return instead of null
     */
    public static Collection<mxGraphStructuralEdge> emptyEdges = new ArrayList<mxGraphStructuralEdge>(0);

    /**
     * External cell representation
     */
    public Object node = null;

    /**
     * Immediate dominator
     */
    public mxGraphStructuralNode immediateDom = null;

    /**
     * DominanceFrontier
     */

    public HashSet<mxGraphStructuralNode> dominanceFrontier = new LinkedHashSet<mxGraphStructuralNode>();

    /**
     * List of children in the dominator tree.
     */

    public LinkedList<mxGraphStructuralNode> dominatorTreeChildren = new LinkedList<mxGraphStructuralNode>();

    /**
     * Collection of structural edges that have this node as a target
     */
    public Collection<mxGraphStructuralEdge> connectsAsTarget = emptyEdges;

    /**
     * Collection of structural edges that have this node as a source
     */
    public Collection<mxGraphStructuralEdge> connectsAsSource = emptyEdges;

    /**
     * Node number
     */
    public int num = 0;

    @Override
    public boolean isVertex(){return true;}

    @Override
    public boolean isEdge(){return false;}

    @Override
    public String show(){
        return new Integer(this.num).toString();
    };

    @Override
    public Rectangle getRectangle(){
        return this.rectangle;
    }

    public mxGraphStructuralNode(){
        this.node = null;
        //this.rectangle = ((mxCell) this.node).getGeometry().getRectangle();
    }

    public mxGraphStructuralNode(Object node){
        this.node = node;
        if(((mxCell)this.node).getGeometry() != null) {
            this.rectangle = ((mxCell) this.node).getGeometry().getRectangle();
        }
    }

    public mxGraphStructuralNode(mxGraphStructuralNode node){
        this.node = node.node;
        this.connectsAsTarget = node.connectsAsTarget;
        this.connectsAsSource = node.connectsAsSource;
        this.dominanceFrontier = node.dominanceFrontier;
        this.immediateDom = node.immediateDom;
        this.num =node.num;
        if(this.node!=null)
        if(((mxCell)this.node).getGeometry() != null) {
            this.rectangle = ((mxCell) this.node).getGeometry().getRectangle();
        }
    }

    public boolean isLoopHeader(){
        boolean result = false;
        for(mxGraphStructuralEdge edge: this.connectsAsSource) {
            mxGraphStructuralNode iDom = new mxGraphStructuralNode(this);
            while (!iDom.immediateDom.equals(iDom)) { // != <> equals
                if (iDom.immediateDom == this) {
                    edge.edgeType = mxGraphStructuralEdge.etBack;
                    return true;
                }
                iDom = iDom.immediateDom;
            }
        }
        return false;
    }


    public boolean isSelfLoop(){
        Collection<mxGraphStructuralEdge> currEdges = this.connectsAsSource;
        boolean result = false;
        for(mxGraphStructuralEdge edge: currEdges){
            if(edge.target == this){
                result = true;
            }
        }
        return result;
    }

    /**
     * Gets all successors (=targets of outgoing edges)
     * @return
     */

    public Queue<mxGraphStructuralNode> getAllSuccessors(){
        Queue<mxGraphStructuralNode> result = new LinkedList();
        Collection<mxGraphStructuralEdge> currEdges = this.connectsAsSource;
        for(mxGraphStructuralEdge edge: currEdges){
            result.add(edge.target);
        }
        return result;
    }

    /**
     * Gets all predecessors (=sources of incoming edges)
     * @return
     */

    public Queue<mxGraphStructuralNode> getAllPredecessors(){
        Queue<mxGraphStructuralNode> result = new LinkedList();
        Collection<mxGraphStructuralEdge> currEdges = this.connectsAsTarget;
        for(mxGraphStructuralEdge edge: currEdges){
            result.add(edge.source);
        }
        return result;
    }

    public boolean isAncestor(mxGraphStructuralNode otherNode){
        // Firstly, the hash code of this node needs to be shorter than the
        // other node
        if (otherNode != null && hashCode != null && otherNode.hashCode != null
                && hashCode.length < otherNode.hashCode.length){
            if (hashCode == otherNode.hashCode){
                return true;
            }

            if (hashCode == null){
                return false;
            }

            // Secondly, this hash code must match the start of the other
            // node's hash code. Arrays.equals cannot be used here since
            // the arrays are different length, and we do not want to
            // perform another array copy.
            for (int i = 0; i < hashCode.length; i++){
                if (hashCode[i] != otherNode.hashCode[i]){
                    return false;
                }
            }
            return true;
        }
        return false;
    }

}
