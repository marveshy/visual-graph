package vg.plugins.layout.structural.model;

import com.mxgraph.util.mxPoint;

import java.util.LinkedHashSet;

/**
 * Created by aamikhai on 25.07.2014.
 */
public class mxGraphStructuralSelfLoopBlock extends mxGraphStructuralNode{

    @Override
    public boolean isVertex(){
        return false;
    }

    @Override
    public boolean isEdge(){
        return false;
    }

    mxGraphStructuralNode selfLoop = null;

    public mxGraphStructuralSelfLoopBlock(mxGraphStructuralNode selfLoop){
        this.selfLoop = selfLoop;
        this.connectsAsTarget = new LinkedHashSet<mxGraphStructuralEdge>();
        this.connectsAsSource = new LinkedHashSet<mxGraphStructuralEdge>();
        int blockHeight = this.selfLoop.getHeight() + hDist;
        int blockWidth = this.getWidth() + wDist;
        this.rectangle.setBounds(0, 0, blockWidth, blockHeight);
    }

    @Override
    public String show(){
        for (mxGraphStructuralEdge edge : this.connectsAsTarget) {
            if (edge.source == edge.target) {
                this.getWidth();
                edge.geometry.add(new mxPoint(this.getWidth(), this.getHeight()));
                edge.geometry.add(new mxPoint(this.getWidth(), 0));
                this.connectsAsSource.remove(edge);
                this.connectsAsTarget.remove(edge);
            }
        }
        return  this.selfLoop.show();
    };
}
