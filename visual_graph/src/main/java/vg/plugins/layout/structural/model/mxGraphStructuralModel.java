package vg.plugins.layout.structural.model;

import com.mxgraph.layout.hierarchical.model.mxGraphHierarchyEdge;
import com.mxgraph.layout.hierarchical.model.mxGraphHierarchyNode;
import com.mxgraph.model.mxCell;
import com.mxgraph.view.mxGraph;
import vg.plugins.layout.structural.mxStructuralLayout;

import java.util.*;

/**
 * Created by sunveil on 13.07.2014.
 */
public class mxGraphStructuralModel {

    public int x0 = 10;

    public int y0 = 10;

    /**
     * Expr
     */

    public mxGraphStructuralNode region = null;

    /**
     * Map from graph vertices to internal model nodes
     */
    protected Map<Object, mxGraphStructuralNode> vertexMapper = null;

    /**
     * Map from graph edges to internal model edges
     */
    protected Map<Object, mxGraphStructuralEdge> edgeMapper = null;

    /**
     * Regular entry point
     * needs to compute dominance tree
     */
    public mxGraphStructuralNode entryPoint = new mxGraphStructuralNode(new mxCell());

    /**
     * Store of roots of this hierarchy model, these are real graph cells, not
     * internal cells
     */
    public List<Object> roots;

    /**
     * The parent cell whose children are being laid out
     */
    public Object parent = null;

    /**
     * Count of the number of times the ancestor dfs has been used
     */
    protected int dfsCount = 0;

    protected int num = 0;


    /**
     * A depth first search through the internal hierarchy model
     *
     * @param visitor
     *            the visitor pattern to be called for each node
     * @param trackAncestors
     *            whether or not the search is to keep track all nodes directly
     *            above this one in the search path
     */
    public void dfsVisit(dfsCellVisitor visitor, mxGraphStructuralNode[] dfsRoots,
                      boolean trackAncestors, Set<mxGraphStructuralNode> seenNodes){
        // Run dfs through on all roots
        if (dfsRoots != null){
            for (int i = 0; i < dfsRoots.length; i++){
                mxGraphStructuralNode internalNode = dfsRoots[i];

                if (internalNode != null){
                    if (seenNodes == null){
                        seenNodes = new HashSet<mxGraphStructuralNode>();
                    }

                    if (trackAncestors){
                        // Set up hash code for root
                        internalNode.hashCode = new int[2];
                        internalNode.hashCode[0] = dfsCount;
                        internalNode.hashCode[1] = i;
                        dfs(null, internalNode, null, visitor, seenNodes, internalNode.hashCode, i, 0);
                    }else{
                        dfs(null, internalNode, null, visitor, seenNodes, 0);
                    }
                }
            }

            dfsCount++;
        }
    }

    /**
     * Performs a depth first search on the internal hierarchy model
     *
     * @param parent
     *            the parent internal node of the current internal node
     * @param root
     *            the current internal node
     * @param connectingEdge
     *            the internal edge connecting the internal node and the parent
     *            internal node, if any
     * @param visitor
     *            the visitor pattern to be called for each node
     * @param seen
     *            a set of all nodes seen by this dfs a set of all of the
     *            ancestor node of the current node
     * @param layer
     *            the layer on the dfs tree ( not the same as the model ranks )
     */
    public void dfs(mxGraphStructuralNode parent, mxGraphStructuralNode root,
                    mxGraphStructuralEdge connectingEdge, dfsCellVisitor visitor,
                    Set<mxGraphStructuralNode> seen, int layer){
        if (root != null){
            if (!seen.contains(root)){
                visitor.visit(root, connectingEdge, layer, 0);
                seen.add(root);

                // Copy the connects as source list so that visitors
                // can change the original for edge direction inversions
                final Object[] outgoingEdges = root.connectsAsSource.toArray();

                for (int i = 0; i < outgoingEdges.length; i++){
                    mxGraphStructuralEdge internalEdge = (mxGraphStructuralEdge) outgoingEdges[i];
                    mxGraphStructuralNode targetNode = internalEdge.target;

                    // Root check is O(|roots|)
                    dfs(root, targetNode, internalEdge, visitor, seen, layer + 1);
                }
            }else{
                // Use the int field to indicate this node has been seen
                visitor.visit(root, connectingEdge, layer, 1);
            }
        }
    }

    /**
     * Performs a depth first search on the internal hierarchy model. This dfs
     * extends the default version by keeping track of cells ancestors, but it
     * should be only used when necessary because of it can be computationally
     * intensive for deep searches.
     *
     * @param parent
     *            the parent internal node of the current internal node
     * @param root
     *            the current internal node
     * @param connectingEdge
     *            the internal edge connecting the internal node and the parent
     *            internal node, if any
     * @param visitor
     *            the visitor pattern to be called for each node
     * @param seen
     *            a set of all nodes seen by this dfs
     * @param ancestors
     *            the parent hash code
     * @param childHash
     *            the new hash code for this node
     * @param layer
     *            the layer on the dfs tree ( not the same as the model ranks )
     */
    public void dfs(mxGraphStructuralNode parent, mxGraphStructuralNode root,
                    mxGraphStructuralEdge connectingEdge, dfsCellVisitor visitor,
                    Set<mxGraphStructuralNode> seen, int[] ancestors, int childHash,
                    int layer){
        if (root != null){
            if (parent != null){
                if (root.hashCode == null || root.hashCode[0] != parent.hashCode[0]){
                    int hashCodeLength = parent.hashCode.length + 1;
                    root.hashCode = new int[hashCodeLength];
                    System.arraycopy(parent.hashCode, 0, root.hashCode, 0, parent.hashCode.length);
                    root.hashCode[hashCodeLength - 1] = childHash;
                }
            }

            if (!seen.contains(root)){
                visitor.visit(root, connectingEdge, layer, 0);
                seen.add(root);
                //root.num += root.hashCode.length;
                //for(int j = 0; j < root.hashCode.length; j++){
                //    root.num += root.hashCode[j];
                //}
                //root.num = dfsCount;
                final Object[] outgoingEdges = root.connectsAsSource.toArray();

                for (int i = 0; i < outgoingEdges.length; i++){
                    mxGraphStructuralEdge internalEdge = (mxGraphStructuralEdge) outgoingEdges[i];
                    mxGraphStructuralNode targetNode = internalEdge.target;

                    dfs(root, targetNode, internalEdge, visitor, seen, root.hashCode, i, layer + 1);
                }
            }
            else
            {
                visitor.visit(root, connectingEdge, layer, 1);
            }
        }
    }



    /**
     *
     * @param startVertex
     */

    public void bfs(mxGraphStructuralNode startVertex, bfsCellVisitor visitor){
        if (startVertex != null){
            Set<mxGraphStructuralNode> queued = new HashSet<mxGraphStructuralNode>();
            LinkedList<mxGraphStructuralNode> queue = new LinkedList<mxGraphStructuralNode>();
            int bfsLevel = 0;
            visitor.visit(startVertex);
            queue.addLast(startVertex);
            queued.add(startVertex);
            startVertex.num = bfsLevel;
            bfsRec(queued, queue, visitor, ++bfsLevel);
        }
    };

    /**
     *
     * @param queued
     * @param queue
     * @param bfsLevel
     */
    private void bfsRec(Set<mxGraphStructuralNode> queued, LinkedList<mxGraphStructuralNode> queue, bfsCellVisitor visitor, int bfsLevel){
        if (queue.size() > 0){
            mxGraphStructuralNode q = queue.removeFirst();
            mxGraphStructuralNode node = q;
            node.num = bfsLevel;
            final Collection<mxGraphStructuralEdge> edges = node.connectsAsSource;
            for (mxGraphStructuralEdge edge: edges){
                if (!queued.contains(edge.target)){
                    queue.addLast(edge.target);
                }
            }
            visitor.visit(node);
            queued.add(node);
            bfsRec(queued,  queue, visitor, ++bfsLevel);
        }
    };

    /**
     * Defines the interface that visitors use to perform operations upon the
     * graph information during depth first search (dfs) or other tree-traversal
     * strategies implemented by subclassers.
     */

    public interface dfsCellVisitor{

            /**
             * The method within which the visitor will perform operations upon the
             * graph model
             *
             //* @param parent
             *            the parent cell the current cell
             * @param cell
             *            the current cell visited
             * @param connectingEdge
             *            the edge that led the last cell visited to this cell
             * @param layer
             *            the current layer of the tree
             * @param seen
             *            an int indicating whether this cell has been seen
             *            previously
             */
            public void visit(mxGraphStructuralNode cell, mxGraphStructuralEdge connectingEdge, int layer, int seen);
        }

    public interface bfsCellVisitor{

        /**
         * The method within which the visitor will perform operations upon the
         * graph model
         *
         //* @param parent
         *            the parent cell the current cell
         * @param cell
         *            the current cell visited
         //* @param connectingEdge
         *            the edge that led the last cell visited to this cell
         //* @param seen
         *            an int indicating whether this cell has been seen
         *            previously
         */
        //public void visit(mxGraphStructuralNode cell);
        public void visit(mxGraphStructuralNode cell);
    }

    /**
     * A bfs through the internal structural model
     *
     * @param visitor
     *            the visitor pattern to be called for each node
     *            whether or not the search is to keep track all nodes directly
     *            above this one in the search path
     */
    public void bfsVisit(bfsCellVisitor visitor, mxGraphStructuralNode[] bfsRoots,
                      Set<mxGraphStructuralNode> seenNodes)
    {
        // Run dfs through on all roots
        if (bfsRoots != null){
            for (int i = 0; i < bfsRoots.length; i++) {
                mxGraphStructuralNode internalNode = bfsRoots[0];

                if (internalNode != null) {
                    if (seenNodes == null) {
                        seenNodes = new HashSet<mxGraphStructuralNode>();
                    }

                    bfs(internalNode, visitor);

                }
           }
        }
    }

    /**
     * @return Returns the vertexMapping.
     */
    public Map<Object, mxGraphStructuralNode> getVertexMapper(){
        if (vertexMapper == null){
            vertexMapper = new Hashtable<Object, mxGraphStructuralNode>();
        }
        return vertexMapper;
    }

    public Map<Object, mxGraphStructuralNode> getCopyVertexMapper(){
        if (vertexMapper == null){
            vertexMapper = new Hashtable<Object, mxGraphStructuralNode>();
        }
        return new Hashtable<Object, mxGraphStructuralNode>(vertexMapper);
    }

     /**
     * @param vertexMapping
     * The vertexMapping to set.
     */
    public void setVertexMapper(Map<Object, mxGraphStructuralNode> vertexMapping){
        this.vertexMapper = vertexMapping;
    }

    /**
     * @return Returns the edgeMapper.
     */
    public Map<Object, mxGraphStructuralEdge> getEdgeMapper(){
        return edgeMapper;
    }

    /**
     * @param edgeMapper
     * The edgeMapper to set.
     */
    public void setEdgeMapper(TreeMap<Object, mxGraphStructuralEdge> edgeMapper){
        this.edgeMapper = edgeMapper;
    }

    protected void createInternalCells(mxStructuralLayout layout, Object[] vertices, mxGraphStructuralNode[] internalVertices){
        mxGraph graph = layout.getGraph();

        // Create internal edges
        for (int i = 0; i < vertices.length; i++){

            internalVertices[i] = new mxGraphStructuralNode(vertices[i]);
            vertexMapper.put(vertices[i], internalVertices[i]);

            // If the layout is deterministic, order the cells
            Object[] conns = layout.getEdges(vertices[i]);
            List<Object> outgoingCells = Arrays.asList(graph.getOpposites(conns, vertices[i]));
            internalVertices[i].connectsAsSource = new LinkedHashSet<mxGraphStructuralEdge>(outgoingCells.size());

            // Create internal edges, but don't do any rank assignment yet
            // First use the information from the greedy cycle remover to
            // invert the leftward edges internally
            Iterator<Object> iter = outgoingCells.iterator();

            while (iter.hasNext()) {
                // Don't add self-loops
                Object cell = iter.next();

                if (cell != vertices[i] && graph.getModel().isVertex(cell) && !layout.isVertexIgnored(cell)){
                    // We process all edge between this source and its targets
                    // If there are edges going both ways, we need to collect
                    // them all into one internal edges to avoid looping problems
                    // later. We assume this direction (source -> target) is the
                    // natural direction if at least half the edges are going in
                    // that direction.

                    // The check below for edgeMapper.get(edges[0]) == null is
                    // in case we've processed this the other way around
                    // (target -> source) and the number of edges in each direction
                    // are the same. All the graph edges will have been assigned to
                    // an internal edge going the other way, so we don't want to
                    // process them again
                    Object[] undirectEdges = graph.getEdgesBetween(vertices[i], cell, false);
                    Object[] directedEdges = graph.getEdgesBetween(vertices[i], cell, true);

                    if (undirectEdges != null && undirectEdges.length > 0 /*&& (edgeMapper.get(undirectEdges[0]) == null)*/
                            && (directedEdges.length * 2 >= undirectEdges.length)){

                        ArrayList<Object> listEdges = new ArrayList<Object>(undirectEdges.length);

                        for (int j = 0; j < undirectEdges.length; j++){
                            listEdges.add(undirectEdges[j]);
                        }

                        mxGraphStructuralEdge internalEdge = new mxGraphStructuralEdge(listEdges);
                        Iterator<Object> iter2 = listEdges.iterator();

                        while (iter2.hasNext()){
                            Object edge = iter2.next();
                            edgeMapper.put(edge, internalEdge);

                            // Resets all point on the edge and disables the edge style
                            // without deleting it from the cell style
                            graph.resetEdge(edge);

                            //if (layout.isDisableEdgeStyle()){
                            //    layout.setEdgeStyleEnabled(edge, false);
                            //    layout.setOrthogonalEdge(edge, true);
                            //}
                        }

                        internalEdge.source = internalVertices[i];
                        internalVertices[i].connectsAsSource.add(internalEdge);
                    }
                }
            }

            // Ensure temp variable is cleared from any previous use
            internalVertices[i].temp[0] = 0;
        }
    }

    public mxGraphStructuralModel(mxStructuralLayout layout, Object[] vertices, List<Object> roots, Object parent) {

        mxGraph graph = layout.getGraph();
        this.roots = roots;
        this.parent = parent;
        //this.

        if (vertices == null){
            vertices = graph.getChildVertices(parent);
        }

        // map of cells to internal cell needed for second run through
        // to setup the sink of edges correctly. Guess size by number
        // of edges is roughly same as number of vertices.
        vertexMapper = new Hashtable<Object, mxGraphStructuralNode>(vertices.length);
        edgeMapper = new Hashtable<Object, mxGraphStructuralEdge>(vertices.length);

        mxGraphStructuralNode[] internalVertices = new mxGraphStructuralNode[vertices.length];
        createInternalCells(layout, vertices, internalVertices);

        // Go through edges set their sink values. Also check the
        // ordering if and invert edges if necessary
        for (int i = 0; i < vertices.length; i++){
            Collection<mxGraphStructuralEdge> edges = internalVertices[i].connectsAsSource;
            Iterator<mxGraphStructuralEdge> iter = edges.iterator();

            while (iter.hasNext()){
                mxGraphStructuralEdge internalEdge = iter.next();
                Collection<Object> realEdges = internalEdge.edges;
                Iterator<Object> iter2 = realEdges.iterator();

                // Only need to process the first real edge, since
                // all the edges connect to the same other vertex
                if (iter2.hasNext()){
                    Object realEdge = iter2.next();
                    Object targetCell = graph.getView().getVisibleTerminal(realEdge, false);
                    mxGraphStructuralNode internalTargetCell = vertexMapper.get(targetCell);

                    if (internalVertices[i] == internalTargetCell){
                        // The real edge is reversed relative to the internal edge
                        targetCell = graph.getView().getVisibleTerminal(realEdge, true);
                        internalTargetCell = vertexMapper.get(targetCell);
                    }

                    if (internalTargetCell != null && internalVertices[i] != internalTargetCell){
                        internalEdge.target = internalTargetCell;

                        if (internalTargetCell.connectsAsTarget.size() == 0){
                            internalTargetCell.connectsAsTarget = new LinkedHashSet<mxGraphStructuralEdge>(4);
                        }

                        internalTargetCell.connectsAsTarget.add(internalEdge);
                    }
                }
            }

            // Use the temp variable in the internal nodes to mark this
            // internal vertex as having been visited.
            internalVertices[i].temp[0] = 1;
        }

    }

}
