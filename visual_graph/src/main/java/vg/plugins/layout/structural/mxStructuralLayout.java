package vg.plugins.layout.structural;

import com.mxgraph.layout.mxGraphLayout;
import com.mxgraph.model.mxCell;
import com.mxgraph.model.mxGraphModel;
import com.mxgraph.model.mxIGraphModel;
import com.mxgraph.util.mxConstants;
import com.mxgraph.util.mxPoint;
import com.mxgraph.view.*;
import vg.plugins.layout.structural.model.mxGraphStructuralEdge;
import vg.plugins.layout.structural.model.mxGraphStructuralModel;
import vg.plugins.layout.structural.model.mxGraphStructuralNode;
import vg.plugins.layout.structural.stage.mxComputeDominace;
import vg.plugins.layout.structural.stage.mxStructural;

import java.util.*;

/**
 * Created by sunveil on 13.07.2014.
 */
public class mxStructuralLayout extends mxGraphLayout {

    /** The root nodes of the layout */
    protected List<Object> roots = null;

    /**
     * The internal model formed of the layout
     */
    protected mxGraphStructuralModel model = null;

    /**
     * Whether or not to navigate edges whose terminal vertices
     * have different parents but are in the same ancestry chain
     */
    protected boolean traverseAncestors = true;

    /**
     *  Specifies if the STYLE_NOEDGESTYLE flag should be set on edges that are
     * modified by the result. Default is true.
     */
    protected boolean disableEdgeStyle = true;

    /**
     *
     */
    public boolean isDisableEdgeStyle(){
        return disableEdgeStyle;
    }

    /**
     * Returns the model for this layout algorithm.
     */
    public mxGraphStructuralModel getModel(){
        return this.model;
    }

    /**
     *
     * @param disableEdgeStyle
     */
    public void setDisableEdgeStyle(boolean disableEdgeStyle){
        this.disableEdgeStyle = disableEdgeStyle;
    }

    /**
     * Creates a set of descendant cells
     * @param cell The cell whose descendants are to be calculated
     * @return the descendants of the cell (not the cell)
     */
    public Set<Object> filterDescendants(Object cell){
        mxIGraphModel model = graph.getModel();
        Set<Object> result = new LinkedHashSet<Object>();

        if (model.isVertex(cell) && cell != this.parent && model.isVisible(cell)){
            result.add(cell);
        }

        if (this.traverseAncestors || cell == this.parent && model.isVisible(cell)){
            int childCount = model.getChildCount(cell);

            for (int i = 0; i < childCount; i++){
                Object child = model.getChildAt(cell, i);
                result.addAll(filterDescendants(child));
            }
        }

        return result;
    }

    protected void traverse(Object vertex, boolean directed, Object edge,
                            Set<Object> allVertices, Set<Object> currentComp,
                            List<Set<Object>> structuralVertices, Set<Object> filledVertexSet)
    {
        mxGraphView view = graph.getView();
        mxIGraphModel model = graph.getModel();

        if (vertex != null && allVertices != null){
            // Has this vertex been seen before in any traversal
            // And if the filled vertex set is populated, only
            // process vertices in that it contains
            if (!allVertices.contains(vertex) && (filledVertexSet == null ? true : filledVertexSet.contains(vertex))){
                currentComp.add(vertex);
                allVertices.add(vertex);

                if (filledVertexSet != null){
                    filledVertexSet.remove(vertex);
                }

                int edgeCount = model.getEdgeCount(vertex);

                if (edgeCount > 0){
                    for (int i = 0; i < edgeCount; i++){
                        Object e = model.getEdgeAt(vertex, i);
                        boolean isSource = view.getVisibleTerminal(e, true) == vertex;

                        if (!directed || isSource){
                            Object next = view.getVisibleTerminal(e, !isSource);
                            traverse(next, directed, e, allVertices,
                                    currentComp, structuralVertices,
                                    filledVertexSet);
                        }
                    }
                }
            }else{
                if (!currentComp.contains(vertex)){
                    // We've seen this vertex before, but not in the current component
                    // This component and the one it's in need to be merged
                    Set<Object> matchComp = null;

                    for (Set<Object> comp : structuralVertices){
                        if (comp.contains(vertex)){
                            currentComp.addAll(comp);
                            matchComp = comp;
                            break;
                        }
                    }

                    if (matchComp != null){
                        structuralVertices.remove(matchComp);
                    }
                }
            }
        }
    }

    /**
     * Returns all visible children in the given parent which do not have
     * incoming edges. If the result is empty then the children with the
     * maximum difference between incoming and outgoing edges are returned.
     * This takes into account edges that are being promoted to the given
     * root due to invisible children or collapsed cells.
     *
     * @param parent Cell whose children should be checked.
     * @return List of tree roots in parent.
     */
    public List<Object> findRoots(Object parent, Set<Object> vertices){
        List<Object> roots = new ArrayList<Object>();

        Object best = null;
        int maxDiff = -100000;
        mxIGraphModel model = graph.getModel();

        for (Object vertex : vertices){
            if (model.isVertex(vertex) && graph.isCellVisible(vertex)){
                Object[] conns = this.getEdges(vertex);
                int fanOut = 0;
                int fanIn = 0;

                for (int k = 0; k < conns.length; k++){
                    Object src = graph.getView().getVisibleTerminal(conns[k], true);

                    if (src == vertex){
                        fanOut++;
                    }else{
                        fanIn++;
                    }
                }

                if (fanIn == 0 && fanOut > 0){
                    roots.add(vertex);
                }

                int diff = fanOut - fanIn;

                if (diff > maxDiff){
                    maxDiff = diff;
                    best = vertex;
                }
            }
        }

        if (roots.isEmpty() && best != null){
            roots.add(best);
        }

        return roots;
    }

    /**
     * Constructs a new structural layout for the specified graph.
     *
     * @param graph
     */
    public mxStructuralLayout(mxGraph graph) {
        super(graph);
        //this.model = graph.getModel();
    }

    public void dominanceStage(Object parent){
        mxComputeDominace dominanceStage = new mxComputeDominace(this);
        dominanceStage.execute(parent);
    }

    public void structuralStage(Object parent){
        mxStructural strcuturalStage = new mxStructural(this);
        strcuturalStage.execute(parent);
    }

    public void execute(Object parent){
        super.execute(parent);
        this.execute(parent, null);
    }

    public void execute(Object parent, List<Object> roots) {
        super.execute(parent);
        mxIGraphModel model = graph.getModel();

        // If the roots are set and the parent is set, only
        // use the roots that are some dependent of the that
        // parent.
        // If just the root are set, use them as-is
        // If just the parent is set use it's immediate
        // children as the initial set

        if (roots == null && parent == null) {
            // TODO indicate the problem
            return;
        }

        if (roots != null && parent != null) {
            for (Object root : roots) {
                if (!model.isAncestor(parent, root)) {
                    roots.remove(root);
                }
            }
        }
        this.roots = roots;

        model.beginUpdate();
        try{
            run(parent);
        }finally {
            model.endUpdate();
        }
    }

    /**
     * The API method used to exercise the layout upon the graph description
     * and produce a separate description of the vertex position and edge
     * routing changes made.
     */

    public void run(Object parent){
        // Separate out unconnected hierarchies
        List<Set<Object>> structuralVertices = new ArrayList<Set<Object>>();
        Set<Object> allVertexSet = new LinkedHashSet<Object>();

        if (this.roots == null && parent != null){
            Set<Object> filledVertexSet = filterDescendants(parent);

            this.roots = new ArrayList<Object>();

            while (!filledVertexSet.isEmpty()){
                List<Object> candidateRoots = findRoots(parent, filledVertexSet);

                for (Object root : candidateRoots){
                    Set<Object> vertexSet = new LinkedHashSet<Object>();
                    structuralVertices.add(vertexSet);

                    traverse(root, true, null, allVertexSet, vertexSet,
                            structuralVertices, filledVertexSet);
                }

                this.roots.addAll(candidateRoots);
            }
        }else{
            // Find vertex set as directed traversal from roots

            for (int i = 0; i < roots.size(); i++){
                Set<Object> vertexSet = new LinkedHashSet<Object>();
                structuralVertices.add(vertexSet);

                traverse(roots.get(i), true, null, allVertexSet, vertexSet,
                        structuralVertices, null);
            }
        }

        // Iterate through the result removing parents who have children in this layout
        Iterator<Set<Object>> iter = structuralVertices.iterator();

        while (iter.hasNext()){
            Set<Object> vertexSet = iter.next();
            this.model = new mxGraphStructuralModel(this, vertexSet.toArray(), roots, parent);
            dominanceStage(parent);
            structuralStage(parent);
            this.model.region.show();
            line(vertexSet.toArray(), 100, 100);
        }
    }

    public void line(Object[] vertices, double left, double top){
        int vertexCount = vertices.length;
        final Set<mxGraphStructuralNode> allNodes = new HashSet<mxGraphStructuralNode>(model.getVertexMapper().values());
        final Set<mxGraphStructuralEdge> allEdges = new HashSet<mxGraphStructuralEdge>(model.getEdgeMapper().values());
        mxStylesheet edgeStyle = new mxStylesheet();
        for(mxGraphStructuralEdge edge: allEdges){
            switch (edge.edgeType) {
                case mxGraphStructuralEdge.etBack: {
                    this.getGraph().getModel().setStyle(edge.edges.get(0),"edgeStyle=orthogonalEdgeStyle");
                    List<mxPoint> points = new LinkedList<mxPoint>();
                    mxPoint p = new mxPoint(edge.source.getX() + edge.source.getWidth() + 15, edge.source.getY() /* + edge.source.getHeight()*/);
                    mxPoint p1 = new mxPoint(edge.target.getX() + edge.target.getWidth() + 15, edge.target.getY() + edge.target.getHeight());
                    points.add(p1);
                    points.add(p);
                    this.setEdgePoints(edge.edges.get(0), points);
                }
            }
        }
        for (mxGraphStructuralNode node: allNodes){
            if (isVertexMovable(node.node)){
                ((mxCell)node.node).setAttribute("Num",new Integer(node.num).toString());
                setVertexLocation(node.node, node.getX(), node.getY());
            }
        }
    }

    /**
     *
     * @param cell
     * @return
     */
    public Object[] getEdges(Object cell){
        mxIGraphModel model = graph.getModel();
        boolean isCollapsed = graph.isCellCollapsed(cell);
        List<Object> edges = new ArrayList<Object>();
        int childCount = model.getChildCount(cell);

        for (int i = 0; i < childCount; i++){
            Object child = model.getChildAt(cell, i);

            if (isCollapsed || !graph.isCellVisible(child)){
                edges.addAll(Arrays.asList(mxGraphModel.getEdges(model, child, true, true, false)));
            }
        }

        edges.addAll(Arrays.asList(mxGraphModel.getEdges(model, cell, true, true, false)));
        List<Object> result = new ArrayList<Object>(edges.size());
        Iterator<Object> it = edges.iterator();

        while (it.hasNext()){
            Object edge = it.next();
            mxCellState state = graph.getView().getState(edge);
            Object source = (state != null) ? state.getVisibleTerminal(true) : graph.getView().getVisibleTerminal(edge, true);
            Object target = (state != null) ? state.getVisibleTerminal(false) : graph.getView().getVisibleTerminal(edge, false);

            if (((source != target) && ((target == cell && (parent == null || graph
                    .isValidAncestor(source, parent, traverseAncestors))) || (source == cell && (parent == null || graph
                    .isValidAncestor(target, parent, traverseAncestors)))))){
                result.add(edge);
            }
        }

        return result.toArray();
    }

}
