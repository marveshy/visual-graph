package vg.plugins.layout.structural.model;

import com.mxgraph.util.mxPoint;

import java.awt.*;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by sunveil on 13.07.2014.
 */
public class mxGraphStructuralEdge extends mxGraphAbstractStructuralCell{

    public static final int etUndefined = 0;
    public static final int etNormal = 1;
    public static final int etBack = 2;
    public static final int etCurve = 3;

    /**
     * Edger geometry points
     */

    public List<mxPoint> geometry = new LinkedList<mxPoint>();

    /**
     * Edge type (back edge, normal, curve)
     */

    public int edgeType = etUndefined;

    /**
     * The graph edge this object represents.
     */
    public List<Object> edges = null;

    /**
     * The node this edge is sourced at
     */
    public mxGraphStructuralNode source = null;

    /**
     * The node this edge targets
     */
    public mxGraphStructuralNode target = null;

    @Override
    public boolean isVertex(){
        return false;
    }

    @Override
    public boolean isEdge(){
        return true;
    }

    @Override
    public String show(){
        return "Edge";
    }

    /**
     * Constructs a structural edge
     * @param edges the edge of this abstraction represents
     */

    mxGraphStructuralEdge(List<Object> edges){this.edges = edges;}

    public mxGraphStructuralEdge(){
    ;}
}
