package vg.plugins.layout.structural.stage;

import com.mxgraph.view.mxGraph;
import vg.impl.main_service.VGMainServiceHelper;
import vg.plugins.layout.structural.model.*;
import vg.plugins.layout.structural.mxStructuralLayout;


import java.lang.reflect.Array;
import java.util.*;

/**
 * Created by aamikhai on 14.07.2014.
 */
public class mxComputeDominace implements mxStructuralLyaoutStage{

     /**
     * Reference to the enclosing layout algorithm
     */
     protected mxStructuralLayout layout;

     /**
     * @param layout
     * Constructor that has the roots specified
     */
     public mxComputeDominace(mxStructuralLayout layout){
        this.layout = layout;
     }

     /**
     * @param roots
     * A Simple, Fast Dominance Algorithm
     * Keith D. Cooper, Timothy J. Harvey, and Ken Kennedy
     */
    public void buildDomTree(mxGraphStructuralNode[] roots){
        mxGraphStructuralModel model = this.layout.getModel();
        final LinkedHashSet<mxGraphStructuralNode> seenNodes = new LinkedHashSet<mxGraphStructuralNode>();
        final Set<mxGraphStructuralNode> unseenNodes = new HashSet<mxGraphStructuralNode>(model.getVertexMapper().values());
        final Set<mxGraphStructuralEdge> allEdges = new HashSet<mxGraphStructuralEdge>(model.getEdgeMapper().values());

        mxGraphStructuralNode root = roots[0];
        model.dfsVisit(new mxGraphStructuralModel.dfsCellVisitor(){
            public void visit(mxGraphStructuralNode cell, mxGraphStructuralEdge connectingEdge,
                              int layer, int seen){
                seenNodes.add(cell);
                unseenNodes.remove(cell);
                cell.temp[0] = 0;
            }
        }, roots, true , null);
        for(mxGraphStructuralNode node: seenNodes){
            VGMainServiceHelper.logger.printInfo(new Integer(node.hashCode.length).toString() + " ");
        }
        /*VGMainServiceHelper.logger.printInfo("hash codes:");
        for(mxGraphStructuralNode node: seenNodes){
            VGMainServiceHelper.logger.printInfo(new Integer(node.num).toString());
        }
        */
        root = roots[0];
        model.bfsVisit(new mxGraphStructuralModel.bfsCellVisitor(){
            public void visit(mxGraphStructuralNode cell){
                seenNodes.add(cell);
                unseenNodes.remove(cell);
                cell.temp[0] = 0;
            }
        }, roots , null);

        LinkedHashSet<mxGraphStructuralNode> allNodesWithoutRoot = new LinkedHashSet<mxGraphStructuralNode>(seenNodes);
        root = roots[0];
        root.immediateDom = root;
        root.immediateDom.num = 1;
        root.temp[0] = 1;
        allNodesWithoutRoot.remove(root);
        boolean changed = true;
        while(changed) {
            changed = false;
            //For all nodes, b, in reverse postorder (except start_node)
            for(mxGraphStructuralNode bNode: allNodesWithoutRoot) {
                //All predecessors of this node
                Queue<mxGraphStructuralNode> preds = bNode.getAllPredecessors();
                mxGraphStructuralNode newIDom = getFirstProcessedPredecessor(preds);
                //For all other predecessors, p, of b
                Iterator iter = preds.iterator();
                while(iter.hasNext()){
                    mxGraphStructuralNode pNode = (mxGraphStructuralNode)iter.next();
                    if(pNode.immediateDom != null){ /* i. e., if doms[p] already calculated */
                        newIDom = intersect(pNode, newIDom);
                    }
                }
                if(bNode.immediateDom != newIDom){
                    bNode.immediateDom = newIDom;
                        changed = true;
                }
                // Use the temp variable in the internal nodes to mark this
                // internal vertex as having been visited.
                bNode.temp[0] = 1;
            }
        }

        LinkedHashSet<mxGraphStructuralNode> allNodes = new LinkedHashSet<mxGraphStructuralNode>(allNodesWithoutRoot);
        allNodes.add(root);
        computeDominanceFrontier(allNodes);
        root.immediateDom = null;

        // Fill list of children in the dominator tree
        for(mxGraphStructuralNode node: allNodes){
            if(node.immediateDom != null){
                node.immediateDom.dominatorTreeChildren.add(node);
            }
        }

        VGMainServiceHelper.logger.printInfo("start curve");
        for(mxGraphStructuralEdge edge: allEdges){
            if(!edge.source.isAncestor(edge.target) && !edge.target.isAncestor(edge.source)){
                VGMainServiceHelper.logger.printInfo(new Integer(edge.source.num).toString());
                VGMainServiceHelper.logger.printInfo(new Integer(edge.target.num).toString());
            }
        }
        VGMainServiceHelper.logger.printInfo("end curve");

        root.immediateDom = root;
        VGMainServiceHelper.logger.printInfo("Doms: ");
        for(mxGraphStructuralNode node: allNodes) {
            VGMainServiceHelper.logger.printInfo("Node: " + new Integer(node.num).toString() + " Dom: " +
                new Integer(node.immediateDom.num).toString() + " Doms frointier: ");
            //for(mxGraphStructuralNode df: node.dominanceFrontier){
            //    VGMainServiceHelper.logger.printInfo(new Integer(df.num).toString() + " ");
           // }
        }

        VGMainServiceHelper.logger.printInfo("Total nodes Cnt: " + new Integer(allNodes.size()).toString());

        final LinkedHashSet<mxGraphStructuralNode> queueNodes = new LinkedHashSet<mxGraphStructuralNode>();
        model.bfsVisit(new mxGraphStructuralModel.bfsCellVisitor(){
            public void visit(mxGraphStructuralNode cell){
                queueNodes.add(cell);
            }
        }, roots , null);

        VGMainServiceHelper.logger.printInfo("Total nodes Cnt: " + new Integer(allNodes.size()).toString());

    }

    public mxGraphStructuralNode intersect(mxGraphStructuralNode p, mxGraphStructuralNode  newIDom){
        mxGraphStructuralNode finger1 = new mxGraphStructuralNode(p);
        mxGraphStructuralNode finger2 = new mxGraphStructuralNode(newIDom);
        while(finger1.num != finger2.num){
            while(finger1.num > finger2.num){
                finger1 = finger1.immediateDom;
            }
            while(finger2.num > finger1.num){
                finger2 = finger2.immediateDom;
            }
        }
        return finger1;
    }


    /** Computes dominance frontiers.
    / This method requires that the dominator tree is already computed!
    */
    public void computeDominanceFrontier(LinkedHashSet<mxGraphStructuralNode> allNodes){
        for(mxGraphStructuralNode node : allNodes) {
            node.dominanceFrontier = new HashSet<mxGraphStructuralNode>();
            for (mxGraphStructuralNode succ : node.getAllSuccessors()){
                if (succ.immediateDom != node) {
                    node.dominanceFrontier.add(succ);
                }
            }
            // DF_up computation
            for(mxGraphStructuralNode child: node.dominatorTreeChildren){
                for(mxGraphStructuralNode p: child.dominanceFrontier) {
                    if (p.immediateDom != node) {
                        node.dominanceFrontier.add(p);
                    }
                }
            }
        }
    }

    /**
     * Return first processed predecessors
     * @param preds
     * @return
     */
    public mxGraphStructuralNode getFirstProcessedPredecessor(Queue<mxGraphStructuralNode> preds){
        mxGraphStructuralNode result = null;
        for(mxGraphStructuralNode node :preds){
            if(node.temp[0] == 1){
                result = node;
                preds.remove(node);
                break;
            }
        }
        return result;
    };

    /**
     * Produces the layer assignment using the graph information specified
     */
    public void execute(Object parent){
        mxGraphStructuralModel model = this.layout.getModel();
        final LinkedHashSet<mxGraphStructuralNode> seenNodes = new LinkedHashSet<mxGraphStructuralNode>();
        final Set<mxGraphStructuralNode> unseenNodes = new HashSet<mxGraphStructuralNode>(model.getVertexMapper().values());

        mxGraphStructuralNode[] rootsArray = null;

        if (model.roots != null){
            Object[] modelRoots = model.roots.toArray();
            rootsArray = new mxGraphStructuralNode[modelRoots.length];

            for (int i = 0; i < modelRoots.length; i++){
                Object node = modelRoots[0];
                mxGraphStructuralNode internalNode = model.getVertexMapper().get(node);
                internalNode.immediateDom = internalNode;
                rootsArray[0] = internalNode;
            }
        }

        /*
        /**
        * TODO: Take all roots
        */
        Set<Object> possibleNewRoots = null;

        if (unseenNodes.size() > 0){
            possibleNewRoots = new HashSet<Object>(unseenNodes);
        }

        mxGraphStructuralNode[] unseenNodesArray = new mxGraphStructuralNode[1];
        unseenNodes.toArray(unseenNodesArray);

        mxGraph graph = layout.getGraph();

        if (possibleNewRoots != null && possibleNewRoots.size() > 0){
            Iterator<Object> iter = possibleNewRoots.iterator();
            List<Object> roots = model.roots;

            while (iter.hasNext()){
                mxGraphStructuralNode node = (mxGraphStructuralNode) iter.next();
                Object realNode = node.node;
                int numIncomingEdges = graph.getIncomingEdges(realNode).length;

                if (numIncomingEdges == 0){
                    roots.add(realNode);
                }
            }
        }

        buildDomTree(rootsArray);
    }
}
