package vg.plugins.layout.structural.stage;

/**
 * Created by aamikhai on 14.07.2014.
 */
public interface mxStructuralLyaoutStage {

    /**
     * Takes the graph detail and configuration information within the facade
     * and creates the resulting laid out graph within that facade for further
     * use.
     */
    public void execute(Object parent);

}
