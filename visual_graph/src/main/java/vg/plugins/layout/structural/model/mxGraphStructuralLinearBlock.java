package vg.plugins.layout.structural.model;

import vg.impl.main_service.VGMainServiceHelper;
import vg.plugins.layout.structural.model.mxGraphStructuralNode;

import java.awt.*;
import java.util.LinkedHashSet;
import com.mxgraph.layout.mxGraphLayout;

/**
 * Created by aamikhai on 17.07.2014.
 */
public class mxGraphStructuralLinearBlock extends mxGraphStructuralNode {

    @Override
    public boolean isVertex(){
        return false;
    }

    @Override
    public boolean isEdge(){
        return false;
    }

    mxGraphStructuralNode src = null;

    mxGraphStructuralNode trg = null;

    public mxGraphStructuralLinearBlock(mxGraphStructuralNode src, mxGraphStructuralNode trg){
        this.src = src;
        this.trg = trg;
        this.connectsAsTarget = new LinkedHashSet<mxGraphStructuralEdge>();
        this.connectsAsSource = new LinkedHashSet<mxGraphStructuralEdge>();
        int blockHeight = src.getHeight() + trg.getHeight() + hDist;
        int blockWidth = Math.max(src.getWidth(), trg.getWidth()); // + wDist;
        this.rectangle.setBounds(0, 0, blockWidth, blockHeight);
    }

    @Override
    public String show(){
        int x = (int) Math.round(this.getX()) + Math.round((this.getWidth() - src.getWidth()) / 2);// + (this.getWidth() / 2) - wDist;
        int y = (int) Math.round(this.getY());
        src.setLocation(x, y);
        x = (int) Math.round(this.getX()) + Math.round((this.getWidth() - trg.getWidth()) / 2);
        trg.setLocation(x, y + this.src.getHeight() + hDist);
        //VGMainServiceHelper.logger.printInfo("Width: " + new Integer(src.getWidth()).toString() + "Height: " + new Integer(trg.getHeight())) ;
        //VGMainServiceHelper.logger.printInfo("Width: " + new Integer(this.getWidth()).toString() + "Height: " + new Integer(this.getHeight())) ;
        //VGMainServiceHelper.logger.printInfo("X: " + new Integer(x).toString() + "Y: " + new Integer(y)) ;
        return this.src.show() + " to " + this.trg.show();
    };

}


