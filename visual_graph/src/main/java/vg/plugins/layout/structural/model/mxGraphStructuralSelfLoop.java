package vg.plugins.layout.structural.model;

import java.util.LinkedHashSet;

/**
 * Created by aamikhai on 21.07.2014.
 */
public class mxGraphStructuralSelfLoop extends mxGraphStructuralNode {

    @Override
    public boolean isVertex(){
        return false;
    }

    @Override
    public boolean isEdge(){
        return false;
    }

    mxGraphStructuralNode selfLoop = null;

    public mxGraphStructuralSelfLoop(mxGraphStructuralNode selfLoop){
        this.selfLoop = selfLoop;
        this.connectsAsTarget = new LinkedHashSet<mxGraphStructuralEdge>();
        this.connectsAsSource = new LinkedHashSet<mxGraphStructuralEdge>();
        int blockHeight = this.selfLoop.getHeight() + hDist;
        int blockWidth = this.getWidth() + wDist;
        this.rectangle.setBounds(0, 0, blockWidth, blockHeight);
    }

    @Override
    public String show(){
        int x = (int) Math.round(this.getX()) + Math.round((this.getWidth() - this.selfLoop.getWidth()) / 2);// + (this.getWidth() / 2) - wDist;
        int y = (int) Math.round(this.getY());
        this.selfLoop.setLocation(x, y);
        x = (int) Math.round(this.getX()) + Math.round((this.getWidth() - this.selfLoop.getWidth()) / 2);;
        return "Self loop" + this.selfLoop.show();
    };

}
