package vg.plugins.layout.structural.model;

import java.util.*;

/**
 * Created by aamikhai on 21.07.2014.
 */
public class mxGraphUnresolvedRegion extends mxGraphStructuralNode {


    protected Set<mxGraphStructuralNode> nodes = new LinkedHashSet<mxGraphStructuralNode>();

    @Override
    public boolean isVertex(){
        return false;
    }

    @Override
    public boolean isEdge(){
        return false;
    }

    public mxGraphUnresolvedRegion(LinkedList<mxGraphStructuralNode> nodes){
        int width = 0;
        int height = 0;
        for(mxGraphStructuralNode node: nodes){
            int blockHeight = height + node.getHeight() + hDist;
            height += blockHeight;
            int blockWidth = Math.max(node.getWidth(), width); // + wDist;
            width = blockWidth;
            node.rectangle.setBounds(0, 0, blockWidth, blockHeight);
            this.nodes.add(node);
        }
        this.rectangle.setBounds(0,0,width,height);
    }

    @Override
    public String show(){
        return "Unresolved";
    };

}
