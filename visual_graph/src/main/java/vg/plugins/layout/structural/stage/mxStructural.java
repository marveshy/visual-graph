package vg.plugins.layout.structural.stage;

import vg.impl.main_service.VGMainServiceHelper;
import vg.plugins.layout.structural.model.*;
import vg.plugins.layout.structural.mxStructuralLayout;

import java.util.*;

/**
 * Created by sunveil on 25.11.2014.
 */
public class mxStructural implements mxStructuralLyaoutStage {

    /**
     * Reference to the enclosing layout algorithm
     */
    protected mxStructuralLayout layout;

    /**
     * @param layout
     * Constructor that has the roots specified
     */
    public mxStructural(mxStructuralLayout layout){
        this.layout = layout;
    }

    @Override
    public void execute(Object parent) {
        mxGraphStructuralModel model = this.layout.getModel();
        final LinkedHashSet<mxGraphStructuralNode> queueNodes = new LinkedHashSet<mxGraphStructuralNode>();

        mxGraphStructuralNode[] rootsArray = null;

        if (model.roots != null){
            Object[] modelRoots = model.roots.toArray();
            rootsArray = new mxGraphStructuralNode[modelRoots.length];

            for (int i = 0; i < modelRoots.length; i++){
                Object node = modelRoots[0];
                mxGraphStructuralNode internalNode = model.getVertexMapper().get(node);
                internalNode.immediateDom = internalNode;
                rootsArray[0] = internalNode;
            }
        }

        model.bfsVisit(new mxGraphStructuralModel.bfsCellVisitor(){
           public void visit(mxGraphStructuralNode cell){
                queueNodes.add(cell);
            }
        }, rootsArray, null);

        Queue<mxGraphStructuralNode> scope = new LinkedList<mxGraphStructuralNode>(queueNodes);
        final Set<mxGraphStructuralEdge> uEdges = new HashSet<mxGraphStructuralEdge>(model.getEdgeMapper().values());

        boolean changed = true;
        while(changed && scope.size() > 0){
            changed = false;
            Queue<mxGraphStructuralNode> agenda = new LinkedList<mxGraphStructuralNode>(scope);
            Object[] agendaArray = agenda.toArray();
            for(int i = agendaArray.length - 1; i >= 0; i--) {
                mxGraphStructuralNode node = (mxGraphStructuralNode) agendaArray[i];
                //Linear block
                if(node.getAllPredecessors().size() <= 1 && node.getAllSuccessors().size() == 1 && node.dominatorTreeChildren.size() == 1){
                    mxGraphStructuralNode trg = node.getAllSuccessors().poll();
                    if(trg.getAllPredecessors().size() <= 1) {
                        mxGraphStructuralLinearBlock linearBlock = new mxGraphStructuralLinearBlock(node, trg);
                        agenda.remove(node);
                        agenda.remove(trg);
                        removeEntryBlock(node, linearBlock, agenda);
                        removeExitBlock(trg, linearBlock, agenda);
                        linearBlock.dominatorTreeChildren = trg.dominatorTreeChildren;
                        linearBlock.immediateDom = node.immediateDom;
                        linearBlock.num = node.num;
                        if(node.immediateDom.dominatorTreeChildren != null) {
                            node.immediateDom.dominatorTreeChildren.remove(node);
                            node.immediateDom.dominatorTreeChildren.add(linearBlock);
                        }
                        agenda.add(linearBlock);
                        scope = new LinkedList<mxGraphStructuralNode>(agenda);
                        changed = true;
                        break;
                    }
                }
                //Loop block
                if(node.getAllPredecessors().size() <= 2 && node.getAllSuccessors().size() == 1 && node.dominatorTreeChildren.size() == 1){
                    mxGraphStructuralNode trg = node.getAllSuccessors().poll();
                    if(trg.getAllPredecessors().size() == 1 && trg.getAllSuccessors().contains(node)) {
                        mxGraphStructuralLoopBlock loopBlock = new mxGraphStructuralLoopBlock(node, trg);
                        agenda.remove(node);
                        agenda.remove(trg);
                        removeEntryBlock(node, loopBlock, agenda);
                        removeExitBlock(trg, loopBlock, agenda);
                        loopBlock.dominatorTreeChildren = trg.dominatorTreeChildren;
                        loopBlock.immediateDom = node.immediateDom;
                        loopBlock.num = node.num;
                        if(node.immediateDom.dominatorTreeChildren != null) {
                            node.immediateDom.dominatorTreeChildren.remove(node);
                            node.immediateDom.dominatorTreeChildren.add(loopBlock);
                        }
                        agenda.add(loopBlock);
                        scope = new LinkedList<mxGraphStructuralNode>(agenda);
                        changed = true;
                        break;
                    }
                }

                //Self loop block
                if(node.getAllSuccessors().contains(node)){
                    mxGraphStructuralSelfLoop selfLoppBlock = new mxGraphStructuralSelfLoop(node);
                    removeEntryBlock(node, selfLoppBlock, agenda);
                    removeExitBlock(node, selfLoppBlock, agenda);
                    for (mxGraphStructuralEdge edge : node.connectsAsTarget) {
                        if (edge.source == edge.target) {
                            edge.edgeType = mxGraphStructuralEdge.etBack;
                            selfLoppBlock.connectsAsSource.remove(edge);
                            selfLoppBlock.connectsAsTarget.remove(edge);
                        }
                    }
                    selfLoppBlock.dominatorTreeChildren = node.dominatorTreeChildren;
                    selfLoppBlock.immediateDom = node.immediateDom;
                    selfLoppBlock.num = node.num;
                    if(node.immediateDom.dominatorTreeChildren != null) {
                        node.immediateDom.dominatorTreeChildren.remove(node);
                        node.immediateDom.dominatorTreeChildren.add(selfLoppBlock);
                    }
                    agenda.add(selfLoppBlock);
                    scope = new LinkedList<mxGraphStructuralNode>(agenda);
                    changed = true;
                    break;

                }

                //Two way condition block
                if (node.dominatorTreeChildren.size() <= 3 && node.getAllSuccessors().size() == 2) {
                    Queue<mxGraphStructuralNode> succ = node.getAllSuccessors();
                    mxGraphStructuralNode brTrue = succ.poll();
                    mxGraphStructuralNode brFalse = succ.poll();
                    mxGraphStructuralNode trg1 = brTrue.getAllSuccessors().poll();
                    mxGraphStructuralNode trg2 = brFalse.getAllSuccessors().poll();
                    if (brTrue.getAllSuccessors().size() == 1 && brTrue.getAllSuccessors().size() == 1 && trg1 == trg2) {
                        mxGraphStructural2WayCond twoWayCondBlock = new mxGraphStructural2WayCond(node, brTrue, brFalse);
                        removeEntryBlock(node, twoWayCondBlock, agenda);
                        removeImmedBlock(brTrue, agenda);
                        removeImmedBlock(brFalse, agenda);
                        mxGraphStructuralEdge connEdge = new mxGraphStructuralEdge();
                        connEdge.source = twoWayCondBlock;
                        connEdge.target = trg1;
                        twoWayCondBlock.connectsAsSource.add(connEdge);
                        trg1.connectsAsTarget.add(connEdge);
                        if(node.dominatorTreeChildren.contains(trg1)) {
                            twoWayCondBlock.dominatorTreeChildren.add(trg1);
                            trg1.immediateDom = twoWayCondBlock;
                        }
                        twoWayCondBlock.num = node.num;
                        twoWayCondBlock.immediateDom = node.immediateDom;
                        node.immediateDom.dominatorTreeChildren.remove(node);
                        node.immediateDom.dominatorTreeChildren.add(twoWayCondBlock);
                        agenda.add(twoWayCondBlock);
                        scope = new LinkedList<mxGraphStructuralNode>(agenda);
                        changed = true;
                        break;
                    }
                }

                //One way condition block
                if (node.dominatorTreeChildren.size() <= 3 && node.getAllSuccessors().size() == 2) {
                    Queue<mxGraphStructuralNode> succ = node.getAllSuccessors();
                    mxGraphStructuralNode suc1 = succ.poll();
                    mxGraphStructuralNode suc2 = succ.poll();
                    mxGraphStructuralNode brTrue = null;
                    mxGraphStructuralNode trg = null;
                    if(suc1.getAllSuccessors().contains(suc2)){
                        trg = suc2;
                        brTrue = suc1;
                    }
                    if(suc2.getAllSuccessors().contains(suc1)){
                        trg = suc1;
                        brTrue = suc2;
                    }
                    if (brTrue != null && trg != null) {
                        mxGraphStructural1WayCond oneWayCondBlock = new mxGraphStructural1WayCond(node, brTrue);
                        removeEntryBlock(node, oneWayCondBlock, agenda);
                        removeImmedBlock(brTrue, agenda);
                        removeImmedBlock(node, agenda);
                        mxGraphStructuralEdge connEdge = new mxGraphStructuralEdge();
                        connEdge.source = oneWayCondBlock;
                        connEdge.target = trg;
                        oneWayCondBlock.connectsAsSource.add(connEdge);
                        oneWayCondBlock.num = node.num;
                        trg.connectsAsTarget.add(connEdge);
                        oneWayCondBlock.immediateDom = node.immediateDom;
                        node.immediateDom.dominatorTreeChildren.remove(node);
                        node.immediateDom.dominatorTreeChildren.add(oneWayCondBlock);
                        if(node.dominatorTreeChildren.contains(trg)) {
                            oneWayCondBlock.dominatorTreeChildren.add(trg);
                            trg.immediateDom = oneWayCondBlock;
                        }
                        agenda.add(oneWayCondBlock);
                        scope = new LinkedList<mxGraphStructuralNode>(agenda);
                        changed = true;
                        break;
                    }
                }

                //Unreachable block
                /*if(isLastNestedStructure(node) && node.dominatorTreeChildren.size() > 0){
                    //VGMainServiceHelper.logger.printInfo("ok");
                    mxGraphUnresolvedRegion uRegion = new mxGraphUnresolvedRegion(node.dominatorTreeChildren);
                    redefineOutgoingEdges(uRegion, node.dominatorTreeChildren, agenda);
                    //removeEntryBlock(node, uRegion, agenda);
                    mxGraphStructuralEdge connEdge = new mxGraphStructuralEdge();
                    connEdge.source = node;
                    connEdge.target = uRegion;
                    node.connectsAsSource.clear();
                    node.connectsAsSource.add(connEdge);
                    uRegion.connectsAsTarget.add(connEdge);
                    node.immediateDom.dominatorTreeChildren.remove(node);
                    node.immediateDom.dominatorTreeChildren.add(uRegion);
                    uRegion.dominatorTreeChildren.clear();
                    uRegion.immediateDom = node.immediateDom;
                    uRegion.num = node.num;
                    agenda.remove(node);
                    agenda.add(uRegion);
                    scope = new LinkedList<mxGraphStructuralNode>(agenda);
                    changed = true;
                    break;
                }*/
            }

        }

        model.region = scope.peek();

    }

    /**
     *
     * @param block new region
     * @param scope
     */
    public void redefineOutgoingEdges(mxGraphStructuralNode block,  List<mxGraphStructuralNode> scope,Queue<mxGraphStructuralNode> agenda){
        for(mxGraphStructuralNode node: scope) {
            for (mxGraphStructuralEdge edge : node.connectsAsSource) {
                if (!scope.contains(edge.target)) {
                    edge.source = block;
                    block.connectsAsSource.add(edge);
                }
            }
            agenda.remove(node);
        }
        agenda.remove(block);
    }


    public boolean markLoopEdges(mxGraphStructuralNode node,mxGraphStructuralNode block){
        boolean result = false;
        for(mxGraphStructuralEdge edge: node.connectsAsSource){
            if(edge.target.equals(node.immediateDom)){
                edge.edgeType = mxGraphStructuralEdge.etBack;
                block.connectsAsSource.remove(edge);
                block.connectsAsTarget.remove(edge);
                return true;
            }
        }
        return result;
    }

    private boolean isLastNestedStructure(mxGraphStructuralNode node){
        boolean result = true;
        for(mxGraphStructuralNode dom: node.dominatorTreeChildren){
            if(dom.dominatorTreeChildren.size() > 0)
                result = false;
            break;
        }
        return result;
    }

    public void removeSelfLoopEdge(mxGraphStructuralNode node) {
        for (mxGraphStructuralEdge edge : node.connectsAsTarget) {
            if (edge.source == edge.target) {
                node.connectsAsSource.remove(edge);
                node.connectsAsTarget.remove(edge);
            }
        }
    }

    public void removeImmedBlock(mxGraphStructuralNode node, Queue<mxGraphStructuralNode> agenda){
        for(mxGraphStructuralEdge edge: node.connectsAsSource){
            edge.target.connectsAsTarget.remove(edge);
            //block.connectsAsTarget.add(edge);
        }
        agenda.remove(node);
    }

    public void removeEntryBlock(mxGraphStructuralNode node, mxGraphStructuralNode block, Queue<mxGraphStructuralNode> agenda){
        for(mxGraphStructuralEdge edge: node.connectsAsTarget){
            edge.target = block;
            block.connectsAsTarget.add(edge);
        }
        agenda.remove(node);
    }

    public void removeExitBlock(mxGraphStructuralNode node, mxGraphStructuralNode block, Queue<mxGraphStructuralNode> agenda){
        for(mxGraphStructuralEdge edge: node.connectsAsSource){
            edge.source = block;
            if(!block.connectsAsSource.contains(edge)) {
                block.connectsAsSource.add(edge);
            }
        }
        agenda.remove(node);
    }

    public void removeImmedBlock(mxGraphStructuralNode immed, mxGraphStructuralNode trg, mxGraphStructuralNode block, Queue<mxGraphStructuralNode> agenda) {
        trg.connectsAsTarget.clear();
        for(mxGraphStructuralEdge edge: immed.connectsAsSource){
            edge.source = block;
            if(edge.target == trg) {
                block.connectsAsSource.add(edge);
                break;
            }
        }
        agenda.remove(immed);
    }
}
