package vg.plugins.layout.structural.model;

import com.mxgraph.util.mxPoint;

/**
 * Created by aamikhai on 22.07.2014.
 */
public class mxGraphStructuralLoopBlock extends mxGraphStructuralLinearBlock {

    public mxGraphStructuralLoopBlock(mxGraphStructuralNode src, mxGraphStructuralNode trg){
        super(src, trg);
    }

    public void removeSelfLoopEdge(){
        for (mxGraphStructuralEdge edge : this.connectsAsTarget) {
            if (edge.source == edge.target) {
                this.getWidth();
                edge.geometry.add(new mxPoint(this.getWidth(), this.getHeight()));
                edge.geometry.add(new mxPoint(this.getWidth(), 0));
                this.connectsAsSource.remove(edge);
                this.connectsAsTarget.remove(edge);
            }
        }
    }


}
