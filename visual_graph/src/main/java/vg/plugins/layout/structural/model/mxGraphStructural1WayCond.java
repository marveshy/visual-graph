package vg.plugins.layout.structural.model;

import java.util.LinkedHashSet;

/**
 * Created by aamikhai on 21.07.2014.
 */
public class mxGraphStructural1WayCond extends mxGraphStructuralNode {


    @Override
    public boolean isVertex(){
        return false;
    }

    @Override
    public boolean isEdge(){
        return false;
    }

    mxGraphStructuralNode head = null; //condition

    mxGraphStructuralNode brTrue = null; //right branch

    mxGraphStructuralNode brFalse = null; //left branch

    public mxGraphStructural1WayCond(mxGraphStructuralNode head, mxGraphStructuralNode brTrue){
        this.head = head;
        this.brTrue = brTrue;
        this.connectsAsTarget = new LinkedHashSet<mxGraphStructuralEdge>();
        this.connectsAsSource = new LinkedHashSet<mxGraphStructuralEdge>();
        int blockHeight = head.getHeight() + brTrue.getHeight() + hDist;
        int blockWidth = Math.max(head.getWidth(), brTrue.getWidth())  + wDist;
        this.rectangle.setBounds(0, 0, blockWidth, blockHeight);
    }

    @Override
    public String show(){
        int x = (int) Math.round(this.getX()) + Math.round((this.getWidth() - head.getWidth()) / 2);// + (this.getWidth() / 2) - wDist;
        int y = (int) Math.round(this.getY());
        head.setLocation(x, y);
        //x = (int) Math.round(this.getX());
        brTrue.setLocation(x - brTrue.getWidth() - hDist / 2, y + this.head.getHeight() + hDist);
        //brFalse.setLocation(x + brTrue.getWidth(), y + this.head.getHeight() + hDist);

        //VGMainServiceHelper.logger.printInfo("Head: ");
        //VGMainServiceHelper.logger.printInfo("Width: " + new Integer(head.getWidth()).toString() + "Height: " + new Integer(head.getHeight())) ;
        //VGMainServiceHelper.logger.printInfo("brTrue: ");
        //VGMainServiceHelper.logger.printInfo("Width: " + new Integer(brTrue.getWidth()).toString() + "Height: " + new Integer(brTrue.getHeight())) ;
        //VGMainServiceHelper.logger.printInfo("brFalse: ");
        //VGMainServiceHelper.logger.printInfo("Width: " + new Integer(brFalse.getWidth()).toString() + "Height: " + new Integer(brFalse.getHeight())) ;

        //VGMainServiceHelper.logger.printInfo("Width: " + new Integer(this.getWidth()).toString() + "Height: " + new Integer(this.getHeight())) ;
        //VGMainServiceHelper.logger.printInfo("X: " + new Integer(x).toString() + "Y: " + new Integer(y)) ;

        return "if(" + head.show() + "){" + brTrue.show() + "}";
    }


}
