package vg.plugins.layout;

/**
 * @author Timur Zolotuhin (tzolotuhin@parallels.com)
 */
public class GraphLayoutPluginGlobals {
    public final static String LAYOUT_PLUGIN_PREFIX = "layout_plugin_";

    public final static String LAYOUT_PLUGIN_SETTINGS_PREFIX = LAYOUT_PLUGIN_PREFIX + "settings_";
}
