package vg.plugins.notepad;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import vg.interfaces.log_service.IWindowMessenger;
import vg.shared.gui.SwingUtils;
import vg.impl.main_service.VGMainServiceHelper;
import vg.interfaces.data_base_service.GraphDataBaseListener;
import vg.interfaces.data_base_service.data.graph.Edge;
import vg.interfaces.data_base_service.data.graph.Vertex;
import vg.interfaces.graph_view_service.GraphView;
import vg.interfaces.plugin_service.Plugin;
import vg.interfaces.user_interface_service.UserInterfaceListener;
import vg.interfaces.user_interface_service.UserInterfacePanel;
import vg.interfaces.user_interface_service.UserInterfaceService;
import vg.interfaces.user_interface_service.UserInterfaceTab;
import vg.plugins.notepad.components.AttachSrcCodeDialog;
import vg.shared.graph.utils.GraphUtils;
import vg.shared.user_interface.DefaultSwingUtilsCallBack;
import vg.shared.user_interface.UserInterfacePanelUtils;

import javax.swing.*;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultHighlighter;
import javax.swing.text.Highlighter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.Map;

/**
 * @author Timur Zolotuhin (e-mail: tzolotuhin@parallels.com)
 */
public class NotepadPlugin implements Plugin, UserInterfacePanel {
    // Main components
    private JPanel outView, innerView;

    // Mutex
    private final Object generalMutex = new Object();

    // Main data
    private BiMap<Integer, String> graphIdToAttachmentFile;
    private String currentText;
    private int currentLineNumber;

    @Override
    public void install() throws Exception {
        outView = new JPanel(new GridLayout(1, 1));
        innerView = new JPanel(new GridBagLayout());
        outView.add(innerView);

        // initialize main data
        graphIdToAttachmentFile = HashBiMap.create();

        // initialize attach src code menu item
        final JMenuItem attachSrcCodeMenuItem = new JMenuItem("Attach src code");
        attachSrcCodeMenuItem.setEnabled(false);

        VGMainServiceHelper.userInterfaceService.addMenuItem(attachSrcCodeMenuItem, UserInterfaceService.EDIT_MENU);

        VGMainServiceHelper.graphDataBaseService.addListener(new GraphDataBaseListener() {
            @Override
            public void onOpenNewGraphModel(int graphModelId) {
                attachSrcCodeMenuItem.setEnabled(true);
            }
        });

        VGMainServiceHelper.userInterfaceService.addObserver(new UserInterfaceListener() {
            @Override
            public void onOpenTab(UserInterfaceTab tab) {
                if (tab instanceof GraphView) {
                    final GraphView tabGraphView = (GraphView)tab;

                    final JMenuItem showInAttachmentFileMenuItem = new JMenuItem("Show in attachment file");
                    showInAttachmentFileMenuItem.addActionListener(new ActionListener() {
                        public void actionPerformed(ActionEvent arg0) {
                            VGMainServiceHelper.executorService.execute(new Runnable() {
                                @Override
                                public void run() {
                                    Map.Entry<Collection<Vertex>, Collection<Edge>> selectedElements = tabGraphView.getOrderedSelectedElements();

                                    Vertex vertex = selectedElements.getKey().iterator().next();
                                    String value = GraphUtils.getLinkToAttachmentFileAttribute(vertex);
                                    if (value == null) {
                                        VGMainServiceHelper.windowMessenger.errorMessage("Please select vertex with attribute link to attachment file", "Show in attachment file", null);
                                        return;
                                    }

                                    String[] values = value.split(":");

                                    if (values.length != 2) {
                                        VGMainServiceHelper.windowMessenger.errorMessage("Format for link attachment file is wrong. " +
                                                "Please use following format file_name:line_number. " +
                                                "For example: main.c:5", "Show in attachment file", null);
                                        return;
                                    }

                                    for (String attachmentFileName : graphIdToAttachmentFile.values()) {
                                        if (attachmentFileName.contains(values[0])) {
                                            doShow(attachmentFileName, new Integer(values[1]));
                                            return;
                                        }
                                    }

                                    VGMainServiceHelper.windowMessenger.errorMessage("Can't found " + values[0] + " file.\n" +
                                            "Please attach it and repeat the operation.", "Show in attachment file", new IWindowMessenger.WindowMessengerCallBack() {
                                        @Override
                                        public void execute() {
                                            AttachSrcCodeDialog.showDialog(NotepadPlugin.this);
                                        }
                                    });
                                }
                            });
                        }
                    });

                    VGMainServiceHelper.executorService.execute(new Runnable() {
                        @Override
                        public void run() {
                            tabGraphView.addPopupMenuItem(showInAttachmentFileMenuItem);
                        }
                    });
                }
            }
        });

        attachSrcCodeMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                AttachSrcCodeDialog.showDialog(NotepadPlugin.this);
            }
        });

        // rebuild view
        rebuildView();

        UserInterfacePanelUtils.createPanel("Notepad", this, UserInterfaceService.SOUTH_INSTRUMENT_PANEL, UserInterfaceService.WEST_SOUTH_PANEL);
    }

    @Override
    public JPanel getView() {
        synchronized (generalMutex) {
            return outView;
        }
    }

    public void attachFile(int graphId, String fileName) {
        synchronized (generalMutex) {
            doAttachFile(graphId, fileName);
        }
    }

//==============================================================================
//-----------------PRIVATE METHODS----------------------------------------------
    private void rebuildView() {
        innerView.removeAll();

        JTextArea textArea = new JTextArea(currentText);
        JScrollPane scrollPane = new JScrollPane(textArea);

        Highlighter h = textArea.getHighlighter();
        h.removeAllHighlights();
        try {
            int startIndex = textArea.getLineStartOffset(currentLineNumber);
            int endIndex = textArea.getLineEndOffset(currentLineNumber);
            h.addHighlight(startIndex, endIndex, DefaultHighlighter.DefaultPainter);
        } catch (BadLocationException ex) {
            VGMainServiceHelper.logger.printException(ex);
        }

        innerView.add(scrollPane, new GridBagConstraints(0, 0, 1, 1, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));

        innerView.updateUI();
    }

    private void doAttachFile(int graphId, String fileName) {
        graphIdToAttachmentFile.put(graphId, fileName);
    }

    private void doShow(String fileName, final int lineNumber) {
        try {
            final byte[] encoded = Files.readAllBytes(Paths.get(fileName));
            final String tmpCurrentText = new String(encoded, StandardCharsets.UTF_8);
            SwingUtils.invokeInEDTIfNeeded(new Runnable() {
                @Override
                public void run() {
                    synchronized (generalMutex) {
                        currentText = tmpCurrentText;
                        currentLineNumber = lineNumber;
                        rebuildView();
                    }
                }
            }, new DefaultSwingUtilsCallBack());
        } catch (IOException ex) {
            VGMainServiceHelper.logger.printException(ex);
        }
    }
}
