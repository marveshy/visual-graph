package vg.plugins.notepad.components;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import vg.impl.main_service.VGMainServiceHelper;
import vg.interfaces.data_base_service.data.record.GraphRecord;
import vg.plugins.navigator.NavigatorGlobals;
import vg.plugins.notepad.NotepadPlugin;
import vg.plugins.opener.GraphOpenerPlugin;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.List;
import java.util.Map;

/**
 * @author Timur Zolotuhin (tzolotuhin@parallels.com)
 */
public class AttachSrcCodeDialog {
    public static void showDialog(NotepadPlugin notepadPlugin) {
        Map<String, Integer> graphNameToId = Maps.newHashMap();
        List<JComponent> dialogComponents = Lists.newArrayList();

        // add select graph panels
        JPanel selectGraphPanel = new JPanel(new GridBagLayout());
        JLabel selectGraphLabel = new JLabel("Graph: ");
        final JComboBox<String> graphRecordsComboBox = new JComboBox<>();
        List<GraphRecord> graphRecords = VGMainServiceHelper.graphDataBaseService.getGraphRecords(-1);
        for (GraphRecord graphRecord : graphRecords) {
            graphRecordsComboBox.addItem(graphRecord.getName());
            graphNameToId.put(graphRecord.getName(), graphRecord.getId());
        }

        selectGraphPanel.add(selectGraphLabel, new GridBagConstraints(0, 0, 1, 1, 0, 0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 0, 0, 0), 0, 0));
        selectGraphPanel.add(graphRecordsComboBox, new GridBagConstraints(1, 0, 1, 1, 0, 0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));

        dialogComponents.add(selectGraphPanel);

        // add attachment file items
        JPanel browseSrcFilePanel = new JPanel(new GridBagLayout());
        JLabel browseSrcFileLabel = new JLabel("Attachment file: ");
        final JTextField srcFileTextField = new JTextField();
        JButton browseSrcFileButton = new JButton("...");
        browseSrcFileButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser fileChooser = new JFileChooser(".");
                String lastDir = VGMainServiceHelper.config.getStringProperty("GRAPH_OPENER_LastDir");
                if(lastDir != null) {
                    fileChooser.setCurrentDirectory(new File(lastDir));
                }
                if (fileChooser.showOpenDialog(VGMainServiceHelper.userInterfaceService.getMainFrame()) == JFileChooser.APPROVE_OPTION) {
                    VGMainServiceHelper.logger.printInfo("Open file = " + fileChooser.getSelectedFile().getPath());
                    srcFileTextField.setText(fileChooser.getSelectedFile().getAbsolutePath());
                }
            }
        });

        browseSrcFilePanel.add(browseSrcFileLabel, new GridBagConstraints(0, 0, 2, 1, 0, 0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 0, 0, 0), 0, 0));
        browseSrcFilePanel.add(srcFileTextField, new GridBagConstraints(0, 1, 1, 1, 1, 0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        browseSrcFilePanel.add(browseSrcFileButton, new GridBagConstraints(1, 1, 1, 1, 0, 0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));

        dialogComponents.add(browseSrcFilePanel);

        // build
        JComponent[] arrayDialogComponents = new JComponent[dialogComponents.size()];
        arrayDialogComponents = dialogComponents.toArray(arrayDialogComponents);
        if (JOptionPane.showConfirmDialog(null, arrayDialogComponents, "Attach src file", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE) == 0) {
            notepadPlugin.attachFile(graphNameToId.get(graphRecordsComboBox.getSelectedItem().toString()), srcFileTextField.getText());
        }
    }
}