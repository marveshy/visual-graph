package vg.plugins.help.about;

import vg.impl.main_service.VGMainServiceHelper;
import vg.interfaces.plugin_service.Plugin;
import vg.interfaces.user_interface_service.UserInterfaceService;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * This plugin implement 'About' menu item.
 * The menu shows short information about program, authors and version.
 *
 * @author Timur Zolotuhin (e-mail: tzolotuhin@gmail.com)
 */
public class AboutPlugin implements Plugin {
	// Components
	private AboutPanel aboutPanel;

    @Override
	public void install() throws Exception {
        JMenuItem jMenuItem = new JMenuItem("About");
        jMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (aboutPanel == null)
                    aboutPanel = new AboutPanel();
                aboutPanel.setVisible(true);
            }
        });

        VGMainServiceHelper.userInterfaceService.addMenuItem(jMenuItem, UserInterfaceService.HELP_MENU);
	}
}
