package vg.plugins.graph_settings;

import com.google.common.collect.Maps;
import com.mxgraph.util.mxUtils;
import vg.impl.main_service.VGMainServiceHelper;
import vg.interfaces.graph_view_service.GraphViewExecutor;
import vg.interfaces.main_service.VGMainGlobals;
import vg.interfaces.plugin_service.Plugin;
import vg.interfaces.user_interface_service.UserInterfaceListener;
import vg.interfaces.user_interface_service.UserInterfaceService;
import vg.interfaces.user_interface_service.UserInterfaceTab;
import vg.interfaces.graph_view_service.GraphView;
import vg.shared.gui.components.FontChooserComboBox;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Map;

/**
 * Graph settings
 *
 * @author Timur Zolotuhin (e-mail: tzolotuhin@gmail.com)
 */
public class GraphSettingsPlugin implements Plugin {
	// Main data
	private boolean init = false;

	private Color defaultFontColor;
	private Color defaultVertexFillColor;
	private String defaultVertexAndEdgeFont;

	private Map<UserInterfaceTab, GraphView> tabs = Maps.newHashMap(); // tab id -> graph view (or null)
	
	// Swing components
	private JComponent[] dialogComponents = null;
	private FontChooserComboBox defaultVertexAndEdgeFontChooserComboBox = null;
	private ColorPreview defaultFontColorPreview = null;
	private ColorPreview defaultVertexFillColorPreview = null;

    // Mutex
    private final Object generalMutex = new Object();
	
	@Override
	public void install() throws Exception {
        defaultFontColor = Color.BLACK;
        defaultVertexFillColor = Color.WHITE;
        defaultVertexAndEdgeFont = VGMainGlobals.UI_MONOSPACED_FONT_FAMILY;

        loadSettings();

        // create components
		JMenuItem graphSettings = new JMenuItem("Graph settings");
		graphSettings.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				synchronized (generalMutex) {
                    if (!init)
                        init();

                    for (JComponent c: dialogComponents) {
                        SwingUtilities.updateComponentTreeUI(c);
                    }

                    if (JOptionPane.showConfirmDialog(null, dialogComponents, "Graph visualization settings", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE) == 0) {
                        String fontName = defaultVertexAndEdgeFontChooserComboBox.getSelectedFontName();
                        if (fontName != null)
                            defaultVertexAndEdgeFont = fontName;
                        defaultFontColor = defaultFontColorPreview.getBackground();
                        defaultVertexFillColor = defaultVertexFillColorPreview.getBackground();

                        // save settings
                        saveSettings();

                        // apply settings to exist graph views
                        for (UserInterfaceTab tab : tabs.values()) {
                            GraphView graphView = null;
                            if (tab instanceof GraphView) {
                                graphView = (GraphView)tab;
                            }

                            if (graphView != null) {
                                applySettings(graphView);
                            }
                        }
                    }
                }
			}
		});

        VGMainServiceHelper.userInterfaceService.addObserver(new UserInterfaceListener() {
            @Override
            public void onOpenTab(UserInterfaceTab tab) {
                synchronized (generalMutex) {
                    if (tab == null)
                        return;

                    GraphView graphView = null;
                    if (tab instanceof GraphView) {
                        graphView = (GraphView) tab;
                    }

                    if (graphView != null) {
                        tabs.put(tab, graphView);
                        applySettings(graphView);
                    }
                }
            }

            @Override
            public void onCloseTab(UserInterfaceTab tab) {
                synchronized (generalMutex) {
                    if (tab != null)
                        tabs.remove(tab);
                }
            }
        }, 10);

        VGMainServiceHelper.userInterfaceService.addMenuItem(graphSettings, UserInterfaceService.EDIT_MENU);
	}
	
//=============================================================================
//-----------------PRIVATE METHODS---------------------------------------------
	private void applySettings(final GraphView graphView) {
        graphView.execute(new GraphViewExecutor.GraphViewRunnable() {
            @Override
            public void run(GraphView graphView) {
                synchronized (generalMutex) {
                    graphView.setDefaultVertexColor(defaultVertexFillColor);
                    graphView.setDefaultVertexFontColor(defaultFontColor);
                    graphView.setDefaultVertexFont(new Font(defaultVertexAndEdgeFont, Font.BOLD, 12));
                    graphView.setDefaultEdgeFont(new Font(defaultVertexAndEdgeFont, Font.BOLD, 12));
                    graphView.refreshView();
                }
            }
        });
    }

    private void loadSettings() {
        defaultFontColor = mxUtils.parseColor(VGMainServiceHelper.config.getStringProperty(GraphSettingsPluginGlobals.VERTEX_FONT_COLOR_KEY, "#000000"));
        defaultVertexFillColor = mxUtils.parseColor(VGMainServiceHelper.config.getStringProperty(GraphSettingsPluginGlobals.VERTEX_FILL_COLOR_KEY, "#000000"));
        defaultVertexAndEdgeFont = VGMainServiceHelper.config.getStringProperty(GraphSettingsPluginGlobals.GRAPH_ELEMENTS_FONT_KEY, "#000000");
	}

    private void saveSettings() {
        VGMainServiceHelper.config.setProperty(GraphSettingsPluginGlobals.GRAPH_ELEMENTS_FONT_KEY, defaultVertexAndEdgeFont);
        VGMainServiceHelper.config.setProperty(GraphSettingsPluginGlobals.VERTEX_FILL_COLOR_KEY, mxUtils.hexString(defaultVertexFillColor));
        VGMainServiceHelper.config.setProperty(GraphSettingsPluginGlobals.VERTEX_FONT_COLOR_KEY, mxUtils.hexString(defaultFontColor));
    }
	
	private void init() {
		if (init)
			return;
		
		loadSettings();
		
		defaultVertexAndEdgeFontChooserComboBox = new FontChooserComboBox(defaultVertexAndEdgeFont);
		
		defaultFontColorPreview = new ColorPreview();
		defaultFontColorPreview.setBackground(defaultFontColor);
		
		JButton fontColorButton = new JButton("change");
		fontColorButton.addActionListener(new ActionListener() {
			@Override
            public void actionPerformed(ActionEvent arg0) {
                try {
                    Color newColor = JColorChooser.showDialog(VGMainServiceHelper.userInterfaceService.getMainFrame(), "Set font color", defaultFontColorPreview.getBackground());
                    if (newColor != null) {
                        defaultFontColorPreview.setBackground(newColor);
                    }
                } catch (Throwable ex) {
                    VGMainServiceHelper.windowMessenger.errorMessage("Please change window style. This function doesn't work with current style.", "Window style error", null);
                    VGMainServiceHelper.logger.printException(ex);
                }
			}
		});

		JPanel colorFontPane = new JPanel();
		colorFontPane.setLayout(new BoxLayout(colorFontPane, BoxLayout.X_AXIS));
		colorFontPane.add(new JLabel("Font color"));
		colorFontPane.add(Box.createHorizontalGlue());
		colorFontPane.add(defaultFontColorPreview);
		colorFontPane.add(Box.createRigidArea(new Dimension(5,0)));
		colorFontPane.add(fontColorButton);
		
		defaultVertexFillColorPreview = new ColorPreview();
		defaultVertexFillColorPreview.setBackground(defaultVertexFillColor);
		
		JButton vertexColorButton = new JButton("change");
		vertexColorButton.addActionListener(new ActionListener() {
			@Override
            public void actionPerformed(ActionEvent arg0) {
                try {
                    Color newColor = JColorChooser.showDialog(VGMainServiceHelper.userInterfaceService.getMainFrame(), "Set font color", defaultVertexFillColorPreview.getBackground());
                    if (newColor != null) {
                        defaultVertexFillColorPreview.setBackground(newColor);
                    }
                } catch (Throwable ex) {
                    VGMainServiceHelper.windowMessenger.errorMessage("Please change window style. This function doesn't work with current style.", "Window style error", null);
                    VGMainServiceHelper.logger.printException(ex);
                }
            }
		});

		JPanel vertexColorPane = new JPanel();
		vertexColorPane.setLayout(new BoxLayout(vertexColorPane, BoxLayout.X_AXIS));
		vertexColorPane.add(new JLabel("Default vertex color"));
		vertexColorPane.add(Box.createHorizontalGlue());
		vertexColorPane.add(this.defaultVertexFillColorPreview);
		vertexColorPane.add(Box.createRigidArea(new Dimension(5,0)));
		vertexColorPane.add(vertexColorButton);
		
        dialogComponents = new JComponent[]{
                new JLabel("Font"),
                defaultVertexAndEdgeFontChooserComboBox,
                colorFontPane,
                vertexColorPane,
                new JLabel("Default vertex shape"),
                new JLabel("Default edge style")
        };

		init = true;
	}

	private static class ColorPreview extends JComponent {
		private static final long serialVersionUID = -2276526078891878352L;
		ColorPreview() {
			setMinimumSize(new Dimension(24,24));
			setPreferredSize(getMinimumSize());
			setMaximumSize(getPreferredSize());
		}

		@Override
		public void paintComponent(Graphics g) {
			super.paintComponent(g);
			g.setColor(new Color(229, 160, 63));
			Rectangle bg = new Rectangle(0, 0, getWidth(), getHeight());
			bg = bg.intersection(g.getClipBounds());
			g.fillRect(bg.x, bg.y, bg.width, bg.height);
			
			g.setColor(getBackground());
			bg = new Rectangle(1, 1, getWidth() - 2, getHeight() - 2);
			bg = bg.intersection(g.getClipBounds());
			g.fillRect(bg.x, bg.y, bg.width, bg.height);
		}
	}
}

