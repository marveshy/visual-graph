package vg.plugins.graph_settings;

/**
 * Graph settings globals
 *
 * @author Timur Zolotuhin (e-mail: tzolotuhin@gmail.com)
 */

public class GraphSettingsPluginGlobals {
    public final static String GRAPH_SETTINGS_PLUGIN_PREFIX = "graph_settings_plugin_";

    public final static String VERTEX_FONT_COLOR_KEY = GRAPH_SETTINGS_PLUGIN_PREFIX + "vertex_font_color";
    public final static String VERTEX_FILL_COLOR_KEY = GRAPH_SETTINGS_PLUGIN_PREFIX + "vertex_fill_color";
    public final static String GRAPH_ELEMENTS_FONT_KEY = GRAPH_SETTINGS_PLUGIN_PREFIX + "graph_elements_font";

}
