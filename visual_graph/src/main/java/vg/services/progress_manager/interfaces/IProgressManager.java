package vg.services.progress_manager.interfaces;

/**
 * @author Timur Zolotuhin (e-mail: tzolotuhin@gmail.com)
 */
public interface IProgressManager {
	void addTask(IProgressTask task);
	void removeTask(IProgressTask task);
	double updateProgress();
	int getTaskCount();
	String getTaskName();
}
