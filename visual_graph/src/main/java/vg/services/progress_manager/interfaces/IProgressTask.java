package vg.services.progress_manager.interfaces;

/**
 * @author Timur Zolotuhin (e-mail: tzolotuhin@gmail.com)
 */
public interface IProgressTask {
	long getValue();
	long getLength();
	String getTaskName();
}
