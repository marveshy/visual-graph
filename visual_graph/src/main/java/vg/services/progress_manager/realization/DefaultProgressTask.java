package vg.services.progress_manager.realization;

import vg.services.progress_manager.interfaces.IProgressTask;

/**
 * Default progress task.
 * @author Timur Zolotuhin (e-mail: tzolotuhin@gmail.com)
 */

public class DefaultProgressTask implements IProgressTask {
    // Defines
    private final static long DEF_LENGTH = 100;

    // Mutex
    private final Object generalMutex = new Object();

    // Main data
    private final String name;
    private long currentValue = 0;
    private final long length;
    private boolean finish = false;

    public DefaultProgressTask(String name, long length) {
        this.name = name;
        this.length = length;
    }

    public DefaultProgressTask(String patchName) {
        this(patchName, DEF_LENGTH);
    }

    public void incValue(long dx) {
        synchronized (generalMutex) {
            currentValue += dx;
        }
    }

    public void setValue(long value) {
        synchronized (generalMutex) {
            currentValue = value;
        }
    }

    public void finish() {
        synchronized (generalMutex) {
            finish = true;
        }
    }

    @Override
    public long getValue() {
        synchronized (generalMutex) {
            if (finish)
                return length;
            return currentValue;
        }
    }

    @Override
    public String getTaskName() {
        return name;
    }

    @Override
    public long getLength() {
        return length;
    }
}