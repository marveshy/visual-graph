package vg.services.progress_manager.realization;

import vg.services.progress_manager.interfaces.IProgressTask;

/**
 * @author Timur Zolotuhin (tzolotuhin@gmail.com)
 */
public class InfinityProgressTask implements IProgressTask {
    // Defines
    private final static long DEF_LENGTH = 100;

    // Mutex
    private final Object generalMutex = new Object();

    // Main data
    private final String name;
    private long currentValue = 0;
    private final long length;
    private boolean finish = false;

    public InfinityProgressTask(String name, long length) {
        this.name = name;
        this.length = length;
    }

    public InfinityProgressTask(String name) {
        this(name, DEF_LENGTH);
    }

    public void finish() {
        synchronized (generalMutex) {
            finish = true;
        }
    }

    @Override
    public long getValue() {
        synchronized (generalMutex) {
            currentValue++;
            if (currentValue == length)
                currentValue = 0;
            if (finish)
                currentValue = length;
            return  currentValue;
        }
    }

    @Override
    public String getTaskName() {
        return name;
    }

    @Override
    public long getLength() {
        return length;
    }
 }
