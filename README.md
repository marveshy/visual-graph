**Visual Graph** is system for drawing hierarchical attributed graphs on the plane and searching different information in it.
For that user may use wide set of tools: navigator, attribute manager, mini map, graph viewer, graph matching, path searcher,
cycle searcher and other.

Visual Graph has wide set of layout for graphs: hierarchical layout (main layout for the program), circle layout, organic layout,
fast organic layout, layered layout and other.

Key application area: graphs in the compiler (control flow graph, syntax trees, call graphs).

The program supports the following popular graph data formats: dot (gz), graphml, gml.

Analogs: yEd, aiSee, Cytoscape, Gephi

Main window of Visual Graph:
![vg_main_window_1.5.4.png](https://bitbucket.org/repo/5Mxb6L/images/3153984640-vg_main_window_1.5.4.png)

Graph matching:
![vg_graph_matching_1.5.4.png](https://bitbucket.org/repo/5Mxb6L/images/2071791486-vg_graph_matching_1.5.4.png)

**Contacts**   
***e-mail***: tzolotuhin@gmail.com  
Support the project, Yandex money: **410012117054201**  
Using Yandex money: https://money.yandex.ru/direct-payment.xml  
Using Visa or MasterCard: https://money.yandex.ru/topup/card/carddetails.xml