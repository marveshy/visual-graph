
package vg.impl.remote_service;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the vg.impl.remote_service package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _PingResponse_QNAME = new QName("http://remote_service.impl.vg/", "pingResponse");
    private final static QName _Ping_QNAME = new QName("http://remote_service.impl.vg/", "ping");
    private final static QName _OpenGraphFromDataResponse_QNAME = new QName("http://remote_service.impl.vg/", "openGraphFromDataResponse");
    private final static QName _About_QNAME = new QName("http://remote_service.impl.vg/", "about");
    private final static QName _OpenGraphFromData_QNAME = new QName("http://remote_service.impl.vg/", "openGraphFromData");
    private final static QName _AboutResponse_QNAME = new QName("http://remote_service.impl.vg/", "aboutResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: vg.impl.remote_service
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link AboutResponse }
     * 
     */
    public AboutResponse createAboutResponse() {
        return new AboutResponse();
    }

    /**
     * Create an instance of {@link OpenGraphFromData }
     * 
     */
    public OpenGraphFromData createOpenGraphFromData() {
        return new OpenGraphFromData();
    }

    /**
     * Create an instance of {@link About }
     * 
     */
    public About createAbout() {
        return new About();
    }

    /**
     * Create an instance of {@link OpenGraphFromDataResponse }
     * 
     */
    public OpenGraphFromDataResponse createOpenGraphFromDataResponse() {
        return new OpenGraphFromDataResponse();
    }

    /**
     * Create an instance of {@link Ping }
     * 
     */
    public Ping createPing() {
        return new Ping();
    }

    /**
     * Create an instance of {@link PingResponse }
     * 
     */
    public PingResponse createPingResponse() {
        return new PingResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PingResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://remote_service.impl.vg/", name = "pingResponse")
    public JAXBElement<PingResponse> createPingResponse(PingResponse value) {
        return new JAXBElement<PingResponse>(_PingResponse_QNAME, PingResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Ping }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://remote_service.impl.vg/", name = "ping")
    public JAXBElement<Ping> createPing(Ping value) {
        return new JAXBElement<Ping>(_Ping_QNAME, Ping.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpenGraphFromDataResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://remote_service.impl.vg/", name = "openGraphFromDataResponse")
    public JAXBElement<OpenGraphFromDataResponse> createOpenGraphFromDataResponse(OpenGraphFromDataResponse value) {
        return new JAXBElement<OpenGraphFromDataResponse>(_OpenGraphFromDataResponse_QNAME, OpenGraphFromDataResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link About }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://remote_service.impl.vg/", name = "about")
    public JAXBElement<About> createAbout(About value) {
        return new JAXBElement<About>(_About_QNAME, About.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpenGraphFromData }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://remote_service.impl.vg/", name = "openGraphFromData")
    public JAXBElement<OpenGraphFromData> createOpenGraphFromData(OpenGraphFromData value) {
        return new JAXBElement<OpenGraphFromData>(_OpenGraphFromData_QNAME, OpenGraphFromData.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AboutResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://remote_service.impl.vg/", name = "aboutResponse")
    public JAXBElement<AboutResponse> createAboutResponse(AboutResponse value) {
        return new JAXBElement<AboutResponse>(_AboutResponse_QNAME, AboutResponse.class, null, value);
    }

}
