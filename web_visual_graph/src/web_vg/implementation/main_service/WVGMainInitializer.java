package web_vg.implementation.main_service;

import tzhomestudio.framework.implementation.main_service.FrameworkMainInitializer;
import tzhomestudio.framework.shared.utils.FileUtils;
import web_vg.implementation.gui_service.GUIService;
import web_vg.implementation.remote_service.WVGRemoteAPIServiceImpl;
import web_vg.interfaces.gui_service.IGUIService;
import web_vg.interfaces.remote_service.WVGRemoteAPIService;

/**
 * This class contains links to all services.
 *
 * @author Timur Zolotuhin (e-mail: tzolotuhin@gmail.com)
 */
public class WVGMainInitializer extends FrameworkMainInitializer {
    // Main data
    protected static IGUIService guiService;
    protected static WVGRemoteAPIService remoteAPIService;

    public static void executeInitialPreparations(String[] args) {
        synchronized (generalMutex) {
            try {
                // setup working dir for applet
                FileUtils.setWorkingDir(FileUtils.getUserHomeDir() + "visual_graph_applet");

                FrameworkMainInitializer.executeInitialPreparations();

                cmdLine.parseCmdLineArgs(args);

                guiService = new GUIService();
                remoteAPIService = new WVGRemoteAPIServiceImpl();

                // update helper
                updateHelper();
            } catch (Throwable ex) {
                throw new RuntimeException(ex);
            }
        }
    }

    public static void updateHelper() {
        synchronized (generalMutex) {
            FrameworkMainInitializer.updateHelper();

            WVGMainServiceHelper.guiService = guiService;
            WVGMainServiceHelper.remoteAPIService = remoteAPIService;
        }
    }
}
