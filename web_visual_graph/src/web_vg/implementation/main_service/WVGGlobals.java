package web_vg.implementation.main_service;

/**
 * @author Timur Zolotuhin (tzolotuhin@parallels.com)
 */
public class WVGGlobals {
    public static final String VG_PATH_ON_LOCAL_SYSTEM_KEY = "vg_path_on_local_system";
    public static final String VG_DOWNLOAD_LINK_KEY = "vg_download_link";
}
