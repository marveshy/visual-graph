package web_vg.implementation.main_service;

import tzhomestudio.framework.implementation.main_service.FrameworkMainServiceHelper;
import web_vg.interfaces.gui_service.IGUIService;
import web_vg.interfaces.remote_service.WVGRemoteAPIService;

/**
 * Contains all necessary service (or resource) links for quick access.
 *
 * Note: Don't edit this class. Read only.
 *
 * @author Timur Zolotuhin (e-mail: tzolotuhin@gmail.com)
 */
public class WVGMainServiceHelper extends FrameworkMainServiceHelper {
    public static IGUIService guiService;
    public static WVGRemoteAPIService remoteAPIService;
}
