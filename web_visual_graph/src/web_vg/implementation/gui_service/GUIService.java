package web_vg.implementation.gui_service;

import web_vg.implementation.gui_service.components.MainWindow;
import web_vg.interfaces.gui_service.IGUIService;

import javax.swing.*;

/**
 * @author Timur Zolotuhin (tzolotuhin@parallels.com)
 */
public class GUIService implements IGUIService {
    private MainWindow mainWindow;

    @Override
    public void init() {
        if (mainWindow == null)
            mainWindow = new MainWindow();
    }

    @Override
    public JPanel getView() {
        return mainWindow.getView();
    }
}
