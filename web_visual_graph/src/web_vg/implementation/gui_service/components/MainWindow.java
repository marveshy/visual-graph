package web_vg.implementation.gui_service.components;

import tzhomestudio.framework.shared.gui.components.TextFieldWithLabel;
import tzhomestudio.framework.shared.utils.FileUtils;
import tzhomestudio.framework.shared.utils.UnzipUtils;
import web_vg.implementation.main_service.WVGGlobals;
import web_vg.implementation.main_service.WVGMainServiceHelper;
import web_vg.interfaces.remote_service.WVGRemoteAPIListener;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;

/**
 * @author Timur Zolotuhin (tzolotuhin@parallels.com)
 */
public class MainWindow {
    // Constants
    private static final String DEFAULT_DOWNLOAD_LINK = "https://bitbucket.org/tzolotuhin/visual-graph/downloads/visual_graph_1.6.1.zip";
    private static final String ZIP_FILE_NAME = "visual_graph.zip";
    private static final String VISUAL_GRAPH_FOLDER = "Visual Graph";

    private static final String RUN_FILE_NAME = "visual_graph.jar";

    private static final int NO_STATUS = 0;
    private static final int INSTALLING_STATUS = 1;
    private static final int INSTALLED_STATUS = 2;
    private static final int RUNNING_STATUS = 11;
    private static final int READY_STATUS = 30;

    private static final String INSTALL_TEXT = "Install";
    private static final String REINSTALL_TEXT = "Reinstall";
    private static final String RUN_TEXT = "Run";

    private static final String NO_INSTALL_STATUS_LABEL = "Visual Graph is not installed";
    private static final String INSTALLING_STATUS_LABEL = "Installing...";
    private static final String INSTALLED_STATUS_LABEL = "Visual Graph is installed";
    private static final String RUNNING_STATUS_LABEL = "Running...";
    private static final String READY_STATUS_LABEL = "Visual Graph is ready";

    // Main components
    private JPanel outView, innerView;

    private JLabel statusLabel, currentStatusLabel;
    private JProgressBar progressBar;

    private JButton browseVisualGraphHomeButton;
    private TextFieldWithLabel visualGraphHomeTextFieldWithLabel;
    private TextFieldWithLabel visualGraphDownloadLinkTextFieldWithLabel;

    private JButton installButton;
    private JButton runButton;
    private JButton openGraphExampleButton;

    // Main data
    private int currentStatus;
    private String visualGraphHome;
    private String visualGraphDownloadLink;

    public MainWindow() {
        visualGraphHome = WVGMainServiceHelper.config.getStringProperty(WVGGlobals.VG_PATH_ON_LOCAL_SYSTEM_KEY, FileUtils.getUserHomeDir() + VISUAL_GRAPH_FOLDER);
        visualGraphDownloadLink = WVGMainServiceHelper.config.getStringProperty(WVGGlobals.VG_DOWNLOAD_LINK_KEY, DEFAULT_DOWNLOAD_LINK);
        WVGMainServiceHelper.config.save();

        if (new File(visualGraphHome + File.separator + RUN_FILE_NAME).exists())
            currentStatus = INSTALLED_STATUS;
        else
            currentStatus = NO_STATUS;

        // create components
        outView = new JPanel(new GridLayout(1, 1));
        innerView = new JPanel(new GridBagLayout());
        outView.add(innerView);

        statusLabel = new JLabel("Status: ");
        currentStatusLabel = new JLabel();

        progressBar = new JProgressBar();

        visualGraphHomeTextFieldWithLabel = new TextFieldWithLabel("Visual Graph Home directory: ", TextFieldWithLabel.LEFT_ALIGNMENT);
        visualGraphDownloadLinkTextFieldWithLabel = new TextFieldWithLabel("Visual Graph Download link: ", TextFieldWithLabel.LEFT_ALIGNMENT);

        browseVisualGraphHomeButton = new JButton("...");

        installButton = new JButton(INSTALL_TEXT);
        runButton = new JButton("Run");
        openGraphExampleButton = new JButton("Open Example Graph");

        // add listeners
        browseVisualGraphHomeButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                JFileChooser chooser = new JFileChooser();
                chooser.setDialogTitle("Browse Visual Graph home directory");
                chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
                chooser.setAcceptAllFileFilterUsed(false);

                if (chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
                    visualGraphHome = chooser.getSelectedFile().toString();
                    WVGMainServiceHelper.config.setProperty(WVGGlobals.VG_PATH_ON_LOCAL_SYSTEM_KEY, visualGraphHome);
                    WVGMainServiceHelper.config.save();

                    if (new File(visualGraphHome + File.separator + RUN_FILE_NAME).exists())
                        currentStatus = INSTALLED_STATUS;
                    else
                        currentStatus = NO_STATUS;

                    rebuildView();
                }
            }
        });

        installButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (isRan()) {
                    WVGMainServiceHelper.windowMessenger.errorMessage("Please, close other Visual Graph instances", "Error: Visual Graph is ran");
                } else {
                    visualGraphDownloadLink = visualGraphDownloadLinkTextFieldWithLabel.getTextField().getText();

                    final int prevStatus = currentStatus;
                    currentStatus = INSTALLING_STATUS;
                    rebuildView();

                    SwingWorker<Void, Void> worker = new SwingWorker<Void, Void>() {
                        @Override
                        protected Void doInBackground() throws Exception {
                            try {
                                doInstall();
                                currentStatus = INSTALLED_STATUS;
                            } catch (Throwable ex) {
                                currentStatus = prevStatus;
                                WVGMainServiceHelper.logger.printError(ex.getMessage(), ex);
                                WVGMainServiceHelper.windowMessenger.errorMessage(ex.getMessage());
                            }
                            return null;
                        }

                        @Override
                        protected void done() {
                            rebuildView();
                        }
                    };
                    worker.execute();
                }
            }
        });

        runButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (isRan()) {
                    WVGMainServiceHelper.windowMessenger.errorMessage("Please, close other Visual Graph instances", "Error: Visual Graph is ran");
                } else {
                    final int prevStatus = currentStatus;
                    currentStatus = RUNNING_STATUS;
                    rebuildView();

                    SwingWorker<Void, Void> worker = new SwingWorker<Void, Void>() {
                        @Override
                        protected Void doInBackground() throws Exception {
                            try {
                                ProcessBuilder builder = new ProcessBuilder("cmd.exe", "/c", "javaw -jar " + RUN_FILE_NAME);

                                builder.directory(new File(visualGraphHome));
                                builder.redirectErrorStream(true);
                                builder.start();
                            } catch (IOException ex) {
                                currentStatus = prevStatus;
                                WVGMainServiceHelper.windowMessenger.errorMessage("Can't run Visual Graph", "Error");
                                WVGMainServiceHelper.logger.printError(ex.getMessage(), ex);
                            }
                            return null;
                        }

                        @Override
                        protected void done() {
                            rebuildView();
                        }
                    };
                    worker.execute();
                }
            }
        });

        openGraphExampleButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (isRan()) {
                    try {
                        WVGMainServiceHelper.remoteAPIService.openGraph("Hello", "graphml", example);
                    } catch (Exception ex) {
                        WVGMainServiceHelper.logger.printError(ex.getMessage(), ex);
                    }
                }
            }
        });

        WVGMainServiceHelper.remoteAPIService.addListener(new WVGRemoteAPIListener() {
            @Override
            public void onConnect() {
                currentStatus = READY_STATUS;
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        rebuildView();
                    }
                });
            }

            @Override
            public void onDisconnect() {
                currentStatus = INSTALLED_STATUS;
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        rebuildView();
                    }
                });
            }
        });



        // build view
        rebuildView();
    }

    public JPanel getView() {
        return outView;
    }

//==============================================================================
//---------------PRIVATE METHODS------------------------------------------------
    private void rebuildView() {
        //clear panel
        innerView.removeAll();

        // packaging
        JPanel leftPanel = new JPanel(new GridBagLayout());
        JPanel rightPanel = new JPanel(new GridBagLayout());

        leftPanel.add(statusLabel,
                new GridBagConstraints(0, 0, 1, 1, 1, 0, GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(3, 3, 3, 3), 0, 0));
        rightPanel.add(currentStatusLabel,
                new GridBagConstraints(0, 0, 2, 1, 1, 0, GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(3, 3, 3, 3), 0, 0));

        visualGraphHomeTextFieldWithLabel.getLabel().setHorizontalAlignment(JLabel.CENTER);
        leftPanel.add(visualGraphHomeTextFieldWithLabel.getLabel(),
                new GridBagConstraints(0, 1, 2, 1, 1, 0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(3, 3, 3, 3), 0, 0));
        rightPanel.add(visualGraphHomeTextFieldWithLabel.getTextField(),
                new GridBagConstraints(0, 1, 1, 1, 1, 0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(3, 3, 3, 3), 0, 0));
        rightPanel.add(browseVisualGraphHomeButton,
                new GridBagConstraints(1, 1, 1, 1, 0, 0, GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(3, 3, 3, 3), 0, 0));

        visualGraphDownloadLinkTextFieldWithLabel.getLabel().setHorizontalAlignment(JLabel.CENTER);
        leftPanel.add(visualGraphDownloadLinkTextFieldWithLabel.getLabel(),
                new GridBagConstraints(0, 2, 1, 1, 1, 0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(3, 3, 3, 3), 0, 0));
        rightPanel.add(visualGraphDownloadLinkTextFieldWithLabel.getTextField(),
                new GridBagConstraints(0, 2, 2, 1, 1, 0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(3, 3, 3, 3), 0, 0));

        innerView.add(leftPanel,
                new GridBagConstraints(0, 0, 1, 1, 0.3, 0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(3, 3, 3, 3), 0, 0));
        innerView.add(rightPanel,
                new GridBagConstraints(1, 0, 1, 1, 0.7, 0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(3, 3, 3, 3), 0, 0));

        innerView.add(installButton,
                new GridBagConstraints(0, 1, 1, 1, 0, 0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(3, 3, 3, 3), 0, 0));
        innerView.add(runButton,
                new GridBagConstraints(1, 1, 1, 1, 1, 0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(3, 3, 3, 3), 0, 0));

        innerView.add(progressBar,
                new GridBagConstraints(0, 2, 2, 1, 1, 0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(3, 3, 3, 3), 0, 0));

        innerView.add(openGraphExampleButton,
                new GridBagConstraints(0, 3, 2, 1, 1, 0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(3, 3, 3, 3), 0, 0));

        // set status
        progressBar.setVisible(false);
        switch (currentStatus) {
            case NO_STATUS: {
                enableUI(true);

                installButton.setText(INSTALL_TEXT);
                runButton.setText(RUN_TEXT);

                runButton.setEnabled(false);
                openGraphExampleButton.setEnabled(false);

                currentStatusLabel.setText(NO_INSTALL_STATUS_LABEL);
                break;
            }

            case INSTALLING_STATUS: {
                enableUI(false);

                installButton.setText(INSTALL_TEXT);
                runButton.setText(RUN_TEXT);
                progressBar.setVisible(true);

                currentStatusLabel.setText(INSTALLING_STATUS_LABEL);
                break;
            }

            case INSTALLED_STATUS: {
                enableUI(true);

                installButton.setText(REINSTALL_TEXT);
                runButton.setText(RUN_TEXT);

                openGraphExampleButton.setEnabled(false);

                currentStatusLabel.setText(INSTALLED_STATUS_LABEL);
                break;
            }

            case RUNNING_STATUS: {
                enableUI(false);

                installButton.setText(REINSTALL_TEXT);
                runButton.setText(RUN_TEXT);

                currentStatusLabel.setText(RUNNING_STATUS_LABEL);
                break;
            }

            case READY_STATUS: {
                enableUI(false);

                installButton.setText(REINSTALL_TEXT);
                runButton.setText(RUN_TEXT);

                openGraphExampleButton.setEnabled(true);

                currentStatusLabel.setText(READY_STATUS_LABEL);
                break;
            }
        }

        visualGraphHomeTextFieldWithLabel.getTextField().setText(visualGraphHome);
        visualGraphDownloadLinkTextFieldWithLabel.getTextField().setText(visualGraphDownloadLink);

        // update ui
        innerView.updateUI();
    }

    private void enableUI(boolean enable) {
        installButton.setEnabled(enable);
        runButton.setEnabled(enable);

        visualGraphHomeTextFieldWithLabel.setEnabled(enable);
        visualGraphDownloadLinkTextFieldWithLabel.setEnabled(enable);
        browseVisualGraphHomeButton.setEnabled(enable);

        openGraphExampleButton.setEnabled(enable);
    }

    private boolean isRan() {
        return currentStatus >= READY_STATUS;
    }

    private void doInstall() throws Exception {
        WVGMainServiceHelper.logger.printInfo("Begin to create home directory: " + visualGraphHome);
        File visualGraphDirectory = new File(visualGraphHome);
        if (!visualGraphDirectory.exists()) {
            if (!visualGraphDirectory.mkdirs()) {
                throw new Exception("Can't create following folder: " + visualGraphDirectory.toString() + ". Please select another directory");
            }
        }
        WVGMainServiceHelper.logger.printInfo("Done");

        WVGMainServiceHelper.logger.printInfo("Begin to remove all files in Visual Graph home directory");
        FileUtils.cleanDirectory(visualGraphHome);
        WVGMainServiceHelper.logger.printInfo("Done");

        WVGMainServiceHelper.logger.printInfo("Save download link in config file");
        WVGMainServiceHelper.config.setProperty(WVGGlobals.VG_DOWNLOAD_LINK_KEY, visualGraphDownloadLink);
        WVGMainServiceHelper.config.save();
        WVGMainServiceHelper.logger.printInfo("Done");

        WVGMainServiceHelper.logger.printInfo("Begin to download zip file");
        URL website = new URL(visualGraphDownloadLink);
        try (FileOutputStream fos = new FileOutputStream(visualGraphHome + File.separator + ZIP_FILE_NAME);
             ReadableByteChannel rbc = Channels.newChannel(website.openStream())) {
            long re, currCount = 0, allCount = FileUtils.tryGetFileSize(website);
            while ((re = fos.getChannel().transferFrom(rbc, currCount, 1024)) > 0) {
                currCount += re;
                doUpdateProgressBar((int) currCount, (int) allCount);
                fos.flush();
            }
        }
        WVGMainServiceHelper.logger.printInfo("Done");

        WVGMainServiceHelper.logger.printInfo("Extracting zip archive");
        File zipFile = new File(visualGraphHome + File.separator + ZIP_FILE_NAME);
        UnzipUtils.unzip(zipFile, new File(visualGraphHome));

        // copy files to home directory
        String rootDir = visualGraphHome + File.separator + UnzipUtils.getRootDir(zipFile);
        FileUtils.moveFilesFromDirectoryToAnotherDirectory(rootDir, visualGraphHome);
        WVGMainServiceHelper.logger.printInfo("Done");
    }

    private void doUpdateProgressBar(final int currCount, final int allCount) {
        if (!SwingUtilities.isEventDispatchThread()) {
            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    doUpdateProgressBar(currCount, allCount);
                }
            });
        } else {
            progressBar.setMinimum(0);
            progressBar.setValue(currCount);
            progressBar.setMaximum(allCount);
        }
    }

    private static final String example = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
            "<graphml xmlns=\"http://graphml.graphdrawing.org/xmlns\"  \n" +
            "      xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\n" +
            "      xsi:schemaLocation=\"http://graphml.graphdrawing.org/xmlns \n" +
            "        http://graphml.graphdrawing.org/xmlns/1.0/graphml.xsd\">\n" +
            "  <key id=\"d0\" for=\"node\" attr.name=\"color\" attr.type=\"string\">\n" +
            "    <default>yellow</default>\n" +
            "  </key>\n" +
            "  <key id=\"d1\" for=\"edge\" attr.name=\"weight\" attr.type=\"double\"/>\n" +
            "  <graph id=\"G\" edgedefault=\"undirected\">\n" +
            "    <node id=\"n0\">\n" +
            "      <data key=\"d0\">green</data>\n" +
            "    </node>\n" +
            "    <node id=\"n1\"/>\n" +
            "    <node id=\"n2\">\n" +
            "      <data key=\"d0\">blue</data>\n" +
            "    </node>\n" +
            "    <node id=\"n3\">\n" +
            "      <data key=\"d0\">red</data>\n" +
            "    </node>\n" +
            "    <node id=\"n4\"/>\n" +
            "    <node id=\"n5\">\n" +
            "      <data key=\"d0\">turquoise</data>\n" +
            "    </node>\n" +
            "    <edge id=\"e0\" source=\"n0\" target=\"n2\">\n" +
            "      <data key=\"d1\">1.0</data>\n" +
            "    </edge>\n" +
            "    <edge id=\"e1\" source=\"n0\" target=\"n1\">\n" +
            "      <data key=\"d1\">1.0</data>\n" +
            "    </edge>\n" +
            "    <edge id=\"e2\" source=\"n1\" target=\"n3\">\n" +
            "      <data key=\"d1\">2.0</data>\n" +
            "    </edge>\n" +
            "    <edge id=\"e3\" source=\"n3\" target=\"n2\"/>\n" +
            "    <edge id=\"e4\" source=\"n2\" target=\"n4\"/>\n" +
            "    <edge id=\"e5\" source=\"n3\" target=\"n5\"/>\n" +
            "    <edge id=\"e6\" source=\"n5\" target=\"n4\">\n" +
            "      <data key=\"d1\">1.1</data>\n" +
            "    </edge>\n" +
            "  </graph>\n" +
            "</graphml>\n";
}
