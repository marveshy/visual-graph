package web_vg.implementation.remote_service;

import com.google.common.collect.Lists;
import vg.impl.remote_service.VGRemoteAPI;
import vg.impl.remote_service.VGRemoteAPIService;
import web_vg.implementation.main_service.WVGMainServiceHelper;
import web_vg.interfaces.remote_service.WVGRemoteAPIListener;
import web_vg.interfaces.remote_service.WVGRemoteAPIService;

import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * @author Timur Zolotuhin (tzolotuhin@parallels.com)
 */
public class WVGRemoteAPIServiceImpl implements Runnable, WVGRemoteAPIService {
    // Main data
    private List<WVGRemoteAPIListener> listeners = Lists.newArrayList();
    private AtomicBoolean close = new AtomicBoolean(false);

    private boolean isConnected = false;

    // Mutex
    private final Object generalMutex = new Object();

    public WVGRemoteAPIServiceImpl() {
        Thread thread = new Thread(this);
        thread.setDaemon(true);
        thread.start();
    }

    @Override
    public void connect() {

    }

    @Override
    public void disconnect() {

    }

    @Override
    public void openGraph(String name, String format, String content) {
        synchronized (generalMutex) {
            VGRemoteAPIService vgRemoteAPIService = new VGRemoteAPIService();
            VGRemoteAPI vgRemoteAPI = vgRemoteAPIService.getVGRemoteAPIPort();
            vgRemoteAPI.openGraphFromData(name, format, content);
        }
    }

    @Override
    public void addListener(WVGRemoteAPIListener listener) {
        synchronized (generalMutex) {
            listeners.add(listener);
        }
    }

    @Override
    public boolean isConnected() {
        synchronized (generalMutex) {
            return isConnected;
        }
    }

    @Override
    public void close() {
        synchronized (generalMutex) {
            close.set(true);
        }
    }

    @Override
    public void run() {
        while (!close.get()) {
            try {
                doCheckConnect();
                doConnect();
            } catch (Throwable ex) {
                WVGMainServiceHelper.logger.printDebug("Please ignore following exception: " + ex.getMessage());
                doDisconnect();
            }

            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                WVGMainServiceHelper.logger.printError(ex.getMessage(), ex);
            }
        }
    }

//==============================================================================
//------------------PRIVATE METHODS---------------------------------------------
    private void doCheckConnect() {
        VGRemoteAPIService vgRemoteAPIService = new VGRemoteAPIService();
        VGRemoteAPI vgRemoteAPI = vgRemoteAPIService.getVGRemoteAPIPort();
        vgRemoteAPI.ping();
    }

    private void doConnect() {
        synchronized (generalMutex) {
            if (!isConnected) {
                for (WVGRemoteAPIListener wvgRemoteAPIListener : listeners) {
                    wvgRemoteAPIListener.onConnect();
                }
                isConnected = true;
            }
        }
    }

    private void doDisconnect() {
        synchronized (generalMutex) {
            if (isConnected) {
                for (WVGRemoteAPIListener wvgRemoteAPIListener : listeners) {
                    wvgRemoteAPIListener.onDisconnect();
                }
                isConnected = false;
            }
        }
    }
}
