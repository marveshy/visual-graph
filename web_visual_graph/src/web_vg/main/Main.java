package web_vg.main;

import web_vg.implementation.main_service.WVGMainInitializer;
import web_vg.implementation.main_service.WVGMainServiceHelper;

import java.applet.Applet;
import java.awt.*;

/**
 * @author Timur Zolotuhin (tzolotuhin@parallels.com)
 */
public class Main extends Applet  {
    @Override
    public void init () {
        // setup general settings
        WVGMainInitializer.executeInitialPreparations(new String[0]);
        WVGMainServiceHelper.config.saveWithExit(false);
        WVGMainServiceHelper.logger.enableDebugMessage(true);

        WVGMainServiceHelper.logger.printInfo("Initialize applet");

        // create components
        setLayout(new GridLayout(1, 1));
        WVGMainServiceHelper.guiService.init();
        add(WVGMainServiceHelper.guiService.getView());

        WVGMainServiceHelper.logger.printInfo("Done");
    }
}
