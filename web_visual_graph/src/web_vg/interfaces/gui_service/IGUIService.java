package web_vg.interfaces.gui_service;

import javax.swing.*;

/**
 * @author Timur Zolotuhin (tzolotuhin@parallels.com)
 */
public interface IGUIService {
    /**
     * Initialize gui service
     */
    public void init();

    /**
     * Returns main windows view.
     */
    public JPanel getView();
}
