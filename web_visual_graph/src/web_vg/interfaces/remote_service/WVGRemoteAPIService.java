package web_vg.interfaces.remote_service;

/**
 * @author Timur Zolotuhin (tzolotuhin@parallels.com)
 */
public interface WVGRemoteAPIService {
    public void connect();

    public void disconnect();

    public void openGraph(String name, String format, String content);

    public void addListener(WVGRemoteAPIListener listener);

    public void close();

    public boolean isConnected();
}
