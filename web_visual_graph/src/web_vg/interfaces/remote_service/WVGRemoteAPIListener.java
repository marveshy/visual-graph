package web_vg.interfaces.remote_service;

/**
 * @author Timur Zolotuhin (tzolotuhin@parallels.com)
 */
public class WVGRemoteAPIListener {
    public void onConnect() {}

    public void onDisconnect() {}
}
